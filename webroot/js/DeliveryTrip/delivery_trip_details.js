/**
 * Created by doanh.lv on 10/21/19.
 */

function addOrderToTripDetail(result){
    $("#fadeMe1").removeClass("fadeMe");
    $("#hidden1").addClass("hidden");
    if (result) {
      var order = jQuery.parseJSON(result);
      var count = $(`.tb-trip tbody tr#${order.trip_id}`).length;
      if (count === 0) {
        yes.play();
        var html =
          `<tr id=${order.trip_id}>` +
          `<td><a href="/orders/view/${order.id}" target="blank"><span>${order.orderId} <br>${order.shopId || ""} </span></a></td>` +
          `<td><span>${order.user.fullname} <br> ${order.user.phone}</span></td>` +
          `<td><span>${order.receiver_name} <br> ${order.arr_receiver_phone}</span></td>` +
          `<td><span>${order.address}, <br> ${order.ward || ""}, <br> ${order.district.name}, <br> ${order.city.name}</span></td>` +
          `<td><span>${order.content || ""}</span></td>`+
          `<td><span>${order.note || ""}</span></td>` +
          `<td><b>${order.formatted_total}</b></td>` +
          `<td>
          <div class="btn-group">
            <a href="/orders/printOrder/${order.id}" target="blank" class="btn btn-outline-info"><i class="fa fa-print"></i></a>                        
            <a href="/orders/view/${order.id}" target="blank" class="btn btn-outline-info"><i class="fa fa-eye"></i></a>                        
            <a href="#" title="Delete" id=${order.trip_id} class="delete_order btn btn-outline-danger"><i class="fa fa-trash-alt"></i></a>
           </td>` +
          "</tr>";
        var oldHtml = $(".tb-trip tbody").html();
        $(".tb-trip tbody").html(html);
        $(".tb-trip tbody").append(oldHtml);

      }else{
        no.play();
      }
    } else {
      no.play();
    }
} 

$(document).ready(function () {
  $("#order_code").on("keypress", function (event) {
    if (event.keyCode == 13) {
      var orderCode = $("#order_code").val();
      var deliveryTripId = $("#delivery_trip_id").val();
      if (orderCode.length > 0) {
        $("#order_code").val("");
        $.ajax({
          method: "get",
          data: { orderCode: orderCode, deliveryTripId: deliveryTripId },
          url: "/delivery_trip_details/checkOrderAndInsert",
          beforeSend: function () {
            $("#fadeMe1").addClass("fadeMe");
            $("#hidden1").removeClass("hidden");
          },
          success: function (result) {
            addOrderToTripDetail(result);
          },
          error: function (error) {
            no.play();
          },
        });
      }
      event.preventDefault();
      return false;
    }
  });

  $("#order_code_return").on("keypress", function (event) {
    if (event.keyCode == 13) {
      var orderCode = $("#order_code_return").val();
      var tripId = $("#return_trip_id").val();
      var addressId = $("#address_id").val();
      if (orderCode.length > 0) {
        $("#order_code_return").val("");
        $.ajax({
          method: "get",
          data: { orderCode: orderCode, tripId: tripId, addressId: addressId },
          url: "/return_trip_details/checkOrderAndInsert",
          beforeSend: function () {
            $("#fadeMe1").addClass("fadeMe");
            $("#hidden1").removeClass("hidden");
          },
          success: function (result) {
            addOrderToTripDetail(result);
          },
          error: function (error) {
            no.play();
          },
        });
      }
      event.preventDefault();
      return false;
    }
  });


    $('#inventory_order_code').on('keypress', function (event) {
        if(event.keyCode == 13) {
            var orderId = $("#inventory_order_code").val();
            var inventorySessionId = $("#inventory_session_id").val();
            if(orderId.length > 0) {
                $("#inventory_order_code").val('');
                $.ajax({
                    method : 'get',
                    data : {orderId : orderId, inventorySessionId: inventorySessionId},
                    url : '/inventory_session_details/inventory',
                    beforeSend : function(){
                        $("#fadeMe1").addClass("fadeMe");
                        $("#hidden1").removeClass("hidden");
                    },
                    success : function(result){
                        $("#fadeMe1").removeClass("fadeMe");
                        $("#hidden1").addClass("hidden");
                        if(result > 0){
                            yes.play();
                            location.reload();
                        }else{
                            no.play();
                        }
                    },
                    error: function (error) {
                        no.play();
                    }
                });
            }
            event.preventDefault();
            return false;
        }
    });



    $(".confirm").click(function(){
        //load action o day
        var id = $(this).attr('data-id');
        $('.deliveryTripDetailId').val(id);
        $('.tab').val(window.location.hash.replace("#",""));
    });


    $('#orderId').on('keypress', function (event) {
        if(event.which === 13){
            //Disable textbox to prevent multiple submit
            var orderId = $.trim($(this).val());
            var deliveryTripId = $.trim($("#delivery_trip_id").val());
            $("#orderId").val("");
            $.ajax({
                method : 'get',
                data : {order_id : orderId, delivery_trip_id : deliveryTripId},
                url : '/delivery_trip_details/inventory',
                beforeSend : function(){
                    $("#fadeMe1").addClass("fadeMe");
                    $("#hidden1").removeClass("hidden");
                },
                success : function(results)
                {
                    $('.modal').modal('hide');
                    $("#fadeMe1").removeClass("fadeMe");
                    $("#hidden1").addClass("hidden");
                    var obj = JSON.parse(results);
                    if(typeof obj.id !== 'undefined')
                    {
                        //show data on view.
                        var checkedListId = "#checked-list li#"+obj.id;
                        var notCheckListId = "#not-check-list li#"+obj.id;
                        $(checkedListId).length;
                        if($(checkedListId).length === 0){
                            $("#checked-list").append("<li id="+obj.id+">"+obj.orderId+"</li>");
                            $(notCheckListId).remove();
                            yes.play();
                        }else{
                            no.play();
                        }
                    }
                    else
                        no.play();

                },
                error: function (error) {
                    no.play();
                }
            });

            //Do Stuff, submit, etc..
        }
    });


    $("#btnCheckAll").click(function(){

            var orderIds = [];
            $("ul#not-check-list li").each(function(){
                var id 	= $(this).attr('id');
                orderIds.push(id);
            });
            var deliveryTripId = $.trim($("#delivery_trip_id").val());
            $.ajax({
                method : 'post',
                data : {result:JSON.stringify({delivery_trip_id : deliveryTripId, order_ids : orderIds})},
                url : '/delivery_trip_details/checkOrderAll',
                beforeSend : function(){
                    $("#fadeMe1").addClass("fadeMe");
                    $("#hidden1").removeClass("hidden");
                    $(this).attr('disabled','disabled');
                },

                // tra ve 1 mang chua cac order neu da insert thanh cong.
                success : function(results){
                    $("#fadeMe1").removeClass("fadeMe");
                    $("#hidden1").addClass("hidden");
                    $(this).removeAttr('disabled');
//	            	//duyet qua tung phan tu de lay gia tri gan vao the ul
                    var obj = JSON.parse(results);
                    if(obj.length > 0){
                        $.each(obj, function(i, val) {
                            var notCheckListId = "#not-check-list li#"+val.id;
                            $("#checked-list").append("<li id="+val.id+">"+val.orderId+"</li>");
                            $(notCheckListId).remove();
                        });
                    }

                },error: function (error) {

                }
                
            });

    });
  

  $(document).on("click", '.delete_order', function () {
    var id = $(this).attr('id');
    if(confirm('Are you confirm delete') && id){
      $.ajax({
        method: "get",
        data: { id: id},
        url: $(this).attr('data-delete-url'),
        beforeSend: function () {
          $("#fadeMe1").addClass("fadeMe");
          $("#hidden1").removeClass("hidden");
        },
        success: function (result) {
          $("#fadeMe1").removeClass("fadeMe");
          $("#hidden1").addClass("hidden");
          if(result){
            $(`tr#${id}`).remove();
          }
        },
        error: function (error) {
          no.play();
        },
      });
    }
  });
});