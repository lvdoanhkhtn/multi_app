$(document).ready(function() {
    const MAX_FILE_SIZE = 10;
    var droppedFiles = false;
    //Drag and drop event
    $('.form-drag')
        .on('drag dragover dragend dragstart dragleave dragenter drop', function(e) {
            //Preventing the unwanted behaviours
            e.preventDefault();
            e.stopPropagation();
        })
        .on('dragenter dragover', function() {
            $('.form-drag').addClass('is-dragover');
        })
        .on('dragend dragleave drop', function() {
            $('.form-drag').removeClass('is-dragover');
        })
        .on('drop', function(e) {
            //The files that were dropped
            droppedFiles = e.originalEvent.dataTransfer.files;
            //Show/hide fields
            $('#csv_file_name').text(droppedFiles[0].name);
            $('#error-mess').text('');
            $('.message.success').hide();
            //Validate on drag & drop
            file_validation();
        });

    //On submit event
    $('.form-drag').on('submit', function (e) {
        e.preventDefault();
        //Create hide form data
        var ajaxData = new FormData();
        //Validate file
        if(file_validation()){
            //Disable submit button on process
            $("#btn-submit").attr('disabled', true);
            //Insert dropped file to hide form
            $.each(droppedFiles, function(i, file) {
                ajaxData.append('file', file);
            });
            //Send file using ajax
            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: ajaxData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    if(data.success){
                        location.reload();
                    } else {
                        //Show error messages
                        $.each(data.errorMsg, function(index, value){
                            $("#error-mess").append(value + '<br><br>');
                        });
                        //Reset upload input
                        droppedFiles = false;
                        $('#csv_file_name').text('');
                        $("#btn-submit").attr('disabled', false);
                    }
                },
            });
        } else {
            e.preventDefault();

            return false;
        }
    });

    //Button submit outside of the form
    $('#btn-submit').click(function() {
        $('#error-mess').text('');
        $('.form-drag').submit();
    });

    //Validate upload file
    function file_validation() {
        //Get validate message from PHP: messages.filerequired, messages.filetype, messages.filesize
        var messages = $('#file-validate-mess').data();
        //Check file required
        if(!droppedFiles || droppedFiles.length == 0){
            $('#error-mess').text(messages.filerequired);

            return false;
        }
        //Check file extension
        if(!droppedFiles[0].name.match(new RegExp("\\.(csv)$", "i"))){
            $('#error-mess').text(messages.filetype.replace('<0>', 'CSV'));

            return false;
        }
        //Check file max size
        if(droppedFiles[0].size > MAX_FILE_SIZE*1024*1024){
            $('#error-mess').text(messages.filesize.replace('<0>', MAX_FILE_SIZE + 'MB'));

            return false;
        }

        return true;
    }
});