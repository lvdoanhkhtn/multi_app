$(document).ready(function () {
    $("#importOrder").validate({
        rules: {
            import_file: {
                extension: "xls|xlsx|xlsm",
                filesize: 4
            }
        }
    });

    $('input[name="import_file"]').change(function(e){
        if($("#importOrder").valid()){
            $('.error-message').remove();
            $('#file_name').html(e.target.files[0].name);

        }
    });

});

