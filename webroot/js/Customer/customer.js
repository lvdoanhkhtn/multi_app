$(document).ready(function(){

    function handleAddressCustomer(obj) {
        $("#addressCustomerId").val(obj.id);
        $('#addressCustomerTab').val(window.location.hash.replace("#",""));
        $('#sender_name').val(obj.agency);
        $('#sender_phone').val(obj.str_phones);
        $('#sender_address').val(obj.address);
        $('#sender_city_id').val(obj.city_id.toString()).trigger('change');
        var isFinishLoadDistrict = loadDistrictByCityIdForPopup(obj.city_id, "#sender_district_id");
        if(isFinishLoadDistrict){
            $('#sender_district_id').val(obj.district_id.toString()).trigger('change');
        }
        var isFinishLoadWard = loadWardByDistrictIdForPopup(obj.district_id, "#sender_ward_id");
        if(isFinishLoadWard){
            $('#sender_ward_id').val(obj.ward_id.toString()).trigger('change');
        }
    }

    $(".address-customer").click(function(){
        getData(handleAddressCustomer, $(this).attr('data-id'));
    });


    $('#sender_city_id').on('select2:select', function (e) {
        var city_id = $(this).val();
        var element = "#sender_district_id";
        loadDistrictByCityId(city_id, element);
    });

    $('#sender_district_id').on('select2:select', function (e) {
        var district_id = $(this).val();
        var element = "#sender_ward_id";
        loadWardByDistrictId(district_id, element);
    });

});




