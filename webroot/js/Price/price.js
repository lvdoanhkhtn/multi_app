$(document).ready(function(){

    $(".modal").on('show.bs.modal', function(){
        $(".modal-body input").val("");
        $('.select2').val("").trigger("change");
        $("div.error-message").remove();
    });
    
    function handleShippingFee(obj) {
        $("#shippingFeeId").val(obj.id);
        $('#shippingFeeTab').val(window.location.hash.replace("#",""));
        $("#service_pack_id").select2("val", obj.service_pack_id.toString());
        $("#location_id").val(obj.location_id);
        $('#start_weight').val(obj.start_weight);
        $('#start_amount').val(obj.start_amount);
        $('#addition_weight').val(obj.addition_weight);
        $('#addition_amount').val(obj.addition_amount);
        $('#maximum_weight').val(obj.maximum_weight);
        $('#pickup_fee').val(obj.pickup_fee);
        $('#return_fee').val(obj.return_fee);
        $('#minimum_return_fee').val(obj.minimum_return_fee);
        $('#exchange_fee').val(obj.exchange_fee);
        $('#minimum_exchange_fee').val(obj.minimum_exchange_fee);
    }

    function handleInsuranceFee(obj) {
        $('#insuranceFeeId').val(obj.id);
        $('#insuranceFeeTab').val(window.location.hash.replace("#",""));
        $("#service_pack_id_insurance").select2("val", obj.service_pack_id.toString());
        $("#location_id_insurance").val(obj.location_id);
        $('#from_goods_value').val(obj.from_goods_value);
        $('#to_goods_value').val(obj.to_goods_value);
        $('#protect_fee').val(obj.protect_fee);
        $('#minimum_protect_fee').val(obj.minimum_protect_fee);
    }

    function handleCodFee(obj) {
        $('#codFeeId').val(obj.id);
        $('#codFeeTab').val(window.location.hash.replace("#",""));
        $("#service_pack_id").select2("val", obj.service_pack_id.toString());
        $("#service_pack_id_cod").select2("val", obj.service_pack_id.toString());
        $("#location_id_cod").val(obj.location_id);
        $('#from_cod').val(obj.from_cod);
        $('#to_cod').val(obj.to_cod);
        $('#amount').val(obj.collection_fee);
        $('#minimum_amount').val(obj.minimum_collection_fee);
    }

    $(".shipping-fee").click(function(){
        getData(handleShippingFee, $(this).attr('data-id'));
    });

    $(".insurance-fee").click(function(){
        getData(handleInsuranceFee, $(this).attr('data-id'));
    });

    $(".cod-fee").click(function(){
        getData(handleCodFee, $(this).attr('data-id'));

    });

});




