/**
 * Created by doanh.lv on 10/29/19.
 */
$(document).ready(function () {


    function createTrip(url, employeeId, addressId) {
        $.ajax({
            method:'GET',
            data: {employeeId : employeeId, addressId: addressId},
            url:url,
            beforeSend: function() {
                $("#fadeMe1").addClass("fadeMe");
                $("#hidden1").removeClass("hidden");
                $("#btnCreateGetTrip").attr('disabled','disabled');
            },
            success:function(data){
                if(data){
                    alert($.validator.messages.success(""));
                }else{
                    alert($.validator.messages.failed(""));
                }
                location.reload();                
            },
            error:function(error){
                alert($.validator.messages.failed(""));
            },

        });
    }


    $(".btnCreateTrip").click(function(e){
        var employeeId = $("#employee_id :selected").val();
        var addressId = [];
        var flag = true;
        if(employeeId.length === 0)
        {
            flag = false;
            alert($.validator.messages.shipper(""));
        }
        else if($(".check").is(':checked')===false)
        {
            flag = false;
            alert($.validator.messages.pickup_order(""));
        }
        if(flag == true)
        {
            $(".check").each(function(){
                if($(this).is(':checked'))
                {
                    var id = $(this).attr('id');
                    addressId.push(id);
                }
            });

            if(addressId.length > 1){
                alert($.validator.messages.only_pickup_address(""));
            }else{
                var id = $(this).attr('id');
                switch (id){
                    case 'btnCreateGetTrip':
                        createTrip('/pickup_trips/add', employeeId, addressId);
                        break;

                    case 'btnCreateReturnTrip':
                        createTrip('/return_trips/addFromWareHouse', employeeId, addressId);
                        break;

                    case 'btnCreateEmptyReturnTrip':
                        createTrip('/return_trips/addEmptyFromWareHouse', employeeId, addressId);
                        break;

                }
            }
        }
        e.preventDefault();


    });
});