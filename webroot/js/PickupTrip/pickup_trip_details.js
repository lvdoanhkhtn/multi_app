/**
 * Created by doanh.lv on 10/21/19.
 *
 * @format
 */

$(document).ready(function () {
  function getElementAfterCheck() {
    var id = $("ul#priceTab li.active a.nav-link")
      .attr("href")
      .replace("#", "");
    var checkElement = ".cbInProcess-" + id;
    return checkElement;
  }

  $("#cbAllInProcess").click(function () {
    var checkElement = getElementAfterCheck();
    if ($(this).is(":checked")) {
      $(checkElement).prop("checked", true);
    } else {
      $(checkElement).prop("checked", false);
    }
  });

  $(".btnConfirm").click(function (e) {
    var ids = [];
    var checkElement = getElementAfterCheck();
    $(checkElement).each(function () {
      if ($(this).is(":checked")) {
        ids.push($(this).attr("id"));
      }
    });
    if (ids.length < 1) {
      alert($.validator.messages.pickup_order(""));
    } else {
      var tripId = $("#trip_id").val();
      var note = "";
      if ($("#pickup_note").length > 0) {
        note = $("#pickup_note").val();
      }
      var id = $(this).attr("id");

      switch (id) {
        case "btnComplete":
          confirmProcess(
            ids,
            "/pickup_trip_details/confirmComplete",
            note,
            tripId
          );
          break;

        case "btnFail":
          confirmProcess(ids, "/pickup_trip_details/confirmFail", note, tripId);
          break;

        case "btnCancel":
          confirmProcess(
            ids,
            "/pickup_trip_details/confirmCancel",
            note,
            tripId
          );
          break;

        case "btnReturnComplete":
          confirmProcess(
            ids,
            "/return_trip_details/confirmComplete",
            note,
            tripId
          );
          break;

        case "btnReturnFail":
          confirmProcess(ids, "/return_trip_details/confirmFail", note, tripId);
          break;

        case "btnDeliveryComplete":
          confirmProcess(
            ids,
            "/delivery_trip_details/confirmOrdersComplete",
            note,
            tripId
          );
          break;

        case "btnDeliveryFail":
          confirmProcess(
            ids,
            "/delivery_trip_details/confirmOrdersFail",
            note,
            tripId
          );
          break;

        case "btnCollectFee":
          confirmProcess(ids, "/pickup_trip_details/collectFee", note, tripId);
          break;
      }
    }
    e.preventDefault();
  });

  $("#priceTab .nav-link").click(function () {
    $("#cbAllInProcess").prop("checked", false);
    var checkElement = getElementAfterCheck();
    $(checkElement).each(function () {
      $(this).prop("checked", false);
    });
    $("#tab").val($(this).attr("href").replace("#", ""));
  });

  $("#order_code").on("keypress", function (event) {
    if (event.keyCode == 13) {
      var orderCode = $("#order_code").val();
      var pickupTripId = $("#trip_id").val();
      var roleId = $("#role_id").val();
      var tab = $("#priceTab li.active a").attr("id");
      $(this).val("");
      if (orderCode.length > 0) {
        $.ajax({
          method: "get",
          data: { orderCode: orderCode, pickupTripId: pickupTripId },
          url: "/pickup_trip_details/checkOrderAndInsert",
          beforeSend: function () {
            $("#fadeMe1").addClass("fadeMe");
            $("#hidden1").removeClass("hidden");
          },
          success: function (result) {
            $("#fadeMe1").removeClass("fadeMe");
            $("#hidden1").addClass("hidden");
            var countInProgress = parseInt($("#pickup_in_progress i").html());
            var countPickupCompleted = parseInt(
              $("#pickup_completed i").html()
            );
            var stored = parseInt($("#stored i").html());
            if (result) {
              var order = jQuery.parseJSON(result);
              var count = $(`.tb-trip tbody tr#${order.id}`).length;
              if (count === 0) {
                yes.play();
                // Is Admin.
                if (roleId == 1) {
                  $("#pickup_completed i").html(--countPickupCompleted);
                  // Is Coordinator.
                } else if (roleId == 7 || roleId == 14) {
                  if (tab === "pickup_completed") {
                    $("#pickup_completed i").html(--countPickupCompleted);
                  } else {
                    $("#pickup_in_progress i").html(--countInProgress);
                  }
                }
                $("#stored i").html(++stored);
                var html =
                  `<tr id=${order.id}>` +
                  `<td><span>${order.orderId} - ${
                    order.shopId || ""
                  } </span></a></td>` +
                  `<td><span>${order.receiver_name} - ${order.arr_receiver_phone}</span></td>` +
                  "</tr>";
                var oldHtml = $(".tb-trip tbody").html();
                $(".tb-trip tbody").html(html);
                $(".tb-trip tbody").append(oldHtml);
                // Remove order in list.
                var table = $(`#dtBasic${tab}`).dataTable();
                table.find(`tr#row-${order.id}`).remove();
              } else {
                no.play();
              }
            } else {
              no.play();
            }
          },
        });
      }
      event.preventDefault();
      return false;
    }
  });

  $("#btnDelete").click(function () {
    $("#key_word").val("");
  });
});
