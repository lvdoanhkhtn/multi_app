var yes = new Audio("/files/yes.wav"); // buffers automatically when created
var no = new Audio("/files/no.wav"); // buffers automatically when created
function loadDistrictByCityId(city_id, element) {
  var params = [];
  $(element).html("");
  $.ajax({
    type: "GET",
    url: "/Commons/loadDistrictByCityId",
    data: { city_id: city_id },
    success: function (results) {
      var obj = $.parseJSON(results);
      var option =
        '<option value="">Select district from the list (*)</option>';
      $.each(obj, function (i, val) {
        option = option + '<option value="' + i + '">' + val + "</option>";
      });
      $(element).html(option);
    },
  });
}

function loadWardByDistrictId(district_id, element) {
  var params = [];
  $(element).html("");
  $.ajax({
    type: "GET",
    url: "/Commons/loadWardByDistrictId",
    data: { district_id: district_id },
    success: function (results) {
      var obj = $.parseJSON(results);
      var option = '<option value="">Select ward from the list</option>';
      $.each(obj, function (i, val) {
        option = option + '<option value="' + i + '">' + val + "</option>";
      });
      $(element).html(option);
    },
  });
}

function loadDistrictByCityIdByAddress(city_id, district_id, element) {
  var params = [];
  $(element).html("");
  $.ajax({
    type: "GET",
    url: "/Commons/loadDistrictByCityId",
    data: { city_id: city_id },
    success: function (results) {
      var obj = $.parseJSON(results);
      var option =
        '<option value="">Select district from the list (*)</option>';
      $.each(obj, function (i, val) {
        option = option + '<option value="' + i + '">' + val + "</option>";
      });
      $(element).html(option);
      $(element).val(district_id);
    },
  });
}

function loadWardByDistrictIdByAddress(district_id, ward_id, element) {
  var params = [];
  $(element).html("");
  $.ajax({
    type: "GET",
    url: "/Commons/loadWardByDistrictId",
    data: { district_id: district_id },
    success: function (results) {
      var obj = $.parseJSON(results);
      var option = '<option value="">Select ward from the list</option>';
      $.each(obj, function (i, val) {
        option = option + '<option value="' + i + '">' + val + "</option>";
      });
      $(element).html(option);
      $(element).val(ward_id);
    },
  });
}

function loadDistrictByCityIdForPopup(city_id, element) {
  var params = [];
  $(element).html("");
  var isFinishLoad = false;
  $.ajax({
    type: "GET",
    url: "/Commons/loadDistrictByCityId",
    data: { city_id: city_id },
    async: false,
    success: function (results) {
      var obj = $.parseJSON(results);
      var option =
        '<option value="">Select district from the list (*)</option>';
      $.each(obj, function (i, val) {
        option = option + '<option value="' + i + '">' + val + "</option>";
      });
      $(element).html(option);
      isFinishLoad = true;
    },
  });
  return isFinishLoad;
}

function loadWardByDistrictIdForPopup(district_id, element) {
  var params = [];
  $(element).html("");
  var isFinishLoad = false;
  $.ajax({
    type: "GET",
    url: "/Commons/loadWardByDistrictId",
    data: { district_id: district_id },
    async: false,
    success: function (results) {
      var obj = $.parseJSON(results);
      var option = '<option value="">Select ward from the list</option>';
      $.each(obj, function (i, val) {
        option = option + '<option value="' + i + '">' + val + "</option>";
      });
      $(element).html(option);
      isFinishLoad = true;
    },
  });
  return isFinishLoad;
}

function splitId(id) {
  var arr = id.split("_");
  var position = arr[arr.length - 1];
  return position;
}

function splitName(id) {
  var arr = id.split("_");
  var name = arr[0];
  return name;
}

// Form track changes
$.fn.extend({
  trackChanges: function () {
    $(this).data("serialize", $(this).serialize());
  },
  isChanged: function () {
    return $(this).serialize() != $(this).data("serialize");
  },
  preventDoubleSubmission: function () {
    $(this).on("submit", function (e) {
      var $form = $(this);

      if ($form.data("submitted") === true) {
        // Previously submitted - don't submit again
        e.preventDefault();
      } else {
        // Mark it so that the next submit can be ignored
        // ADDED requirement that form be valid
        if ($form.valid()) {
          $form.data("submitted", true);
        }
      }
    });
    // Keep chainability
    return this;
  },
});

// Change timepicker default options
if ($.fn.timepicker) {
  $.extend($.fn.timepicker.defaults, {
    showMeridian: false,
    defaultTime: false,
  });
}

// Change validator messages method
$.extend(jQuery.validator, {
  messages: {
    required: $.validator.format("{0} is a required item."),
    email: $.validator.format("Please enter {0} is email"),
    url: $.validator.format("Please enter {0} is email"),
    date: $.validator.format("Please enter {0} is "),
    datetime: $.validator.format("Please enter {0} is datetime"),
    number: $.validator.format("Please enter {0} is number"),
    integer: $.validator.format("Please enter {0} is integer"),
    digits: $.validator.format("Please enter {0} is digits"),
    equalTo: $.validator.format("{0} confirm must match with {1}"),
    maxlength: $.validator.format(
      "Please enter {0} with '{1}' characters. (Currently {2} characters)"
    ),
    minlength: $.validator.format(
      "Please enter {0} with '{1}' characters or less. (Currently {2} characters)"
    ),
    exist: $.validator.format("Please enter {0} another."),
    extension: $.validator.format(
      "The file format is incorrect. Please select {0}"
    ),
    filesize: $.validator.format("File size limit {0} exceeded"),
    valid_format: $.validator.format("Please enter {0} in valid format"),
    shipper: $.validator.format("Please select shipper"),
    pickup_order: $.validator.format("Please select pickup order"),
    only_pickup_address: $.validator.format("Please select pickup address"),
    success: $.validator.format("Your registration is complete"),
    failed: $.validator.format("Could not register. Please try again later"),
    payment: $.validator.format("Please select customer"),
  },
});

$.validator.setDefaults({
  errorClass: "error-message",
  errorElement: "div",
  // add default behaviour for on focus out
  onfocusout: function (element) {
    this.element(element);
  },
});
//=================================================//
// Override check length method for compatibility with PHP
$.validator.methods.minlength = function (value, element, param) {
  var length = $.isArray(value)
    ? value.length
    : customGetLength(value, element);
  return this.optional(element) || length >= param;
};

$.validator.methods.maxlength = function (value, element, param) {
  var length = $.isArray(value)
    ? value.length
    : customGetLength(value, element);
  return this.optional(element) || length <= param;
};

function customGetLength(value, element) {
  if (element) {
    switch (element.nodeName.toLowerCase()) {
      case "select":
        return $("option:selected", element).length;
      case "input":
        if (checkable(element)) {
          return this.findByName(element.name).filter(":checked").length;
        }
    }
  }
  // Look for any "\n" occurences
  var matches = value.match(/\n/g);
  // Duplicate count for break line (for matching with PHP)
  var addLength = matches ? matches.length : 0;
  return value.length + addLength;
}

function checkable(element) {
  return /radio|checkbox/i.test(element.type);
}
//=================================================//

var lastLimit = new Date("2200/12/31");
var firstLimit = new Date("1700/01/01");
$.validator.methods.date = function (value, element, param) {
  var inputDate = new Date(value);
  return (
    value == "" ||
    (moment(value, "YYYY/MM/DD", true).isValid() &&
      firstLimit <= inputDate &&
      inputDate <= lastLimit)
  );
};

$.validator.addMethod("datetime", function (value, element, params) {
  var inputDate = new Date(value);
  return (
    value == "" ||
    (moment(value, "YYYY/MM/DD H:mm:ss", true).isValid() &&
      firstLimit <= inputDate &&
      inputDate <= lastLimit) ||
    (moment(value, "YYYY/MM/DD H:mm", true).isValid() &&
      firstLimit <= inputDate &&
      inputDate <= lastLimit) ||
    (moment(value, "YYYY/MM/DD", true).isValid() &&
      firstLimit <= inputDate &&
      inputDate <= lastLimit)
  );
});

$.validator.addMethod("date_time", function (value, element, params) {
  return (
    value == "" ||
    moment(value, "YYYY/MM/DD H:mm:ss", true).isValid() ||
    moment(value, "YYYY/MM/DD H:mm", true).isValid()
  );
});

$.validator.addMethod("passwordrange", function (value, element, params) {
  return (
    this.optional(element) ||
    /^[0-9a-zA-Z\#\$\%\(\)\*\+\-\.\/\:\;\?\@\[\]\_\{\}\~]{8,20}$/i.test(value)
  );
});

$.validator.addMethod("admin_passwordrange", function (value, element, params) {
  return (
    this.optional(element) ||
    /^[0-9a-zA-Z\#\$\%\(\)\*\+\-\.\/\:\;\?\@\[\]\_\{\}\~]{8,25}$/i.test(value)
  );
});

$.validator.addMethod("screen_range", function (value, element, params) {
  return (
    this.optional(element) ||
    /^[0-9a-zA-Z\#\$\%\(\)\*\+\-\.\/\:\;\?\@\[\]\_\{\}\~]{6,75}$/i.test(value)
  );
});

$.validator.addMethod("latin", function (value, element) {
  return (
    this.optional(element) ||
    /^[a-zA-Z0-9~`!@#$%^&*()-_=+<>?,./:;"'{}]*$/.test(value)
  );
});

$.validator.addMethod("mail_valid", function (value, element, dependent) {
  if (
    ($(dependent).val() != "" && value == "") ||
    ($(dependent).val() == "" && value != "")
  ) {
    return false;
  }
  var email = $(dependent).val().concat("@");
  email = email.concat(value);
  return (
    this.optional(element) ||
    /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(
      email
    )
  );
});

$.validator.addMethod("character_invalid", function (value, element) {
  return this.optional(element) || /^[a-zA-Z0-9`!^&=<>,"']*$/.test(value);
});

$.validator.addMethod("passwordsameTouserid", function (
  value,
  element,
  params
) {
  // bind to the blur event of the target in order to revalidate whenever the target field is updated
  // TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
  var target = $(params);
  if (this.settings.onfocusout) {
    target
      .unbind(".validate-equalTo")
      .bind("blur.validate-equalTo", function () {
        $(element).valid();
      });
  }
  return !(value === target.val());
});

$.validator.addMethod("passwordisAlphanum", function (value, element, params) {
  if (this.optional(element)) {
    return "dependency-mismatch";
  }
  // accept only spaces, digits and dashes
  return !(/^[0-9]*$/.test(value) || /^[a-zA-Z]*$/.test(value));
});

$.validator.addMethod("passwordInvalid", function (value, element, params) {
  return (
    this.optional(element) ||
    /^[a-zA-Z0-9~#$%\*+\-_\[\]\\.;/{}\\:\()?@]*$/.test(value)
  );
});

//custom validation method for file size
$.validator.addMethod("filesize", function (value, element, param) {
  return this.optional(element) || element.files[0].size <= param * 1024 * 1024;
});

$.validator.addMethod("fixedFileSize", function (value, element, param) {
  return value
    ? this.optional(element) || element.files[0].size <= 10 * 1024 * 1024
    : true;
});

$.validator.addMethod("check2Byte", function (value, element) {
  //return ! value.match(/^[^\u3000-\u303f\u3040-\u309f\u30a0-\u30ff\uff00-\uff9f\u4e00-\u9faf\u3400-\u4dbf]+$/);
  if (value.length > 0)
    return value.match(/^[^\x01-\x7E\xA1-\xDF]+$/)
      ? value.match(/^[ｱ-ﾝﾞﾟｧ-ｫｬ-ｮｰ｡｢｣､]+$/)
        ? false
        : true
      : false;
  else return true;
});

$.validator.addMethod("check2ByteHfS", function (value, element) {
  for (var i = 0; i < value.length; i++) {
    var unicode = value.charCodeAt(i);
    if (unicode >= 0xff61 && unicode <= 0xff9f) {
      //hankaku kana
      return false;
    } else if (
      (unicode >= 0x4e00 && unicode <= 0x9fcf) || // CJK統合漢字
      (unicode >= 0x3400 && unicode <= 0x4dbf) || // CJK統合漢字拡張A
      (unicode >= 0x20000 && unicode <= 0x2a6df) || // CJK統合漢字拡張B
      (unicode >= 0xf900 && unicode <= 0xfadf) || // CJK互換漢字
      (unicode >= 0x2f800 && unicode <= 0x2fa1f) ||
      (unicode >= 0x30a0 && unicode <= 0x30ff) || //check kana 2 byte.
      (unicode >= 0x3040 && unicode <= 0x309f) || //check hiragana 2 byte.
      unicode == 0x0020 || //space 1 byte
      unicode == 0x3000 || //space 2 byte
      (unicode >= 0xff00 && unicode <= 0xfff0) //alphabet 2 byte
    ) {
    } else {
      return false;
    }
  }
  return true;
});

$.validator.addMethod("rangeEmail", function (value, element, param) {
  //!#$%&'*+-/=?^_`{|}~.
  return (
    this.optional(element) ||
    /^[0-9a-zA-Z\#\!\$\%\(\)\*\+\-\.\/\:\;\?\'\=\`\|\&\@\^\[\]\_\{\}\~]{6,75}$/i.test(
      value
    )
  );
});

$.validator.addMethod("checkKanji", function (value, element) {
  for (var i = 0; i < value.length; i++) {
    var unicode = value.charCodeAt(i);
    if (
      (unicode >= 0x4e00 && unicode <= 0x9fcf) || // CJK統合漢字
      (unicode >= 0x3400 && unicode <= 0x4dbf) || // CJK統合漢字拡張A
      (unicode >= 0x20000 && unicode <= 0x2a6df) || // CJK統合漢字拡張B
      (unicode >= 0xf900 && unicode <= 0xfadf) || // CJK互換漢字
      (unicode >= 0x2f800 && unicode <= 0x2fa1f) ||
      (unicode >= 0x30a0 && unicode <= 0x30ff) || //check kana 2 byte.
      (unicode >= 0x3040 && unicode <= 0x309f) //check hiragana 2 byte.
    ) {
    } else {
      return false;
    }
  }
  return true;
});

$.validator.addMethod("checkKatakana", function (value, element) {
  for (var i = 0; i < value.length; i++) {
    var unicode = value.charCodeAt(i);
    if (
      (unicode >= 0x30a0 && unicode <= 0x30ff) ||
      unicode == 0x0020 || //space 1 byte
      unicode == 0x3000
    ) {
    } else {
      return false;
    }
  }
  return true;
});

$.validator.addMethod("checkKatakana1Byte2Byte", function (value, element) {
  var result = true;
  if (value.length > 0) {
    result = value.match(/^[\uFF65-\uFF9F\u30A0-\u30FF.\)\(\/\-\　]+$/)
      ? true
      : false;
  }
  return result;
});

$.validator.addMethod("checkKatakana2ByteAndCharacter", function (
  value,
  element
) {
  var result = true;
  if (value.length > 0) {
    result = value.match(/^[\u30A0-\u30FF]+$/) ? true : false;
  }
  return result;
});

$.validator.addMethod("checkCharacterlatin", function (value, element) {
  return this.optional(element) || /^[a-zA-Z0-9]*$/.test(value);
});

$.validator.addMethod("checkAlphabet", function (value, element) {
  return this.optional(element) || /^[a-zA-Z]*$/.test(value);
});

$.validator.addMethod(
  "checkInteger",
  function (value, element) {
    return this.optional(element) || /^[0-9]*$/.test(value);
  },
  function (params, element) {
    return $.validator.messages.integer($(element).data("name"));
  }
);

$.validator.addMethod("digitsCustom", function (value, element) {
  return this.optional(element) || /^[0-9-]*$/.test(value);
});

$.validator.addMethod(
  "checkSumPort",
  function (value, element, params) {
    var kosu = 0;
    var portCount = 0;
    var free_n = 0;
    var paid_n = 0;
    var owner_n = 0;
    var free_h = 0;
    var paid_h = 0;
    var owner_h = 0;
    var free_s = 0;
    var paid_s = 0;
    var owner_s = 0;
    var free_o = 0;
    var paid_o = 0;
    var owner_o = 0;
    if ($("#kosu").val()) {
      kosu = parseInt($("#kosu").val());
    }
    if ($("#port-count").val()) {
      portCount = parseInt($("#port-count").val());
    }
    if ($("#free-port-count-n").val()) {
      free_n = parseInt($("#free-port-count-n").val());
    }
    if ($("#free-port-count-h").val()) {
      free_h = parseInt($("#free-port-count-h").val());
    }
    if ($("#free-port-count-s").val()) {
      free_s = parseInt($("#free-port-count-s").val());
    }
    if ($("#free-port-count-o").val()) {
      free_o = parseInt($("#free-port-count-o").val());
    }
    if ($("#paid-port-count-n").val()) {
      paid_n = parseInt($("#paid-port-count-n").val());
    }
    if ($("#paid-port-count-h").val()) {
      paid_h = parseInt($("#paid-port-count-h").val());
    }
    if ($("#paid-port-count-s").val()) {
      paid_s = parseInt($("#paid-port-count-s").val());
    }
    if ($("#paid-port-count-o").val()) {
      paid_o = parseInt($("#paid-port-count-o").val());
    }
    if ($("#owner-port-count-n").val()) {
      owner_n = parseInt($("#owner-port-count-n").val());
    }
    if ($("#owner-port-count-h").val()) {
      owner_h = parseInt($("#owner-port-count-h").val());
    }
    if ($("#owner-port-count-s").val()) {
      owner_s = parseInt($("#owner-port-count-s").val());
    }
    if ($("#owner-port-count-o").val()) {
      owner_o = parseInt($("#owner-port-count-o").val());
    }
    if (
      kosu > 0 ||
      portCount > 0 ||
      free_n > 0 ||
      free_h > 0 ||
      free_s > 0 ||
      free_o > 0 ||
      paid_n > 0 ||
      paid_h > 0 ||
      paid_s > 0 ||
      paid_o > 0 ||
      owner_n > 0 ||
      owner_h > 0 ||
      owner_s > 0 ||
      owner_o > 0
    ) {
      var totalPortCount =
        free_n +
        free_h +
        free_s +
        free_o +
        paid_n +
        paid_h +
        paid_s +
        paid_o +
        owner_n +
        owner_h +
        owner_s +
        owner_o;
      if (portCount === totalPortCount && kosu === totalPortCount) {
        return true;
      }
    } else if (
      kosu === 0 &&
      portCount === 0 &&
      free_n === 0 &&
      free_h === 0 &&
      free_s === 0 &&
      free_o === 0 &&
      paid_n === 0 &&
      paid_h === 0 &&
      paid_s === 0 &&
      paid_o === 0 &&
      owner_n === 0 &&
      owner_h === 0 &&
      owner_s === 0 &&
      owner_o === 0
    ) {
      return true;
    } else {
      return false;
    }
  },
  function (params, element) {
    var kosu = 0;
    var portCount = 0;
    if ($("#kosu").val()) {
      kosu = parseInt($("#kosu").val());
    }
    if ($("#port-count").val()) {
      portCount = parseInt($("#port-count").val());
    }
    return $.validator.messages.checkData(portCount, kosu);
  }
);

$.validator.addMethod("checkValidEmailRFC", function (value, element) {
  var matchRules = new RegExp(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
  var latinRule = /^[a-zA-Z0-9~`!@#$%^&*()-_=+<>?,./:;"'{}]*$/.test(value);
  return this.optional(element) || (matchRules.test(value) && latinRule);
});

$.validator.addMethod("mail_valid_RFC", function (value, element, dependent) {
  if (
    ($(dependent).val() != "" && value == "") ||
    ($(dependent).val() == "" && value != "")
  ) {
    return false;
  }
  var email = $(dependent).val().concat("@");
  email = email.concat(value);
  var matchRules = new RegExp(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
  var latinRule = /^[a-zA-Z0-9~`!@#$%^&*()-_=+<>?,./:;"'{}]*$/.test(email);
  return this.optional(element) || (matchRules.test(email) && latinRule);
});

$.validator.addMethod("greaterThanDate", function (value, element, params) {
  if ($(params).val().length > 0 && value.length > 0) {
    if (!/Invalid|NaN/.test(new Date(value))) {
      if (new Date(value) <= new Date($(params).val())) {
        if ($(params).hasClass("error-message")) {
          $(params).removeClass("error-message");
          $(params).next().remove();
        }
      }
      return new Date(value) <= new Date($(params).val());
    }

    return (
      (isNaN(value) && isNaN($(params).val())) ||
      Number(value) > Number($(params).val())
    );
  } else {
    return true;
  }
});

$.validator.addMethod("lessThanDate", function (value, element, params) {
  if ($(params).val().length > 0 && value.length > 0) {
    if (!/Invalid|NaN/.test(new Date(value))) {
      if (new Date(value) >= new Date($(params).val())) {
        if ($(params).hasClass("error-message")) {
          $(params).removeClass("error-message");
          $(params).next().remove();
        }
      }
      return new Date(value) >= new Date($(params).val());
    }

    return (
      (isNaN(value) && isNaN($(params).val())) ||
      Number(value) > Number($(params).val())
    );
  } else return true;
});

/**
 * jQuery Validation custom rule,
 * check format xxx-xxxx
 */
$.validator.addMethod("checkFormatPostCode", function (value, element) {
  var form = $(element).form().attr("id");
  var postcode1 = $("#postcode-1").val();
  var postcode2 = $("#postcode-2").val();
  $("#postcode-1").change(function () {
    $("form").validate().element("#postcode-2");
  });
  $("#postcode-2").change(function () {
    $("form").validate().element("#postcode-1");
  });
  if (
    $(element).attr("id") == "postcode-1" &&
    postcode1 == "" &&
    postcode2 != ""
  ) {
    return false;
  }
  if (
    $(element).attr("id") == "postcode-2" &&
    postcode1 != "" &&
    postcode2 == ""
  ) {
    return false;
  }
  return true;
});

/**
 * jQuery Validation custom rule,
 * only accept 1 byte for post code
 */
$.validator.addMethod("checkPostCode", function (value, element, param) {
  if (param && !$("form").hasClass("export")) {
    return this.optional(element) || /^[0-9]*$/.test(value);
  }
  return true;
});

/**
 * jQuery Validation custom rule,
 * only accept 1 byte for post code
 */
$.validator.addMethod("maxSearchLength", function (value, element, param) {
  if (param && !$("form").hasClass("export")) {
    var length = $.isArray(value)
      ? value.length
      : customGetLength(value, element);
    return this.optional(element) || length <= param;
  }
  return true;
});

/**
 * jQuery Validation custom rule,
 * only accept 1 byte and '-' for tel number
 */
$.validator.addMethod("checkTel", function (value, element, param) {
  if (param && !$("#frmDho01").hasClass("export")) {
    return this.optional(element) || /^[0-9-]*$/.test(value);
  }
  return true;
});

/**
 * jQuery Validation custom rule,
 * only accept 1 byte and 20 types of character
 */
$.validator.addMethod("check1ByteSpecialChars", function (value, element) {
  return (
    this.optional(element) ||
    /^[a-zA-Z0-9#$%()*+\-./:;?@\[\]_{}~]*$/.test(value)
  );
});

//custom validate.
function isInt(value) {
  return (
    !isNaN(value) &&
    (function (x) {
      return (x | 0) === x;
    })(parseFloat(value))
  );
}

$.validator.addMethod(
  "checkMaxlength",
  function (val, element) {
    return val.length > 0 && val.length > $(element).data("max-length")
      ? false
      : true;
  },
  function (params, element) {
    return $.validator.messages.maxlength([
      $(element).data("name"),
      $(element).data("max-length"),
      $(element).val().length,
    ]);
  }
);

$.validator.addMethod(
  "checkMinlength",
  function (val, element) {
    return val.length > 0 && val.length < $(element).data("min-length")
      ? false
      : true;
  },
  function (params, element) {
    return $.validator.messages.minlength([
      $(element).data("name"),
      $(element).data("min-length"),
      $(element).val().length,
    ]);
  }
);

$.validator.addMethod(
  "checkNumber",
  function (value, element) {
    if (value.length === 0) return true;
    return this.optional(element) || /^[0-9]*$/.test(value);
  },
  function (params, element) {
    return $.validator.messages.number($(element).data("name"));
  }
);

$.validator.addMethod(
  "checkDecimal",
  function (val, element) {
    if (val.length === 0) return true;
    return !isNaN(val) && Number(val) >= 0;
  },
  function (params, element) {
    return $.validator.messages.digits($(element).data("name"));
  }
);

$.validator.addMethod(
  "checkRequired",
  function (val, element) {
    return !(val === null || val === undefined || val.length === 0);
  },
  function (params, element) {
    return $.validator.messages.required($(element).data("name"));
  }
);

function checkKatakana2Byte(value, element) {
  var result = true;
  if (value.length > 0) {
    result = value.match(/^[・\u30a0-\u30ff　]*$/) ? true : false;
  }
  return result;
}

$.validator.addMethod("checkKatakana2ByteAndCharacter", function (
  value,
  element
) {
  return checkKatakana2Byte(value, element);
});

$.validator.addMethod(
  "checkKatakana2ByteAndCharacterWithMessage",
  function (value, element) {
    return checkKatakana2Byte(value, element);
  },
  function (params, element) {
    return $.validator.messages.checkKatakana2Byte($(element).data("name"));
  }
);

$.validator.addMethod(
  "checkDatetime",
  function (value, element, params) {
    if (value.length === 0) return true;
    var date = new Date(value);
    return !isNaN(date.getTime());
  },
  function (params, element) {
    return $.validator.messages.datetime($(element).data("name"));
  }
);

$.validator.addMethod(
  "checkExistPrivateCode",
  function (value, element, params) {
    var result = true;
    var shopId = value;
    var hiddenShopId = $(params).length > 0 ? $(params).val() : "";
    if (
      (shopId.length > 0 && hiddenShopId.length === 0) ||
      (hiddenShopId.length > 0 && hiddenShopId !== shopId)
    ) {
      var url = $("#check_shop_id").val();
      var userId = $("#user_id").val();
      $.ajax({
        url: url,
        method: "GET",
        data: {
          shopId: value,
          userId: userId,
        },
        async: false,
        success: function (data) {
          result = !data;
        },
      });
    }
    return result;
  },
  function (params, element) {
    return $.validator.messages.exist($(element).data("name"));
  }
);

$.validator.addMethod(
  "checkExistPhone",
  function (value, element, params) {
    var result = true;
    var phone = value;
    var hiddenPhone = $(params).length > 0 ? $(params).val() : "";
    if (
      (phone.length > 0 && hiddenPhone.length === 0) ||
      (hiddenPhone.length > 0 && hiddenPhone !== phone)
    ) {
      var url = $("#check_phone").val();
      $.ajax({
        url: url,
        method: "GET",
        data: {
          phone: value,
        },
        async: false,
        success: function (data) {
          result = !data;
        },
      });
    }
    return result;
  },
  function (params, element) {
    return $.validator.messages.exist($(element).data("name"));
  }
);

$.validator.addMethod(
  "checkExistEmail",
  function (value, element, params) {
    var result = true;
    var email = value;
    var hiddenEmail = $(params).length > 0 ? $(params).val() : "";
    console.log(hiddenEmail);
    console.log(email);
    if (
      (email.length > 0 && hiddenEmail.length === 0) ||
      (hiddenEmail.length > 0 && hiddenEmail !== email)
    ) {
      var url = $("#check_email").val();
      $.ajax({
        url: url,
        method: "GET",
        data: {
          email: value,
        },
        async: false,
        success: function (data) {
          result = !data;
        },
      });
    }
    return result;
  },
  function (params, element) {
    return $.validator.messages.exist($(element).data("name"));
  }
);

$.validator.addMethod(
  "positiveNumber",
  function (value) {
    if (value.length === 0) return true;
    return Number(value) >= 0;
  },
  function (params, element) {
    return $.validator.messages.number($(element).data("name"));
  }
);

$.validator.addMethod(
  "checkUsername",
  function (value, element) {
    if (value.length === 0) return true;
    return this.optional(element) || /^[0-9a-zA-Z_]*$/.test(value);
  },
  function (params, element) {
    return $.validator.messages.valid_format($(element).data("name"));
  }
);

$.validator.addMethod(
  "checkPassport",
  function (value, element) {
    if (value.length === 0) return true;
    return this.optional(element) || /^[0-9a-zA-Z]*$/.test(value);
  },
  function (params, element) {
    return $.validator.messages.valid_format($(element).data("name"));
  }
);

$.validator.addClassRules({
  number: { checkNumber: true },
  decimal: { checkDecimal: true },
  "max-length": { checkMaxlength: true },
  "min-length": { checkMinlength: true },
  required: { checkRequired: true },
  checkDatetime: { checkDatetime: true },
  exist: { checkExistPrivateCode: "#hidden_shop_id" },
  "phone-exist": { checkExistPhone: "#hidden_phone" },
  "email-exist": { checkExistEmail: "#hidden_email" },
  positiveNumber: { positiveNumber: true },
  integer: { checkInteger: true },
  username: { checkUsername: true },
  passport: { checkPassport: true },
});

$(function () {
  $("ul.pagination").find("li.active > a").bind("click", false);
});

$(function () {
  $(".list-group a").bind("click", function (e) {
    var link = $(this).attr("href");
    var pos = $(".bb-sidebar").scrollTop();
    var page = $(document).scrollTop();
    sessionStorage.setItem("sidebarPosition", pos);
    sessionStorage.setItem("pagePosition", page);
  });
});

$(document).ready(function () {
  var sidebarPosition = sessionStorage.getItem("sidebarPosition");
  var page = sessionStorage.getItem("pagePosition");
  if (parseInt(page) > 110) {
    $("html, body").scrollTop(110);
  } else {
    $("html, body").scrollTop(page);
  }
  setTimeout(function () {
    $(".bb-sidebar").scrollTop(parseInt(sidebarPosition));
  }, 200);
});

function getData(callback, id) {
  $(".validateForm").data("validator").resetForm();
  if (typeof id !== "undefined") {
    $(".btn-success").html("Update");
    return $.ajax({
      type: "get",
      url: $("#base_url").val() + "/" + id,
      beforeSend: function () {
        $("#fadeMe1").addClass("fadeMe");
        $("#hidden1").removeClass("hidden");
      },
      success: function (result) {
        $("#fadeMe1").removeClass("fadeMe");
        $("#hidden1").addClass("hidden");
        if (result.length > 0) {
          var obj = $.parseJSON(result);
          callback(obj);
        }
      },
    });
  } else {
    $(".btn-success").html("Add");
  }
}

function loadCalendar(id) {
  var element = "#" + id;
  if ($(element).length > 0) {
    var tempDate = $("#temp_" + id).val();
    $(element).val(tempDate.length > 0 ? tempDate : "");
    if ($(element).val().length > 0) {
      var arrDate = tempDate.split("-");
      $(element).daterangepicker({
        format: "DD/MM/YYYY",
        startDate: arrDate[0],
        endDate: arrDate[1],
      });
    } else {
      $(element).daterangepicker({
        format: "DD/MM/YYYY",
        startDate: new Date(),
        endDate: new Date(),
      });
    }
  }
}

$(function () {});

$("#btnPrint").click(function () {
  var order_ids = [];
  $(".check_get:checked").each(function (i, element) {
    order_ids.push($(element).val());
  });
  if (order_ids.length > 0) {
    var href = $("#print_url").val() + "/" + order_ids.join(",");
    window.open(href);
    return false;
  }
});

$("#btnNewPrint").click(function () {
  var order_ids = [];
  $(".check_get:checked").each(function (i, element) {
    order_ids.push($(element).val());
  });
  if (order_ids.length > 0) {
    var href = $("#new_print_url").val() + "/" + order_ids.join(",");
    window.open(href);
    return false;
  }
});

$("#btnPrint100").click(function () {
  var order_ids = [];
  $(".check_get:checked").each(function (i, element) {
    order_ids.push($(element).val());
  });
  if (order_ids.length > 0) {
    var href = "/orders/printOrdersV1/" + order_ids.join(",");
    window.open(href);
    return false;
  }
});

$("#btnPrint100_60").click(function () {
  var order_ids = [];
  $(".check_get:checked").each(function (i, element) {
    order_ids.push($(element).val());
  });
  if (order_ids.length > 0) {
    var href = "/orders/printOrdersV2/" + order_ids.join(",");
    window.open(href);
    return false;
  }
});

$("#btnPrint60_40").click(function () {
  console.log('btnPrint60_40');
  var order_ids = [];
  $(".check_get:checked").each(function (i, element) {
    order_ids.push($(element).val());
  });
  if (order_ids.length > 0) {
    var href = "/orders/printOrdersV3/" + order_ids.join(",");
    window.open(href);
    return false;
  }
});

function confirmProcess(ids, url, note, tripId) {
  $.ajax({
    method: "post",
    data: { ids: ids, note: note, tripId: tripId },
    url: url,
    beforeSend: function () {
      $("#fadeMe1").addClass("fadeMe");
      $("#hidden1").removeClass("hidden");
    },
    success: function (results) {
      $("#fadeMe1").removeClass("fadeMe");
      $("#hidden1").addClass("hidden");
      location.reload();
    },
  });
}

function confirmHanding(ids, url, note) {
  $.ajax({
    method: "post",
    data: { ids: ids, note: note },
    url: url,
    beforeSend: function () {
      $("#fadeMe1").addClass("fadeMe");
      $("#hidden1").removeClass("hidden");
    },
    success: function (results) {
      $("#fadeMe1").removeClass("fadeMe");
      $("#hidden1").addClass("hidden");
      location.reload();
    },
  });
}

function getAddress(ids, phone) {
  var arrId = ids.split("_");
  var position = arrId[arrId.length - 1];
  $.ajax({
    method: "get",
    data: { phone: phone, user_id: $("#user_id").val() },
    url: "/Orders/getAddressByPhone",
    beforeSend: function () {
      $("#fadeMe1").addClass("fadeMe");
      $("#hidden1").removeClass("hidden");
    },
    success: function (result) {
      var address = JSON.parse(result);
      if (typeof address.address !== "undefined") {
        $("#receiver_name_" + position).val($.trim(address.receiver_name));
        $("#address_" + position).val($.trim(address.address));
        $("#city_id_" + position).val(address.city_id);
        var districtElement = "#district_id_" + position;
        loadDistrictByCityIdByAddress(
          address.city_id,
          address.district_id,
          districtElement
        );
      }
      $("#fadeMe1").removeClass("fadeMe");
      $("#hidden1").addClass("hidden");
    },
  });
}
