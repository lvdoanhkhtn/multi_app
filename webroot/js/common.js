// /**
//  * Created by doanh.lv on 8/14/19.
//  */
jQuery(function() {
    jQuery(".validateForm").on("change", "[id^=city_id]", function () {
        var city_id = $(this).val();
        var position = splitId($(this).attr('id'));
        var element = "#district_id_" + position;
        loadDistrictByCityId(city_id, element);
    });

    jQuery(".validateForm").on("change", "[id^=district_id]", "[id^=city_id]", function () {
        var district_id = $(this).val();
        var position = splitId($(this).attr('id'));
        var city_id = $("#city_id_" + position).val();
        var element = "#ward_id_" + position;
        loadWardByDistrictId(district_id, element);
    });



    $('.customValidateForm').validate({
            errorPlacement: function(error, element) {
                error.appendTo(element.parent().next("div.errorTxt"));
            }
        }
    );

    $('.validateForm').validate();
    

    $('.selectSingleDate').daterangepicker({
            "singleDatePicker": true,
            "startDate": "01/01/1990",
            "endDate": moment(),
            "format": 'DD/MM/YYYY'

    });

    $('#priceTab a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

// store the currently selected tab in the hash value
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });

// on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
    $('#priceTab a[href="' + hash + '"]').tab('show');

    // Change paginate page
    $('.btnStatus').on('click', function (e) {
        var $this  = $(this);
        // console.log($this);
        var order_status = $this.attr('id');
        var url = new Url(window.location.href);
        delete url.query.order_status;    // Remove page parameter
        url.query.order_status = order_status;  // Set limit
        // Submit form
        var formSelector = $(this).data('form');
        var $form = $('form.search-by-post');
        // If pagination tag has form-data value, then use that form selector
        if(formSelector){
            $form = $(formSelector);
        }
        if($form.length){
            $form.attr('action', url);
            $form.submit();
        }else{
            window.location.href = url.toString();
        }
    });

    $("body").on("submit", "form", function() {
        $(this).submit(function() {
            return false;
        });
        return true;
    });

    $('#reason').change(function () {
        $('.note').val($(this).val());
    });

    $('.note').val( $('#reason').val());



    $(".checkOrder").focus();

    $('.paginate_menu').on('change', function(){
        var limit = $(this).val();
        var url = new Url(window.location.href);
        delete url.query.page;    // Remove page parameter
        url.query.limit = limit;  // Set limit
        // Submit form
        var formSelector = $(this).data('form');
        var $form = $('form.search-by-post');
        // If pagination tag has form-data value, then use that form selector
        if(formSelector){
            $form = $(formSelector);
        }
        if($form.length){
            $form.attr('action', url);
            $form.submit();
        }else{
            window.location.href = url.toString();
        }
    });

    // Change paginate page
    $('.pagination li a').on('click', function (e) {
        var $this  = $(this);
        var href = $this.attr('href');         // Get current href attribute
        if(href){
            var url = new Url(href);
            // Get limit
            var limit = $('.paginate_menu').val();
            if(limit){
                url.query.limit = limit;
            }

            var formSelector = $this.closest('.pagination').data('form');
            var $form = $('form.search-by-post');
            // If pagination tag has form-data value, then use that form selector
            if(formSelector){
                $form = $(formSelector);
            }
            if($form.length){
                $form.attr('action', url.toString());
                $form.submit();
                e.preventDefault();
            }else{
                window.location.href = url.toString();
                e.preventDefault();
            }
        }
    });

    if($('.dtBasicExample').length > 0){
        $('.dtBasicExample').DataTable({
            "searching": true, // false to disable search (or any other option)
            "lengthMenu": false,
            "paging": false,
            "bInfo": false,
            "bSort" : false
        });

        $('.dataTables_filter').addClass('col-xs-12');

        $('.dataTables_wrapper').attr('style', 'width:100%');
    }


    $('.date').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        $('.daterange').data('daterangepicker').setStartDate(moment());
        $('.daterange').data('daterangepicker').setEndDate(moment());
    });

    $('#notify').click(function (){
        console.log('clicked');
        $.ajax({
            type: 'GET',
            url: '/notifies/read',
            success: function(result){
                if(result){
                    $('#notifyBadge').html("");
                }
            }
        });
    });
    
    $('#date-range').on('change', function() {
        var val = $(this).val();
        if (val == 100) {
            $('#reportDate').css('display', 'block');
        } else {
            $('#reportDate').css('display', 'none');

        }
    });
});






