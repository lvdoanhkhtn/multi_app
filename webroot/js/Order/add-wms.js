$(document).ready(function () {
  $("#items").on("select2:select", function (e) {
    var data = e.params.data;
    var form =
      '<div id="wrapper-' +
      data.id +
      '" class="form-group">' +
      "<label class='col-md-2'>" +
      data.text +
      "</label>" +
      "<input name=qty_items[" +
      data.id +
      '] type="number" value="1" min="1" id="qty' +
      data.id +
      '" />' +
      "<input name=name_items[" +
      data.id +
      '] type="hidden" value="' +
      data.text +
      '" />' +
      "</div>";
    $("#items-wrapper").append(form);
    console.log(data);
  });

  $("#items").on("select2:unselect", function (e) {
    var data = e.params.data;
    $("#wrapper-" + data.id).remove();
  });
});
