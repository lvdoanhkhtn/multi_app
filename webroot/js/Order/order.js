$(document).ready(function () {
  //$("#service_pack_id_1 option[value=6]").hide();
  //change field to create cost fee
  $(".Order").on(
    "change",
    "[id^=payment], [id^=address_id], [id^=city_id], [id^=district_id], [id^=service_pack_id], [id^=weight], [id^=money_collection], [id^=product_value], [id^=product_value], [id^=length], [id^=width], [id^=height]",
    function () {
      var position = splitId($(this).attr("id"));
      var name = "#" + $(this).attr("id");
      var city_id = $("#city_id_" + position).val();
      var district_id = $("#district_id_" + position).val();
      var service_pack_id = $("#service_pack_id_" + position).val();
      var weight =
        $("#weight_" + position).val().length > 0
          ? $("#weight_" + position).val()
          : 0;
      var length = $("#length_" + position).val();
      var width = $("#width_" + position).val();
      var height = $("#height_" + position).val();
      var user_id = $("#user_id").val();

      var payment = $("#payment_" + position).val();
      var address_id = $("#address_id_1").val().split(",");
      var city_sender_id = address_id[1];
      var district_sender_id = address_id[2];
    //   if (city_sender_id != null && city_id !== undefined) {
    //     if (city_sender_id == city_id && city_sender_id == 64) {
    //       $("#service_pack_id_1 option[value=6]").show();
    //     } else {
    //       $("#service_pack_id_1 option[value=6]").hide();
    //     }
    //   }

      var moneyCollection = 0;
      var productValue = 0;
      var lb_money_collection = "#money_collection_" + position;
      if ($(lb_money_collection).length > 0 && lb_money_collection === name) {
        moneyCollection = $(lb_money_collection).val().replace(/,/g, "");
        $("#product_value_" + position).val(
          $("#money_collection_" + position).val()
        );
      }

      if (city_id.length > 0 && service_pack_id.length > 0) {
        if (length.length > 0 && width.length > 0 && height.length > 0) {
          var tempWeight = (length * width * height) / 6000;
          if (tempWeight > weight) {
            weight = tempWeight;
          }
        }
        var money_collection = $("#money_collection_" + position).val();
        var product_value = $("#product_value_" + position).val();
        var cost = $("#OrderCost").val();
        getCostAjax(
          city_id,
          district_id,
          city_sender_id,
          district_sender_id,
          money_collection,
          product_value,
          service_pack_id,
          weight,
          user_id,
          cost,
          position,
          payment
        );
      }
    }
  );

  //get cost for order
  function getCostAjax(
    city_id,
    district_id,
    sender_city_id,
    sender_district_id,
    money_collection,
    product_value,
    service_pack_id,
    weight,
    user_id,
    cost,
    position,
    payment
  ) {
    $.ajax({
      type: "get",
      url: "/orders/getCostAjax/",
      data: {
        city_id: city_id,
        district_id: district_id,
        ward_id: null,
        sender_city_id: sender_city_id,
        sender_district_id: sender_district_id,
        money_collection: money_collection,
        product_value: product_value,
        service_pack_id: service_pack_id,
        weight: weight,
        user_id: user_id,
        cost: cost,
      },
      beforeSend: function () {
        resetListFee(position);
      },
      success: function (results) {
        var result = $.parseJSON(results);
        if (payment == 0) {
          $("#total_amount_" + position).html(result.total_amount);
        } else {
          $("#total_amount_" + position).html(result.total);
        }
        $("#total_fee_" + position).html(result.total_fee);
        $("#shipping_fee_" + position).html(result.fee);
        $("#cod_fee_" + position).html(result.collection_fee);
        $("#insurance_fee_" + position).html(result.protect_fee);
      },
    });
  }

  //reset fee after fill
  function resetListFee(position) {
    $("#total_amount_" + position).html("");
    $("#total_fee_" + position).html("");
    $("#shipping_fee_" + position).html("");
    $("#cod_fee_" + position).html("");
    $("#insurance_fee_" + position).html("");
  }

  $(".check_get_all").on("ifChecked ifUnchecked", function (event) {
    if (event.type == "ifChecked") $(".check_get").iCheck("Check");
    if (event.type == "ifUnchecked") $(".check_get").iCheck("uncheck");
  });

  $(".btnHanding").click(function (e) {
    var ids = [];
    $(".check_get:checked").each(function (i, element) {
      ids.push($(element).val());
    });
    if (ids.length === 0) {
      alert($.validator.messages.pickup_order(""));
    } else {
      var note = "";
      if ($("#pickup_note").length > 0) {
        note = $("#pickup_note").val();
      }

      var id = $(this).attr("id");
      switch (id) {
        case "btnReDelivery":
          confirmHanding(ids, "/orders/confirmReDelivery", note);
          break;
        case "btnRePickup":
          confirmHanding(ids, "/orders/confirmRePickup", note);
          break;
        case "btnReStored":
          confirmHanding(ids, "/orders/confirmReStored", note);
          break;
        case "btnReturn":
          confirmHanding(ids, "/orders/confirmReturn", note);
          break;
        case "btnPayment":
          confirmHanding(ids, "/payments/addPayments", note);
          break;
        case "btnPaid":
          confirmHanding(ids, "/payment_details/confirmPaid", note);
          break;
        case "btnRemove":
          confirmHanding(ids, "/payment_details/confirmRemove", note);
          break;
      }
    }
    e.preventDefault();
  });

  $("#btnCollect").click(function (e) {
    var ids = [];
    var deliveryId = $("#deliveryId").val();
    var pickupTripId = $("#pickupTripId").val();
    $(".check_get:checked").each(function (i, element) {
      ids.push($(element).val());
    });
    if (ids.length === 0) {
      alert($.validator.messages.pickup_order(""));
    } else {
      $.ajax({
        method: "post",
        data: {
          ids: ids,
          deliveryId: deliveryId,
          pickupTripId: pickupTripId,
        },
        url: "/orders/confirmCollect",
        beforeSend: function () {
          $("#fadeMe1").addClass("fadeMe");
          $("#hidden1").removeClass("hidden");
        },
        success: function (results) {
          console.log("🚀 ~ file: order.js ~ line 201 ~ results", results);
          $("#fadeMe1").removeClass("fadeMe");
          $("#hidden1").addClass("hidden");
          location.href = "/accountants/report";
        },
      });
    }
    e.preventDefault();
  });

  $('input[type="checkbox"], input[type="radio"]').iCheck({
    checkboxClass: "icheckbox_flat-blue",
    radioClass: "iradio_square",
    increaseArea: "20%", // optional
  });

  function createIfNotExist(id, name) {
    if ($("#cbCustomers").find("option[value='" + id + "']").length === 0) {
      if (typeof id != undefined) {
        var newOption = new Option(name, id, true, true);
        // Append it to the select
        $("#cbCustomers").append(newOption).trigger("change");
      } else {
        $("#cbCustomers").val(id).trigger("change");
      }
    } else {
      // Create a DOM Option and pre-select by default
      var newOption = new Option(name, id, true, true);
      // Append it to the select
      $("#cbCustomers").append(newOption).trigger("change");
    }
  }

  $("#cbCustomers").on("select2:select", function (e) {
    var id = e.params.data.id;
    $("#cb_" + id).iCheck("Check");
  });

  $(".check_get").on("ifChecked ifUnchecked", function (event) {
    if (event.type == "ifChecked") {
      var id = $(this).val();
      var name = $(this).attr("data-name");
      createIfNotExist(id, name);
    }
    if (event.type == "ifUnchecked") {
      var id = $(this).val();
      $("#cbCustomers")
        .find("option[value='" + id + "']")
        .remove();
      $("#cbCustomers").trigger("change");
    }
  });

  $("#cbCustomers").on("select2:unselect", function (e) {
    var id = e.params.data.id;
    $("#cb_" + id).iCheck("uncheck");
  });

  $("#frmOrder").on("keypress", function (event) {
    if (event.keyCode == 13) {
      $(this).submit();
    }
  });

  $(".btnAddPayment").click(function (e) {
    var ids = [];
    $(".check_get:checked").each(function (i, element) {
      ids.push($(element).val());
    });
    if (ids.length === 0) {
      alert($.validator.messages.payment(""));
      e.preventDefault();
    } else {
      $("#payment-add").submit();
    }
  });
});
