var csrfToken = $('#search_orders input[name="_csrfToken"]').val();
$(document).ready(function () {
  $("#btn-search-orders").click(function (e) {
    $.ajax({
      type: "POST",
      async: true,
      cache: false,
      data: { keyWord: $("#key-word").val() },
      url: "/orders/public_search",
      beforeSend: function (xhr) {
        xhr.setRequestHeader("X-CSRF-Token", csrfToken);
        $("#fadeMe1").addClass("fadeMe");
        $("#hidden1").removeClass("hidden");
      },
      success: function (result) {
        var order = $.parseJSON(result);
        $("#fadeMe1").removeClass("fadeMe");
        $("#hidden1").addClass("hidden");
        $("#key-word").val("");
        if (order.orderId) {
          $("#order_result_popup").css("display", "block");
          $("#order_code").html(order.orderId);
          $("#receiver_name").html(order.receiver_name);
          $("#receiver_phone").html(order.arr_receiver_phone);
          $("#receiver_address").html(order.address_delivery);
          $("#total_cod").html(order.total);
          $("#note").html(order.note);
          $("#order_status").html(order.order_status_name);
          var html = "";
          html = order.histories.map(
            (history) => `
          <li class="time-label">
              <span class="bg-red">${history.modified}</span>
              <span class="bg-red">${history.modifier}</span>
          </li>
          <li>
              <div class="timeline-body">${history.text[0]}</div>
          </li>`
          );
          $(".timeline").html(html);
        } else {
        }
        // console.log(result);
      },
      error: function (tab) {
        alert("error" + tab.statusText);
      },
    });
    e.preventDefault();
  });

  $(".close").on("click", function () {
    $("#order_result_popup").css("display", "none");
  });
});

