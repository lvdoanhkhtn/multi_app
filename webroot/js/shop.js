// /**
//  * Created by doanh.lv on 8/14/19.
//  */
$(function() {
    $('.select2').select2()
        // Change paginate menu
    loadCalendar('create_date');
    loadCalendar('complete_date');
    loadCalendar('stored_date');
});

$('#lang').change(function() {
    var id = $(this).val();
    $.cookie('lang', id, { expires: 7, path: '/' });
    location.reload();
    return false;
});