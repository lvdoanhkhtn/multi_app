$(document).ready(function () {
  var firebaseConfig = {
    apiKey: "AIzaSyDt1J_1LQRFu8ap0H3xHBFrGd1oX7lvZus",
    authDomain: "cbs-notify.firebaseapp.com",
    databaseURL: "https://cbs-notify.firebaseio.com",
    projectId: "cbs-notify",
    storageBucket: "cbs-notify.appspot.com",
    messagingSenderId: "618234163844",
    appId: "1:618234163844:web:f6ecb758ee6dd9b0fa48d1",
    measurementId: "G-0CF2RD67QD",
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  // [START get_messaging_object]
  // Retrieve Firebase Messaging object.
  const messaging = firebase.messaging();
  // [END get_messaging_object]
  // [START set_public_vapid_key]
  // Add the public key generated from the console here.
  messaging.usePublicVapidKey(
    "BH1E0YRgSE4MQ0c6JoY5qZC_J4BRS7GwmMdB8mwmrMHFeVB8WBk_Plm-GR0wArM_o7BEb5ZH6VtlXr6ouf6MYRY"
  );
  // [END set_public_vapid_key]

  // IDs of divs that display Instance ID token UI or request permission UI.
  const tokenDivId = "token_div";
  const permissionDivId = "permission_div";

  // [START refresh_token]
  // Callback fired if Instance ID token is updated.
  messaging.onTokenRefresh(() => {
    messaging
      .getToken()
      .then((refreshedToken) => {
        // Indicate that the new Instance ID token has not yet been sent to the
        // app server.
        setTokenSentToServer(false);
        // Send Instance ID token to app server.
        sendTokenToServer(refreshedToken);
        // [START_EXCLUDE]
        // Display new Instance ID token and clear UI of all previous messages.
        resetUI();
        // [END_EXCLUDE]
      })
      .catch((err) => {
      });
  });
  // [END refresh_token]

  // [START receive_message]
  // Handle incoming messages. Called when:
  // - a message is received while the app has focus
  // - the user clicks on an app notification created by a service worker
  //   `messaging.setBackgroundMessageHandler` handler.
  //   messaging.setBackgroundMessageHandler(function (payload) {
  //     console.log(
  //       "[firebase-messaging-sw.js] Received background message ",
  //       payload
  //     );
  //     // Customize notification here
  //     const notificationTitle = "Background Message Title";
  //     const notificationOptions = {
  //       body: "Background Message body.",
  //       icon: "/firebase-logo.png",
  //     };

  //     return self.registration.showNotification(
  //       notificationTitle,
  //       notificationOptions
  //     );
  //   });
  messaging.onMessage((payload) => {
    // console.log("Message received. ", payload);
    // $(".show").toast("show");

    // // [START_EXCLUDE]
    // // Update the UI to include the received message.
    appendMessage(payload);
    // [END_EXCLUDE]
  });
  // [END receive_message]

  function resetUI() {
    // [START get_token]
    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging
      .getToken()
      .then((currentToken) => {
        if (currentToken) {
          $.ajax({
            type: 'POST',
            url: 'https://iid.googleapis.com/iid/v1/'+currentToken+'/rel/topics/alerts',
            headers: {"Content-Type": "application/json", "Authorization": "key=AAAAj_GgsoQ:APA91bFQhha5PircJciuI3_XMmkwk01RN3nwuQWNzp9ZPpH-4BQs4oboiK9kDQPnKfJMK2ZLLAtS6gSjJin1-NuXE6fMp5hSGEcBGqccKRVv_YlGCXZezbejx3LQU_PoMm_OVuyotwpo"},
            success: function(result){
              // console.log("resetUI -> result", result);
        
            }
          });
          sendTokenToServer(currentToken);
          (currentToken);
        } else {
          // Show permission request.
          // console.log(
          //   "No Instance ID token available. Request permission to generate one."
          // );
          // Show permission UI.
          setTokenSentToServer(false);
        }
      })
      .catch((err) => {
        // console.log("An error occurred while retrieving token. ", err);
        showToken("Error retrieving Instance ID token. ", err);
        setTokenSentToServer(false);
      });
    // [END get_token]
  }

  
  // Send the Instance ID token your application server, so that it can:
  // - send messages back to this app
  // - subscribe/unsubscribe the token from topics
  function sendTokenToServer(currentToken) {
    if (!isTokenSentToServer()) {
      // console.log("Sending token to server...");
      // TODO(developer): Send the current token to your server.
      setTokenSentToServer(true);
    } else {
      // console.log(
      //   "Token already sent to server so won't send it again " +
      //     "unless it changes"
      // );
    }
  }

  function isTokenSentToServer() {
    return window.localStorage.getItem("sentToServer") === "1";
  }

  function setTokenSentToServer(sent) {
    window.localStorage.setItem("sentToServer", sent ? "1" : "0");
  }

  function showHideDiv(divId, show) {
    const div = document.querySelector("#" + divId);
    if (show) {
      div.style = "display: visible";
    } else {
      div.style = "display: none";
    }
  }

  function requestPermission() {
    // console.log("Requesting permission...");
    // [START request_permission]
    Notification.requestPermission().then((permission) => {
      if (permission === "granted") {
        // console.log("Notification permission granted.");
        // TODO(developer): Retrieve an Instance ID token for use with FCM.
        // [START_EXCLUDE]
        // In many cases once an app has been granted notification permission,
        // it should update its UI reflecting this.
        resetUI();
        // [END_EXCLUDE]
      } else {
        // console.log("Unable to get permission to notify.");
      }
    });
    // [END request_permission]
  }

  function deleteToken() {
    // Delete Instance ID token.
    // [START delete_token]
    messaging
      .getToken()
      .then((currentToken) => {
        messaging
          .deleteToken(currentToken)
          .then(() => {
            // console.log("Token deleted.");
            setTokenSentToServer(false);
            // [START_EXCLUDE]
            // Once token is deleted update UI.
            resetUI();
            // [END_EXCLUDE]
          })
          .catch((err) => {
            // console.log("Unable to delete token. ", err);
          });
        // [END delete_token]
      })
      .catch((err) => {
        // console.log("Error retrieving Instance ID token. ", err);
      });
  }

  // Add a message to the messages element.
  function appendMessage(payload) {
    var notify = document.getElementById("notifyBadge");
    var notifyBadge = notify.innerHTML ? parseInt(notify.innerHTML) : 0;
    notify.innerHTML = parseInt(notifyBadge) + 1;
  
  }

  // Clear the messages element of all children


  resetUI();
});
