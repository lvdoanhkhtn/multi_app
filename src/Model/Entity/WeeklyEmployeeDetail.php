<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WeeklyEmployeeDetail Entity
 *
 * @property int $id
 * @property int $employee_id
 * @property int $symbol_id
 * @property int $weekly_employee_id
 * @property string $note
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Employee $employee
 * @property \App\Model\Entity\Symbol $symbol
 * @property \App\Model\Entity\WeeklyEmployee $weekly_employee
 */
class WeeklyEmployeeDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'employee_id' => true,
        'symbol_id' => true,
        'weekly_employee_id' => true,
        'note' => true,
        'created' => true,
        'modified' => true,
        'employee' => true,
        'symbol' => true,
        'weekly_employee' => true
    ];
}
