<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Customer Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Address[] $addresses
 * @property \App\Model\Entity\BankAccount[] $bank_accounts
 * @property \App\Model\Entity\Enterprise[] $enterprises
 * @property \App\Model\Entity\Payment[] $payments
 */
class Customer extends CommonEntity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];

    public function getCreated(){
        return $this->convertDateTimeToString($this->created);
    }


    public function getAddress(){
        $address = "";
        if(!empty($this->has_one_master_addres)){
            $masterAddress = $this->has_one_master_addres;
            $address =  sprintf("%s,%s,%s,%s",
                $masterAddress->address,
                !empty($masterAddress->ward->name) ? $masterAddress->ward->name : '',
                !empty($masterAddress->district->name) ? $masterAddress->district->name : '',
                    $masterAddress->city->name
            );
        }
        return $address;
    }
}
