<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BallotOther Entity
 *
 * @property int $id
 * @property int $user_id
 * @property float $money
 * @property string $note
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenDate $modified
 * @property int $category_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Category $category
 */
class BallotOther extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'money' => true,
        'note' => true,
        'created' => true,
        'modified' => true,
        'category_id' => true,
        'user' => true,
        'category' => true
    ];
}
