<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PickupTrip Entity
 *
 * @property int $id
 * @property int $address_id
 * @property int $employee_id
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Address $address
 * @property \App\Model\Entity\Employee $employee
 * @property \App\Model\Entity\PickupTripDetail[] $pickup_trip_details
 */
class PickupTrip extends Trip
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        '*' => true
    ];
    
}
