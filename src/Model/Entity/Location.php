<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Location Entity
 *
 * @property int $id
 * @property string $name
 * @property int $active
 *
 * @property \App\Model\Entity\Area[] $areas
 * @property \App\Model\Entity\ShippingFee[] $shipping_fees
 * @property \App\Model\Entity\ShopFee[] $shop_fees
 */
class Location extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'active' => true,
        'areas' => true,
        'shipping_fees' => true,
        'shop_fees' => true
    ];
}
