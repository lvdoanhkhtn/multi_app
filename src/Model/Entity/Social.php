<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Social Entity
 *
 * @property int $id
 * @property string $name
 * @property string $link
 * @property int $partner_id
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Partner $partner
 */
class Social extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'link' => true,
        'partner_id' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'partner' => true
    ];
}
