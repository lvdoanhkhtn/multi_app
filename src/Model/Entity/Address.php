<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Address Entity
 *
 * @property int $id
 * @property string $agency
 * @property string $address
 * @property string $str_phones
 * @property int $city_id
 * @property int $district_id
 * @property int $ward_id
 * @property int $is_master_address
 * @property string $history
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $customer_id
 *
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\District $district
 * @property \App\Model\Entity\Ward $ward
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Order[] $orders
 */
class Address extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];

    public function getIdForAddOrder(){
        return sprintf("%s,%s,%s", $this->id, $this->city_id, $this->district_id);
    }

    public function getFullAddress(){
        return sprintf("%s, %s, %s, %s, %s, %s", h($this->agency), h($this->str_phone), h($this->address), !empty($this->ward->name) ? h($this->ward->name) : '', h($this->district->name), h($this->city->name));
    }

    public function getAddress(){
        return sprintf("%s, %s, %s, %s",h($this->address), !empty($this->ward->name) ? h($this->ward->name) : '', h($this->district->name), h($this->city->name));
    }

    public function getAddressForCustomer(){
        return sprintf("%s, %s",h($this->address), !empty($this->ward->name) ? h($this->ward->name) : '');
    }

    public function getFullAddressForCustomer(){
        return sprintf("%s, %s", h($this->district->name), h($this->city->name));
    }

}
