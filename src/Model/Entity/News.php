<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * News Entity
 *
 * @property int $id
 * @property string $title
 * @property string $title_no_unicode
 * @property string $description
 * @property string $content
 * @property int $user_id
 * @property int $category_id
 * @property int $active
 * @property int $views
 * @property bool $is_hot
 * @property int $creater
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Category[] $categories
 * @property \App\Model\Entity\Tag[] $tags
 */
class News extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'title_no_unicode' => true,
        'description' => true,
        'content' => true,
        'user_id' => true,
        'category_id' => true,
        'active' => true,
        'views' => true,
        'is_hot' => true,
        'creater' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'categories' => true,
        'tags' => true
    ];
}
