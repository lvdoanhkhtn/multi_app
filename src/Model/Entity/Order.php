<?php
namespace App\Model\Entity;

use App\Libs\ValueUtil;
use App\Libs\ConfigUtil;

/**
 * Order Entity
 *
 * @property int $id
 * @property string $orderId
 * @property int $address_id
 * @property string $receiver_name
 * @property string $arr_receiver_phone
 * @property int $service_pack_id
 * @property \Cake\I18n\FrozenTime $finish_date
 * @property int $order_status
 * @property int $owe_status
 * @property int $pay_status
 * @property int $charg_status
 * @property int $collect_status
 * @property int $city_id
 * @property int $district_id
 * @property int $ward_id
 * @property string $shopId
 * @property string $content
 * @property string $note
 * @property string $note_admin
 * @property float $money_collection
 * @property int $payment
 * @property float $sub_fee
 * @property float $discount
 * @property float $postage
 * @property float $total
 * @property float $weight
 * @property int $length
 * @property int $height
 * @property int $width
 * @property int $delivery_method_id
 * @property int $ware_house_id
 * @property int $user_id
 * @property int $active
 * @property int $check_cancel_order
 * @property int $awaiting_payment
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $finished
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $creater
 * @property int $deleted
 * @property string $history
 * @property string $order_history
 * @property int $type
 * @property bool $is_rotatory
 * @property string $api_order_id
 *
 * @property \App\Model\Entity\Address $address
 * @property \App\Model\Entity\ServicePack $service_pack
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\District $district
 * @property \App\Model\Entity\Ward $ward
 * @property \App\Model\Entity\DeliveryMethod $delivery_method
 * @property \App\Model\Entity\WareHouse $ware_house
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\ApiOrder $api_order
 * @property \App\Model\Entity\Note[] $notes
 * @property \App\Model\Entity\PaymentDetail[] $payment_details
 * @property \App\Model\Entity\RotatoryDetail[] $rotatory_details
 * @property \App\Model\Entity\TransactionTripDetail[] $transaction_trip_details
 * @property \App\Model\Entity\TripDetail[] $trip_details
 */
use Cake\ORM\TableRegistry;

class Order extends CommonEntity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];

    public function getId(){
        return sprintf("%s </br> Mã ĐH : %s </br>", $this->orderId, $this->shopId);
    }

    public function getIdNoText(){
        return sprintf("%s/ %s", $this->orderId, $this->shopId);
    }
    
    public function getCompleted(){
        return !empty($this->finished) ? $this->convertDateTimeToString($this->finished) : '';
    }

    public function getStoredTime(){
        return sprintf("%s: %s <br> %s: %s", __('first_stored_time'), !empty($this->first_stored_time) ? $this->convertDateTimeToString($this->first_stored_time) : "",
            __('stored_time'), !empty($this->stored_time) ? $this->convertDateTimeToString($this->stored_time) : "") ;

    }

    public function getSimpleStoredTime(){
        return !empty($this->first_stored_time) ? $this->convertDateTimeToString($this->first_stored_time) : "";
    }

    public function getSenderAddress(){
        return sprintf("%s, %s, %s, %s",
            $this->sender_address->address,
            !empty($this->sender_address->ward->name) ? $this->sender_address->ward->name : '',
            $this->sender_address->district->name,
            $this->sender_address->city->name
        );
    }

    public function getFullSenderAddress($tab = "<br>"){
        return sprintf("%s  %s %s<b>%s</b> %s %s", $this->user->fullname,
            !empty($this->sender_address->agency) ? '<br> <b>'.$this->sender_address->agency.'</b>' : '',
            $tab,
            $this->user->phone,
            $tab,
            $this->getSenderAddress()
        );
    }

    public function getSenderAddressForPrint(){
        return sprintf("%s, %s, %s", $this->user->fullname,
            !empty($this->sender_address->agency) ? '<br> <b>'.$this->sender_address->agency.'</b>' : '',
            $this->user->phone
        );
    }

    public function getReceiverAddress(){
        return sprintf("%s, %s,%s,%s",
            $this->address,
            !empty($this->ward->name) ? $this->ward->name : '',
//            $this->delivery_district->name,
//            $this->delivery_city->name
            $this->district->name,
            $this->city->name
        );
    }

    public function getNameAndPhoneReceiver(){
        return sprintf("%s, %s",
            $this->receiver_name,
            $this->arr_receiver_phone
        );
    }

    public function getFullReceiverAddress(){
        return sprintf("%s, %s",
            $this->getNameAndPhoneReceiver(),
            $this->getReceiverAddress()
        );
    }

    public function cost(){
        return number_format($this->postage + $this->sub_fee + $this->discount);
    }

    public function getCost(){
        return sprintf("Cước phí : <b>%s</b> <br> Thu người nhận : <b>%s</b>", $this->cost(), $this->total);
    }

    public function getPayment($charg_status, $collect_status, $pay_status, $owe_status){
        return sprintf("<b>Thu phí</b> : %s <br> <b>Thu tiền</b> : %s <br>
                                                <b>Phí</b> : %s <br> <b>Công nợ</b> : %s
                                        ", $charg_status[$this->charg_status], $collect_status[$this->collect_status],
            $pay_status[$this->pay_status], $owe_status[$this->owe_status]);
    }

    public function getServicePack(){
        return $this->service_pack_id;
    }

    public function getStatus($isName = true){
        $orderStatus = ValueUtil::get('order.order_statuses_eng');
        $orderConstant = ValueUtil::get('order.order_constant');
        $orderStatusId = $this->order_status;
        switch ($orderStatusId){
            case $orderConstant['delivery_completed']:
                if($this->pay_status == $orderConstant['success_status'] && $this->owe_status == $orderConstant['success_status']) {
                    $orderStatusId = $orderConstant['order_paid'];
                }
                break;

            case $orderConstant['return_completed']:
                if($this->pay_status == $orderConstant['success_status'] && $this->owe_status == $orderConstant['success_status']) {
                    $orderStatusId = $orderConstant['order_charged'];
                }
                break;
        }
        $orderStatusId =  $orderStatusId * 1;
        return ($isName) ?  $orderStatus[$orderStatusId] : $orderStatusId;
    }

    public function getChargStatus(){
        return ValueUtil::get('order.charg_status')[$this->charg_status];
    }

    public function getCollectStatus(){
        return ValueUtil::get('order.collect_status')[$this->collect_status];
    }

    public function getPayStatus(){
        return ValueUtil::get('order.pay_status')[$this->pay_status];
    }

    public function getOweStatus(){
        return ValueUtil::get('order.owe_status')[$this->owe_status];
    }





    public function getTotalFee(){
        return $this->postage + $this->sub_fee - $this->discount + $this->collection_fee + $this->protect_fee;
    }

    public function getTotalAmount(){
        return ($this->total - $this->getTotalFee());
    }

    public function getCustomReceiverAddress()
    {
        return sprintf(
            "%s,%s,%s",
            !empty($this->ward->name) ? $this->ward->name : '',
            $this->district->name,
            $this->city->name
        );
    }
}
