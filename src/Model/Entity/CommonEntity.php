<?php
namespace App\Model\Entity;

use App\Libs\ConfigUtil;
use App\Libs\ValueUtil;
use Cake\ORM\Entity;

class CommonEntity extends Entity
{
    public function getCreated(){
        return $this->convertDateTimeToString($this->created);
    }

    public function getModified(){
        return $this->convertDateTimeToString($this->modified);
    }

    public function convertDateTimeToString($dateTime){
        return date_format($dateTime, ConfigUtil::get('date_time'));
    }

    public function getStatus(){
        return ValueUtil::get('order.delivery_status')[$this->status];
    }
}
