<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BankAccount Entity
 *
 * @property int $id
 * @property int $bank_id
 * @property string $account_number
 * @property string $account_holder
 * @property string $branch_name
 * @property int $is_master
 * @property string $history
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $customer_id
 *
 * @property \App\Model\Entity\Bank $bank
 * @property \App\Model\Entity\Customer $customer
 */
class BankAccount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'bank_id' => true,
        'account_number' => true,
        'account_holder' => true,
        'branch_name' => true,
        'is_master' => true,
        'history' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'customer_id' => true,
        'bank' => true,
        'customer' => true
    ];
}
