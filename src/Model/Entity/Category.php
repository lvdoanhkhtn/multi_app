<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Category Entity
 *
 * @property int $id
 * @property string $name
 * @property string $name_no_unicode
 * @property string $meta_keywords
 * @property string $description
 * @property string $short_link
 * @property int $parent_id
 * @property int $lft
 * @property int $rght
 * @property int $position
 * @property int $show_sitemap
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\ParentCategory $parent_category
 * @property \App\Model\Entity\BallotOther[] $ballot_others
 * @property \App\Model\Entity\ChildCategory[] $child_categories
 * @property \App\Model\Entity\News[] $news
 */
class Category extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'name_no_unicode' => true,
        'meta_keywords' => true,
        'description' => true,
        'short_link' => true,
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'position' => true,
        'show_sitemap' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'parent_category' => true,
        'ballot_others' => true,
        'child_categories' => true,
        'news' => true
    ];
}
