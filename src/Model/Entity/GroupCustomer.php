<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GroupCustomer Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $eng_name
 * @property string|null $kh_name
 * @property int $active
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class GroupCustomer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'eng_name' => true,
        'kh_name' => true,
        'active' => true,
        'created' => true,
        'modified' => true
    ];
}
