<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WareHouse Entity
 *
 * @property int $id
 * @property string $name
 * @property int $show
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Employee[] $employees
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\User[] $users
 */
class WareHouse extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'show' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'employees' => true,
        'orders' => true,
        'users' => true
    ];
}
