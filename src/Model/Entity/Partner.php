<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Partner Entity
 *
 * @property int $id
 * @property string $name
 * @property string $name_no_unicode
 * @property string $logo
 * @property string $website
 * @property string $description
 * @property string $content
 * @property string $comments
 * @property string $social_icons
 * @property int $position
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Social[] $socials
 */
class Partner extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'name_no_unicode' => true,
        'logo' => true,
        'website' => true,
        'description' => true,
        'content' => true,
        'comments' => true,
        'social_icons' => true,
        'position' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'socials' => true
    ];
}
