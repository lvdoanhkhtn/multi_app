<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Rotatory Entity
 *
 * @property int $id
 * @property int $employee_id
 * @property int $start_ware_house_id
 * @property int $distination_ware_house_id
 * @property int $rotatory_partner_id
 * @property string $note
 * @property int $status
 * @property int $active
 * @property \Cake\I18n\FrozenTime $finished
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Employee $employee
 * @property \App\Model\Entity\StartWareHouse $start_ware_house
 * @property \App\Model\Entity\DistinationWareHouse $distination_ware_house
 * @property \App\Model\Entity\RotatoryPartner $rotatory_partner
 * @property \App\Model\Entity\RotatoryDetail[] $rotatory_details
 */
class Rotatory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'employee_id' => true,
        'start_ware_house_id' => true,
        'distination_ware_house_id' => true,
        'rotatory_partner_id' => true,
        'note' => true,
        'status' => true,
        'active' => true,
        'finished' => true,
        'created' => true,
        'modified' => true,
        'employee' => true,
        'start_ware_house' => true,
        'distination_ware_house' => true,
        'rotatory_partner' => true,
        'rotatory_details' => true
    ];
}
