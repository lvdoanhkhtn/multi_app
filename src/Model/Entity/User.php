<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property string $password
 * @property string $email
 * @property string $phone
 * @property int $role_id
 * @property int $notification
 * @property int $cost
 * @property int $print
 * @property int $payment_status
 * @property string $note
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $history
 * @property int $creater
 * @property string $avartar
 * @property string $fullname
 * @property \Cake\I18n\FrozenTime $last_activity
 * @property int $ware_house_id
 * @property string $token
 * @property \Cake\I18n\FrozenTime $created_time
 *
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\WareHouse $ware_house
 * @property \App\Model\Entity\AccessToken[] $access_tokens
 * @property \App\Model\Entity\AuthCode[] $auth_codes
 * @property \App\Model\Entity\BallotOther[] $ballot_others
 * @property \App\Model\Entity\Campaign[] $campaigns
 * @property \App\Model\Entity\Client[] $clients
 * @property \App\Model\Entity\Customer[] $customers
 * @property \App\Model\Entity\Email[] $emails
 * @property \App\Model\Entity\Employee[] $employees
 * @property \App\Model\Entity\News[] $news
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Payment[] $payments
 * @property \App\Model\Entity\RefreshToken[] $refresh_tokens
 * @property \App\Model\Entity\ShopFee[] $shop_fees
 * @property \App\Model\Entity\TransactionTripDetail[] $transaction_trip_details
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */

    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }

    protected $_accessible = [
        '*' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];
}
