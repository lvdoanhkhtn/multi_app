<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TransactionTripDetail Entity
 *
 * @property int $id
 * @property int $order_id
 * @property int $trip_id
 * @property int $trip_detail_id
 * @property int $user_id
 * @property int $payment_status
 * @property string $note
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\Trip $trip
 * @property \App\Model\Entity\TripDetail $trip_detail
 * @property \App\Model\Entity\User $user
 */
class TransactionTripDetail extends CommonEntity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        '*' => true
    ];
}
