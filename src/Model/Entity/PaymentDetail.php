<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PaymentDetail Entity
 *
 * @property int $id
 * @property int $payment_id
 * @property int $order_id
 * @property float $total
 * @property float $cost
 * @property int $is_update_status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Payment $payment
 * @property \App\Model\Entity\Order $order
 */
class PaymentDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'payment_id' => true,
        'order_id' => true,
        'total' => true,
        'cost' => true,
        'is_update_status' => true,
        'created' => true,
        'modified' => true,
        'payment' => true,
        'order' => true
    ];
}
