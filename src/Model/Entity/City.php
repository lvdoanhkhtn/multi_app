<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * City Entity
 *
 * @property int $id
 * @property int $id_api
 * @property string $name
 * @property int $show
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Address[] $addresses
 * @property \App\Model\Entity\Area[] $areas
 * @property \App\Model\Entity\Cost[] $costs
 * @property \App\Model\Entity\District[] $districts
 * @property \App\Model\Entity\Employee[] $employees
 * @property \App\Model\Entity\GroupLocation[] $group_locations
 * @property \App\Model\Entity\Order[] $orders
 */
class City extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
