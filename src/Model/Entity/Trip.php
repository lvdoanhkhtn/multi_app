<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PickupTrip Entity
 *
 * @property int $id
 * @property int $address_id
 * @property int $employee_id
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Address $address
 * @property \App\Model\Entity\Employee $employee
 * @property \App\Model\Entity\PickupTripDetail[] $pickup_trip_details
 */
class Trip extends CommonEntity
{
    public function getTotalComplete(){
        $completeOrders = !empty($this->complete_orders[0]->total_complete_orders) ? $this->complete_orders[0]->total_complete_orders : 0;
        $totalOrders = !empty($this->trip_details[0]->total_orders) ? $this->trip_details[0]->total_orders : 0;
        return sprintf("%s/%s", $completeOrders, $totalOrders);
    }

    public function countTotalComplete(){
        return !empty($this->trip_details[0]->total_orders) ? $this->trip_details[0]->total_orders : 0;
    }
    
    
}
