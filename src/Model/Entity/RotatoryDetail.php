<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RotatoryDetail Entity
 *
 * @property int $id
 * @property int $order_id
 * @property int $rotatory_id
 * @property int $rotatory_status_id
 * @property int $is_update_status
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\Rotatory $rotatory
 * @property \App\Model\Entity\RotatoryStatus $rotatory_status
 */
class RotatoryDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'order_id' => true,
        'rotatory_id' => true,
        'rotatory_status_id' => true,
        'is_update_status' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'order' => true,
        'rotatory' => true,
        'rotatory_status' => true
    ];
}
