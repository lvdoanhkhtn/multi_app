<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ShopFee Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $location_id
 * @property int $service_pack_id
 * @property float $fee
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Location $location
 * @property \App\Model\Entity\ServicePack $service_pack
 */
class ShopFee extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'location_id' => true,
        'service_pack_id' => true,
        'fee' => true,
        'user' => true,
        'location' => true,
        'service_pack' => true
    ];
}
