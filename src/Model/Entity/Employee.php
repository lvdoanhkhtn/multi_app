<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Employee Entity
 *
 * @property int $id
 * @property string $employee_id
 * @property int $is_api
 * @property int $user_id
 * @property int $gender
 * @property \Cake\I18n\FrozenDate $birthday
 * @property string $address
 * @property int $ward_id
 * @property int $district_id
 * @property int $city_id
 * @property int $ware_house_id
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Employee[] $employees
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Ward $ward
 * @property \App\Model\Entity\District $district
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\WareHouse $ware_house
 * @property \App\Model\Entity\Rotatory[] $rotatories
 * @property \App\Model\Entity\Trip[] $trips
 * @property \App\Model\Entity\WeeklyEmployeeDetail[] $weekly_employee_details
 */
class Employee extends CommonEntity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    public function getCreated(){
        return $this->convertDateTimeToString($this->created);
    }


    public function getAddress(){
        return sprintf("%s, %s,%s,%s",
            $this->address,
            !empty($this->ward->name) ? $this->ward->name : '',
            !empty($this->district->name) ? $this->district->name : '',
            $this->city->name
        );
    }

}
