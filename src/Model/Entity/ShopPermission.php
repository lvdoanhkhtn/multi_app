<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Area Entity
 *
 * @property int $id
 * @property int $city_id
 * @property int $district_id
 * @property int $ward_id
 * @property int $location_id
 *
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\District $district
 * @property \App\Model\Entity\Ward $ward
 * @property \App\Model\Entity\Location $location
 */
class ShopPermission extends CommonEntity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        '*' => true
    ];
}
