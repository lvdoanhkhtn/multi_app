<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReturnTripDetail Entity
 *
 * @property int $id
 * @property int $return_trip_id
 * @property int $order_id
 * @property int $order_status
 * @property int $is_update_status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\ReturnTrip $return_trip
 * @property \App\Model\Entity\Order $order
 */
class ReturnTripDetail extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'return_trip_id' => true,
        'order_id' => true,
        'order_status' => true,
        'is_update_status' => true,
        'created' => true,
        'modified' => true,
        'return_trip' => true,
        'order' => true
    ];
}
