<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NewsTag Entity
 *
 * @property int $id
 * @property int $tag_id
 * @property int $news_id
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Tag $tag
 * @property \App\Model\Entity\News $news
 */
class NewsTag extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tag_id' => true,
        'news_id' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'tag' => true,
        'news' => true
    ];
}
