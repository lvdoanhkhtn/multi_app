<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RotatoryStatus Model
 *
 * @property \App\Model\Table\RotatoryDetailsTable|\Cake\ORM\Association\HasMany $RotatoryDetails
 *
 * @method \App\Model\Entity\RotatoryStatus get($primaryKey, $options = [])
 * @method \App\Model\Entity\RotatoryStatus newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RotatoryStatus[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RotatoryStatus|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RotatoryStatus|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RotatoryStatus patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RotatoryStatus[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RotatoryStatus findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RotatoryStatusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rotatory_status');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('RotatoryDetails', [
            'foreignKey' => 'rotatory_status_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmpty('name');

        $validator
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }
}
