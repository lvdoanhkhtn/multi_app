<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Districts Model
 *
 * @property \App\Model\Table\CitiesTable|\Cake\ORM\Association\BelongsTo $Cities
 * @property \App\Model\Table\AddressesTable|\Cake\ORM\Association\HasMany $Addresses
 * @property \App\Model\Table\AreasTable|\Cake\ORM\Association\HasMany $Area
 * @property \App\Model\Table\EmployeesTable|\Cake\ORM\Association\HasMany $Employees
 * @property \App\Model\Table\GroupLocationsTable|\Cake\ORM\Association\HasMany $GroupLocations
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\WardsTable|\Cake\ORM\Association\HasMany $Wards
 *
 * @method \App\Model\Entity\District get($primaryKey, $options = [])
 * @method \App\Model\Entity\District newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\District[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\District|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\District|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\District patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\District[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\District findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DistrictsTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('districts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id'
        ]);
        $this->hasMany('Addresses', [
            'foreignKey' => 'district_id'
        ]);
        $this->hasMany('Area', [
            'foreignKey' => 'district_id'
        ]);
        $this->hasMany('Employees', [
            'foreignKey' => 'district_id'
        ]);
        $this->hasMany('GroupLocations', [
            'foreignKey' => 'district_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'district_id'
        ]);
        $this->hasMany('Wards', [
            'foreignKey' => 'district_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');


        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
        return $rules;
    }

    public function getDistrictByCityId($cityId)
    {
        return $this->find('list', [
            'conditions' => ['city_id' => $cityId]
        ]);
    }

    public function getDefaultDistrictByCityId($cityId)
    {
        $districtId = "";
        if ($cityId == PHNOMPHENH){
            $district = $this->get(DISTRICT_PHNOMPHENH_GROUP2);
            $districtId = $district->id;
        }else{
            $district = $this->find('all', array(
                'conditions' => array(
                    'city_id' => $cityId
                ),
                'fields' => 'id'
            ))->first();
            $districtId = $district['id'];
        }
        return $districtId;
    }

    public function getDefaultNameDistrictByCityId($cityId)
    {
        $districtName = "";
        if ($cityId == PHNOMPHENH){
            $district = $this->get(DISTRICT_PHNOMPHENH_GROUP2);
            $districtName = $district->name;
        }else{
            $district = $this->find('all', array(
                'conditions' => array(
                    'city_id' => $cityId
                ),
                'fields' => 'name'
            ))->first();
            $districtName = $district->name;

        }
        return $districtName;
    }

    public function getDistrictIdByName($name, $cityId)
    {
        $query = $this->find();
        return $query
            ->select([
                'id',
                'name' => 'MATCH (name) AGAINST ("' . $name . '")',
            ])->where(['city_id' => $cityId])
            ->enableHydration(false)->toArray();
    }
}
