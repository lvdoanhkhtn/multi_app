<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use App\Libs\ConfigUtil;


class EmployeesTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('employees');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Employees', [
            'foreignKey' => 'employee_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Wards', [
            'foreignKey' => 'ward_id'
        ]);
        $this->belongsTo('Districts', [
            'foreignKey' => 'district_id'
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id'
        ]);
        $this->belongsTo('WareHouses', [
            'foreignKey' => 'ware_house_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Employees', [
            'foreignKey' => 'employee_id'
        ]);
        $this->hasMany('Rotatories', [
            'foreignKey' => 'employee_id'
        ]);

        $this->hasMany('WeeklyEmployeeDetails', [
            'foreignKey' => 'employee_id'
        ]);

        $this->hasMany('TransactionTripDetails', [
            'foreignKey' => 'employee_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('DeliveryTrips', [
            'foreignKey' => 'employee_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');


        $validator
            ->notEmpty('gender');

        $validator
            ->date('birthday')
            ->notEmpty('birthday');

        $validator
            ->add('passport', ['length' => $this->checkMaxLength(__('passport'), 25, 'username')]);


        $validator
            ->notEmpty('address', ['message' => ConfigUtil::getMessage('ECL001', [__('address')])])
            ->add('address', ['length' => $this->checkMaxLength(__('address'), 100, 'address')]);

        $validator
            ->notEmpty('city_id', ['message' => ConfigUtil::getMessage('ECL001', [__('city')])]);

        $validator
            ->notEmpty('district_id', ['message' => ConfigUtil::getMessage('ECL001', [__('district')])]);

        $validator
            ->notEmpty('ward_id', ['message' => ConfigUtil::getMessage('ECL001', [__('ward')])]);

        $validator
            ->notEmpty('ware_house_id', ['message' => ConfigUtil::getMessage('ECL001', [__('ware_house')])]);



        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['employee_id'], 'Employees'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['ward_id'], 'Wards'));
        $rules->add($rules->existsIn(['district_id'], 'Districts'));
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
        $rules->add($rules->existsIn(['ware_house_id'], 'WareHouses'));

        return $rules;
    }

    public function getIdByUserId($user_id)
    {
        $options['conditions']['user_id'] = $user_id;
        return $this->find('all', $options)->first()->id;
    }

    public function search($data)
    {
        $options['contain'] = [
            'Users' => ['fields' => ['id', 'username', 'fullname', 'email', 'phone', 'active', 'role_id'], 'Roles' => ['fields' => ['name' => 'eng_name']]],
            'Wards' => ['fields' => ['name']],
            'Districts' => ['fields' => ['name']],
            'Cities' => ['fields' => ['name']],
            'WareHouses' => ['fields' => ['name']]
        ];
        $options['conditions'] = [];

        if (!empty($data['key_word'])) {
            $keyWord = trim($data['key_word']);
            $dataSearchLike = ['OR' => [
                "employee_id LIKE '%" . $keyWord . "%'",
                "Users.fullname LIKE '%" . $keyWord . "%'",
                "Users.phone LIKE '%" . $keyWord . "%'",
                "address LIKE '%" . $keyWord . "%'",
                "Users.email LIKE '%" . $keyWord . "%'",
                "Users.username LIKE '%" . $keyWord . "%'",
            ]];
            array_push($options['conditions'], $dataSearchLike);
        }

        $options = $this->commonSearchDate($options, $data);


        if (isset($data['active']) && (strlen($data['active']) > 0)) {
            $options['conditions']['Users.active'] = $data['active'];
        }
        if (!empty($data['role_id'])) {
            $options['conditions']['Users.role_id'] = $data['role_id'];
        }
        if (!empty($data['ware_house_id'])) {
            $options['conditions']['Employees.ware_house_id'] = $data['ware_house_id'];
        }
        if (!empty($data['IS_SHOP'])) {
            $name = $this->getLanguage().'_name';
            $options['contain']['Users']['ShopPermissions'] = ['fields' => ['user_id'], 'ShopPermissionStatuses' => ['fields' => ['name' => $name]]]; 
            $options['conditions']['Users.parent_id'] = $this->getUserId();
            $options['conditions']['Users.role_id'] = SHOP_EMPLOYEE;
        }
        $options['order'] = ['Users.id' => 'desc'];
        $query = $this->find('all', $options);
        return $query;
    }

    public function afterSave(Event $event)
    {
        $entity = $event->getData('entity');
        $entity->employee_id  = $this->createEmployeeId($entity->id);
        if (!empty($entity)) {
            $this->updateAll(['employee_id' => $entity->employee_id], ['id' => $entity->id]);
        }
    }

    public function loadEmployees($roleId)
    {
        $datas =  $this->find('all', [
            'contain' => 'Users',
            'conditions' => ['Users.role_id' => $roleId, 'Users.active' => COMPLETE_STATUS]
        ])->toArray();
        $employees = [];
        foreach ($datas as $data) {
            $employees[$data->id] = $data->user->fullname;
        }
        return $employees;
    }

    public function loadDeliveryShippers($roleId)
    {
        $options['fields'] = ['id'];
        $options['contain'] = [
            'Users' => [
                'fields' => ['id', 'fullname'],
                'Employees' => [
                    'fields' => ['id', 'user_id', 'is_addmore_trip'],
                    'DeliveryTrips' => [
                        'DeliveryTripDetails' => ['fields' => ['id', 'order_id', 'delivery_trip_id'], 'CustomOrderCollect' => ['fields' => ['id']]]
                    ]
                ]
            ]
        ];
        $options['conditions'] = [
            'Users.role_id' => $roleId,
            'Users.active' => COMPLETE_STATUS
        ];
        $datas =  $this->find('all', $options)->toArray();
        $employees = [];
        foreach ($datas as $data) {
            $user = $data->user;
            $deliveryTrips = !empty($user->employees[0]->delivery_trips) ? $user->employees[0]->delivery_trips : [];
            $flag = false;
            if (count($deliveryTrips) > 0) {
                $deliveryTrips = json_decode(json_encode($deliveryTrips), true);
                $deliveryCompleteOrders = array_column($deliveryTrips, 'trip_details');
                $deliveryTripStatus = array_column($deliveryTrips, 'status');
                $deliveryCompleteOrders = array_filter(array_map('array_filter', $deliveryCompleteOrders));
                $countStatus = array_count_values($deliveryTripStatus);
                if (count($deliveryCompleteOrders) > 0 || !empty($countStatus[0])) {
                    if (!empty($user->employees[0]->is_addmore_trip)) {
                        $flag = false;
                    } else {
                        $flag = true;
                    }
                }
            }
            if (!$flag) {
                $employees[$data->id] = $user->fullname;
            }
        }
        return $employees;
    }
}
