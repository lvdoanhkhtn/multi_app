<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WeeklyEmployeeDetails Model
 *
 * @property \App\Model\Table\EmployeesTable|\Cake\ORM\Association\BelongsTo $Employees
 * @property \App\Model\Table\SymbolsTable|\Cake\ORM\Association\BelongsTo $Symbols
 * @property \App\Model\Table\WeeklyEmployeesTable|\Cake\ORM\Association\BelongsTo $WeeklyEmployees
 *
 * @method \App\Model\Entity\WeeklyEmployeeDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\WeeklyEmployeeDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WeeklyEmployeeDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WeeklyEmployeeDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WeeklyEmployeeDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WeeklyEmployeeDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WeeklyEmployeeDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WeeklyEmployeeDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WeeklyEmployeeDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('weekly_employee_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Employees', [
            'foreignKey' => 'employee_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Symbols', [
            'foreignKey' => 'symbol_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('WeeklyEmployees', [
            'foreignKey' => 'weekly_employee_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('note')
            ->maxLength('note', 1000)
            ->allowEmpty('note');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['employee_id'], 'Employees'));
        $rules->add($rules->existsIn(['symbol_id'], 'Symbols'));
        $rules->add($rules->existsIn(['weekly_employee_id'], 'WeeklyEmployees'));

        return $rules;
    }
}
