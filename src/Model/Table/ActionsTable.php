<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Actions Model
 *
 * @property \App\Model\Table\ControllersTable&\Cake\ORM\Association\BelongsTo $Methods
 * @property \App\Model\Table\PermissionsTable&\Cake\ORM\Association\HasMany $Permissions
 *
 * @method \App\Model\Entity\Action get($primaryKey, $options = [])
 * @method \App\Model\Entity\Action newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Action[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Action|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Action saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Action patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Action[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Action findOrCreate($search, callable $callback = null, $options = [])
 */
class ActionsTable extends CommonTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('actions');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Methods', [
            'foreignKey' => 'method_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Permissions', [
            'foreignKey' => 'action_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 25)
            ->allowEmptyString('name');

        $validator
            ->scalar('alias')
            ->maxLength('alias', 100)
            ->allowEmptyString('alias');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['method_id'], 'Methods'));

        return $rules;
    }

    public function getAllActions($ids){
        $ids = explode(",", $ids);
        $options['conditions']['Actions.id IN'] = $ids;
        $options['contain'] = ['Methods'];
        $actions = $this->find('all', $options);
        $methodActions = [];
        foreach($actions as $action){
            $strMethodAction = sprintf("%s - %s", $action->method->alias, $action->alias);
            $methodActions[] = $strMethodAction;
        }
        return implode(",", $methodActions);
    }

    public function getActionIdInPermisson($controller, $action){
        $options['contain'] = ['Methods'];
        $options['conditions']['Methods.name'] = $controller;
        $options['conditions']['Actions.name'] = $action;
        $permission = $this->find('all', $options)->first();
        return !empty($permission->id) ? $permission->id : '';
    }

    public function getMethodIdsForMenu($ids){
        $options['fields'] = ['method_id'];
        if($this->getRoleId() !== ADMIN){
            $options['conditions']['Actions.id IN'] = $ids;
        }
        $options['conditions']['Actions.is_view'] = COMPLETE_STATUS;
        $options['group'] = ['method_id'];
        $methodIds = [];
        $methods = $this->find('all', $options);
        if(!empty($methods)){
            $methods = json_decode(json_encode($methods), true);
            $methodIds = array_column($methods, 'method_id');
        }
        return $methodIds;
    }

    public function getIdsWithBasicPermission(){
        $options['fields'] = ['id'];
        $options['conditions']['is_basic'] = COMPLETE_STATUS;
        $actionIds = [];
        $actions = $this->find('all', $options);
        if(!empty($actions)){
            $actions = json_decode(json_encode($actions), true);
            $actionIds = array_column($actions, 'id');
        }
        return implode(",", $actionIds);
    }
}
