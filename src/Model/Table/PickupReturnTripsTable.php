<?php

namespace App\Model\Table;

use Cake\Core\Exception\Exception;
use Cake\Http\Session;
use Cake\ORM\TableRegistry;

/**
 * PickupTrips Model
 *
 * @property \App\Model\Table\AddressesTable&\Cake\ORM\Association\BelongsTo $Addresses
 * @property \App\Model\Table\EmployeesTable&\Cake\ORM\Association\BelongsTo $Employees
 * @property \App\Model\Table\PickupTripDetailsTable&\Cake\ORM\Association\HasMany $PickupTripDetails
 *
 * @method \App\Model\Entity\PickupTrip get($primaryKey, $options = [])
 * @method \App\Model\Entity\PickupTrip newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PickupTrip[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PickupTrip|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PickupTrip saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PickupTrip patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PickupTrip[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PickupTrip findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PickupReturnTripsTable extends TripsTable
{

    public function initialize(array $config)
    {
        $this->belongsTo('Addresses', [
            'foreignKey' => 'address_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('CustomerUsers', [
            'className' => 'Users',
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Employees', [
            'foreignKey' => 'employee_id',
            'joinType' => 'INNER'
        ]);


        parent::initialize($config);
    }

    public function search($data)
    {
        $tableAlias = $this->getRegistryAlias();
        $table = $this->getTable();
        $this->tableId = str_replace("s", "", $table) . '_id';
        $tbDetail = TableRegistry::getTableLocator()->get(str_replace("s", "", $tableAlias) . 'Details');


        //$options = $data;
        //        $options['fields'] = ['fee'];id
        $options['conditions'] = [];
        $options['contain'] = [
            'CustomerUsers' => ['fields' => ['fullname']],
            'Addresses' => [
                'Wards' => ['fields' => ['id', 'name']],
                'Districts' => ['fields' => ['id', 'name']],
                'Cities' => ['fields' => ['id', 'name']],
            ],
            'Employees' => [
                'fields' => ['employee_id'],
                'Users' => ['fields' => ['fullname', 'phone']],
            ],
            'TripDetails' => function ($q) {
                return $q->select(
                    [
                        $this->tableId,
                        'total_orders' => $q->func()->count('*'),
                    ]
                )->group([$this->tableId]);
            },
            'CompleteOrders' => function ($q) {
                return $q->select(
                    [
                        $this->tableId,
                        'total_complete_orders' => $q->func()->count('*'),
                    ]
                )->group([$this->tableId]);
            },


        ];
        if (!empty($data['user_id'])) {
            $options['conditions'][$tableAlias . '.user_id'] = $data['user_id'];
        }
        if (!empty($data['employee_id'])) {
            $options['conditions'][$tableAlias . '.employee_id'] = $data['employee_id'];
        }
        if (!empty($data['orderId'])) {
            //find id with orderId
            $ids = $tbDetail->searchOrderCodeInTrip($data['orderId']);
            if (count($ids) > 0) {
                $options['conditions'][$tableAlias . '.id IN'] = $ids;
            } else {
                $options['conditions'][$tableAlias . '.id'] = null;
            }
        }
        if (!empty($data['delivery_from'])) {
            $options['conditions'][$tableAlias . '.address_id IN'] = $this->Addresses->searchAddressInTrip($data['delivery_from']);
        }


        if (isset($data['status']) && strlen($data['status']) > 0) {
            $options['conditions'][$tableAlias . '.status'] = $data['status'];
        }

        $options['order'] = [$tableAlias . '.id' => 'DESC'];

        $options = $this->commonSearchDate($options, $data, COMPLETE_STATUS);
        if ($this->getRoleId() === SHIPPER) {
            $options['conditions'][$tableAlias . '.employee_id'] = $this->Employees->getIdByUserId($this->getUserId());
        }
        //        pr($options['conditions']);die;

        return $this->find('all', $options);
    }

    public function add($table, $data, $currentOrderStatus, $newOrderStatus, $isEmptyOrder = false)
    {
        $this->getConnection()->begin();

        $tbCustomers = $this->getMainTable('Customers');
        $tbWms = $this->getMainTable('Wms');
        $tbOrders = $this->getMainTable('Orders');
        $this->tableId = str_replace("details", "", $table->getTable()) . 'id';
        $tripEntity = $this->newEntity($data);
        $tripId = $this->getIdByAddressId($data['address_id']);
        if (empty($tripId) && $this->save($tripEntity)) {
            $tripId = $tripEntity->id;
        }

        $datas = [];
        if (empty($isEmptyOrder)) {
            $datas = $table->Orders->getListOrdersByStatus($data['address_id'], $currentOrderStatus);
        }
        $tripOrders = $this->addElementToAllArray($datas, $this->tableId, $tripId);

        # count orders.
        $response['success'] = false;
        $errors = [];
        try {
            $i = 0;
            foreach ($tripOrders as $tripOrder) {
                $order = $table->Orders->get($tripOrder['id']);
                $isMemberOfWms = $tbCustomers->isMemberOfWms($order->user_id);
                if ($isMemberOfWms && !empty($order->shopId)) {
                    $tableName = $table->getName();
                    if ($tableName === 'PickupTripDetails') {
                        $error = $tbWms->confirmPickup($order->shopId);
                        if (!empty($error['error'])) {
                            $errors[] = $error; 
                            $i--;
                            continue;
                        }
                    } else if ($tableName === 'ReturnTripDetails') {
                        $error = $tbWms->confirmReturn($order->shopId);
                        if (!empty($error['error'])) {
                            $errors[] = $error; 
                            $i--;
                            continue;
                        }
                    }
                }
                $tripDetailEntity = $table->newEntity($tripOrder);
                if ($table->save($tripDetailEntity)) {
                    $order->order_status = $newOrderStatus;
                    if ($tbOrders->save($order)) {
                        $response['success'] = true;
                        $this->getConnection()->commit();
                    }
                }
                $i++;
            }
            if ($i === 0) {
                $this->getConnection()->rollback();
            }
        } catch (Exception $e) {
            $this->getConnection()->rollback();
        }
        $response['errors'] = $errors;
        return $response;
    }
}
