<?php

namespace App\Model\Table;

use Cake\Http\Session;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Libs\ConfigUtil;



class AddressesTable extends CommonTable
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->setTable('addresses');
		$this->setDisplayField('id');
		$this->setPrimaryKey('id');

		$this->addBehavior('Timestamp');

		$this->belongsTo('Cities', [
			'foreignKey' => 'city_id'
		]);
		$this->belongsTo('Districts', [
			'foreignKey' => 'district_id'
		]);
		$this->belongsTo('Wards', [
			'foreignKey' => 'ward_id'
		]);
		$this->belongsTo('Customers', [
			'foreignKey' => 'customer_id',
			'joinType' => 'INNER'
		]);
		$this->hasMany('Orders', [
			'foreignKey' => 'address_id'
		]);
	}


	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->integer('id')
			->allowEmpty('id', 'create');

		$validator
			->notEmpty('agency', ['message' => ConfigUtil::getMessage('ECL001', ['Name'])])
			->add('agency', ['length' => $this->checkMaxLength('Name', 50)]);

		$validator
			->notEmpty('address', ['message' => ConfigUtil::getMessage('ECL001', ['Address'])])
			->add('address', ['length' => $this->checkMaxLength('Address', 100)]);

		$validator
			->notEmpty('str_phones', ['message' => ConfigUtil::getMessage('ECL001', [''])])
			->add('str_phones', [
				'length' => $this->checkMaxLength('Phone', 25, 'phone')
				//				,
				//				'validFormat' => [
				//					'rule' => array('custom', '/^[0-9]*$/i'),
				//					'message' => ConfigUtil::getMessage("ECL004", ["Phone"])
				//				]
			]);

		$validator
			->notEmpty('district_id', ['message' => ConfigUtil::getMessage('ECL001', ['District'])]);

		$validator
			->notEmpty('city_id', ['message' => ConfigUtil::getMessage('ECL001', ['City'])]);

		$validator
			->allowEmpty('history');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->existsIn(['city_id'], 'Cities'));
		$rules->add($rules->existsIn(['district_id'], 'Districts'));
		$rules->add($rules->existsIn(['ward_id'], 'Wards'));
		$rules->add($rules->existsIn(['customer_id'], 'Customers'));

		return $rules;
	}


	public function checkIsMasterAddress($customerId)
	{
		$options['conditions']['customer_id'] = $customerId;
		$options['conditions']['is_master_address'] = 1;
		return ($this->find('all', $options)->count() > 0) ? true : false;
	}

	public function createAddress($data, $addressData)
	{
		if (!empty($data['phone'])) {
			$addressData['str_phones'] = $data['phone'];
		}
		if (!empty($addressData['id'])) {
			$addressEntity = $this->get($addressData['id']);
			$addressEntity = $this->patchEntity($addressEntity, $addressData);
		} else {
			$addressEntity = $this->newEntity($addressData);
		}
		if ($this->save($addressEntity)) {
			return true;
		}
		return false;
	}

	public function convertAddressInCustomer($addresses)
	{
		$convertAddresses = [];
		for ($i = 0; $i < count($addresses); $i++) {
			$convertAddresses[$i]['id'] = $addresses[$i]['id'];
			$convertAddresses[$i]['is_master_address'] = $addresses[$i]['is_master_address'];
			$convertAddresses[$i]['agency'] = $addresses[$i]['agency'];
			$convertAddresses[$i]['str_phones'] = $addresses[$i]['str_phones'];
			$convertAddresses[$i]['address'] = $addresses[$i]['address'];
			$convertAddresses[$i]['city_id'] = $addresses[$i]['city']['id'];
			$convertAddresses[$i]['city_name'] = $addresses[$i]['city']['name'];
			$convertAddresses[$i]['district_id'] = $addresses[$i]['district']['id'];
			$convertAddresses[$i]['district_name'] = $addresses[$i]['district']['name'];
			$convertAddresses[$i]['ward_id'] = !empty($addresses[$i]['ward']['id']) ? $addresses[$i]['ward']['id'] : "";
			$convertAddresses[$i]['ward_name'] = !empty($addresses[$i]['ward']['name']) ? $addresses[$i]['ward']['name'] : "";
		}
		return $convertAddresses;
	}

	public function convertAddressInUser($user)
	{
		if (!empty($user['customers']['master_address'])) {
			$masterAddress  = $user['customers']['master_address'];
			$user['is_master_address'] = $masterAddress['is_master_address'];
			$user['address'] = $masterAddress['address'];
			$user['city_id'] = $masterAddress['city']['id'];
			$user['city_name'] = $masterAddress['city']['name'];
			$user['district_id'] = $masterAddress['district']['id'];
			$user['district_name'] = $masterAddress['district']['name'];
			$user['ward_id'] = !empty($masterAddress['ward']['id']) ? $masterAddress['ward']['id'] : null;
			$user['ward_name'] = !empty($masterAddress['ward']['name']) ? $masterAddress['ward']['name'] : null;
			$user['group_customer_id'] = !empty($user['customers']['group_customer']['id']) ?: null;
			unset($user['customers']);
			unset($user['token']);
		}
		return $user;
	}

	public function insertOrUpdate($data, $isEdit)
	{
		return $this->_insertOrUpdate($data, $isEdit);
	}


	public function getPickUp($customerId = null)
	{
		return $this->find('list', [
			'keyField' => 'id',
			'valueField' => function ($q) {
				return $q->getFullAddress();
			},
			'conditions' => ['customer_id' => !empty($customerId) ? $customerId : $this->getCustomerId()]
		])->contain(['Wards', 'Districts', 'Cities'])->toArray();
	}

	public function getAddresses($customerId = null)
	{
		$options['order'] = ['Addresses.id' => 'desc'];
		$options['fields'] = ['id', 'agency', 'address', 'str_phones', 'is_master_address', 'ward_id', 'district_id', 'city_id', 'active', 'created'];
		$fields = ['fields' => ['name']];
		$options['contain'] = ['Customers' => ['fields' => ['id', 'user_id'], 'Users' => ['fields' => ['fullname']]], 'Cities' => $fields, 'Districts' => $fields, 'Wards' => $fields];
		$options['conditions']['customer_id'] = !empty($customerId) ? $customerId : $this->getCustomerId();
		$options['order'] = ['is_master_address' => 'DESC', 'Addresses.id' => 'DESC'];
		$addresses = $this->find('all', $options);
		return $addresses;
	}

	public function getPickUpForAddOrder($customerId = null)
	{
		$tempPickUp = $this->find('all', ['conditions' => ['customer_id' => !empty($customerId) ? $customerId : $this->getCustomerId()]])->contain(['Wards', 'Districts', 'Cities'])->sortBy('id')->toArray();

		$pickUpAddresses = [];
		foreach ($tempPickUp as $pickUp) {
			$key = sprintf("%s,%s,%s", $pickUp->id, $pickUp->city_id, $pickUp->district_id);
			$pickUpAddresses[$key] = $pickUp->getFullAddress();
		}
		return $pickUpAddresses;
	}

	public function getMasterAddressId($customerId = null)
	{
		$options['conditions']['customer_id'] = $customerId;
		$options['fields'] = 'id';
		$address = $this->find('all', $options)->first();
		$id = "";
		if ($address) {
			$id = $address->id;
		}
		return $id;
	}

	public function searchAddressInTrip($districtId)
	{
		$options['contain'] = ['Districts'];
		$options['conditions']['Districts.id'] = $districtId;
		$options['fields'] = ['id'];
		$address = $this->find('all', $options)->enableHydration(false)->toArray();
		if (!empty($address)) {
			return array_column($address, 'id');
		}
		return "";
	}
}
