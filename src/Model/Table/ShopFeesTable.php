<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ShopFees Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\LocationsTable|\Cake\ORM\Association\BelongsTo $Locations
 * @property \App\Model\Table\ServicePacksTable|\Cake\ORM\Association\BelongsTo $ServicePacks
 *
 * @method \App\Model\Entity\ShopFee get($primaryKey, $options = [])
 * @method \App\Model\Entity\ShopFee newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ShopFee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ShopFee|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ShopFee|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ShopFee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ShopFee[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ShopFee findOrCreate($search, callable $callback = null, $options = [])
 */
class ShopFeesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('shop_fees');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Locations', [
            'foreignKey' => 'location_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ServicePacks', [
            'foreignKey' => 'service_pack_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('fee')
            ->allowEmpty('fee');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['location_id'], 'Locations'));
        $rules->add($rules->existsIn(['service_pack_id'], 'ServicePacks'));

        return $rules;
    }

    public function search($data){
        //$options = $data;
//        $options['fields'] = ['fee'];
        $options['conditions'] = [];
        if(!empty($data['user_id'])){
            $options['conditions']['user_id'] = $data['user_id'];
        }
        if(!empty($data['service_pack_id'])){
            $options['conditions']['service_pack_id'] = $data['service_pack_id'];
        }
        if(!empty($data['location_id'])){
            $options['conditions']['location_id'] = $data['location_id'];
        }

        $shopFee = [];
        if(count($options['conditions']) > 0){
            $shopFee = $this->find('all', $options)->first();
        }
        return $shopFee;

    }



}
