<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReturnTripDetails Model
 *
 * @property \App\Model\Table\ReturnTripsTable&\Cake\ORM\Association\BelongsTo $ReturnTrips
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \App\Model\Entity\ReturnTripDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReturnTripDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReturnTripDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReturnTripDetail|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReturnTripDetail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReturnTripDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReturnTripDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReturnTripDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReturnTripDetailsTable extends PickupReturnTripDetailsTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('return_trip_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ReturnTrips', [
            'foreignKey' => 'return_trip_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('order_status')
            ->notEmptyString('order_status');

        $validator
            ->integer('is_update_status')
            ->notEmptyString('is_update_status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['return_trip_id'], 'ReturnTrips'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }

    public function checkOrderAndInsert($orderCode, $tripId, $addressId, $orderStatus)
    {
        $orderConstant = $this->orderConstant;
        $orderCode = trim($orderCode);
        $tripId = trim($tripId);
        $addressId = trim($addressId);
        //check is order_status is stored.
        $order = $this->Orders->searchOrderWithStatus($orderCode, $orderStatus);
        if (empty($order->address_id) || empty($addressId) || $order->address_id != $addressId) {
            return false;
        }
        //check exist in detail
        if (!empty($order->id)) {
            $this->getConnection()->begin();
            try {
                try {
                    $options['conditions']['order_id'] = $order->id;
                    $options['conditions']['return_trip_id'] = $tripId;
                    $tripDetail = $this->find('all', $options)->first();
                    if (!empty($tripDetail)) {
                        $this->errorException();
                    }
                    $data['order_id'] = $order->id;
                    $data['return_trip_id'] = $tripId;
                    $entity = $this->newEntity($data);
                    if ($this->save($entity)) {
                        $newOrder = $this->Orders->get($order->id);
                        $newOrder->order_status = $orderConstant['return_in_progress'];
                        $order->trip_id = $entity->id;
                        if ($this->Orders->save($newOrder)) {
                            $this->getConnection()->commit();
                        } else {
                            $this->errorException();
                        }
                    } else {
                        $this->errorException();
                    }
                } catch (\Exception $e) {
                    $this->getConnection()->rollback();
                }
            } catch (\PDOException $e) {
                $this->getConnection()->rollback();
            }
        }
        return $order;
    }

    public function getIdByOrderId($orderId)
    {
        $options['fields'] = ['id'];
        $options['conditions'] = [
            'order_id' => $orderId,
            'order_status' => 7
        ];
        return $this->find('all', $options)->first();
    }
}
