<?php
namespace App\Model\Table;


use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use Cake\Utility\Hash;

/**
 * PickupTripDetails Model
 *
 * @property \App\Model\Table\PickupTripsTable&\Cake\ORM\Association\BelongsTo $PickupTrips
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \App\Model\Entity\PickupTripDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\PickupTripDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PickupTripDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PickupTripDetail|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PickupTripDetail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PickupTripDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PickupTripDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PickupTripDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PickupTripDetailsTable extends PickupReturnTripDetailsTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pickup_trip_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PickupTrips', [
            'foreignKey' => 'pickup_trip_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('OrderCollected', [
            'className' => 'Orders',
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
            'conditions' => ['OrderCollected.order_status in' => [3, 5],  'OrderCollected.charg_status' => NO_COMPLETE_STATUS, 'pay_status' => COMPLETE_STATUS]
        ]);
        
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('order_status')
            ->notEmptyString('order_status');

        $validator
            ->integer('is_update_status')
            ->notEmptyString('is_update_status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
//        $rules->add($rules->existsIn(['pickup_trip_id'], 'PickupTrips'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }

    public function collectFee($ids){
        $updateOrders = $this->find('all', ['fields' => ['id' => 'order_id'],'conditions' => ['id IN' => $ids]])->enableHydration(false)->enableBufferedResults(false)->toArray();
        $orderIds = array_column($updateOrders, 'id');
        $updateOrders = Hash::insert($updateOrders, '{n}.pay_status', COMPLETE_STATUS);
        $options['conditions'] = ['id IN' => $orderIds,'pay_status' => NO_COMPLETE_STATUS, 'order_status' => 3];
        $orderEntities = $this->Orders->find('all', $options)->toArray();
        if(count($orderEntities)  > 0){
            $orderEntities = $this->Orders->patchEntities($orderEntities, $updateOrders);
        }
        return $this->Orders->saveMany($orderEntities);
    }
    

    public function getPickupTripIdsHaveCollectOrders(){
        $options['contain'] = ['OrderCollected'];
        $options['conditions'] = ['PickupTripDetails.order_status' => 3];
        $options['fields'] = ['pickup_trip_id' => 'DISTINCT PickupTripDetails.pickup_trip_id'];
        $pickupTripIds = [];
        $pickupTrips = $this->find('all', $options)->enableHydration(false)->toArray();
        if(count($pickupTrips) > 0){
            $pickupTripIds = array_column($pickupTrips, 'pickup_trip_id');
        }
        return $pickupTripIds;
    }

    public function collectDetail($pickupTripId)
    {
        $options['contain'] = ['OrderCollected' => function ($q) {
            return $q->select($this->Orders->dataField('OrderCollected'))->contain($this->Orders->dataContain());
        }];
        $options['conditions'] = [
            'pickup_trip_id' => $pickupTripId,
            'PickupTripDetails.order_status' => $this->orderConstant['stored']
        ];
        return $this->find('all', $options);
    }

}
