<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WareHouses Model
 *
 * @property \App\Model\Table\EmployeesTable|\Cake\ORM\Association\HasMany $Employees
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\HasMany $Users
 *
 * @method \App\Model\Entity\WareHouse get($primaryKey, $options = [])
 * @method \App\Model\Entity\WareHouse newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WareHouse[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WareHouse|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WareHouse|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WareHouse patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WareHouse[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WareHouse findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WareHousesTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ware_houses');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Employees', [
            'foreignKey' => 'ware_house_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'ware_house_id'
        ]);

        $this->hasMany('PickUpOrders', [
            'className' => 'Orders',
            'foreignKey' => 'ware_house_id',
            'conditions' => ['PickUpOrders.order_status'  => $this->orderConstant['new']],
            'group' => ['user_id', 'address_id'],

        ]);

        $this->hasMany('Users', [
            'foreignKey' => 'ware_house_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmpty('name');

        return $validator;
    }


    public function getList($lang = null){
        $options['fields'] = [
            'id',
            'name'
        ];
        $options['conditions'] = [
            'active' => 1];
        return $this->find('list', $options)->toArray();
    }
    

}
