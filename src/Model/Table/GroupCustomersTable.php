<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GroupCustomer Model
 *
 * @method \App\Model\Entity\GroupCustomer get($primaryKey, $options = [])
 * @method \App\Model\Entity\GroupCustomer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\GroupCustomer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GroupCustomer|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GroupCustomer saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GroupCustomer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GroupCustomer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\GroupCustomer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class GroupCustomersTable extends CommonTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('group_customers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');


        $this->hasMany('Customers', [
            'foreignKey' => 'group_customer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('eng_name')
            ->maxLength('eng_name', 255)
            ->allowEmptyString('eng_name');

        $validator
            ->scalar('kh_name')
            ->maxLength('kh_name', 255)
            ->allowEmptyString('kh_name');

        $validator
            ->notEmptyString('active');

        return $validator;
    }
}
