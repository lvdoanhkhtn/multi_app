<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WeeklyEmployees Model
 *
 * @property \App\Model\Table\WeeklyEmployeeDetailsTable|\Cake\ORM\Association\HasMany $WeeklyEmployeeDetails
 *
 * @method \App\Model\Entity\WeeklyEmployee get($primaryKey, $options = [])
 * @method \App\Model\Entity\WeeklyEmployee newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WeeklyEmployee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WeeklyEmployee|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WeeklyEmployee|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WeeklyEmployee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WeeklyEmployee[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WeeklyEmployee findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WeeklyEmployeesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('weekly_employees');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('WeeklyEmployeeDetails', [
            'foreignKey' => 'weekly_employee_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
