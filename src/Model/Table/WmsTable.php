<?php

namespace App\Model\Table;

use App\Libs\ValueUtil;
// use Cake\Http\Response;
use Cake\Http\Client;
use Cake\Log\Log;
use Exception;

class WmsTable extends CommonTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */

    protected $endpoint;
    protected $client;
    protected $code;
    protected $token;
    protected $user;
    protected $wareHouse;
    protected $order;


    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->endpoint = ValueUtil::get('wms.endpoint');
        $this->code = ValueUtil::get('wms.code');
        $this->client = new Client();
        $this->token = $this->readSessionFile();
        $this->user = $this->getMainTable('Users');
        $this->order = $this->getMainTable('Orders');
        $this->wareHouse = $this->getMainTable('WareHouses');
    }

    public function getToken()
    {
        $token = "";
        $request = [
            "data" => [
                "Ma_Dang_Nhap" => 'api_cbs',
                "Mat_Khau" => 'api@12345'
            ],
            "Auth" => [
                "Token" => '',
                "Type_ID" => 300,
                "Prototype_ID" => 2,
                "Device_ID" => ''
            ]
        ];
        $response = $this->client->post($this->endpoint, json_encode($request));
        $responseData = $response->getJson();
        if (!empty($responseData['Data']['Token'])) {
            $token = $responseData['Data']['Token'];
        }
        return $token;
    }

    public function processResponse($request)
    {
        $success = false;
        try {
            $responseData = $this->callPostApi($request);
            $messageCode = $responseData['Message']['Message_Code'];
            if (!empty($messageCode) && $messageCode == $this->code['success']) {
                $success = true;
            }
        } catch (Exception $e) {
            Log::debug($e);
        }
        return $success;
    }

    public function processResponseWithErrors($request)
    {
        $error = [];
        try {
            $responseData = $this->callPostApi($request);
            $messageCode = $responseData['Message']['Message_Code'];
            if (!empty($messageCode) && $messageCode != $this->code['success']) {
                $error = [
                    'DO_No' => $request['Data']['DO_No'],
                    'error' =>  $responseData['Message']['Message_Desc']
                ];
            }
        } catch (Exception $e) {
            Log::debug($e);
        }
        return $error;
    }

    public function callPost($request){
        $response = $this->client->post($this->endpoint, json_encode($request));
        return $response->getJson();
    }

    public function callPostApi($request){
        $responseData = $this->callPost($request);
        if($this->checkValidToken($responseData)){
            $request['Auth']['Token'] = $this->readSessionFile();
            $responseData = $this->callPost($request);
        }
        Log::debug($request);
        Log::debug($responseData);
        return $responseData;
    }

    public function getResponseData($request)
    {
        $results = [];
        try {
            $responseData = $this->callPostApi($request);
            if (!empty($responseData['Data'])) {
                $results = $responseData['Data'];
            }
            Log::debug($request);
            Log::debug($responseData);
        } catch (Exception $e) {
            Log::debug($e);
        }
        return $results;
    }

    public function getNewResponseData($request)
    {
        $results = [];
        try {
            $responseData = $this->callPostApi($request);
            $messageCode = $responseData['Message']['Message_Code'];
            if (!empty($responseData['Data'])) {
                $results['shopId'] = $responseData['Data'];
            } else if (!empty($messageCode) && $messageCode != $this->code['success']) {
                $results = [
                    'content' => sprintf("%s-%s-%s", $request['Data']['Customer_Name'], $request['Data']['Customer_Phone'], $responseData['Message']['Message_Desc'])
                ];
            };
            Log::debug($request);
            Log::debug($responseData);
        } catch (Exception $e) {
            Log::debug($e);
        }
        return $results;
    }


    public function checkValidToken($data)
    {
        $flag = false;
        if (!empty($data['Message']['Message_Code']) && $data['Message']['Message_Code'] == '999') {
            $token = $this->getToken();
            $this->writeSessionFile($token);
            $flag = true;
        }
        return $flag;
    }


    public function getRequest($shopId, $typeId)
    {

        $pattern1 = "/XA" . date('Y') . "/i";
        $pattern2 = "/X" . date('Y') . "/i";
        if (empty(preg_match($pattern1, $shopId)) && empty(preg_match($pattern2, $shopId))) {
            return true;
        }
        return [
            "Data" => [
                "DO_No" => $shopId,
                "Transaction_Detail" => []
            ],
            "Auth" => [
                "Token" => $this->token,
                "Type_ID" => $typeId,
                "Prototype_ID" => 0,
                "Device_ID" => ''
            ]
        ];
    }

    public function confirm($shopId, $typeId)
    {
        $request = $this->getRequest($shopId, $typeId);
        return $this->processResponse($request);
    }

    public function confirmPickup($shopId)
    {
        $request = $this->getRequest($shopId, 315);
        return $this->processResponseWithErrors($request);
    }

    public function confirmStored($shopId)
    {
        return $this->confirm($shopId, 317);
    }

    public function confirmReturn($shopId)
    {
        $request = $this->getRequest($shopId, 316);
        return $this->processResponseWithErrors($request);
    }

    public function getWareHouseCode($wareHouseId)
    {
        $tbWareHouse = $this->getMainTable('WareHouses');
        $wareHouseId = !empty($wareHouseId) ? $wareHouseId : 2;
        $wareHouse = $tbWareHouse->get($wareHouseId);
        return $wareHouse->name;
    }

    public function manualReturn($shopId, $userId)
    {
        $user = $this->user->get($userId);
        $wareHouseName = $this->getWareHouseCode($user->ware_house_id);
        $request = [
            "Data" => [
                "ASN_No" => sprintf("%s_TH", $shopId),
                "ASN_Date" => date("d/m/Y"),
                "Owner_Code" => $user->email,
                "Warehouse_Code" => $wareHouseName,
                "Type_ID" =>  2,
                "Ship_To" => ""
            ],
            "Auth" => [
                "Token" => $this->token,
                "Type_ID" => 301,
                "Prototype_ID" => 2,
                "Device_ID" => ''
            ]
        ];
        return $this->processResponse($request);
    }

    public function getProducts($userId)
    {
        $user = $this->user->get($userId);
        $request = [
            "Data" => ["Owner_Code" => $user->email],
            "Auth" => [
                "Token" => $this->token,
                "Type_ID" => 303,
                "Prototype_ID" => 1,
                "Device_ID" => ''
            ]
        ];
        $products = [];
        $responseData = $this->getResponseData($request);
        foreach ($responseData as $data) {
            $products[$data['Item_Code']] = $data['Item_Name'];
        }
        return $products;
    }

    public function getProductsForMobile($userId)
    {
        $user = $this->user->get($userId);
        $request = [
            "Data" => ["Owner_Code" => $user->email],
            "Auth" => [
                "Token" => $this->token,
                "Type_ID" => 303,
                "Prototype_ID" => 1,
                "Device_ID" => ''
            ]
        ];
        return $this->getResponseData($request);
    }

    public function changeContentToTransactionTripDetails($content)
    {
        $products = explode("|", $content);
        $transactionDetails = [];
        for ($i = 0; $i < count($products); $i++) {
            if (!empty($products[$i])) {
                $item = explode(";", $products[$i]);
                if (!empty($item[0]) && !empty($item[2])) {
                    $transactionDetails[] = [
                        'Item_Code' => $item[0],
                        'Qty' => $item[2],
                    ];
                }
            }
        }
        return $transactionDetails;
    }

    public function createDo($data)
    {
        $order = $data['orders'][0];
        $order['address_id'] = explode(",", $data['address_id'])[0];
        $order['user_id'] = $data['user_id'];
        $order = $this->order->fillOrder($order)['order'];
        $cityName = !empty($order['city_id']) ? $this->order->Cities->get($order['city_id'])->name : "";
        $districtName = !empty($order['district_id']) ? $this->order->Districts->get($order['district_id'])->name : "";
        $wardName = !empty($order['ward_id']) ? $this->order->Wards->get($order['ward_id'])->name : "";
        $user = $this->user->get($data['user_id']);
        $wareHouseName = $this->getWareHouseCode($user->ware_house_id);
        $transactionDetails = [];
        if (!empty($data['qty_items'])) {
            foreach ($data['qty_items'] as $id => $qty) {
                $transactionDetails[] = [
                    'Item_Code' => $id,
                    'Qty' => $qty,
                ];
            }
        }
        $data = [
            'DO_Date' => date('d/m/Y'),
            'Owner_Code' => $user->email,
            'Type_ID' => 1,
            'Driver' => '',
            'Customer_Name' => $order['receiver_name'],
            'Customer_Phone' =>  $order['arr_receiver_phone'],
            'Customer_So_Nha' =>  $order['address'],
            'Customer_Phuong' =>  $wardName,
            "Customer_Quan" => $districtName,
            "Customer_Thanh_Pho" => $cityName,
            "Ship_To" => "",
            'Transaction_Detail' => $transactionDetails
        ];

        $data['Warehouse_Code'] = $wareHouseName;
        $data['COD'] = $order['money_collection'];
        $data['Notes'] = $order['note'];
        $request = [
            'Data' => $data,
            "Auth" => [
                "Token" => $this->token,
                "Type_ID" => 302,
                "Prototype_ID" => 2,
                "Device_ID" => ''
            ]
        ];
        $shopId = $this->getResponseData($request);
        return $shopId;
    }

    function createDoWithExcel($order, $transactionDetails)
    {
        $user = $this->user->get($order['user_id']);
        $cityName = !empty($order['city_id']) ? $this->order->Cities->get($order['city_id'])->name : "";
        $districtName = !empty($order['district_id']) ? $this->order->Districts->get($order['district_id'])->name : "";
        $wardName = !empty($order['ward_id']) ? $this->order->Wards->get($order['ward_id'])->name : "";
        $wareHouseName = $this->getWareHouseCode($user->ware_house_id);
        $data = [
            'DO_Date' => date('d/m/Y'),
            'Owner_Code' => $user->email,
            'Type_ID' => 1,
            'Driver' => '',
            'Customer_Name' => $order['receiver_name'],
            'Customer_Phone' =>  $order['arr_receiver_phone'],
            'Customer_So_Nha' =>  $order['address'],
            'Customer_Phuong' =>  $wardName,
            "Customer_Quan" => $districtName,
            "Customer_Thanh_Pho" => $cityName,
            "Ship_To" => "",
            'Transaction_Detail' => $transactionDetails
        ];

        $data['Warehouse_Code'] = $wareHouseName;
        $data['COD'] = $order['money_collection'];
        $data['Notes'] = $order['note'];
        $request = [
            'Data' => $data,
            "Auth" => [
                "Token" => $this->token,
                "Type_ID" => 302,
                "Prototype_ID" => 2,
                "Device_ID" => ''
            ]
        ];
        return $this->getNewResponseData($request);
    }

    public function createDoWithMobile($order)
    {
        $cityName = !empty($order['city_id']) ? $this->order->Cities->get($order['city_id'])->name : "";
        $districtName = !empty($order['district_id']) ? $this->order->Districts->get($order['district_id'])->name : "";
        $wardName = !empty($order['ward_id']) ? $this->order->Wards->get($order['ward_id'])->name : "";
        $user = $this->user->get($this->getUserId());
        $wareHouseName = $this->getWareHouseCode($user->ware_house_id);
        $data = [
            'DO_Date' => date('d/m/Y'),
            'Owner_Code' => $user->email,
            'Type_ID' => 1,
            'Driver' => '',
            'Customer_Name' => $order['receiver_name'],
            'Customer_Phone' =>  $order['arr_receiver_phone'],
            'Customer_So_Nha' =>  $order['address'],
            'Customer_Phuong' =>  $wardName,
            "Customer_Quan" => $districtName,
            "Customer_Thanh_Pho" => $cityName,
            "Ship_To" => "",
            'Transaction_Detail' => $order['Transaction_Detail']
        ];

        $data['Warehouse_Code'] = $wareHouseName;
        $data['COD'] = $order['money_collection'];
        $data['Notes'] = !empty($order['note']);
        $request = [
            'Data' => $data,
            "Auth" => [
                "Token" => $this->token,
                "Type_ID" => 302,
                "Prototype_ID" => 2,
                "Device_ID" => ''
            ]
        ];
        return $this->getNewResponseData($request);
    }

    public function readSessionFile()
    {
        $filePath = WWW_ROOT . 'files' . DS . 'token';
        return file_get_contents($filePath);
    }

    public function writeSessionFile($token)
    {
        $filePath = WWW_ROOT . 'files' . DS . 'token';
        file_put_contents($filePath, $token);
    }
}
