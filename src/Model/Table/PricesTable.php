<?php
namespace App\Model\Table;

use Cake\Core\Exception\Exception;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Prices Model
 *
 * @method \App\Model\Entity\Price get($primaryKey, $options = [])
 * @method \App\Model\Entity\Price newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Price[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Price|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Price saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Price patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Price[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Price findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PricesTable extends CommonTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('prices');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');


        $this->hasMany('ShippingFees', [
            'foreignKey' => 'price_id'
        ]);

        $this->hasMany('PriceUsers', [
            'foreignKey' => 'price_id',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('PriceUser', [
            'className' => 'PriceUsers',
            'foreignKey' => 'price_id',
            'joinType' => 'INNER'
//            ,
//            'conditions' => 'PriceUser.price_id = Prices.id'
        ]);

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 25)
            ->notEmpty('code');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        return $validator;
    }


    public function search($data){
        $options['conditions'] = [];

        if(!empty($data['key_word'])){
            $keyWord = $data['key_word'];
            $dataSearchLike = ['OR' => [
                "code LIKE '%".$keyWord."%'",
                "description LIKE '%".$keyWord."%'"
            ]];
            array_push($options['conditions'], $dataSearchLike);
        }

        if(isset($data['active']) && (strlen($data['active']) > 0)){
            $options['conditions']['Prices.active'] = $data['active'];
        }
        $options['order'] = ['Prices.id' => 'desc'];
        $query = $this->find('all', $options);
        return $query;
    }

    public function getList(){
        $options['keyField'] = 'id';
        $options['valueField'] = 'code';
        $options['conditions']['active'] = 1;
        return $this->find('list',$options)->toArray();
    }

    public function searchToGetCost($userId){
        $options['conditions'] = [];
        $options['contain'] = ['PriceUser'];
        if(empty($userId)){
            //get default price.
            return $this->get(PRICE_DEFAULT)->id;
        }else{
            $options['conditions']['PriceUser.user_id'] = $userId;
            $price = $this->find('all', $options)->first();
            if(empty($price)){
                return $this->get(PRICE_DEFAULT)->id;
            }
            return $price->id;
        }
    }

    public function copy($id){
        $price = $this->get($id);
        $price->isNew(true);
        $price->code = $price->code.'_COPY';
        $price->unsetProperty('id');
        $price->unsetProperty('created');
        $price->unsetProperty('modified');
        $this->getConnection()->begin();
        try{
            if($this->save($price)){
                $shippingFees = $this->ShippingFees->find('all', ['conditions' => ['price_id' => $id]]);
                foreach ($shippingFees as $shippingFee){
                    $shippingFee->isNew(true);
                    $shippingFee->unsetProperty('id');
                    $shippingFee->price_id = $price->id;
                    if(!$this->ShippingFees->save($shippingFee)){
                        $this->errorException();
                    }
                }
                $this->getConnection()->commit();

            }else{
                $this->errorException();
            }
        }catch (Exception $e){
            $this->getConnection()->rollback();
        }
    }
}
