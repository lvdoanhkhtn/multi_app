<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrderNotes Model
 *
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \App\Model\Entity\OrderNote get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrderNote newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrderNote[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrderNote|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderNote saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderNote patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrderNote[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrderNote findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NotifiesTable extends CommonTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('notifies');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function read(){
        $notifies = $this->find('all')->toArray();
        for($i = 0; $i<count($notifies); $i++){
            $notifies[$i]['user_ids'] = $this->getReadUser($notifies[$i]['id']);
        }
        if($this->saveMany($notifies)){
            return true;
        }
        return false;    
    }

    public function getReadUser($id){
        $notify = $this->get($id);
        $userIds = !empty($notify->user_ids) ? explode( ",", $notify->user_ids) : [];
        $userId = $this->getUserId();
        $notifyUserIds = $userId;
        //if have users read
        if(count($userIds) > 0){
            //check current user is read.
            if(!in_array($userId, $userIds)){
                $userIds[] = $userId;
                $notifyUserIds = implode(',', $userIds);
            }
        }
        return $notifyUserIds;
    }
}
