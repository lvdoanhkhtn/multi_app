<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Libs\ConfigUtil;

/**
 * BankAccounts Model
 *
 * @property \App\Model\Table\BanksTable|\Cake\ORM\Association\BelongsTo $Banks
 * @property \App\Model\Table\CustomersTable|\Cake\ORM\Association\BelongsTo $Customers
 *
 * @method \App\Model\Entity\BankAccount get($primaryKey, $options = [])
 * @method \App\Model\Entity\BankAccount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BankAccount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BankAccount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BankAccount|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BankAccount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BankAccount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BankAccount findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BankAccountsTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('bank_accounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Banks', [
            'foreignKey' => 'bank_id'
        ]);
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('bank_id', ['message' => ConfigUtil::getMessage('ECL001', ['Bank'])]);


        $validator
            ->notEmpty('account_number', ['message' => ConfigUtil::getMessage('ECL001', ['Accounts number'])])
            ->add('account_number', ['length' => $this->checkMaxLength('Accounts number', 50)]);

        $validator
            ->notEmpty('account_holder', ['message' => ConfigUtil::getMessage('ECL001', ['Accounts holder'])])
            ->add('account_holder', ['length' => $this->checkMaxLength('Accounts holder', 50)]);

        $validator
            ->notEmpty('branch_name', ['message' => ConfigUtil::getMessage('ECL001', ['Branch name'])])
            ->add('branch_name', ['length' => $this->checkMaxLength('Branch name',50)]);

        
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['bank_id'], 'Banks'));
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }

    public function insertOrUpdate($data, $isEdit = false){
        return $this->_insertOrUpdate($data, $isEdit);
    }
        
}
