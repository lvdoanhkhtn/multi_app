<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrderNotes Model
 *
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \App\Model\Entity\OrderNote get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrderNote newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrderNote[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrderNote|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderNote saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderNote patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrderNote[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrderNote findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrderNotesTable extends CommonTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('order_notes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('note')
            ->maxLength('note', 255)
            ->allowEmptyString('note');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }

    public function saveNote($orderId, $note){
        $deliveryNoteData['order_id'] = $orderId;
        $deliveryNoteData['note'] = strip_tags($note);
        $deliveryNoteData['user_id'] = $this->getUserId();
        $deliveryNoteEntity = $this->newEntity($deliveryNoteData);
        $flag = false;
        if($this->save($deliveryNoteEntity)){
            $flag = true;
        }
        return $flag;
    }

    public function getNoteInOrder($orderId){
        $options['conditions']['order_id'] = $orderId;
        $options['limit'] = 1;
        $options['order'] = ['id' => 'DESC'];
        $options['fields'] = ['note'];
        $note = "";
        $order = $this->find('all', $options)->first();
        if(!empty($order)){
            $note = $order->note;
        }
        return $note;
    }
}
