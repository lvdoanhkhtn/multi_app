<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Methods Model
 *
 * @property \App\Model\Table\ActionsTable&\Cake\ORM\Association\HasMany $Actions
 *
 * @method \App\Model\Entity\Controller get($primaryKey, $options = [])
 * @method \App\Model\Entity\Controller newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Controller[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Controller|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Controller saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Controller patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Controller[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Controller findOrCreate($search, callable $callback = null, $options = [])
 */
class MethodsTable extends CommonTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('methods');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Actions', [
            'foreignKey' => 'method_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 25)
            ->allowEmptyString('name');

        $validator
            ->scalar('alias')
            ->maxLength('alias', 100)
            ->allowEmptyString('alias');

        return $validator;
    }

    public function getIdByName($name){
        $options['conditions']['name'] = trim($name);
        return $this->find('all', $options)->first()->id;
    }
    
    public function insertOrUpdate(){
        $controllers = $this->getAllController();

        foreach ($controllers as $controller){
            $controllerData['name'] = $controller;
//            $controllerData['alias'] = $controller;
            $isExistController = $this->checkExists($this, $controllerData);
            if(!$isExistController) {
                $controllerEntity = $this->newEntity($controllerData);
                if($this->save($controllerEntity)){
                    $methodId = $controllerEntity->id;
                }
            }else {
                $methodId = $this->getIdByName($controller);
            }
            $actions = $this->getActions($controller);
            foreach ($actions as $action){
                $actionData['name'] = $action;
//                $actionData['alias'] = $action;
                $actionData['method_id'] = $methodId;
                $isExistAction = $this->checkExists($this->Actions, $actionData);
                if(!$isExistAction){
                    $actionEntity = $this->Actions->newEntity($actionData);
                    $this->Actions->save($actionEntity);
                }
            }
        }
    }

    public function getListActions(){
        $options['contain'] = ['Actions' => function($q){
            return $q->where(['Actions.active' => COMPLETE_STATUS]);
        }];
        $options['conditions']['Methods.active'] = COMPLETE_STATUS;
        $results =  $this->find('all', $options);
        $actions = [];
        foreach ($results as $result){
            foreach ($result->actions as $action){
                $actions[$action->id] = sprintf("%s - %s", $result->alias, $action->alias);
            }
        }
        return $actions;
    }


    public function getMethodsForMenu($lang, $ids){
        $options['fields'] = [
            'id',
            'name',
            'alias' => ($lang == "vn") ? 'alias' : $lang.'_'.'alias'
        ];
        $actionOptions = [];
        if($this->getRoleId() !== ADMIN){
            $actionOptions = ['Actions.id IN' => $ids, 'Actions.is_view' => COMPLETE_STATUS];
        }else{
            $actionOptions = ['Actions.is_view' => COMPLETE_STATUS];
        }


        $actionFields = $options['fields'];
        $actionFields[] = 'method_id';

        $options['contain'] = ['Actions' => function($q) use ($actionOptions, $actionFields){
            return $q->select($actionFields)->where($actionOptions);
        }];
        $options['conditions']['Methods.id IN'] = $this->Actions->getMethodIdsForMenu($ids);
        if($this->getRoleId() === ADMIN) {
            $options['conditions']['Methods.is_show_admin'] = COMPLETE_STATUS;
        }
        $options['order'] = ['sort' => 'DESC', 'id' => 'DESC'];
        return $this->find('all', $options);
    }



}
