<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cities Model
 *
 * @property \App\Model\Table\AddressesTable|\Cake\ORM\Association\HasMany $Addresses
 * @property \App\Model\Table\AreasTable|\Cake\ORM\Association\HasMany $Area
 * @property \App\Model\Table\CostsTable|\Cake\ORM\Association\HasMany $Costs
 * @property \App\Model\Table\DistrictsTable|\Cake\ORM\Association\HasMany $Districts
 * @property \App\Model\Table\EmployeesTable|\Cake\ORM\Association\HasMany $Employees
 * @property \App\Model\Table\GroupLocationsTable|\Cake\ORM\Association\HasMany $GroupLocations
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\City get($primaryKey, $options = [])
 * @method \App\Model\Entity\City newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\City[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\City|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\City|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\City patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\City[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\City findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CitiesTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cities');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Addresses', [
            'foreignKey' => 'city_id'
        ]);
        $this->hasMany('Area', [
            'foreignKey' => 'city_id'
        ]);
        $this->hasMany('Costs', [
            'foreignKey' => 'city_id'
        ]);
        $this->hasMany('Districts', [
            'foreignKey' => 'city_id'
        ]);
        $this->hasMany('Employees', [
            'foreignKey' => 'city_id'
        ]);
        $this->hasMany('GroupLocations', [
            'foreignKey' => 'city_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'city_id'
        ]);

        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');


        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->allowEmpty('name');


        return $validator;
    }

    public function getPickupCities()
    {
        $options['conditions'] = ['is_get' => 1];
        return $this->find('list', $options)->toArray();
    }

    public function getDeliveryCities()
    {
        return $this->find('list', [
            'conditions' => ['Cities.is_show' => 1]
        ])->toArray();
    }

    public function getCitiesForMobile($conditions)
    {
        $options['fields'] = ['id', 'name', 'code'];
        $options['conditions'] = $conditions;
        return $this->find('all', $options);
    }

    public function getCities($isPickUp = false)
    {
        $options['conditions'] = ['Cities.active' => 1];
        if ($isPickUp) {
            $options['conditions'] = ['is_get' => 1];
        }
        return $this->find('all', $options)->contain(['Districts' => ['fields' => ['id', 'name', 'city_id']]]);
    }

    public function getCityIdByName($name)
    {
        $query = $this->find();
        return $query
          ->select([
            'id',
            'name' => 'MATCH (name) AGAINST ("' . $name . '")',
          ])
          ->enableHydration(false)->toArray();
    }
}
