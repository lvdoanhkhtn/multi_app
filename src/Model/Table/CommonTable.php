<?php

/**
 * Created by PhpStorm.
 * User: doanh.lv
 * Date: 6/23/19
 * Time: 9:28 AM
 */

namespace App\Model\Table;

use App\Libs\ConfigUtil;
use App\Libs\ValueUtil;
use Cake\Core\Exception\Exception;
use Cake\Http\Session;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use ReflectionClass;
use ReflectionMethod;

class CommonTable extends Table
{
    protected $valueUtil;
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->orderConstant = ValueUtil::get('order.order_constant');
        $this->valueUtil = new ValueUtil();
    }

    public function getCustomerId()
    {
        $session = new Session();
        return $session->read('Auth.User.customers.id');
    }

    public function getUser()
    {
        $session = new Session();
        return $session->read('Auth.User');
    }

    public function getUserId()
    {
        $session = new Session();
        return $session->read('Auth.User.id');
    }

    public function getRoleId()
    {
        $session = new Session();
        return $session->read('Auth.User.role_id');
    }

    public function proccessDate($date)
    {
        $date = str_replace('/', '-', trim($date));
        $date = date_create($date);
        return date_format($date, ConfigUtil::get('format_date'));
    }

    public function proccessFromDatetime($createFrom)
    {
        $createFrom = str_replace('/', '-', trim($createFrom));
        $date_from = date_create($createFrom);
        date_time_set($date_from, 00, 00);
        return date_format($date_from, ConfigUtil::get('format_date'));
    }

    public function proccessToDatetime($createTo)
    {
        $createTo = str_replace('/', '-', trim($createTo));
        $createTo = date_create($createTo);
        date_time_set($createTo, 23, 59);
        return date_format($createTo, ConfigUtil::get('format_date_time'));
    }

    public function getLastDays($day)
    {
        return date('Y-m-d', strtotime('-' . $day . ' days'));
    }

    public function checkMaxLength($title, $length, $field = null)
    {
        return [
            'rule' => ['maxLength', $length],
            'message' => ConfigUtil::getMessage("ECL002", [$title, $length]),
        ];
    }

    public function checkMinLength($title, $length, $field = null)
    {
        return [
            'rule' => ['minLength', $length],
            'message' => ConfigUtil::getMessage("ECL003", [$title, $length]),
        ];
    }

    //    private function checkInteger($title){
    //        return[
    //            'rule' => array('isInteger'),
    //            'message' => ConfigUtil::getMessage("ECL004", [$title])
    //        ];
    //    }

    public function setNewLanguague($type)
    {
        $lang = "vi";
        switch ($type) {
            case 1:
                $lang = "eng";
                break;
            case 2:
                $lang = "kh";
                break;
        }
        return $lang;
    }

    public function formatCurrency($currency)
    {
        return round($currency, 2, PHP_ROUND_HALF_EVEN);
    }

    public function _insertOrUpdate($data, $isEdit)
    {
        $response = ['code' => CODE_ERROR, 'data' => []];
        if ($isEdit) {
            $newEntity = $this->get($data['id']);
            $newEntity = $this->patchEntity($newEntity, $data);
        } else {
            if (!empty($data['id'])) {
                unset($data['id']);
            }
            $newEntity = $this->newEntity($data);
        }
        try {
            $result = $this->save($newEntity);
            if (!empty($result['id'])) {
                $response['code'] = CODE_SUCCESS;
                $response['data'] = ['id' => $result['id']];
            } else {
                $response['data'] = $newEntity->getErrors();
            }
        } catch (\Exception $e) {
        }
        return $response;
    }

    public function createOrderId($id)
    {
        $replace = str_repeat("0", 6 - strlen($id)) . $id;
        $orderId = preg_replace('/000000/', $replace, ValueUtil::get('order.code_invoke'), 1);
        return $orderId;
    }

    public function createEmployeeId($id)
    {
        $replace = str_repeat("0", 4 - strlen($id)) . $id;
        return preg_replace('/0000/', $replace, ValueUtil::get('employee.code_invoke'), 1);
    }

    public function formatDate($data)
    {
        return $data->format(ConfigUtil::get('date'));
    }

    public function formatDateTime($data)
    {
        return $data->format(ConfigUtil::get('date_time'));
    }

    public function searchDate($data, $conditions, $dateType)
    {
        $table = !empty($data['table']) ? $data['table'] : $this->getRegistryAlias();
        switch ($dateType) {
            case 2:
                $dateField = "finished";
                break;
            case 3:
                $dateField = "modified";
                break;
            case 4:
                $dateField = "first_stored_time";
                break;
            default:
                $dateField = "created";
                break;
        }
        $fromDate = !empty($data['from_date']) ? $data['from_date'] : '';
        $toDate = !empty($data['to_date']) ? $data['to_date'] : '';
        if (!empty($fromDate)) {
            $fromDate = $this->proccessFromDatetime($fromDate);
            array_push($conditions, $table . "." . $dateField . " >= '" . $fromDate . "'");
        }
        if (!empty($toDate)) {
            $toDate = $this->proccessToDatetime($toDate);
            array_push($conditions, $table . "." . $dateField . " <= '" . $toDate . "'");
        }
        return $conditions;
    }

    public function getCustomList($request, $param)
    {
        $options['conditions'] = [];
        if (!empty($param) && !in_array($param, ['id', 'limit', 'page'])) {
            $options['conditions'][$param] = $request[$param];
        }
        $options['order'] = ['id' => 'desc'];
        return $this->find('list', $options);
    }

    public function commonSearch($request, $param = null)
    {
        $options['conditions'] = [];
        $id = $this->getAlias() . '.id';
        if (!empty($param) && !in_array($param, ['id', 'limit', 'page'])) {
            $options['conditions'][$param] = $request[$param];
        }
        if (!empty($request['id'])) {
            $options['conditions'][$id] = $request['id'];
        }

        if (!empty($request['contain'])) {
            $options['contain'] = $request['contain'];
        }
        $options['order'] = [$id => 'DESC'];
        return $this->find('all', $options);
    }

    public function getAllController()
    {
        $files = scandir('../src/Controller/Center');
        $results = [];
        $ignoreList = [
            '.',
            '..',
            'Component',
            'AppController.php',
        ];
        foreach ($files as $file) {
            if (!in_array($file, $ignoreList)) {
                $controller = explode('.', $file)[0];
                array_push($results, str_replace('Controller', '', $controller));
            }
        }
        return $results;
    }

    public function getActions($controllerName)
    {
        $className = 'App\\Controller\\Center\\' . $controllerName . 'Controller';
        $class = new ReflectionClass($className);
        $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        $results = [];
        $ignoreList = ['beforeFilter', 'afterFilter', 'initialize'];
        foreach ($actions as $action) {
            if ($action->class == $className && !in_array($action->name, $ignoreList)) {
                array_push($results, $action->name);
            }
        }
        return $results;
    }

    public function checkExists($tbOrders, $data)
    {
        $isExist = $tbOrders->exists($data);
        if (!$isExist) {
            return false;
        }
        return true;
    }

    public function getOrderWithConditions($conditions = [])
    {
        $data = $this->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    public function addElementToAllArray($array, $key, $value)
    {
        return array_map(function ($x) use ($key, $value) {
            $x[$key] = $value;
            return $x;
        }, $array);
    }

    public function commonSearchDate($options, $data, $isCompleteStatus = 0, $completeDateType = 3)
    {
        if (!empty($data['from_create_date']) || !empty($data['to_create_date'])) {
            $data['from_date'] = !empty($data['from_create_date']) ? $data['from_create_date'] : "";
            $data['to_date'] = !empty($data['to_create_date']) ? $data['to_create_date'] : "";
            $options['conditions'] = $this->searchDate($data, $options['conditions'], CREATED);
        }

        if (!empty($data['from_complete_date']) || !empty($data['to_complete_date'])) {
            if ($isCompleteStatus > 0) {
                $options['conditions']['status'] = 1;
            }
            $data['from_date'] = !empty($data['from_complete_date']) ? $data['from_complete_date'] : "";
            $data['to_date'] = !empty($data['to_complete_date']) ? $data['to_complete_date'] : "";
            $options['conditions'] = $this->searchDate($data, $options['conditions'], $completeDateType);
        }

        if (!empty($data['from_stored_date']) || !empty($data['to_stored_date'])) {
            $data['from_date'] = !empty($data['from_stored_date']) ? $data['from_stored_date'] : "";
            $data['to_date'] = !empty($data['to_stored_date']) ? $data['to_stored_date'] : "";
            $options['conditions'] = $this->searchDate($data, $options['conditions'], 4);
        }

        return $options;
    }

    public function errorException()
    {
        throw new Exception('Error');
    }

    public function checkEmpty($data)
    {
        return (isset($data) && strlen($data) > 0);
    }

    public function getEmailToSend($email)
    {
        $toMail = "";
        $env = getenv('APP_ENV');
        if (empty($env)) {
            $toMail = MAIL_TEST;
        } else {
            $toMail = $email;
        }
        return $toMail;
    }

    public function getWmsToken()
    {
        $session = new Session();
        return $session->read('Auth.User.WMS_TOKEN');
    }

    public function getMainTable($table)
    {
        return TableRegistry::getTableLocator()->get($table);
    }

    public function getCurrencyCam($total)
    {
        return sprintf("<b>%s (%s USD) </b>", number_format($total * ConfigUtil::get('CAM_CURRENCY')), $total);
    }

    public function getLanguage()
    {
        $session = new Session();
        return $session->read('Config.language');
    }
}
