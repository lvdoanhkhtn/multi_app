<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TransactionTripDetails Model
 *
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\BelongsTo $Orders
 * @property \App\Model\Table\TripsTable|\Cake\ORM\Association\BelongsTo $DeliveryTrips
 * @property \App\Model\Table\TripDetailsTable|\Cake\ORM\Association\BelongsTo $TripDetails
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\TransactionTripDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\TransactionTripDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TransactionTripDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TransactionTripDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TransactionTripDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TransactionTripDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TransactionTripDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TransactionTripDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TransactionTripDetailsTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('transaction_trip_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);


        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Employees', [
            'foreignKey' => 'employee_id',
            'joinType' => 'INNER'
        ]);

        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('payment_status', 'create')
            ->notEmpty('payment_status');

        $validator
            ->scalar('note')
            ->allowEmpty('note');

        $validator
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['order_id'], 'Orders'));
        // $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function search($data){
        $options['contain'] = [
            'Orders' => [
                'fields' => ['id','orderId', 'total', 'sub_fee', 'postage', 'collection_fee', 'protect_fee'],
                'CompleteDeliveryTripDetails' => [
                    'fields' => ['order_id','delivery_trip_id'],
                    'DeliveryTrips' => ['fields' => ['id', 'employee_id'], 'Employees' => ['fields' => ['id', 'user_id'], 'Users' => ['fields' => ['fullname']]]],
                ],
                'CompletePickupTripDetails' => [
                    'fields' => ['order_id','pickup_trip_id'],
                    'PickupTrips' => ['fields' => ['id', 'employee_id'], 'Employees' => ['fields' => ['id', 'user_id'], 'Users' => ['fields' => ['fullname']]]],
                ]
            ],
            'Users' => ['fields' => ['fullname']],
        ];

    
        $options['conditions'] = ['TransactionTripDetails.employee_id IS NOT' => null];
        $options['order'] = ['TransactionTripDetails.id' => 'DESC'];
        if(!empty($data['user_id'])){
            $options['conditions']['TransactionTripDetails.user_id'] = $data['user_id'];
        }
        if(!empty($data['order_code'])){
            $options['conditions']['TransactionTripDetails.order_id'] = $this->Orders->searchOrderWithOrderCode($data['order_code']);
        }
        if(!empty($data['employee_id'])){
            $options['conditions']['TransactionTripDetails.employee_id'] = $data['employee_id'];
        }
        if(!empty($data['delivery_trip_id'])){
            $options['contain']['Orders']['CompleteDeliveryTripDetails']['conditions'] = ['delivery_trip_id' => $data['delivery_trip_id']];
        }
        $options = $this->commonSearchDate($options, $data);
        if(!empty($data['is_export_excel'])){
            $options['limit'] = 4000;
        }
        return $this->find('all', $options);
    }

    public function searchAccordingToShipper($data){
        $options['fields'] = ['type','id','employee_id','date_created' => 'DATE_FORMAT(TransactionTripDetails.created, "%d/%m/%Y")' ];
        $options['contain'] = [
            'Orders' => [
                'fields' => ['id','orderId', 'total', 'sub_fee', 'postage', 'collection_fee', 'protect_fee'],
                'CompleteDeliveryTripDetails' => [
                    'fields' => ['order_id','delivery_trip_id'],
                ]
            ],
            'Employees' => ['fields' => ['id', 'user_id'],'Users' => ['fields' => ['id','fullname']]],
            'Users' => ['fields' => ['fullname']],
        ];

        $options['conditions'] = ['TransactionTripDetails.employee_id IS NOT' => null];
        $options['order'] = ['TransactionTripDetails.id' => 'DESC'];
        if(!empty($data['user_id'])){
            $options['conditions']['TransactionTripDetails.user_id'] = $data['user_id'];
        }
        if(!empty($data['order_code'])){
            $options['conditions']['TransactionTripDetails.order_id'] = $this->Orders->searchOrderWithOrderCode($data['order_code']);
        }
        if(!empty($data['employee_id'])){
            $options['conditions']['TransactionTripDetails.employee_id'] = $data['employee_id'];
        }
        if(!empty($data['delivery_trip_id'])){
            $options['contain']['Orders']['CompleteDeliveryTripDetails']['conditions'] = ['delivery_trip_id' => $data['delivery_trip_id']];
        }
        if(empty($data['create_date'])){
            $currentDate = date('d/m/Y');
            $data['from_create_date'] = $currentDate;
            $data['to_create_date'] = $currentDate;
        }
        $options = $this->commonSearchDate($options, $data);
        return $this->find('all', $options)->disableHydration(true);
    }

    public function getCollectCashBook($data, $excelComponent, $mailComponent){
        $options['fields'] = ['id'];
        $options['contain'] = [
            'Orders' => [
                'fields' => [
                    'id',
                    'orderId',
                    'total', 
                    'postage',
                    'sub_fee',
                    'discount',
                    'collection_fee',
                    'protect_fee',
                    'transfer_fee',
                    'order_status'
                ],
                'CompleteDeliveryTripDetails' => [
                    'fields' => ['order_id','delivery_trip_id'],
                    'DeliveryTrips' => [
                        'fields' => ['id', 'employee_id'], 
                        'Employees' => [
                            'fields' => ['id', 'user_id'], 
                            'Users' => ['fields' => ['fullname']]
                        ]
                    ],
                ]
            ],
        ];
        $options['conditions'] = [];
        $options['order'] = ['TransactionTripDetails.id' => 'DESC'];
        $options = $this->commonSearchDate($options, $data);
        $orders =  $this->find('all', $options)->toArray();
        if(count($orders) > 0){
            $attachFile = $excelComponent->getFilenameCashBook($orders);
            $flag = $mailComponent->sendMailCashBook($attachFile, COLLECT_TITLE);
            if (!$flag) {
                $this->errorException();
            }
            unlink($attachFile);
        }
    }

    public function deleteWithOrderId($orderId){
        $options['conditions']['order_id'] = $orderId;
        $transactionTripDetail = $this->find('all', $options)->first();
        if(empty($transactionTripDetail)){
            return true;
        }
        $options['conditions']['order_status'] = 5;
        $deliveryTripDetail = $this->Orders->DeliveryTripDetails->find('all', $options)->first();
        if(!empty($deliveryTripDetail->id)){
            $deliveryTripDetail->order_status = 3;
            $this->Orders->DeliveryTripDetails->save($deliveryTripDetail);
        }
        return $this->delete($transactionTripDetail);
    }

}
