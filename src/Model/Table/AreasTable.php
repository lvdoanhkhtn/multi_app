<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Area Model
 *
 * @property \App\Model\Table\CitiesTable|\Cake\ORM\Association\BelongsTo $Cities
 * @property \App\Model\Table\DistrictsTable|\Cake\ORM\Association\BelongsTo $Districts
 * @property \App\Model\Table\WardsTable|\Cake\ORM\Association\BelongsTo $Wards
 * @property \App\Model\Table\LocationsTable|\Cake\ORM\Association\BelongsTo $Locations
 *
 * @method \App\Model\Entity\Area get($primaryKey, $options = [])
 * @method \App\Model\Entity\Area newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Area[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Area|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Area|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Area patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Area[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Area findOrCreate($search, callable $callback = null, $options = [])
 */
class AreasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('areas');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Districts', [
            'foreignKey' => 'district_id',
        ]);
        $this->belongsTo('Wards', [
            'foreignKey' => 'ward_id'
        ]);

        $this->belongsTo('SenderCities', [
            'className' => 'Cities',
            'foreignKey' => 'sender_city_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SenderDistricts', [
            'className' => 'Districts',
            'foreignKey' => 'sender_district_id',
        ]);

        $this->belongsTo('Wards', [
            'foreignKey' => 'ward_id'
        ]);
        $this->belongsTo('Locations', [
            'foreignKey' => 'location_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
//        $rules->add($rules->existsIn(['district_id'], 'Districts'));
//        $rules->add($rules->existsIn(['ward_id'], 'Wards'));
        $rules->add($rules->existsIn(['location_id'], 'Locations'));

        return $rules;
    }


    public function search($data){
        //$options = $data;
        $options['fields'] = ['location_id'];
        $options['conditions'] = [];
        if(!empty($data['sender_city_id'])){
            $options['conditions']['sender_city_id'] = $data['sender_city_id'];
        }
        if(!empty($data['city_id'])){
            $options['conditions']['city_id'] = $data['city_id'];
        }
        if(!empty($data['district_id'])){
            $options['conditions']['district_id'] = $data['district_id'];
        }
        if(!empty($data['ward_id'])){
            $options['conditions']['ward_id'] = $data['ward_id'];
        }



        $areaArr = [];
        if(count($options['conditions']) > 0){
            //case contain city, district
            $area = $this->find('all', $options);
            $countArea = count($area->toArray());
            if($countArea == 0 || $countArea > 1){
                //case only city
                $options['conditions']['district_id'] = 0;
                $area = $this->find('all', $options);
                if(count($area->toArray()) > 0){
                    $areaArr = $area->first()->toArray();
                }
            }else{
                $areaArr = $area->first()->toArray();
            }
        }
        return $areaArr;

    }


    public function customSearch($data){
        //$options = $data;
        $options['conditions'] = [];
        $options['contain'] = ['Locations','Cities', 'Districts', 'SenderCities', 'SenderDistricts'];
        if(!empty($data['key_word'])){
            $keyWord = $data['key_word'];
            $dataSearchLike = ['OR' => [
                "Areas.name LIKE '%".$keyWord."%'",
                "Locations.name LIKE '%".$keyWord."%'"
            ]];
            array_push($options['conditions'], $dataSearchLike);
        }
        if(!empty($data['sender_city_id'])){
            $options['conditions']['Areas.sender_city_id'] = $data['sender_city_id'];
        }
        if(!empty($data['city_id'])){
            $options['conditions']['Areas.city_id'] = $data['city_id'];
        }
        $options['order'] = ['Areas.id' => 'DESC'];
        $area = $this->find('all', $options);
        return $area;

    }
}
