<?php

namespace App\Model\Table;

use App\Libs\ConfigUtil;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Customers Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\AddressesTable|\Cake\ORM\Association\HasMany $Addresses
 * @property \App\Model\Table\BankAccountsTable|\Cake\ORM\Association\HasMany $BankAccounts
 * @property \App\Model\Table\EnterprisesTable|\Cake\ORM\Association\HasMany $Enterprises
 * @property \App\Model\Table\PaymentsTable|\Cake\ORM\Association\HasMany $Payments
 *
 * @method \App\Model\Entity\Customer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Customer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Customer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Customer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Customer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Customer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CustomersTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('customers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Addresses', [
            'foreignKey' => 'customer_id',
            'conditions' => ['Addresses.active' => 1, 'is_master_address != 1'],
            'joinType' => 'INNER'
        ]);

        $this->hasMany('MasterAddress', [
            'className' => 'Addresses',
            'foreignKey' => 'customer_id',
            'conditions' => ['MasterAddress.active' => 1, 'is_master_address' => 1]
        ]);

        $this->hasOne('HasOneMasterAddress', [
            'className' => 'Addresses',
            'foreignKey' => 'customer_id',
            'conditions' => ['HasOneMasterAddress.active' => 1, 'is_master_address' => 1],
            'joinType' => 'INNER'
        ]);
        $this->hasMany('BankAccounts', [
            'foreignKey' => 'customer_id'
        ]);
        $this->hasMany('Enterprises', [
            'foreignKey' => 'customer_id'
        ]);
        $this->hasMany('Payments', [
            'foreignKey' => 'customer_id'
        ]);

        $this->belongsTo('GroupCustomers', [
            'foreignKey' => 'group_customer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('user_id', 'create')
            ->notEmpty('user_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function getIdByUserId($userId)
    {
        $options['fields'] = ['id'];
        $options['conditions']['user_id'] = $userId;
        $customer = $this->find('all', $options)->first();
        return $customer['id'];
    }

    public function getAddressesInCustomer()
    {
        $options['fields'] = ['id'];
        $addressFields = ['id', 'customer_id', 'agency', 'str_phones', 'address', 'is_master_address'];
        $nameField = ['fields' => ['id', 'name']];
        $user = $this->getUser();
        if (!empty($user['parent_id'])) {
            $customer = $this->getCustomer($user['parent_id']);
            $options['conditions']['id'] = $customer['id'];
        } else {
            $options['conditions']['id'] = $this->getCustomerId();
        }
        $options['contain'] = [
            //            'MasterAddress' => ['Wards', 'fields' => $addressFields],
            'MasterAddress' => ['fields' => $addressFields, 'Wards' => $nameField, 'Districts' => $nameField, 'Cities' => $nameField],
            'Addresses' => ['queryBuilder' => function ($q) use ($nameField) {
                return $q->contain(['Wards' => $nameField, 'Districts' => $nameField, 'Cities' => $nameField])->order(['Addresses.id' => 'DESC'])->limit(ConfigUtil::get('address_limit'));
            }]
        ];
        return $this->find('all', $options)->first()->toArray();
    }

    public function getBankAccountsInCustomer($customerId)
    {
        $options['fields'] = ['id'];
        $options['conditions']['id'] = $customerId;
        $options['contain'] = [
            'BankAccounts' => function ($q) {
                return $q->select(['id', 'customer_id', 'bank_id', 'account_number', 'account_holder', 'branch_name'])->limit(ConfigUtil::get('bank_account_limit'))->orderDesc('id');
            }
        ];
        return $this->find('all', $options)->first();
    }

    public function createCustomer($userId)
    {
        $customerId = "";
        $customerData['user_id'] = $userId;
        $customerEntity = $this->newEntity($customerData);
        $customerResult = $this->save($customerEntity);
        if (!empty($customerResult['id'])) {
            $customerId = $customerResult['id'];
        }
        return $customerId;
    }


    public function search($data)
    {
        $options['contain'] = [
            'HasOneMasterAddress' => [
                'fields' => ['address', 'customer_id'],
                'Wards' => ['fields' => ['name']],
                'Districts' => ['fields' => ['name']],
                'Cities' => ['fields' => ['name']]
            ],
            'Users' => ['fields' => ['id', 'username', 'fullname', 'email', 'phone', 'active']],
            'GroupCustomers' => ['fields' => ['name']]
        ];
        $options['conditions'] = [];

        if (!empty($data['key_word'])) {
            $keyWord = $data['key_word'];
            $dataSearchLike = ['OR' => [
                "Users.fullname LIKE '%" . $keyWord . "%'",
                "Users.phone LIKE '%" . $keyWord . "%'",
                "address LIKE '%" . $keyWord . "%'",
                "Users.email LIKE '%" . $keyWord . "%'",
                "Users.username LIKE '%" . $keyWord . "%'",
                "HasOneMasterAddress.address LIKE '%" . $keyWord . "%'"
            ]];
            array_push($options['conditions'], $dataSearchLike);
        }

        $options = $this->commonSearchDate($options, $data);


        if (isset($data['active']) && (strlen($data['active']) > 0)) {
            $options['conditions']['Users.active'] = $data['active'];
        }
        if (!empty($data['group_customer_id'])) {
            $options['conditions']['Customers.group_customer_id'] = $data['group_customer_id'];
        }

        $options['order'] = ['Users.id' => 'desc'];
        $query = $this->find('all', $options);
        return $query;
    }

    public function loadCustomers()
    {
        $datas =  $this->find('all', [
            'contain' => 'Users',
            'conditions' => ['Users.role_id' => CUSTOMER]
        ])->toArray();
        $customers = [];
        foreach ($datas as $data) {
            $customers[$data->id] = $data->user->fullname;
        }
        return $customers;
    }

    public function getCustomer($userId)
    {
        $options['fields'] = ['id'];
        $options['conditions']['user_id'] = $userId;
        $options['contain'] = ['GroupCustomers' => ['fields' => ['id', 'name']]];
        return $this->find('all', $options)->first();
    }

    public function isMemberOfWms($userId)
    {
        $customer = $this->getCustomer($userId);
        return (!empty($customer['group_customer']['name']) && $customer['group_customer']['name'] == 'WMS') ? true : false;
    }

    public function updateIsMemberOfWms($userId)
    {
        $customer = $this->getCustomer($userId);
        if (!$this->isMemberOfWms($userId)) {
            //update is member of wms.
            $customer->group_customer_id = 1;
            $this->save($customer);
        }
    }
}
