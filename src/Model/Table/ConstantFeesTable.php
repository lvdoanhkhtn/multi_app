<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConstantFees Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Locations
 *
 * @method \App\Model\Entity\ConstantFee get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConstantFee newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ConstantFee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConstantFee|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConstantFee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConstantFee[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConstantFee findOrCreate($search, callable $callback = null, $options = [])
 */
class ConstantFeesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('constant_fees');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Locations', [
            'foreignKey' => 'location_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('over_kg')
            ->requirePresence('over_kg', 'create')
            ->notEmpty('over_kg');

        $validator
            ->integer('continue_kg')
            ->requirePresence('continue_kg', 'create')
            ->notEmpty('continue_kg');

        $validator
            ->numeric('continue_kg_money')
            ->requirePresence('continue_kg_money', 'create')
            ->notEmpty('continue_kg_money');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['location_id'], 'Locations'));

        return $rules;
    }
}
