<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PriceUsers Model
 *
 * @property \App\Model\Table\PricesTable&\Cake\ORM\Association\BelongsTo $Prices
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\PriceUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\PriceUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PriceUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PriceUser|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PriceUser saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PriceUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PriceUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PriceUser findOrCreate($search, callable $callback = null, $options = [])
 */
class PriceUsersTable extends CommonTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('price_users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Prices', [
            'foreignKey' => 'price_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['price_id'], 'Prices'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function insertOrUpdate($data)
    {
        $options['conditions']['user_id'] = $data['user_id'];
        $options['fields'] = 'id';
        $priceUser = $this->find('all', $options)->first();
        if ($priceUser) {
            $entity = $this->get($priceUser->id);
            $entity = $this->patchEntity($entity, $data);
        } else {
            $entity = $this->newEntity($data);
        }
        if(!empty($data['price_id'])){
            $this->save($entity);
        }else{
            $this->delete($entity);
        }
    }

    public function searchUserInPrice($userId){
        $options['fields'] = ['price_id'];
        $options['contain'] = ['Prices'];
        $options['conditions']['user_id'] = $userId;
        $options['conditions']['Prices.active'] = COMPLETE_STATUS;
        return $this->find('all', $options)->toArray();
    }

}