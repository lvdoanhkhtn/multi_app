<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConstantFees Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Locations
 *
 * @method \App\Model\Entity\ConstantFee get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConstantFee newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ConstantFee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConstantFee|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConstantFee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConstantFee[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConstantFee findOrCreate($search, callable $callback = null, $options = [])
 */
class ReportColorConstantsTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('report_color_constants');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }
}