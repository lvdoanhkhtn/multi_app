<?php

namespace App\Model\Table;

use Cake\Core\Exception\Exception;
use Cake\ORM\Table;

/**
 * PickupTrips Model
 *
 * @property \App\Model\Table\AddressesTable&\Cake\ORM\Association\BelongsTo $Addresses
 * @property \App\Model\Table\EmployeesTable&\Cake\ORM\Association\BelongsTo $Employees
 * @property \App\Model\Table\PickupTripDetailsTable&\Cake\ORM\Association\HasMany $PickupTripDetails
 *
 * @method \App\Model\Entity\PickupTrip get($primaryKey, $options = [])
 * @method \App\Model\Entity\PickupTrip newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PickupTrip[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PickupTrip|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PickupTrip saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PickupTrip patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PickupTrip[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PickupTrip findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PickupReturnTripDetailsTable extends TripDetailsTable
{

    public function addOrdersByAddressId($pickupTripId, $table, $currentOrderStatus, $newOrderStatus)
    {
        $tbCustomers = $this->getMainTable('Customers');
        $tbWms = $this->getMainTable('Wms');

        $currentTable = $this->getTable();
        $this->tableId = str_replace("details", "", $currentTable) . 'id';
        $datas = $this->getOrdersByAddressId($pickupTripId, $table, $currentOrderStatus);
        $flag = true;
        if (count($datas) > 0) {
            $pickupTripDetails = $this->addElementToAllArray($datas, $this->tableId, $pickupTripId);
            foreach ($pickupTripDetails as $pickupTrip) {
                $this->getConnection()->begin();
                try {
                    $order = $this->Orders->get($pickupTrip['id']);
                    $order->order_status = $newOrderStatus;
                    $isMemberOfWms = $tbCustomers->isMemberOfWms($order->user_id);
                    if ($isMemberOfWms && !empty($order->shopId)) {
                        if (!$tbWms->confirmPickup($order->shopId)) {
                            $this->errorException();
                        }
                    }
                    $pickupTripDetailEntity = $this->newEntity();
                    $pickupTripDetailEntity->order_id = $pickupTrip['id'];
                    $pickupTripDetailEntity->pickup_trip_id = $pickupTripId;
                    if (!$this->save($pickupTripDetailEntity)) {
                        $this->errorException();
                    }
                    if ($this->Orders->save($order)) {
                        $this->getConnection()->commit();
                    }
                } catch (Exception $e) {
                    $flag = false;
                    $this->getConnection()->rollback();
                }
            }
        }
        return $flag;
    }
}
