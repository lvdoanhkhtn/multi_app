<?php

namespace App\Model\Table;


use Cake\Validation\Validator;
use Cake\Http\Session;

/**
 * ServicePacks Model
 *
 * @property \App\Model\Table\CostsTable|\Cake\ORM\Association\HasMany $Costs
 * @property \App\Model\Table\GroupLocationsTable|\Cake\ORM\Association\HasMany $GroupLocations
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\ShippingFeesTable|\Cake\ORM\Association\HasMany $ShippingFees
 * @property \App\Model\Table\ShopFeesTable|\Cake\ORM\Association\HasMany $ShopFees
 *
 * @method \App\Model\Entity\ServicePack get($primaryKey, $options = [])
 * @method \App\Model\Entity\ServicePack newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ServicePack[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ServicePack|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ServicePack|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ServicePack patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ServicePack[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ServicePack findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ServicePacksTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('service_packs');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Costs', [
            'foreignKey' => 'service_pack_id'
        ]);
        $this->hasMany('GroupLocations', [
            'foreignKey' => 'service_pack_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'service_pack_id'
        ]);
        $this->hasMany('ShippingFees', [
            'foreignKey' => 'service_pack_id'
        ]);
        $this->hasMany('ShopFees', [
            'foreignKey' => 'service_pack_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmpty('name');

        $validator
            ->integer('active')
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }

    public function getList($lang, $isViewApp = true)
    {
        $options['fields'] = [
            'id',
            'name' => (($lang == "vi") || ($lang == "vn")) ? 'name' : $lang . '_' . 'name'
        ];
        $options['conditions'] = [
            'active' => 1,
            'is_show' => 1
        ];
        if ($isViewApp) {
            $options['conditions']['is_view_app'] = 1;
        }
        $session = new Session();
        if ($session->started()) {
            $roleId = $session->read('Auth.User.role_id');
        }
        if (!empty($roleId) && $roleId === CUSTOMER) {
            $options['conditions'] = [
                'id' => 1,
            ];
        }

        return $this->find('list', $options)->toArray();
    }
}
