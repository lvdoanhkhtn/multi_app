<?php
namespace App\Model\Table\Shop;


use App\Model\Table\OrdersTable;

class ShopOrdersTable extends OrdersTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */

    public function initialize(array $config)
    {
        parent::initialize($config);
    }



}
