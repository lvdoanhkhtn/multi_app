<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ShippingFees Model
 *
 * @property \App\Model\Table\LocationsTable|\Cake\ORM\Association\BelongsTo $Locations
 * @property \App\Model\Table\ServicePacksTable|\Cake\ORM\Association\BelongsTo $ServicePacks
 *
 * @method \App\Model\Entity\ShippingFee get($primaryKey, $options = [])
 * @method \App\Model\Entity\ShippingFee newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ShippingFee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ShippingFee|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ShippingFee|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ShippingFee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ShippingFee[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ShippingFee findOrCreate($search, callable $callback = null, $options = [])
 */
class ShippingFeesTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('shipping_fees');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Locations', [
            'foreignKey' => 'location_id'
        ]);
        $this->belongsTo('ServicePacks', [
            'foreignKey' => 'service_pack_id'
        ]);

        $this->belongsTo('Prices', [
            'foreignKey' => 'price_id'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('fee')
            ->allowEmpty('fee');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['location_id'], 'Locations'));
        $rules->add($rules->existsIn(['service_pack_id'], 'ServicePacks'));

        return $rules;
    }

    public function customSearch($data){
        //$options = $data;
//        $options['fields'] = ['fee'];
        $options['order'] = ['id' => 'DESC'];
        $options['conditions'] = [];
        if(!empty($data['location_id'])){
            $options['conditions']['location_id'] = $data['location_id'];
        }
        if(!empty($data['service_pack_id'])){
            $options['conditions']['service_pack_id'] = $data['service_pack_id'];
        }
        if(!empty($data['price_id'])){
            $options['conditions']['price_id IN'] = $data['price_id'];
        }
        $shippingFee = [];
        if(count($options['conditions']) > 0){
            $shippingFee = $this->find('all', $options)->first();
        }
        return $shippingFee;
    }

    public function copy($priceId){
        
    }


}
