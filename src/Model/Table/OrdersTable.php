<?php

namespace App\Model\Table;

use App\Libs\ConfigUtil;
use App\Libs\ValueUtil;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Http\Client;
use Cake\Http\Session;
use Cake\Log\Log;
use Cake\ORM\Entity;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;


class OrdersTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ServicePacks', [
            'foreignKey' => 'service_pack_id',
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
        ]);
        $this->belongsTo('Districts', [
            'foreignKey' => 'district_id',
        ]);
        $this->belongsTo('Wards', [
            'foreignKey' => 'ward_id',
        ]);
        $this->belongsTo('DeliveryMethods', [
            'foreignKey' => 'delivery_method_id',
        ]);
        $this->belongsTo('WareHouses', [
            'foreignKey' => 'ware_house_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('UserCreater', [
            'className' => 'Users',
            'foreignKey' => 'creater',
        ]);

        $this->hasMany('Notes', [
            'foreignKey' => 'order_id',
        ]);
        $this->hasMany('PaymentDetails', [
            'foreignKey' => 'order_id',
        ]);
        $this->hasMany('RotatoryDetails', [
            'foreignKey' => 'order_id',
        ]);
        $this->hasMany('TransactionTripDetails', [
            'foreignKey' => 'order_id',
        ]);
        $this->hasMany('DeliveryTripDetails', [
            'foreignKey' => 'order_id',
        ]);

        $this->belongsTo('SenderAddresses', [
            'className' => 'Addresses',
            'foreignKey' => 'address_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('DeliveryCities', [
            'className' => 'Cities',
            'foreignKey' => 'city_id',
        ]);

        $this->belongsTo('DeliveryDistricts', [
            'className' => 'Districts',
            'foreignKey' => 'district_id',
        ]);

        $this->hasMany('OrderNotes', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('CompleteDeliveryTripDetails', [
            'className' => 'DeliveryTripDetails',
            'foreignKey' => 'order_id',
            'conditions' => ['CompleteDeliveryTripDetails.order_status' => $this->orderConstant['delivery_completed']],
        ]);

        $this->hasMany('CompletePickupTripDetails', [
            'className' => 'PickupTripDetails',
            'foreignKey' => 'order_id',
            'conditions' => ['CompletePickupTripDetails.order_status' => $this->orderConstant['stored']],
        ]);

        $this->session = new Session();
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('orderId', ['message' => ConfigUtil::getMessage('ECL001', ["Order code"])])
            ->add('orderId', ['length' => $this->checkMaxLength("Order code", 50)]);

        $validator
            ->notEmpty('receiver_name', ['message' => ConfigUtil::getMessage('ECL001', ["Receiver's name"])])
            ->add('receiver_name', ['length' => $this->checkMaxLength("Receiver's name", 255)]);

        $validator
            ->notEmpty('arr_receiver_phone')
            //            ->add('arr_receiver_phone', [
            //                'length' => $this->checkMaxLength("Phone's number", 15),
            //                'validFormat' => [
            //                    'rule' => array('custom', '/^[0-9]*$/i'),
            //                    'message' => ConfigUtil::getMessage("ECL004", ["Phone's number"])
            //                ]
            //            ]);

            ->add('arr_receiver_phone', [
                'length' => $this->checkMaxLength("Phone's number", 25),
            ]);

        $validator
            ->notEmpty('address', ['message' => ConfigUtil::getMessage('ECL001', ["Address"])])
            ->add('address', ['length' => $this->checkMaxLength("Address", 255)]);

        $validator
            ->add('note', ['length' => $this->checkMaxLength("Delivery's Note", 1000)])
            ->allowEmpty('note');

        $validator
            ->notEmpty('city_id', ['message' => ConfigUtil::getMessage('ECL001', ["Province"])]);


        $validator
            ->add('shopId', ['length' => $this->checkMaxLength("Product's Code", 50)])
            ->allowEmpty('shopId');

        $validator
            ->add('content', ['length' => $this->checkMaxLength("Product's Name", 1000)])
            ->allowEmpty('content');

        $validator
            ->maxLength('note_admin', 1000)
            ->allowEmpty('note_admin');

        $validator
            ->allowEmpty('payment');

        $validator
            ->integer('length')
            ->allowEmpty('length');

        $validator
            ->integer('height')
            ->allowEmpty('height');

        $validator
            ->integer('width')
            ->allowEmpty('width');

        $validator
            ->dateTime('finished')
            ->allowEmpty('finished');

        $validator
            ->allowEmpty('history');

        $validator
            ->allowEmpty('order_history');

        $validator
            ->allowEmpty('custom_address')
            ->add('custom_address', ['length' => $this->checkMaxLength("Custom ", 100)]);

        $validator
            ->allowEmpty('group_code');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //        $rules->add($rules->existsIn(['address_id'], 'Addresses'));
        $rules->add($rules->existsIn(['service_pack_id'], 'ServicePacks'));
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
        $rules->add($rules->existsIn(['district_id'], 'Districts'));
        $rules->add($rules->existsIn(['ward_id'], 'Wards'));
        $rules->add($rules->existsIn(['delivery_method_id'], 'DeliveryMethods'));
        $rules->add($rules->existsIn(['ware_house_id'], 'WareHouses'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }

    public function contains()
    {
        return [
            'ServicePacks' => ['fields' => ['name']],
            'Cities' => ['fields' => ['name']],
            'Districts' => ['fields' => ['name']],
            'Wards' => ['fields' => ['name']],
            'Addresses' => [
                'fields' => ['agency', 'address', 'str_phones', 'city_id', 'district_id', 'ward_id'],
                'Cities' => ['fields' => ['id', 'name']],
                'Districts' => ['fields' => ['id', 'name']],
                'Wards' => ['fields' => ['id', 'name']],
            ],
            'Users' => ['fields' => ['fullname']],

        ];
    }

    public function dataContain()
    {
        return [
            'UserCreater' => ['fields' => ['id', 'fullname']],
            'OrderNotes' => function ($q) {
                return $q->contain(['Users' => ['fields' => ['fullname']]])->limit(1)->orderDesc('OrderNotes.id');
            },
            'Users' => ['fields' => ['fullname', 'phone']],
            'WareHouses' => ['fields' => ['id', 'name']],
            'SenderAddresses' => [
                'fields' => ['address', 'ward_id', 'district_id', 'city_id'],
                'Cities' => ['fields' => ['id', 'name']],
                'Districts' => ['fields' => ['id', 'name']],
                'Wards' => ['fields' => ['id', 'name']]
            ], 'Districts' => ['fields' => ['id', 'name']], 'Cities' => ['fields' => ['id', 'name']], 'ServicePacks' => ['fields' => ['id', 'name']],
        ];
    }

    public function dataField($table = "Orders")
    {
        $orderStatus = $table . '.order_status';
        return [
            'money_collection', 'first_stored_time', 'order_status', 'ward_id', 'district_id', 'city_id', 'id', 'orderId', 'shopId', 'receiver_name', 'arr_receiver_phone', 'address', 'total', 'created', 'finished', 'order_status' =>
            'CASE
                WHEN (' . $orderStatus . ' = 5 AND pay_status = 1 AND owe_status = 1) THEN 13
                WHEN (' . $orderStatus . '  = 8 AND pay_status = 1 AND owe_status = 1) THEN 14
                ELSE ' . $orderStatus . '
             END', 'postage', 'sub_fee', 'discount', 'collection_fee', 'protect_fee', 'transfer_fee', 'total_amount' => '(total - (postage + sub_fee - discount + collection_fee + protect_fee))', 'note', 'pay_status', 'owe_status', 'charg_status', 'collect_status', 'service_pack_id', 'content'
        ];
    }

    public function search($data, $isCountStatus = false)
    {

        if (!$isCountStatus) {
            $options['contain'] = $this->dataContain();
            $options['fields'] = $this->dataField();
        } else {
            $options['fields'] = ['order_status' =>
            'CASE
                WHEN (order_status = 5 AND pay_status = 1 AND owe_status = 1) THEN 13
                WHEN (order_status = 8 AND pay_status = 1 AND owe_status = 1) THEN 14
                ELSE order_status
             END'];
        }
        if (!empty($data['isRepickup'])) {
            $ids = $this->DeliveryTripDetails->getIdsByOrderStatus($this->orderConstant['stored']);
            $options['conditions']['Orders.id NOT IN'] = $ids;
        }

        if (empty($data['isAdmin'])) {
            $userSession = $this->session->read('Auth.User');
            $options['conditions']['user_id'] = !empty($userSession['parent_id']) ? $userSession['parent_id'] : $userSession['id'];
        }

        $options['conditions']['deleted'] = 0;
        if (!empty($data['key_word'])) {
            $keyWord = $data['key_word'];
            $dataSearchLike = ['OR' => [
                "orderId LIKE '%" . $keyWord . "%'",
                "receiver_name LIKE '%" . $keyWord . "%'",
                "arr_receiver_phone LIKE '%" . $keyWord . "%'",
                "Orders.address LIKE '%" . $keyWord . "%'",
                "shopId LIKE '%" . $keyWord . "%'",
            ]];

            array_push($options['conditions'], $dataSearchLike);
        }

        if (!empty($data['order_code'])) {
            $dataSearchLike = ['OR' => [
                "orderId LIKE '%" . $data['order_code'] . "%'",
                "shopId LIKE '%" . $data['order_code'] . "%'",
            ]];

            array_push($options['conditions'], $dataSearchLike);
        }

        if (isset($data['order_status']) && strlen($data['order_status']) > 0) {
            $arrOrderStatus = explode(",", $data['order_status']);
            if (count(array_filter($arrOrderStatus, "strlen")) > 0) {
                $completeOrderStatus = ValueUtil::get('order.order_constant')['order_charged'];
                $deliveryOrderStatus = ValueUtil::get('order.order_constant')['delivery_completed'];
                $returnCompleteStatus = ValueUtil::get('order.order_constant')['return_completed'];
                $orderPaidStatus = ValueUtil::get('order.order_constant')['order_paid'];
                $orderStatusSearchLike = [];

                if (in_array($deliveryOrderStatus, $arrOrderStatus)) {
                    $arrOrderStatus = array_diff($arrOrderStatus, [$deliveryOrderStatus]);
                    $orderStatusSearchLike['OR'][] = [
                        'AND' => [
                            'pay_status != 1',
                            'owe_status != 1',
                            'order_status' => $deliveryOrderStatus,
                        ],
                    ];
                }
                if (in_array($orderPaidStatus, $arrOrderStatus)) {
                    $arrOrderStatus = array_diff($arrOrderStatus, [$orderPaidStatus]);
                    $orderStatusSearchLike['OR'][] = [
                        'AND' => [
                            'pay_status' => 1,
                            'owe_status' => 1,
                            'order_status' => $deliveryOrderStatus,
                        ],

                    ];
                }

                if (in_array($returnCompleteStatus, $arrOrderStatus)) {
                    $arrOrderStatus = array_diff($arrOrderStatus, [$returnCompleteStatus]);
                    $orderStatusSearchLike['OR'][] = [
                        'AND' => [
                            'pay_status != 1',
                            'owe_status != 1',
                            'order_status' => $returnCompleteStatus,
                        ],

                    ];
                }
                if (in_array($completeOrderStatus, $arrOrderStatus)) {
                    $arrOrderStatus = array_diff($arrOrderStatus, [$completeOrderStatus]);
                    $orderStatusSearchLike['OR'][] = [
                        'AND' => [
                            'pay_status' => 1,
                            'owe_status' => 1,
                            'order_status' => $returnCompleteStatus,
                        ],

                    ];
                }

                if (count($arrOrderStatus) > 0) {
                    $orderStatusSearchLike['OR']['order_status IN'] = $arrOrderStatus;
                }

                array_push($options['conditions'], $orderStatusSearchLike);
            }
        }
        if (isset($data['province']) && strlen($data['province']) > 0) {
            $province = $data['province'];
            $arrProvince = explode(",", $province);
            if (count(array_filter($arrProvince, "strlen")) > 0) {
                $options['conditions']['Orders.city_id IN'] = $arrProvince;
            }
        }

        if (!empty($data['service_pack_id'])) {
            $options['conditions']['service_pack_id'] = $data['service_pack_id'];
        }

        if (!empty($data['ware_house_id'])) {
            $options['conditions']['Orders.ware_house_id'] = $data['ware_house_id'];
        }

        $data['date_type'] = !empty($data['date_type']) ? $data['date_type'] : 1;
        $options['conditions'] = $this->searchDate($data, $options['conditions'], $data['date_type']);
        if (isset($data['range_of_date']) && strlen($data['range_of_date']) > 0 && $data['range_of_date'] != 100) {
            $lastDay = $this->getLastDays($data['range_of_date']);
            $lastDayConditions = ['from_date' => $lastDay];
            $options['conditions'] = $this->searchDate($lastDayConditions, $options['conditions'], $data['date_type']);
        }

        //search on web.
        elseif (empty($data['range_of_date'])) {

            $options = $this->commonSearchDate($options, $data, 0, FINISHED);
        }

        if (!empty($data['order_type'])) {
            $like = "";
            $keyWord = "TH";
            if ($data['order_type'] == '1') {
                $like = "NOT";
            }
            array_push($options['conditions'], "orderId " . $like . " LIKE '%" . $keyWord . "%'");
        }

        if (!empty($data['district_id'])) {
            $options['conditions']['Orders.district_id'] = $data['district_id'];
        }

        if (!empty($data['ward_id'])) {
            $options['conditions']['ward_id'] = $data['ward_id'];
        }

        if (isset($data['pay_status']) && strlen($data['pay_status']) > 0) {
            $options['conditions']['pay_status'] = $data['pay_status'];
        }

        if (isset($data['owe_status']) && strlen($data['owe_status']) > 0) {
            $options['conditions']['owe_status'] = $data['owe_status'];
        }

        if (isset($data['charg_status']) && strlen($data['charg_status']) > 0) {
            $options['conditions']['charg_status'] = $data['charg_status'];
        }

        if (isset($data['collect_status']) && strlen($data['collect_status']) > 0) {
            $options['conditions']['collect_status'] = $data['collect_status'];
        }

        if (!empty($data['address_id'])) {
            $options['conditions']['address_id'] = $data['address_id'];
        }

        if (isset($data['fee']) && strlen($data['fee']) > 0) {
            $arrFee = explode(",", $data['fee']);
            foreach ($arrFee as $fee) {
                switch ($fee) {
                    case 1:
                        array_push($options['conditions'], 'collection_fee > 0');
                        break;
                    case 2:
                        array_push($options['conditions'], 'sub_fee > 0');
                        break;
                    case 3:
                        array_push($options['conditions'], 'protect_fee > 0');
                        break;
                    case 4:
                        array_push($options['conditions'], 'transfer_fee > 0');
                        break;
                }
            }
        }
        if (!empty($data['user_id'])) {
            $options['conditions']['user_id'] = $data['user_id'];
        }
        if (!empty($data['delivery_from'])) {
            $options['contain']['SenderAddresses'] = function ($q) use ($data) {
                return $q->select(['address', 'ward_id', 'district_id', 'city_id'])->where(['SenderAddresses.district_id' => $data['delivery_from']])->contain(['Wards' => ['fields' => ['id', 'name']], 'Districts' => ['fields' => ['id', 'name']], 'Cities' => ['fields' => ['id', 'name']]]);
            };
        }

        if (!empty($data['delivery_to'])) {
            $options['conditions']['Orders.district_id'] = $data['delivery_to'];
        }
        $options['order'] = ['Orders.id' => 'desc'];
        //$options['limit'] = 60000;

        if (!empty($data['isMobile'])) {
            $options['limit'] = ConfigUtil::get('max_limit');
            return $this->find('all', $options);
        }
        return $options;
    }

    private function processTextInHistory($key, $order_status, $data, $label, $oldValue, $newValue, $id, $item, $orderNotes)
    {
        $text = "";
        if ($key == "order_status") {
            if (in_array($newValue, [$order_status[1]])) {
                $text = sprintf(__('assigned_shipper_to_pickup'), $data['shipper'], $data['phone']);
            } elseif (in_array($newValue, [$order_status[4]])) {
                $text = sprintf(__('assigned_shipper_to_delivery'), $data['shipper'], $data['phone']);
            } elseif (in_array($newValue, [$order_status[7]])) {
                $text = sprintf(__('assigned_shipper_to_return'), $data['shipper'], $data['phone']);
            } elseif (in_array($newValue, [$order_status[2], $order_status[5], $order_status[8]])) {
                $text = $newValue;
            } elseif (in_array($newValue, [$order_status[9], $order_status[11], $order_status[3], $order_status[6]])) {
                $text = $newValue;
                if (count($orderNotes)) {
                    foreach ($orderNotes as $note) {
                        if ($item['modified'] == $this->formatDateTime($note->created)) {
                            $text = sprintf("%s - %s", $newValue, $note->note);
                        }
                    }
                }
            } else {
                $text = sprintf("[%s], %s", $label[$key], $newValue);
            }
        } elseif (in_array($key, ["charg_status"]) && $newValue == 1) {
            $text = __('collected_money_successfully');
        } elseif (in_array($key, ["owe_status"]) && $newValue == 1) {
            $text = __('paid_money_successfully');
        } elseif (in_array($key, ["collect_status", "pay_status"])) {
            $text = "";
        } else {
            $text = sprintf("[%s], Old value : %s, New value : %s", $label[$key], $oldValue, $newValue);
        }
        return $text;
    }

    public function history($id, $type = 1)
    {
        $options['fields'] = ['order_history'];
        $options['conditions'] = ['id' => $id];
        $order = $this->find('all', $options)->first();
        $response = [];
        if (empty($order)) {
            return $response;
        }
        $order = $order->toArray();

        if (empty($order['order_history'])) {
            return $response;
        }
        $lang = $this->setNewLanguague($type);
        $label = ValueUtil::get('order.order_history_' . $lang);
        $order_status = ValueUtil::get('order.order_statuses_' . $lang);
        $payment = ValueUtil::get('order.payment_' . $lang);
        $orders = json_decode($order['order_history'], true);
        $data['id'] = $id;
        $orderNotes = !empty($this->getOrder($data)) ? $this->getOrder($data)['order_notes'] : [];
        foreach ($orders as $items) {
            if ($this->getRoleId() == CUSTOMER) {
                $values = array_column($items['data'], 'key');
                if (in_array('charg_status', $values)) {
                    continue;
                }
            }
            $data['modified'] = $items['modified'];
            $data['modifier'] = $items['fullname'];
            $data['shipper'] = "";
            $data['phone'] = "";
            if (!empty($items['shipper'])) {
                $data['shipper'] = $items['shipper'];
                $data['phone'] = $this->Users->getPhoneByName($items['shipper']);
            }
            $data['text'] = [];
            foreach ($items['data'] as $item) {
                if (!empty($label[$item['key']])) {
                    $oldValue = $item['old_value'];
                    $newValue = $item['new_value'];
                    switch ($item['key']) {
                        case "order_status":
                            $oldValue = isset($oldValue) ? $order_status[$oldValue] : "";
                            $newValue = isset($newValue) ? $order_status[$newValue] : "";
                            break;
                        case "city_id":
                            $oldValue = isset($oldValue) ? $this->Cities->get($oldValue)->name : "";
                            $newValue = isset($newValue) ? $this->Cities->get($newValue)->name : "";
                            break;
                        case "district_id":
                            $oldValue = isset($oldValue) ? $this->Districts->get($oldValue)->name : "";
                            $newValue = isset($newValue) ? $this->Districts->get($newValue)->name : "";
                            break;
                        case "ward_id":
                            $oldValue = isset($oldValue) ? $this->Wards->get($oldValue)->name : "";
                            $newValue = isset($newValue) ? $this->Wards->get($newValue)->name : "";
                            break;
                        case "payment":
                            $oldValue = isset($oldValue) ? $payment[$oldValue] : "";
                            $newValue = isset($newValue) ? $payment[$newValue] : "";
                            break;
                    }
                    $data['text'][] = $this->processTextInHistory($item['key'], $order_status, $data, $label, $oldValue, $newValue, $id, $items, $orderNotes);
                }
            }
            if (!empty($data['text'])) {
                $response[] = $data;
            }
        }
        return (count($response) > 0) ? array_reverse($response) : $response;
    }

    public function parseOrder($order, $type)
    {
        $lang = $this->setNewLanguague($type);

        $payment = ValueUtil::get('order.payment_' . $lang);
        $servicePacks = $this->ServicePacks->getList($lang, false);
        $result['id'] = $order['id'];
        $result['orderId'] = $order['orderId'];
        $result['created'] = $order['created'];
        $result['creater'] = $order['user_creater']['fullname'] ? $order['user_creater']['fullname'] : '';
        $result['finished'] = $order['finished'];
        $result['receiver_name'] = $order['receiver_name'];
        $result['arr_receiver_phone'] = $order['arr_receiver_phone'];
        $result['address_delivery'] = $order['address'];
        $result['address_delivery_id'] = $order['address_id'];
        $result['money_collection'] = $order['money_collection'];
        $result['note'] = $order['note'];
        $result['shopId'] = $order['shopId'];
        $result['content'] = $order['content'];
        $result['product_value'] = $order['product_value'];
        $result['weight'] = $order['weight'];
        $result['length'] = $order['length'];
        $result['width'] = $order['width'];
        $result['height'] = $order['height'];
        $result['total'] = $this->formatCurrency($order['total']);
        $result['postage'] = $this->formatCurrency($order['postage']);
        $result['collection_fee'] = $this->formatCurrency($order['collection_fee']);
        $result['protect_fee'] = $this->formatCurrency($order['protect_fee']);
        $result['sub_fee'] = $this->formatCurrency($order['sub_fee']);
        $result['transfer_fee'] = $this->formatCurrency($order['transfer_fee']);
        $result['order_status_id'] = $order->getStatus(false);
        $result['order_status_name'] = $order->getStatus();
        $result['sender_pickup'] = $order['user']['fullname'];
        $senderAddress = $order['sender_address'];
        $result['phone_pickup'] = $senderAddress['str_phones'];
        $result['address_pickup'] = $senderAddress['address'];
        $result['address_pickup_id'] = $senderAddress['id'];
        $result['province_pickup'] = $senderAddress['city']['name'];
        $result['province_pickup_id'] = $senderAddress['city']['id'];
        $result['district_pickup'] = $senderAddress['district']['name'];
        $result['district_pickup_id'] = $senderAddress['district']['id'];

        $result['province_delivery'] = $order['city']['name'];
        $result['province_delivery_id'] = $order['city']['id'];
        $result['district_delivery'] = $order['district']['name'];
        $result['district_delivery_id'] = $order['district']['id'];
        $result['delivery_service'] = $servicePacks[$order['service_pack']['id']];
        $result['delivery_service_id'] = $order['service_pack']['id'];
        $order['payment'] = !empty($order['payment']) ? $order['payment'] : 0;
        $result['payer'] = $payment[$order['payment']];
        $result['payer_id'] = $order['payment'];
        //        $result['total_fee'] = doubleval($result['postage']) + doubleval($result['sub_fee']) + doubleval($result['collection_fee']) + doubleval($result['protect_fee']);
        $result['total_fee'] = $order->getTotalFee();
        $result['custom_address'] = $order['custom_address'];
        $result['group_code'] = $order['group_code'];
        $result['total_amount'] = $order->getTotalAmount();
        $result['agency'] = $senderAddress['agency'];
        $result['full_address_delivery'] = strip_tags($order->getFullReceiverAddress());
        $result['receiver_delivery'] = strip_tags($order->getReceiverAddress());
        $result['full_sender_delivery'] = strip_tags($order->getFullSenderAddress());
        $result['order_notes'] = $order->order_notes;
        $result['is_pick_up'] = $order->is_pick_up;
        $result['order_status'] = $order->order_status;
        return $result;
    }

    public function getContain()
    {
        $options['contain'] = [
            'Cities' => ['fields' => ['id', 'name']],
            'Districts' => ['fields' => ['id', 'name']],
            'SenderAddresses' => ['Cities' => ['fields' => ['id', 'name']], 'Districts' => ['fields' => ['id', 'name']]],
            'Users',
            'UserCreater' => ['fields' => ['id', 'fullname']],
            'ServicePacks' => ['fields' => ['id', 'name']],
            'OrderNotes' => ['Users' => ['fields' => ['fullname']]],
        ];
        return $options;
    }

    public function generateParseOrder($options, $type)
    {
        $orders = $this->find('all', $options);
        $resultOrders = [];
        foreach ($orders as $order) {
            $result = $this->parseOrder($order, $type);
            array_push($resultOrders, $result);
        }
        return $resultOrders;
    }

    public function parseOrders($orders, $type)
    {
        $resultOrders = [];
        foreach ($orders as $order) {
            $result = $this->parseOrder($order, $type);
            array_push($resultOrders, $result);
        }
        return $resultOrders;
    }

    public function getOrders($data, $type = 1)
    {
        $options = $this->getContain($data);
        if (!empty($data['id'])) {
            $options['conditions'] = ['Orders.id' => $data['id']];
        } else {
            $options['conditions'] = ['group_code' => $data['group_code']];
        }
        if (!empty($data['user_id'])) {
            $options['conditions']['user_id'] = $data['user_id'];
        }
        return $this->generateParseOrder($options, $type);
    }

    public function getListOrders($data, $type = 1)
    {
        $options = $this->getContain($data);
        if (!empty($data['ids'])) {
            $options['conditions'] = ['Orders.id IN' => explode(",", $data['ids'])];
        }
        if (!empty($data['user_id'])) {
            $options['conditions']['user_id'] = $data['user_id'];
        }
        if (!empty($data['is_sort'])) {
            $options['order'] = ['Orders.city_id', 'Orders.district_id'];
        }
        return $this->generateParseOrder($options, $type);
    }

    public function getOrder($data, $type = 1)
    {
        $options = $this->getContain();
        if (!empty($data['user_id'])) {
            $options['conditions']['user_id'] = $data['user_id'];
        }
        $order = $this->get($data['id'], $options);
        return $this->parseOrder($order, $type);
    }

    private function validAddress($address_id, $user)
    {
        $flag = false;
        if (in_array($address_id, array_column($user['addresses'], 'id'))) {
            $flag = true;
        }
        if (in_array($address_id, array_column($user['master_address'], 'id'))) {
            $flag = true;
        }
        return $flag;
    }

    public function fillOrder($data)
    {
        $order = $data;
        $order['money_collection'] = !empty($order['money_collection']) ? $order['money_collection'] : 0;
        $order['product_value'] = !empty($order['product_value']) ? $order['product_value'] : 0;
        $order['payment'] = !empty($data['payment']) ? $data['payment'] : 0;

        $order['service_pack_id'] = 1;
        if (!empty($data['service_pack_id'])) {
            $servicePack = $this->ServicePacks->get($data['service_pack_id'])->toArray();
            if (!empty($servicePack['id'])) {
                $order['service_pack_id'] = $data['service_pack_id'];
            }
        }

        $order['weight'] = 0;
        if (!empty($data['weight'])) {
            $order['weight'] = $data['weight'];
        }
        $userSession = $this->getUser();

        $userId = !empty($data['user_id']) ? $data['user_id'] : $userSession['id'];
        $userId = !empty($userSession['parent_id']) ? $userSession['parent_id'] : $userId;
        $user = $this->Users->loadAddresses(['id' => $userId]);
        $customer = $user['customers'];
        $order['address_id'] = (!empty($data['address_id']) && $this->validAddress($data['address_id'], $customer)) ? $data['address_id'] : $customer['master_address']['id'];
        //get sender address.

        $address = $this->SenderAddresses->get($order['address_id']);
        $order['user_id'] = $user['id'];
        $order['creater'] = !empty($userSession['child_id']) ? $userSession['child_id'] : $userSession['id'];
        $weight = $order['weight'];
        if (!empty($data['length']) && !empty($data['height']) && !empty($data['height'])) {
            $tempWeight = ($data['length'] * $data['height'] * $data['height']) / ValueUtil::get('order.weight')['constant'];
            if ($tempWeight > $order['weight']) {
                $weight = $tempWeight;
            }
        }
        if (empty($order['district_id'])) {
            $order['district_id'] = $this->Districts->getDefaultDistrictByCityId($order['city_id']);
        }
        $cost_data = [
            'service_pack_id' => $order['service_pack_id'],
            'city_id' => $order['city_id'],
            'district_id' => $order['district_id'],
            'ward_id' => !empty($order['ward_id']) ? $order['ward_id'] : "",
            'weight' => $weight,
            'user_id' => $user['id'],
            'sender_city_id' => !empty($order['sender_city_id']) ? $order['sender_city_id'] : $address['city_id'],
            'product_value' => !empty($order['product_value']) ? $order['product_value'] : 0,
            'money_collection' => !empty($order['money_collection']) ? $order['money_collection'] : 0,
        ];
        $cost = $this->getCosts($cost_data);
        $order['postage'] = !empty($cost['fee']) ? doubleval($cost['fee']) : 0;
        $order['collection_fee'] = !empty($cost['collection_fee']) ? doubleval($cost['collection_fee']) : 0;
        $order['transfer_fee'] = !empty($cost['transfer_fee']) ? doubleval($cost['transfer_fee']) : 0;
        $order['protect_fee'] = !empty($cost['protect_fee']) ? doubleval($cost['protect_fee']) : 0;
        $order['total'] = !empty($data['money_collection']) ? doubleval($data['money_collection']) : 0;
        if (isset($data['is_pick_up'])) {
            if (!empty($data['is_pick_up'])) {
                $order['is_pick_up'] = 1;
                $order['postage'] = 0;
            } else {
                $order['is_pick_up'] = 0;
            }
        }
        $cost['total_fee'] = $order['postage'] + $order['collection_fee'] + $order['protect_fee'];
        if (!empty($data['payment']) && $data['payment'] == 1) {
            $order['payment'] = $data['payment'];
            $order['total'] += $order['postage'] + $order['collection_fee'] + $order['protect_fee'];
        }
        $response['order'] = $order;
        $response['cost'] = $cost;
        return $response;
    }

    public function getCosts($data)
    {
        $service_pack_id = $data['service_pack_id'];
        $city_id = $data['city_id'];
        $district_id = $data['district_id'];
        $sender_city_id = $data['sender_city_id'];
        $userId = !empty($data['user_id']) ? $data['user_id'] : "";
        $data['money_collection'] = !empty($data['money_collection']) ? $data['money_collection'] : 0;
        $data['product_value'] = !empty($data['product_value']) ? $data['product_value'] : 0;

        $this->Areas = TableRegistry::getTableLocator()->get('Areas');
        $this->ShopFees = TableRegistry::getTableLocator()->get('ShopFees');
        $this->ShippingFees = TableRegistry::getTableLocator()->get('ShippingFees');
        $this->Prices = TableRegistry::getTableLocator()->get('Prices');

        $area_data = [
            'sender_city_id' => $sender_city_id,
            'city_id' => $city_id,
            'district_id' => $district_id,
        ];
        $area = $this->Areas->search($area_data);
        if (count($area) === 0) {
            unset($area_data['district_id']);
            switch ($sender_city_id) {
                case PHNOMPHENH:
                    $area_data = [
                        'sender_city_id' => PHNOMPHENH,
                        'city_id' => PHNOMPHENH,
                    ];
                    break;
                case HCM:
                    $area_data = [
                        'sender_city_id' => HCM,
                        'city_id' => PHNOMPHENH,
                        'district_id' => DISTRICT_PHNOMPHENH,
                    ];
                    break;
            }

            $area = $this->Areas->search($area_data);
        }

        $priceUser = $this->Prices->PriceUsers->searchUserInPrice($userId);
        //Check in case is Phnomphenh and empty district_id.
        if ($sender_city_id == $city_id && $city_id == PHNOMPHENH && empty($district_id)) {
            $area['location_id'] = 2;
        }
        $dataFee = [
            'location_id' => $area['location_id'],
            'service_pack_id' => $service_pack_id,
            'price_id' => PRICE_DEFAULT,
        ];
        if (!empty($priceUser)) {
            $priceUser = json_decode(json_encode($priceUser), true);
            $dataFee['price_id'] = array_column($priceUser, 'price_id');
        }
        $fee = $this->ShippingFees->customSearch($dataFee);
        if (empty($fee)) {
            $dataFee['price_id'] = PRICE_DEFAULT;
            $fee = $this->ShippingFees->customSearch($dataFee);
        }
        $defaultFee = $this->ShippingFees->get(PRICE_DEFAULT);
        //always get default price.
        $listFee['fee'] = $this->getFee($data, $fee, $defaultFee);
        $listFee['collection_fee'] = $this->getCollectionFee($data, $fee, $defaultFee);
        $listFee['protect_fee'] = $this->getInsuranceFee($data, $fee, $defaultFee);
        //case transfer fee.
        $data['fee'] = $listFee['fee'];
        $data['protect_fee'] = $listFee['protect_fee'];
        $listFee['transfer_fee'] = $this->getTransferFee($data, $fee, $defaultFee);
        $listFee['exchange_fee'] = $this->getExchangeFee($data, $fee, $defaultFee);

        return $listFee;
        //case 2 user normal not set cost
        //return $costs;
    }

    public function getFee($data, $fee, $defaultFee)
    {
        $product_weight = !empty($data['weight']) ? $data['weight'] : 0;
        $weight = $product_weight * CONVERT_KG_G;
        $postage = $this->checkEmpty($fee->start_amount) ? $fee->start_amount : $defaultFee->start_amount;
        $overKg = $this->checkEmpty($fee->start_weight) ? $fee->start_weight : $defaultFee->start_weight;
        $countinueKg = $this->checkEmpty($fee->addition_weight) ? $fee->addition_weight : $defaultFee->addition_weight;
        $countinueKgMoney = $this->checkEmpty($fee->addition_amount) ? $fee->addition_amount : $defaultFee->addition_amount;
        $overKg = $overKg * CONVERT_KG_G;
        $countinueKg = $countinueKg * CONVERT_KG_G;
        $subFee = 0;
        if ($weight > $overKg) {
            $diffWeight = ceil(($weight - $overKg) / $countinueKg);
            $subFee = $diffWeight * $countinueKgMoney;
        }
        return $postage + $subFee;
    }

    public function getCollectionFee($data, $fee, $defaultFee)
    {
        $minimumCollectionFee = $this->checkEmpty($fee->minimum_collection_fee) ? $fee->minimum_collection_fee : $defaultFee->minimum_collection_fee;
        $collectionFee = $this->checkEmpty($fee->collection_fee) ? $fee->collection_fee : $defaultFee->collection_fee;
        $overCollectionFee = $this->checkEmpty($fee->to_cod) ? $fee->to_cod : $defaultFee->to_cod;
        $moneyCollection = $data['money_collection'];
        $postage = 0;
        if ($moneyCollection > $overCollectionFee) {
            $postage = $moneyCollection * $collectionFee;
        }
        if ($postage > 0.0 && $postage < $minimumCollectionFee) {
            $postage = $minimumCollectionFee;
        }
        return $postage;
    }

    public function getInsuranceFee($data, $fee, $defaultFee)
    {
        $insuranceFee = 0;
        $productValue = $data['product_value'];
        $minimumInsuranceFee = $this->checkEmpty($fee->minimum_protect_fee) ? $fee->minimum_protect_fee : $defaultFee->minimum_protect_fee;
        $overProductValue = $this->checkEmpty($fee->to_goods_value) ? $fee->to_goods_value : $defaultFee->to_goods_value;
        $overMoneyProductValue = $this->checkEmpty($fee->protect_fee) ? $fee->protect_fee : $defaultFee->protect_fee;
        if ($productValue > $overProductValue) {
            $insuranceFee = $productValue * $overMoneyProductValue;
        }
        if ($insuranceFee > 0.0 && $insuranceFee < $minimumInsuranceFee) {
            $insuranceFee = $minimumInsuranceFee;
        }
        return $insuranceFee;
    }

    public function getTransferFee($data, $fee, $defaultFee)
    {
        $minimumTransferFee = $this->checkEmpty($fee->minimum_return_fee) ? $fee->minimum_return_fee : $defaultFee->minimum_return_fee;
        $transferFee = $this->checkEmpty($fee->return_fee) ? $fee->return_fee : $defaultFee->return_fee;
        //        $overCollectionFee = !empty($fee->to_cod) ? $fee->to_cod : $defaultFee->to_cod;
        $postage = ($data['fee'] * $transferFee) + $data['protect_fee'];
        if ($postage > 0.0 && $postage < $minimumTransferFee) {
            $postage = $minimumTransferFee;
        }
        return $postage;
    }

    public function getExchangeFee($data, $fee, $defaultFee)
    {
        $minimumExchangeFee = $this->checkEmpty($fee->minimum_exchange_fee) ? $fee->minimum_exchange_fee : $defaultFee->minimum_exchange_fee;
        $exchangeFee = $this->checkEmpty($fee->exchange_fee) ? $fee->exchange_fee : $defaultFee->exchange_fee;
        //        $overCollectionFee = !empty($fee->to_cod) ? $fee->to_cod : $defaultFee->to_cod;
        $moneyCollection = $data['money_collection'];
        $postage = $moneyCollection * $exchangeFee;
        if ($postage > 0.0 && $postage < $minimumExchangeFee) {
            $postage = $minimumExchangeFee;
        }
        return $postage;
    }

    public function insertOrUpdate($data, $isEdit)
    {
        return parent::_insertOrUpdate($data, $isEdit); // TODO: Change the autogenerated stub
    }

    private function getShipperName($newData)
    {
        $shipperName = "";
        $statusPickupOrder = array_values(ValueUtil::get('order.pickup_order'));
        $statusDeliveryOrder = array_values(ValueUtil::get('order.delivery_order'));
        $statusReturnOrder = array_values(ValueUtil::get('order.return_order'));
        if (in_array($newData['order_status'], $statusDeliveryOrder) || ($newData['order_status'] === STORED)) {
            $this->DeliveryTripDetails = TableRegistry::getTableLocator()->get('DeliveryTripDetails');
            $shipperName = $this->DeliveryTripDetails->getShipperName($newData['id'], $newData['order_status']);
        }
        if (empty($shipperName) && (in_array($newData['order_status'], $statusPickupOrder) || ($newData['order_status'] === STORED))) {
            $this->PickupTripDetails = TableRegistry::getTableLocator()->get('PickupTripDetails');
            $shipperName = $this->PickupTripDetails->getShipperName($newData['id'], $newData['order_status']);
        }
        if (in_array($newData['order_status'], $statusReturnOrder)) {
            $this->ReturnTripDetails = TableRegistry::getTableLocator()->get('ReturnTripDetails');
            $shipperName = $this->ReturnTripDetails->getShipperName($newData['id'], $newData['order_status']);
        }
        return $shipperName;
    }

    public function updateOrderHistory($oldData, $newData, $changeFields)
    {
        $changedDataFields = [];
        foreach ($changeFields as $key) {
            if ($key !== "modified") {
                //lay thong tin shipper save vao history

                if (strcmp($oldData[$key], $newData[$key]) !== 0) {
                    $changedDataFields['data'][] = array(
                        'key' => $key,
                        'old_value' => $oldData[$key],
                        'new_value' => $newData[$key],
                    );
                    //$changed_fields[] = $key.':'.$old[$this->alias][$key].'=>'.$value;
                }
            }
        }
        $changedDataFieldsDB = [];

        if (count($changedDataFields) > 0) {
            $changedDataFields['modified'] = $this->formatDateTime($newData['modified']);
            //            $changedDataFields['modified'] = $newData['modified'];
            $changedDataFields['fullname'] = $this->session->read('Auth.User.fullname');
            //get shipper
            $changedDataFields['shipper'] = $this->getShipperName($newData);
            if (!empty($oldData['order_history'])) {
                $changedDataFieldsDB = json_decode($oldData['order_history'], true);
            }
            if (count($changedDataFieldsDB) > 0) {
                array_push($changedDataFieldsDB, $changedDataFields);
            } else {
                $changedDataFieldsDB[] = $changedDataFields;
            }
        }
        return $changedDataFieldsDB;
    }

    public function beforeSave(Event $event, Entity $entity)
    {
        if (!$entity->isNew()) {
            $newData = $entity->toArray();
            $oldData = $entity->getOriginalValues();
            $changeFiels = $entity->getDirty();

            $changedDataFieldsDB = $this->updateOrderHistory($oldData, $newData, $changeFiels);
            if (count($changedDataFieldsDB) > 0) {
                $entity->set('order_history', json_encode($changedDataFieldsDB));
            }
            if (in_array(ORDER_STATUS, $changeFiels)) {

                //call api to shop.
                $this->Webhooks = TableRegistry::getTableLocator()->get('Webhooks');
                $webhooks = $this->Webhooks->getList();
                foreach ($webhooks as $webhook) {
                    if (!empty($newData['shopId'])) {
                        $http = new Client();
                        $data = [
                            'partner_id' => $newData['shopId'],
                            'order_id' => $newData['id'],
                            'status_id' => $newData['order_status'],
                            'action_time' => $newData['created'],
                            'reason' => $this->OrderNotes->getNoteInOrder($newData['id']),
                        ];
                        try {
                            Log::write('debug', $data);
                            $response = $http->post(
                                trim($webhook->url),
                                json_encode($data),
                                ['type' => 'json']
                            );
                            Log::write('debug', $response);
                        } catch (Exception $e) {
                            Log::write('debug', $e);
                        }
                    }
                }
            }
        }
    }

    public function afterSave(Event $event)
    {
        $entity = $event->getData('entity');
        if (empty($entity->orderId)) {
            $entity->orderId = $this->createOrderId($entity->id);
        }
        if (
            in_array($entity->order_status, [$this->orderConstant['delivery_completed'], $this->orderConstant['return_completed']])
            && $entity->owe_status === NO_COMPLETE_STATUS
        ) {
            $this->updateAll(['finished' => new \DateTime()], ['id' => $entity->id, 'finished IS NULL']);
        }
        if (in_array($entity->order_status, [$this->orderConstant['stored']])) {
            if (empty($entity->first_stored_time)) {
                $this->updateAll(['first_stored_time' => new \DateTime()], ['id' => $entity->id]);
            } else {
                $this->updateAll(['stored_time' => new \DateTime()], ['id' => $entity->id]);
            }
        }
        if (!empty($entity)) {
            $this->updateAll(['orderId' => $entity->orderId], ['id' => $entity->id]);
        }
    }

    public function countStatus($response, $isReport = false)
    {
        ini_set('memory_limit', '256M');
        $datas = json_decode(json_encode($response), true);
        $orderStatus = ($isReport) ? ValueUtil::get('order.order_statuses_custom_eng') : ValueUtil::get('order.order_statuses_eng');
        $orderStatusData = array_column($datas, 'order_status');
        $orderStatusResult = [];
        foreach ($orderStatus as $k => $v) {
            $orderStatusCount = count(array_keys($orderStatusData, $k));
            if ($orderStatusCount > 0) {
                $orderStatusResult[$v] = $orderStatusCount;
            } else {
                $orderStatusResult[$v] = 0;
            }
        }
        return $orderStatusResult;
    }

    public function checkOrderCode($shopId, $userId = null)
    {
        if (empty($shopId)) {
            return false;
        }
        $options['conditions'] = array('shopId' => $shopId, 'user_id' => !empty($userId) ? $userId : $this->session->read('Auth.User.id'));
        $orderCount = $this->find('all', $options)->count();
        return !empty($orderCount) ? true : false;
    }

    public function createReturnOrder($id)
    {
        $orderEntity = $this->newEntity();
        $order = $this->get($id);
        $orderEntity->address_id = $order->address_id;
        $orderEntity->receiver_name = $order->receiver_name;
        $orderEntity->arr_receiver_phone = $order->arr_receiver_phone;
        $orderEntity->service_pack_id = $order->service_pack_id;
        $orderEntity->order_status = $this->orderConstant['return'];
        $orderEntity->address = $order->address;
        $orderEntity->user_id = $order->user_id;
        $orderEntity->ward_id = $order->ward_id;
        $orderEntity->district_id = $order->district_id;
        $orderEntity->city_id = $order->city_id;
        $orderEntity->content = $order->content;
        $orderEntity->postage = !empty($order->exchange_fee) ? $order->exchange_fee : 0;
        $orderEntity->creater = $this->getUserId();
        if ($this->save($orderEntity)) {
            $id = $orderEntity->id;
            $newOrder = $this->get($id);
            $newOrder->orderId = $order->orderId . EXCHANGE_PREFIX;
            $this->save($newOrder);
        }
        return $id;
    }

    public function commonContains()
    {
        return [
            'Users' => ['fields' => ['fullname', 'phone']],
            'SenderAddresses' => [
                'fields' => ['str_phones', 'id', 'address', 'ward_id', 'district_id', 'city_id'],
                'Cities' => ['fields' => ['id', 'name']],
                'Districts' => ['fields' => ['id', 'name']],
                'Wards' => ['fields' => ['id', 'name']]
            ], 'Districts' => ['fields' => ['id', 'name']], 'Cities' => ['fields' => ['id', 'name']], 'ServicePacks' => ['fields' => ['id', 'name']],
        ];
    }

    public function getOrdersByStatus($wareHouseId, $orderStatus)
    {
        $options['fields'] = ['count' => $this->find()->func()->count('*'), 'user_id', 'address_id', 'created'];
        $options['contain'] = $this->commonContains();
        $options['group'] = ['user_id', 'address_id'];
        $options['sort'] = ['Orders.id'];
        $options['conditions']['Orders.order_status'] = $orderStatus;
        $options['conditions']['Orders.ware_house_id'] = $wareHouseId;
        return $this->find('all', $options);
    }

    public function printOrderInWareHouse($userId, $addressId, $servicePackId, $isSort = false)
    {
        $options['conditions'] = array('order_status' => 0, 'user_id' => $userId, 'address_id' => $addressId, 'deleted' => 0);
        if (!empty($servicePackId)) {
            $options['conditions']['Order.service_pack_id'] = $servicePackId;
        }

        $options['desc'] = array('id' => 'desc');
        $options['contain'] = $this->commonContains();
        $options['fields'] = ['type', 'shopId', 'Orders.id', 'orderId', 'receiver_name', 'arr_receiver_phone', 'address', 'Orders.created', 'payment', 'total', 'content', 'note', 'city_id', 'district_id', 'ward_id'];
        if ($isSort) {
            $options['order'] = ['Orders.city_id', 'Orders.district_id'];
        }
        return $this->find('all', $options);
    }

    public function getListOrdersByStatus($addressId, $orderStatus = 0)
    {
        $options['conditions']['address_id'] = $addressId;
        $options['conditions']['order_status'] = $orderStatus;
        $options['fields'] = ['order_id' => 'Orders.id', 'Orders.id'];
        $datas = $this->find('all', $options);
        return json_decode(json_encode($datas->toArray()), true);
    }

    public function searchOrderWithStatus($orderCode, $status)
    {
        $options['conditions'] = [
            'OR' => [
                "Orders.orderId LIKE '" . $orderCode . "'",
                "Orders.id LIKE '" . $orderCode . "'",
                "Orders.shopId LIKE '" . $orderCode . "'",
            ],
            'Orders.order_status' => $status,
        ];
        $options['contain'] = $this->commonContains();
        $order = $this->find('all', $options)->first();
        if (!empty($order->id)) {
            $order['formatted_total'] = $this->getCurrencyCam($order->total);
        }
        return $order;
    }

    public function saveNoteAndUpdateStatus($data, $orderStatus)
    {
        $this->getConnection()->begin();
        try {
            $deliveryTripDetailsEntity = $this->DeliveryTripDetails->get($data['id']);
            $orderId = $deliveryTripDetailsEntity->order_id;
            if (!empty($data['delivery_note'])) {
                if (!$this->OrderNotes->saveNote($orderId, $data['delivery_note'])) {
                    $this->errorException();
                }
            }
            //update status delivery_trip_details and orders.
            $deliveryTripDetailsEntity->order_status = $orderStatus;
            $deliveryTripDetailsEntity->is_update_status = COMPLETE_STATUS;
            if (!$this->DeliveryTripDetails->save($deliveryTripDetailsEntity)) {
                $this->errorException();
            }

            $orderEntity = $this->get($orderId);
            $orderEntity->order_status = $orderStatus;
            if ($this->save($orderEntity)) {
                $this->getConnection()->commit();
            } else {
                $this->errorException();
            }
        } catch (Exception $e) {
            $this->getConnection()->rollback();
        }
    }

    public function confirmHanding($orderIds, $newOrderStatus, $note)
    {
        $this->getConnection()->begin();
        $flag = true;
        try {
            foreach ($orderIds as $id) {
                $order = $this->get($id);
                if ($order->collect_status === COMPLETE_STATUS && $order->charg_status === COMPLETE_STATUS) {
                    if (!$this->TransactionTripDetails->deleteWithOrderId($order->id)) {
                        $this->errorException();
                    } else {
                        $order->collect_status = NO_COMPLETE_STATUS;
                        $order->charg_status = NO_COMPLETE_STATUS;
                    }
                }
                $order->finished = null;
                $order->order_status = $newOrderStatus;
                if (!$this->save($order)) {
                    $this->errorException();
                }
                if (!empty($note)) {
                    if (!$this->OrderNotes->saveNote($id, $note)) {
                        $this->errorException();
                    }
                }
            }
            $this->getConnection()->commit();
        } catch (Exception $e) {
            $flag = false;
            $this->getConnection()->rollback();
        }
        return $flag;
    }

    public function confirmCollect($orderIds, $employeeId, $isPickup = false)
    {
        $this->getConnection()->begin();
        $flag = true;
        try {
            foreach ($orderIds as $id) {
                $order = $this->get($id);
                $transData['type'] = "";
                if(empty($order->pay_status)){
                    $order->charg_status = COMPLETE_STATUS;
                    $order->collect_status = COMPLETE_STATUS;
                }else{
                    if(!empty($isPickup)){
                        $transData['type'] = 1;
                        $order->charg_status = COMPLETE_STATUS;
                    }else{
                        $order->collect_status = COMPLETE_STATUS;
                    }
                }
                if (!$this->save($order)) {
                    $this->errorException();
                }
                $transaction = $this->TransactionTripDetails->find('all', ['conditions' => ['order_id' => $id, 'type' => $transData['type']]])->first();
                if (!empty($transaction->id)) {
                    $this->errorException();
                }
                $transData['order_id'] = $id;
                $transData['user_id'] = $this->session->read('Auth.User.id');
                $transData['employee_id'] = $employeeId;
                $transEntity = $this->newEntity($transData);
                if (!$this->TransactionTripDetails->save($transEntity)) {
                    $this->errorException();
                }
            }
            $this->getConnection()->commit();
        } catch (Exception $e) {
            $flag = false;
            $this->getConnection()->rollback();
        }
        return $flag;
    }

    

    public function getOrderPaymentWithUser($userId)
    {
        $options['conditions']['user_id'] = $userId;
        $options['conditions']['owe_status'] = NO_COMPLETE_STATUS;
        $options['conditions']['awaiting_payment'] = NO_COMPLETE_STATUS;
        $options['conditions'][] = [
            'OR' => [
                [
                    'order_status' => $this->orderConstant['delivery_completed'],
                    'charg_status' => COMPLETE_STATUS,
                    'collect_status' => COMPLETE_STATUS,
                ],
                [
                    'order_status' => $this->orderConstant['return_completed'],
                ],
            ],
        ];
        $options['fields'] = ['order_id' => 'id', 'id'];
        return $this->find('all', $options)->enableHydration(false)->toArray();
    }

    public function searchOrderWithOrderCode($orderCode)
    {
        $orderConditions = [
            'OR' => [
                "Orders.orderId LIKE '" . $orderCode . "'",
                "Orders.id LIKE '" . $orderCode . "'",
                "Orders.shopId LIKE '" . $orderCode . "'",
            ],
        ];

        $id = "";
        $order = $this->getOrderWithConditions($orderConditions);
        if (!empty($order)) {
            $id = $order->id;
        }
        return $id;
    }

    public function beforeFind($event, $query, $options, $primary)
    {
        // if ->applyOptions(['default' => false]) not use default conditions
        $table = $event->getSubject()->getRegistryAlias();
        $query->andWhere([$table . '.deleted' => 0]);
        $query->order([$table . '.id' => 'DESC']);

        return $query;
    }

    public function getListPhoneWithUserId($userId)
    {
        $options['fields'] = ['arr_receiver_phone' => 'DISTINCT(arr_receiver_phone)'];
        $options['conditions'] = array('user_id' => $userId);
        $phones = $this->find('all', $options);
        if (count($phones->toArray()) > 0) {
            $phones = json_decode(json_encode($phones->toArray()), true);
            return array_column($phones, 'arr_receiver_phone');
        }
        return [];
    }

    public function getListPhone($keyWord)
    {
        $options['fields'] = ['arr_receiver_phone' => 'DISTINCT(arr_receiver_phone)'];
        $options['conditions'] = [
            'user_id' => $this->getUserId(),
        ];
        if (isset($keyWord) && strlen($keyWord) > 0) {
            array_push($options['conditions'], 'arr_receiver_phone LIKE' . '"%' . $keyWord . '%"');
        }
        $options['limit'] = 10;
        $options['order'] = ['Orders.id' => 'DESC'];
        $phones = $this->find('all', $options)->enableHydration(false);
        return $phones;
    }

    public function getAddressByPhone($phone, $userId)
    {
        $options['fields'] = ['receiver_name', 'address', 'ward_id', 'district_id', 'city_id'];
        $options['conditions'] = [
            'user_id' => !empty($userId) ? $userId : $this->getUserId(),
            "INSTR(arr_receiver_phone, '" . $phone . "') > 0",
        ];
        return $this->find('all', $options)->first();
    }

    public function changeOrderStatus($id, $orderStatus)
    {
        $order = $this->get($id);
        $order->order_status = $orderStatus;
        return $this->save($order);
    }

    public function getOrderIdsWithStatus($status)
    {
        $orderConditions['fields'] = ['order_id' => 'id'];
        $orderConditions['conditions'] = ['Orders.order_status' => $status];

        //check is order_status is stored.
        $orders = $this->find('all', $orderConditions)->disableBufferedResults(true)->disableHydration(true)->toArray();
        return $orders;
    }

    public function searchOrderToNotify()
    {

        $query = $this->find('all');
        $date = date('Y-m-d H:i:s', strtotime("-30 minutes"));
        $conditions = ['order_status' => 0, 'Orders.created >' => sprintf('%s', $date)];
        $orders = $query->select([
            'user_id',
            'count' => $query->func()->count('*'),
        ])
            ->contain(['Users' => ['fields' => ['id', 'fullname']]])
            ->where($conditions)
            ->group('user_id');
        $message = "";
        foreach ($orders as $order) {
            $message .= sprintf("Shop %s có %s đơn mới </br>", $order->user->fullname, $order->count);
        }
        return $message;
    }

    public function orderWithCustomerConditions($searchData)
    {
        $data['conditions']['user_id'] = !empty($searchData['user_id']) ? $searchData['user_id'] : $this->getUserId();
        if (empty($searchData['user_id']) && $this->getRoleId() != CUSTOMER) {
            unset($data['conditions']['user_id']);
        }
        if (!isset($searchData['date_range'])) {
            $searchData['date_range'] = 30;
        }
        if (isset($searchData['date_range']) && $searchData['date_range'] != 100) {
            $searchData['stored_from'] = $this->getLastDays($searchData['date_range']);
            unset($searchData['stored_to']);
            if (!empty($searchData['stored_from'])) {
                array_push($data['conditions'], sprintf("Orders.first_stored_time >= '%s'", $searchData['stored_from']));
            }
        } else if (!empty($searchData['stored_date'])) {
            $createDate = explode("-", $searchData['stored_date']);
            $searchData['from_stored_date'] = trim($createDate[0]);
            $searchData['to_stored_date'] = trim($createDate[1]);
            $data = $this->commonSearchDate($data, $searchData);
        }
        return $data['conditions'];
    }

    public function reportOrderWithCustomer($searchData)
    {
        $data['fields'] = ['id', 'order_status'];
        $data['conditions'] = $this->orderWithCustomerConditions($searchData);
        $data['fields']['order_status'] =
            'CASE
                WHEN (Orders.order_status = 5 AND pay_status = 1 AND owe_status = 1) THEN 13
                WHEN (Orders.order_status = 8 AND pay_status = 1 AND owe_status = 1) THEN 14
                ELSE Orders.order_status
             END';

        $data['join'] = [
            'pickup_trip_details' => [
                'table' => 'pickup_trip_details',
                'type' => 'INNER',
                'conditions' => 'pickup_trip_details.order_id = Orders.id',
            ]
        ];
        $orderStatus = $this->find('all', $data);
        $orderDatas['join'] = $data['join'];
        $orderDatas['conditions'] = $data['conditions'];
        $orderDatas['fields'] = [
            'total_order' => 'COUNT(*)', 'sum_total' => 'SUM(CASE WHEN Orders.order_status IN (2, 3, 4, 5, 13) THEN total ELSE 0 END)', 'sum_cost' => 'SUM(CASE WHEN Orders.order_status IN (2, 3, 4, 5, 13) THEN (postage - discount + collection_fee + protect_fee) ELSE 0 END)',
            'sum_return_cost' => 'SUM(CASE WHEN Orders.order_status IN(6, 7, 8, 11, 14) THEN transfer_fee ELSE 0 END)'
        ];
        $orders = $this->find('all', $orderDatas)->enableHydration(false)->toArray();
        $isReport = !empty($searchData['is_report']) ? $searchData['is_report'] : false;
        $countStatus = $this->countStatus($orderStatus, $isReport);
        $countStatus[0] = $orders[0];
        return $countStatus;
    }

    public function groupOrderByCity($searchData)
    {
        $data['conditions'] = $this->orderWithCustomerConditions($searchData);
        $data['conditions'][] = 'city_id IS NOT NULL';
        $data['conditions']['Orders.order_status IN'] = [2, 3, 4, 5, 6, 7, 8, 9, 11, 13, 14];
        $data['fields'] = ['city_id', 'total' => 'COUNT(city_id)'];
        $data['contain'] = ['Cities' => ['fields' => ['name']]];
        $data['group'] = ['city_id'];
        $data['join'] = [
            'pickup_trip_details' => [
                'table' => 'pickup_trip_details',
                'type' => 'INNER',
                'conditions' => 'pickup_trip_details.order_id = Orders.id',

            ]
        ];
        $orders = $this->find('all', $data);
        $orders = json_decode(json_encode($orders), true);
        array_multisort(array_column($orders, 'total'), SORT_DESC, $orders);
        $newOrders = [];
        $this->ReportColorConstants = TableRegistry::get('ReportColorConstants');
        $reportColorConstants = $this->ReportColorConstants->find('all')->toArray();
        if (count($orders) >= 1) {
            $totalOrder = count($orders);
            $totalOrder = ($totalOrder > 10) ? 10 : $totalOrder;
            $total = array_sum(array_column($orders, 'total'));
            for ($i = 0; $i < $totalOrder; $i++) {
                $newOrders[$i] = $orders[$i];
                $newOrders[$i]['color'] = $reportColorConstants[$i]->value;
            }
            $subTotal = array_sum(array_column($newOrders, 'total'));
            $newOrders[$totalOrder]['total'] = $total - $subTotal;
            $newOrders[$totalOrder]['color'] = $reportColorConstants[$totalOrder - 1]->value;
            $newOrders[$totalOrder]['city']['name']  = __('remain');
        }
        return $newOrders;
    }

    public function updateOrderByShopId($shopId)
    {
        $tbReturnTripDetail = $this->getMainTable('ReturnTripDetails');
        $options['conditions'] = [
            'shopId' => $shopId,
            'user_id' => $this->getUserId(),
            'order_status' => 7,
        ];
        $order = $this->find('all', $options)->first();
        if (empty($order->id)) {
            return false;
        }

        $returnTripDetail = $tbReturnTripDetail->getIdByOrderId($order->id);
        if (empty($returnTripDetail->id)) {
            return false;
        }

        $flag = false;

        $order->order_status = 8;
        $returnTripDetail->order_status = 8;
        $returnTripDetail->is_update_status = 1;

        $this->getConnection()->begin();
        try {
            if (!$tbReturnTripDetail->save($returnTripDetail)) {
                $this->errorException();
            }
            if ($this->save($order)) {
                $flag = true;
                $this->getConnection()->commit();
            }
        } catch (Exception $e) {
            $this->getConnection()->rollback();
        }
        return $flag;
    }

    public function parseSearchOrder($order)
    {
        $result['orderId'] = $order['orderId'];
        $result['receiver_name'] = h($order['receiver_name']);
        $result['arr_receiver_phone'] = h($order['arr_receiver_phone']);
        $result['address_delivery'] = $order->getCustomReceiverAddress();
        $result['total'] = $this->formatCurrency($order['total']);
        $result['note'] = h($order['note']);
        $result['order_status_name'] = $order->getStatus();
        $histories = $this->history($order['id']);
        $result['histories'] = Hash::remove($histories, '{n}.phone');
        return $result;
    }

    public function searchPublic($orderId)
    {
        $options['fields'] = ['id','orderId', 'receiver_name', 'arr_receiver_phone', 'ward_id', 'district_id', 'city_id', 'total', 'note', 'order_status'];
        $options['conditions'] = ['orderId' => trim($orderId)];
        $options['contain'] = [
            'Cities' => ['fields' => ['id', 'name']],
            'Districts' => ['fields' => ['id', 'name']],
            'Wards' => ['fields' => ['id', 'name']]
        ];
        $order = $this->find('all', $options)->first();
        return !empty($order) ? $this->parseSearchOrder($order) : [];
    }
}
