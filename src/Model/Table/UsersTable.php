<?php

namespace App\Model\Table;

use App\Libs\ConfigUtil;
use Cake\Core\Exception\Exception;
use Cake\Http\Session;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

class UsersTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('WareHouses', [
            'foreignKey' => 'ware_house_id',
        ]);
        $this->hasMany('AccessTokens', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('AuthCodes', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('BallotOthers', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Campaigns', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Clients', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Customers', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Emails', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Employees', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('News', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Payments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('RefreshTokens', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('ShopFees', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('TransactionTripDetails', [
            'foreignKey' => 'user_id',
        ]);

        $this->hasMany('OrderCompletePayment', [
            'className' => 'Orders',
            'foreignKey' => 'user_id',
            'conditions' => ['awaiting_payment' => NO_COMPLETE_STATUS, 'order_status' => $this->orderConstant['delivery_completed'], 'charg_status' => COMPLETE_STATUS, 'collect_status' => COMPLETE_STATUS, 'owe_status' => NO_COMPLETE_STATUS],

        ]);

        $this->hasMany('OrderReturnPayment', [
            'className' => 'Orders',
            'foreignKey' => 'user_id',
            'conditions' => ['awaiting_payment' => NO_COMPLETE_STATUS, 'order_status' => $this->orderConstant['return_completed'], 'owe_status' => NO_COMPLETE_STATUS],

        ]);

        $this->hasMany('ShopPermissions', [
            'foreignKey' => 'user_id',
        ]);

        $this->session = new Session();
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        //        $validator->requirePresence('email', 'create');

        $validator
            ->allowEmpty('username')
            ->add('username', ['length' => $this->checkMaxLength('User name', 25, 'username')]);

        $validator
            ->notEmpty('fullname', ['message' => ConfigUtil::getMessage('ECL001', ['Full name'])])
            ->add('fullname', ['length' => $this->checkMaxLength('Fullname', 255, 'fullname')]);

        $validator
            ->notEmpty('email')
            ->add('email', [
                'length' => $this->checkMaxLength('Email', 50, 'email'),
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => ConfigUtil::getMessage("ECL006", ["Email", "Email"]),
                ],
                'validFormat' => [
                    'rule' => 'email',
                    'message' => ConfigUtil::getMessage("ECL011", ["Email"]),

                ],
            ]);

        $validator
            ->notEmpty('fullname', ['message' => ConfigUtil::getMessage('ECL001', ['Full name'])])
            ->add('fullname', ['length' => $this->checkMaxLength('Fullname', 255, 'fullname')]);

        $validator
            ->notEmpty('phone')
            ->add('phone', [
                'length' => $this->checkMaxLength('Phone', 11, 'phone'),
                'validFormat' => [
                    'rule' => array('custom', '/^[0-9]*$/i'),
                    'message' => ConfigUtil::getMessage("ECL004", ["Phone"]),
                ],
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => ConfigUtil::getMessage("ECL006", ["Phone", "Phone"]),
                ],
            ]);

        $validator
            ->notEmpty('address', ['message' => ConfigUtil::getMessage('ECL001', ['Address'])])
            ->add('address', ['length' => $this->checkMaxLength('Address', 100, 'address')]);

        $validator
            ->notEmpty('city_id', ['message' => ConfigUtil::getMessage('ECL001', ['City'])]);

        $validator
            ->notEmpty('district_id', ['message' => ConfigUtil::getMessage('ECL001', ['District'])]);

        $validator
            ->notEmpty('password')
            ->add('password', ['length' => $this->checkMinLength('Password', 6, 'password')]);

        $validator
            ->notEmpty('repeat_password')
            ->add('repeat_password', [
                'length' => $this->checkMinLength('Password', 6, 'password'),
                'equalToPassword' => [
                    'rule' => function ($value, $context) {
                        return $value === $context['data']['password'];
                    },
                    'message' => ConfigUtil::getMessage('ECL005', ["Repeat password", "Password"]),
                ],
            ]);

        return $validator;
    }

    public function validationUpdatePassword(Validator $validator)
    {
        $validator
            // you might want to add some actual password validation here
            ->requirePresence('password')
            ->notEmpty('password')
            ->add('name', [
                'password' => [
                    'rule' => [6, 20],
                    'message' => 'Please enter password in 6 or 20 character',
                ],
            ]);

        return $validator;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));
        $rules->add($rules->existsIn(['ware_house_id'], 'WareHouses'));

        return $rules;
    }

    public function loadAddresses($data)
    {
        $options['fields'] = ['parent_id', 'id', 'fullname', 'role_id', 'email', 'notification_email', 'phone', 'token', 'created', 'active'];
        $options['conditions'] = [];

        if (!empty($data['token'])) {
            $options['conditions']['token'] = $data['token'];
        }
        if (!empty($data['id'])) {
            $options['conditions']['Users.id'] = $data['id'];
        }
        if (!empty($data['email'])) {
            $options['conditions']['Users.email'] = $data['email'];
        }

        $conditions = [
            'fields' => ['customer_id', 'agency', 'id', 'str_phones', 'address', 'is_master_address'],
            'Wards' => [
                'fields' => ['id', 'name'],
            ],
            'Districts' => [
                'fields' => ['id', 'name'],
            ],
            'Cities' => [
                'fields' => ['id', 'name'],
            ],
        ];

        $options['contain'] = [
            'Roles' => ['fields' => ['id', 'name']],
            'Customers' => [
                'fields' => ['id', 'user_id'],
                'MasterAddress' => $conditions,
                'Addresses' => $conditions,
                'GroupCustomers' => [
                    'fields' => ['id', 'name']
                ]
            ],
        ];
        $user = $this->find('all', $options)->enableHydration(false)->first();
        if (!empty($user['customers'])) {
            $user['customers'] = $user['customers'][0];
            if (!empty($user['customers']['master_address'])) {
                $user['customers']['master_address'] = $user['customers']['master_address'][0];
            }
        }
        return $user;
    }

    public function information($data)
    {
        $user = $this->loadAddresses($data);
        return $this->Customers->Addresses->convertAddressInUser($user);
    }

    public function updateInfo($data)
    {
        $user = $this->get($data['id']);
        $user->fullname = $data['fullname'];
        $flag = true;
        $this->getConnection()->begin();
        try {
            if ($this->save($user)) {
                $this->getConnection()->commit();
            } else {
                $this->errorException();
            }
        } catch (Exception $e) {
            $flag = false;
            $this->getConnection()->rollback();
        }
        return $flag;
    }

    public function insertOrUpdate($data, $isEdit = false)
    {
        $response = ['code' => CODE_ERROR, 'data' => []];
        if ($isEdit) {
            $userData['fullname'] = $data['fullname'];
            if (!empty($data['email'])) {
                $userData['email'] = $data['email'];
            }
            if (!empty($data['phone'])) {
                $userData['phone'] = $data['phone'];
            }
            if (!empty($data['notification_email'])) {
                $userData['notification_email'] = $data['notification_email'];
            }

            $userData['active'] = !empty($data['active']) ? 1 : 0;
            $newEntity = $this->get($data['id']);
            $newEntity = $this->patchEntity($newEntity, $userData);
        } else {
            //set default is customer.
            $data['role_id'] = 2;
            $data['password'] = !empty($data['password']) ? $data['password'] :  $data['phone'];
            $data['token'] = bin2hex(openssl_random_pseudo_bytes(16));
            $newEntity = $this->newEntity($data);
        }
        $this->getConnection()->begin();
        try {
            $result = $this->save($newEntity);
            if (!empty($result['id'])) {
                $data['id'] = $result['id'];
                $addressData = [
                    'address' => $data['address'],
                    'ward_id' => $data['ward_id'],
                    'district_id' => $data['district_id'],
                    'city_id' => $data['city_id'],
                    'is_master_address' => 1,
                ];
                if ($isEdit) {
                    if (empty($data['customer_id'])) {
                        $user = $this->session->read('Auth.User');
                        if ($user['customers']['master_address']['id']) {
                            $addressData['id'] = $user['customers']['master_address']['id'];
                        }
                        $addressData['customer_id'] = $user['customers']['id'];
                    } else {
                        $addressData['id'] = $this->Customers->MasterAddress->getMasterAddressId($data['customer_id']);
                        $addressData['customer_id'] = $data['customer_id'];
                    }
                } else {
                    $customerId = $this->Customers->createCustomer($data['id']);
                    if (!empty($customerId)) {
                        $addressData['customer_id'] = $customerId;
                    } else {
                        $this->errorException();
                    }
                }
                if ($this->Customers->Addresses->createAddress($data, $addressData)) {
                    $this->getConnection()->commit();
                    $data = $this->loadAddresses($data);
                    $response['code'] = CODE_SUCCESS;
                    $response['data'] = $data;
                } else {
                    $this->errorException();
                }
            } else {
                $response['data'] = $newEntity->getErrors();
                $this->getConnection()->rollback();
            }
        } catch (Exception $e) {
            $this->getConnection()->rollback();
        }
        return $response;
    }

    public function changePassword($data, $user)
    {
        $response = ['code' => CODE_ERROR, 'data' => []];
        $message['error'] = "";
        if (!$user) {
            $message['error'] = ConfigUtil::getMessage("ECL008", ["Password"]);
        } else if (strcmp($data['newPassword'], $data['newConfirmPassword']) != 0) {
            $message['error'] = ConfigUtil::getMessage("ECL005", ["ConfirmPassword", "Password"]);
        }
        if (empty($message['error'])) {
            $updateData['password'] = $data['newPassword'];
            $options['fields'] = ['id'];
            $userId = $this->session->read('Auth.User.id');
            $user = $this->get($userId, $options);
            $user = $this->patchEntity($user, $updateData);
            if ($this->save($user)) {
                $response['code'] = CODE_SUCCESS;
                $response['data'] = ['id' => $userId];
            } else {
                $response['data'] = $user->getErrors();
            }
        } else {
            $response['data'] = $message['error'];
        }
        return $response;
    }

    public function loadUsers($roleId)
    {
        return $this->find('list', [
            'keyField' => 'id',
            'valueField' => 'fullname',
            'conditions' => ['role_id' => $roleId],
        ])->toArray();
    }

    public function insertOrUpdateEmployee($data, $isEdit = false)
    {
        $data['birthday'] = $this->proccessDate($data['birthday']);
        $response = ['code' => CODE_ERROR, 'data' => []];
        $userData['fullname'] = trim($data['fullname']);
        $userData['email'] = trim($data['email']);
        $userData['phone'] = trim($data['phone']);
        $userData['password'] = trim($data['email']);
        if (!empty($data['role_id'])) {
            $userData['role_id'] = $data['role_id'];
        }
        $userData['username'] = trim($data['username']);
        if ($isEdit) {
            unset($userData['password']);
            if (strcmp($userData['email'], trim($data['hidden_email'])) === 0) {
                unset($userData['email']);
            }
            if (strcmp($userData['phone'], trim($data['hidden_phone'])) === 0) {
                unset($userData['phone']);
            }
            $userData['active'] = (!empty($data['active'])) ? 1 : 0;
            $newEntity = $this->get($data['user_id']);
            $newEntity = $this->patchEntity($newEntity, $userData);
        } else {
            if (!empty($data['IS_SHOP'])) {
                $userData['parent_id'] = trim($data['parent_id']);
            }
            $newEntity = $this->newEntity($userData);
        }
        $this->getConnection()->begin();
        try {
            $result = $this->save($newEntity);
            if (!empty($result['id'])) {
                $employeeData = [
                    'passport' => $data['passport'],
                    'gender' => $data['gender'],
                    'birthday' => $data['birthday'],
                    'address' => $data['address'],
                    'ward_id' => $data['ward_id'],
                    'district_id' => $data['district_id'],
                    'city_id' => $data['city_id'],
                    'is_addmore_trip' => !empty($data['is_addmore_trip']) ? $data['is_addmore_trip'] : 0
                ];
                if ($isEdit) {
                    $newEntityEmployee = $this->get($data['id']);
                    $newEntityEmployee = $this->patchEntity($newEntityEmployee, $employeeData);
                    $shopPermissionEntity = $this->Employees->Users->ShopPermissions->getShopPermissionByUserId($result['id']);
                    if (!empty($data['IS_SHOP'])) {
                        $shopPermissionEntity =  $this->Employees->Users->ShopPermissions->patchEntity($shopPermissionEntity, [
                            'user_id' => $result['id'],
                            'shop_permission_status_id' => $data['shop_permission_status_id']
                        ]);
                    }
                } else {
                    $employeeData['user_id'] = $result['id'];
                    $newEntityEmployee = $this->Employees->newEntity($employeeData);
                    if (!empty($data['IS_SHOP'])) {
                        $shopPermissionEntity =  $this->Employees->Users->ShopPermissions->newEntity([
                            'user_id' => $result['id'],
                            'shop_permission_status_id' => $data['shop_permission_status_id']
                        ]);
                    }
                }

                if ($this->Employees->save($newEntityEmployee)) {
                    $response['code'] = CODE_SUCCESS;
                    $response['data']['id'] = $newEntityEmployee->id;
                    if (!empty($data['IS_SHOP'])) {
                        if (!$this->Employees->Users->ShopPermissions->save($shopPermissionEntity)) {
                            $this->getConnection()->rollback();
                        }
                    }
                    $this->getConnection()->commit();
                } else {
                    $response['data'] = $newEntityEmployee->getErrors();
                    $this->getConnection()->rollback();
                }
            } else {
                $response['data'] = $newEntity->getErrors();
                $this->getConnection()->rollback();
            }
        } catch (Exception $e) {
            $this->getConnection()->rollback();
        }
        return $response;
    }

    public function getEmployee($id)
    {
        $options['fields'] = ['id', 'username', 'email', 'fullname', 'phone', 'role_id', 'active'];
        $options['contain'] = ['Employees' => ['fields' => ['user_id', 'passport', 'gender', 'birthday', 'address', 'ward_id', 'district_id', 'city_id', 'is_addmore_trip']]];
        $user = $this->get($id, $options)->toArray();
        $employee = $user['employees'][0];
        unset($user['employees']);
        return array_merge($user, $employee);
    }

    public function getShopEmployee($id)
    {
        $options['fields'] = ['id', 'username', 'email', 'fullname', 'phone', 'role_id', 'active'];
        $options['contain'] = ['ShopPermissions' => ['fields' => ['user_id', 'shop_permission_status_id']], 'Employees' => ['fields' => ['user_id', 'passport', 'gender', 'birthday', 'address', 'ward_id', 'district_id', 'city_id', 'is_addmore_trip']]];
        $user = $this->get($id, $options)->toArray();
        $employee = $user['employees'][0];
        $employee['shop_permission_status_id'] = !empty($user['shop_permissions'][0]['shop_permission_status_id']) ? $user['shop_permissions'][0]['shop_permission_status_id'] : "";
        unset($user['employees']);
        unset($user['shop_permissions']);
        return array_merge($user, $employee);
    }

    public function getCustomer($id)
    {
        $options['fields'] = ['id', 'username', 'email', 'fullname', 'phone', 'role_id', 'active'];
        $options['contain'] = ['Employees' => ['fields' => ['user_id', 'passport', 'gender', 'birthday', 'address', 'ward_id', 'district_id', 'city_id']]];
        $user = $this->get($id, $options)->toArray();
        $employee = $user['employees'][0];
        unset($user['employees']);
        return array_merge($user, $employee);
    }

    public function updatePromotion($id, $promotionCode)
    {
        $user = $this->get($id);
        $user->promotion_code = $promotionCode;
        $this->save($user);
    }

    public function loadOrderInUser()
    {
        $options['conditions']['role_id'] = CUSTOMER;
        $options['fields'] = ['id', 'fullname'];
        $options['contain'] = [
            'Customers' => ['fields' => ['id', 'user_id'], 'MasterAddress' => ['Wards', 'Districts', 'Cities']],
            'OrderCompletePayment' => function ($q) {
                return $q->select([
                    'user_id',
                    'total_order' => $q->func()->count('*'),
                    'total_cod' => 'SUM(CASE When OrderCompletePayment.pay_status=0 Then total Else money_collection End )',
                    'total_fee' => 'SUM(CASE When OrderCompletePayment.pay_status=0 Then (postage + collection_fee + protect_fee + sub_fee) Else sub_fee  End )',
                ])->group('user_id');
            },
            'OrderReturnPayment' => function ($q) {
                return $q->select([
                    'user_id',
                    'total_order' => $q->func()->count('*'),
                    'total_cod' => $q->func()->sum('total'),
                    'total_fee' => 'SUM(CASE When OrderReturnPayment.pay_status=1 Then transfer_fee - (postage + collection_fee + protect_fee) Else transfer_fee  End )',
                ])->group('user_id');
            },

        ];
        return $this->find('all', $options);
    }

    public function loadCustomers()
    {
        $datas = $this->find('all', [
            'contain' => 'Users',
            'conditions' => ['Users.role_id' => CUSTOMER],
        ])->toArray();
        $employees = [];
        foreach ($datas as $data) {
            $employees[$data->id] = $data->user->fullname;
        }
        return $employees;
    }

    public function getPhoneByName($name)
    {
        $options['fields'] = ['phone'];
        $options['conditions'] = ['fullname' => $name];
        $user = $this->find('all', $options)->first();
        return ($user->phone) ?: "";
    }
}
