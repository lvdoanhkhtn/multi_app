<?php

namespace App\Model\Table;

use Cake\Core\Exception\Exception;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeliveryTripDetails Model
 *
 * @property \App\Model\Table\DeliveryTripsTable&\Cake\ORM\Association\BelongsTo $DeliveryTrips
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \App\Model\Entity\DeliveryTripDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\DeliveryTripDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DeliveryTripDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryTripDetail|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeliveryTripDetail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeliveryTripDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryTripDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryTripDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DeliveryTripDetailsTable extends TripDetailsTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('delivery_trip_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('DeliveryTrips', [
            'foreignKey' => 'delivery_trip_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('FirstOrderCollect', [
            'className' => 'Orders',
            'foreignKey' => false,
            'joinType' => 'INNER',
            'conditions' => array(
                'FirstOrderCollect.id = FirstDeliveryCompleteOrders.order_id',
                'FirstOrderCollect.order_status' => $this->orderConstant['delivery_completed'],
                'charg_status' => NO_COMPLETE_STATUS,
                'collect_status' => NO_COMPLETE_STATUS
            )
        ]);

        $this->belongsTo('OrderCollect', [
            'className' => 'Orders',
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
            'conditions' => ['OrderCollect.order_status' => $this->orderConstant['delivery_completed'], 'collect_status' => NO_COMPLETE_STATUS]
        ]);

        $this->belongsTo('CustomOrderCollect', [
            'className' => 'Orders',
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
            'conditions' => ['DeliveryTripDetails.order_status' => $this->orderConstant['delivery_completed'], 'CustomOrderCollect.order_status' => $this->orderConstant['delivery_completed'], 'charg_status' => 0, 'collect_status' => 0]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('is_update_status')
            ->notEmptyString('is_update_status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['delivery_trip_id'], 'DeliveryTrips'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }

    //kiểm kho.

    public function inventory($orderId, $deliveryTripId)
    {
        $options['conditions']['delivery_trip_id'] = $deliveryTripId;
        $options['conditions']['is_check'] = 0;
        $options['conditions']['DeliveryTripDetails.order_status IN'] = [$this->orderConstant['stored'], $this->orderConstant['pending'], $this->orderConstant['return']];
        $options['contain'] = ['Orders' => ['fields' => ['id', 'orderId']]];
        $options['conditions']['OR'] = [
            'Orders.orderId' => $orderId,
            'Orders.id' => $orderId,
            'Orders.shopId' => $orderId
        ];
        $order = [];

        $deliveryTripDetail = $this->find('all', $options)->first();
        if ($deliveryTripDetail) {
            $orderId = $deliveryTripDetail->order->id;
            $deliveryTripDetail->is_check = 1;
            $this->save($deliveryTripDetail);
            $order = [
                'id' => $orderId,
                'orderId' => $deliveryTripDetail->order->orderId
            ];
        }
        return $order;
    }

    public function checkOrderAll($orderIds, $deliveryTripId)
    {
        $orders = [];
        foreach ($orderIds as $orderId) {
            $orders[] = $this->inventory($orderId, $deliveryTripId);
        }
        return $orders;
    }

    public function checkComplete($deliveryTripId)
    {
        $options['conditions'] = [
            'OR' => [
                [
                    'is_check' => 0, 'DeliveryTripDetails.order_status NOT IN' => [$this->orderConstant['delivery_in_progress'], $this->orderConstant['delivery_completed']]
                ],
                'DeliveryTripDetails.order_status' => $this->orderConstant['delivery_in_progress']
            ]
        ];
        $options['conditions']['delivery_trip_id'] = $deliveryTripId;
        $deliveryTripDetail = $this->find('all', $options);
        if (count($deliveryTripDetail->toArray()) === 0) {
            $deliveryTrip = $this->DeliveryTrips->get($deliveryTripId);
            $deliveryTrip->status = 1;
            $this->DeliveryTrips->save($deliveryTrip);
        }
    }

    public function checkOrderAndInsert($orderCode, $deliveryTripId)
    {
        $orderConstant = $this->orderConstant;
        $orderCode = trim($orderCode);
        $deliveryTripId = trim($deliveryTripId);
        //check is order_status is stored.
        $order = $this->Orders->searchOrderWithStatus($orderCode, $orderConstant['stored']);
        //check exist in detail
        if (!empty($order->id)) {
            $this->getConnection()->begin();
            try {
                try {
                    $options['conditions']['order_id'] = $order->id;
                    $options['conditions']['delivery_trip_id'] = $deliveryTripId;
                    $deliveryTripDetail = $this->find('all', $options)->first();
                    if (!empty($deliveryTripDetail) && empty($deliveryTripDetail->is_check)) {
                        $this->errorException();
                    }
                    $data['order_id'] = $order->id;
                    $data['delivery_trip_id'] = $deliveryTripId;
                    $entity = $this->newEntity($data);
                    if ($this->save($entity)) {
                        $newOrder = $this->Orders->get($order->id);
                        $newOrder->order_status = $orderConstant['delivery_in_progress'];
                        $order->trip_id = $entity->id;
                        if ($this->Orders->save($newOrder)) {
                            $this->getConnection()->commit();
                        } else {
                            $this->errorException();
                        }
                    } else {
                        $this->errorException();
                    }
                } catch (Exception $e) {
                    $this->getConnection()->rollback();
                }
            } catch (\PDOException $e) {
                $this->getConnection()->rollback();
            }
        }
        return $order;
    }



    public function createExchangeOrder($data)
    {
        $this->getConnection()->begin();
        try {
            //send api to wms.
            $order = $this->Orders->get($this->get($data['id'])->order_id);
            $tbCustomers = $this->getMainTable('Customers');
            $tbWms = $this->getMainTable('Wms');
            $isMemberOfWms = $tbCustomers->isMemberOfWms($order->user_id);
            if ($isMemberOfWms && !empty($order->shopId)) {
                if (!$tbWms->manualReturn($order->shopId, $order->user_id)) {
                    $this->errorException();
                }
            }

            $orderIdExchange = $this->Orders->createReturnOrder($this->get($data['id'])->order_id);
            if (empty($orderIdExchange)) {
                $this->errorException();
            }
            $exchangeEntity = $this->newEntity();
            $exchangeEntity->order_id = $orderIdExchange;
            $exchangeEntity->order_status = $this->orderConstant['return'];
            $exchangeEntity->is_update_status = 1;
            $exchangeEntity->is_exchange = 1;
            $exchangeEntity->delivery_trip_id = $data['delivery_id'];
            if ($this->save($exchangeEntity)) {
                $this->getConnection()->commit();
            } else {
                $this->errorException();
            }
        } catch (Exception $e) {
            $this->getConnection()->rollback();
        }
    }

    public function confirmDeliveryDelete($data)
    {
        $this->getConnection()->begin();
        try {
            $delivery = $this->DeliveryTrips->get($data['delivery_id']);
            if (!empty($delivery->status)) {
                $this->errorException();
            }

            $deliveryTripDetail = $this->get($data['id']);
            if (!$this->delete($deliveryTripDetail)) {
                $this->errorException();
            }

            $order = $this->Orders->get($deliveryTripDetail->order_id);
            if ($this->Orders->delete($order)) {
                $this->getConnection()->commit();
            } else {
                $this->errorException();
            }
        } catch (Exception $e) {
            $this->getConnection()->rollback();
        }
    }

    public function collectDetail($deliveryId)
    {
        $options['contain'] = ['OrderCollect' => function ($q) {
            return $q->select($this->Orders->dataField('OrderCollect'))->contain($this->Orders->dataContain());
        }];
        $options['conditions'] = [
            'delivery_trip_id' => $deliveryId,
            'DeliveryTripDetails.order_status' => $this->orderConstant['delivery_completed']
        ];
        return $this->find('all', $options);
    }

    public function getIdsByOrderStatus($orderStatus)
    {
        $options['fields'] = ['order_id'];
        $options['conditions'] = ['DeliveryTripDetails.order_status' => $orderStatus];
        $orders = $this->find('all', $options)->disableHydration(true)->toArray();
        $ids = [];
        if (count($orders) > 0) {
            $ids = array_column($orders, 'order_id');
        }
        return $ids;
    }

    public function getDeliveryTripIdsHaveCollectOrders(){
        $options['contain'] = ['OrderCollect'];
        $options['conditions'] = ['DeliveryTripDetails.order_status' => 5, 'OrderCollect.collect_status' => NO_COMPLETE_STATUS];
        $options['fields'] = ['delivery_trip_id' => 'DISTINCT DeliveryTripDetails.delivery_trip_id'];
        $deliveryTripIds = [];
        $deliveryTrips = $this->find('all', $options)->enableHydration(false)->toArray();
        if(count($deliveryTrips) > 0){
            $deliveryTripIds = array_column($deliveryTrips, 'delivery_trip_id');
        }
        return $deliveryTripIds;
    }
}
