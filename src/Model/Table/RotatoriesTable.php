<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rotatories Model
 *
 * @property \App\Model\Table\EmployeesTable|\Cake\ORM\Association\BelongsTo $Employees
 * @property \App\Model\Table\StartWareHousesTable|\Cake\ORM\Association\BelongsTo $StartWareHouses
 * @property \App\Model\Table\DistinationWareHousesTable|\Cake\ORM\Association\BelongsTo $DistinationWareHouses
 * @property \App\Model\Table\RotatoryPartnersTable|\Cake\ORM\Association\BelongsTo $RotatoryPartners
 * @property \App\Model\Table\RotatoryDetailsTable|\Cake\ORM\Association\HasMany $RotatoryDetails
 *
 * @method \App\Model\Entity\Rotatory get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rotatory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Rotatory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rotatory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rotatory|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rotatory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rotatory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rotatory findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RotatoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rotatories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Employees', [
            'foreignKey' => 'employee_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('StartWareHouses', [
            'foreignKey' => 'start_ware_house_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DistinationWareHouses', [
            'foreignKey' => 'distination_ware_house_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('RotatoryPartners', [
            'foreignKey' => 'rotatory_partner_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('RotatoryDetails', [
            'foreignKey' => 'rotatory_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('note')
            ->allowEmpty('note');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        $validator
            ->dateTime('finished')
            ->allowEmpty('finished');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['employee_id'], 'Employees'));
        $rules->add($rules->existsIn(['start_ware_house_id'], 'StartWareHouses'));
        $rules->add($rules->existsIn(['distination_ware_house_id'], 'DistinationWareHouses'));
        $rules->add($rules->existsIn(['rotatory_partner_id'], 'RotatoryPartners'));

        return $rules;
    }
}
