<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InventorySessionDetails Model
 *
 * @property \App\Model\Table\InventorySessionsTable&\Cake\ORM\Association\BelongsTo $InventorySessions
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \App\Model\Entity\InventorySessionDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\InventorySessionDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\InventorySessionDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InventorySessionDetail|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InventorySessionDetail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InventorySessionDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InventorySessionDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\InventorySessionDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InventorySessionDetailsTable extends CommonTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('inventory_session_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('InventorySessions', [
            'foreignKey' => 'inventory_session_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('StoredOrders', [
            'className' => 'Orders',
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
            'conditions' => ['order_status' => $this->orderConstant['stored']]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('status')
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['inventory_session_id'], 'InventorySessions'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }

    public function inventory($orderCode, $inventorySessionId){
        $flag = false;
        $orderId = $this->Orders->searchOrderWithOrderCode($orderCode);
        if(!empty($orderId)){
            $options['conditions'] = [
                'order_id' => $orderId,
                'inventory_session_id' => $inventorySessionId,
                'status' => NO_COMPLETE_STATUS
            ];
            $inventorySessionDetail = $this->find('all', $options)->first();
            if(!empty($inventorySessionDetail)){
                $inventorySessionDetail->status = COMPLETE_STATUS;
                if($this->save($inventorySessionDetail)){
                    $flag = true;
                }
            }
        }
        return $flag;

    }

    public function confirmRemove($ids){
        $this->getConnection()->begin();
        $flag = true;
        try{
            foreach ($ids as $id){
                $inventorySessionDetail = $this->get($id);
                if(!$this->delete($inventorySessionDetail)){
                    $this->errorException();
                }
            }
            $this->getConnection()->commit();
        }catch (Exception $e){
            $flag = false;
            $this->getConnection()->rollback();
        }
        return $flag;
    }

    public function getIds($inventorySessionId){
        $options['fields'] = ['id'];
        $options['conditions'] = [
            'inventory_session_id' => $inventorySessionId,
        ];
        $inventorySessionDetails = $this->find('all', $options)->disableHydration(true);
        $ids = [];
        if(count($inventorySessionDetails->toArray()) > 0){
            $ids = array_column($inventorySessionDetails->toArray(), 'id');
        }
        return $ids;
    }
}
