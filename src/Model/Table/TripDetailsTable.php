<?php

/**
 * Created by PhpStorm.
 * User: doanh.lv
 * Date: 6/23/19
 * Time: 9:28 AM
 */

namespace App\Model\Table;

use Cake\Log\Log;
use Cake\ORM\Table;

class TripDetailsTable extends CommonTable
{

    public function searchOrderCodeInTrip($orderId)
    {
        $this->tableId = str_replace("details", "", $this->getTable()) . 'id';
        $options['fields'] = [$this->tableId];
        $options['contain'] = ['Orders'];
        $options['conditions'] = [
            'OR' => [
                "Orders.orderId LIKE '" . $orderId . "'",
                "Orders.id LIKE '" . $orderId . "'",
                "Orders.shopId LIKE '" . $orderId . "'",
            ],
        ];
        $tripIds = $this->find('all', $options)->disableHydration(true);
        $ids = [];
        if (!empty($tripIds)) {
            $ids = array_column($tripIds->toArray(), $this->tableId);
        }
        return $ids;
    }

    public function searchOrderInTrip($keyWord)
    {
        $options['contain'] = ['Orders'];
        $options['fields'] = 'id';
        $options['conditions']['OR'] = [
            "Orders.orderId LIKE '%" . $keyWord . "%'",
            "Orders.receiver_name LIKE '%" . $keyWord . "%'",
            "Orders.arr_receiver_phone LIKE '%" . $keyWord . "%'",
            "Orders.address LIKE '%" . $keyWord . "%'",
            "Orders.shopId LIKE '%" . $keyWord . "%'",
        ];

        $tripDetails = $this->find('all', $options);
        $ids = [];
        if (count($tripDetails->toArray()) > 0) {
            $tripDetails = json_decode(json_encode($tripDetails), true);
            $ids = array_column($tripDetails, "id");
        }
        return $ids;
    }

    public function getOrdersByAddressId($pickupTripId, $table, $orderStatus)
    {
        $addressId = $table->get($pickupTripId)->address_id;
        return $this->Orders->getListOrdersByStatus($addressId, $orderStatus);
    }

    public function report($data, $dateType)
    {
        $parentTable = str_replace('Detail', "", $this->getRegistryAlias());
        $options['contain'] = [
            $parentTable => ['joinType' => 'LEFT', 'Employees' => ['joinType' => 'LEFT', 'fields' => ['id', 'user_id'], 'Users' => ['fields' => ['id', 'fullname']]]],
            'Orders' => [
                'fields' => [
                    'weight', 'order_status', 'orderId', 'created', 'finished', 'first_stored_time', 'order_status', 'charg_status', 'collect_status', 'pay_status', 'owe_status', 'total', 'postage',
                    'collection_fee', 'sub_fee', 'protect_fee', 'transfer_fee', 'note', 'user_id',
                ],
                'Users' => ['fields' => ['id', 'fullname']],
                'Districts' => ['fields' => ['name']],
                'Cities' => ['fields' => ['name']],
                'ServicePacks' => ['fields' => ['name']],
            ],
        ];
        $options['contain']['Orders']['fields']['order_status'] =
            "CASE
                WHEN (Orders.order_status = 5 AND pay_status = 1 AND owe_status = 1) THEN 13
                WHEN (Orders.order_status = 8 AND pay_status = 1 AND owe_status = 1) THEN 14
                ELSE Orders.order_status
             END";
        $options['conditions'] = [$this->getRegistryAlias() . '.order_status' => $data['order_status']];
        $data['table'] = "Orders";
        $options = $this->commonSearchDate($options, $data, 0, FINISHED);
        $options['order'] = ['Orders.' . $dateType => 'DESC'];
        $orders = $this->find('all', $options)->enableBufferedResults(false);
        return $orders;
    }

    public function reportByChart($data, $dateType, $isMonth = false)
    {
        $options['conditions'] = ['Orders.' . $dateType . ' IS NOT NULL'];
        $dateTypeConstant = STORED_DATE;
        if ($dateType === FINISHED_CONSTANT) {
            $dateTypeConstant = FINISHED;
            $options['conditions'][$this->getRegistryAlias() . '.order_status'] = $data['order_status'];
        }
        $options['fields'] = ['date' => 'Orders.' . $dateType, 'order_count' => 'count(Orders.id)'];
        $options['contain'] = ['Orders'];
        $data['table'] = "Orders";
        $options = $this->commonSearchDate($options, $data, 0, $dateTypeConstant);
        $options['order'] = ['Orders.' . $dateType => 'ASC'];
        if ($isMonth) {
            $options['group'] = ['MONTH(Orders.' . $dateType . ')'];
        } else {
            $options['group'] = ['DATE(Orders.' . $dateType . ')'];
        }
        return $this->find('all', $options)->enableBufferedResults(false);
    }

    public function getShipperName($orderId, $orderStatus)
    {

        $table = str_replace("Detail", "", $this->getRegistryAlias());
        $lowerTable = str_replace("_details", "", $this->getTable());
        $options['fields'] = ['id'];
        $options['contain'] = [
            $table => [
                'fields' => ['id', 'employee_id'],
                'Employees' => [
                    'fields' => ['id', 'employee_id'],
                    'Users' => ['fields' => ['fullname']],
                ],
            ],
        ];
        $options['conditions'] = [
            'order_id' => $orderId,
            'order_status' => $orderStatus,
        ];
        $trip = $this->find('all', $options)->first();
        Log::debug($trip);

        return !empty($trip->{$lowerTable}->employee->user->fullname) ? $trip->{$lowerTable}->employee->user->fullname : "";
    }

    public function getOptionsInTrips($orderStatus, $id)
    {
        $orderAttr = [
            'fields' => ['payment','pay_status', 'id', 'orderId', 'shopId', 'address_id', 'ward_id', 'district_id', 'city_id', 'created', 'order_status', 'service_pack_id', 'address', 'receiver_name', 'arr_receiver_phone', 'note', 'content', 'total', 'weight', 'history', 'finished', 'postage', 'sub_fee', 'discount', 'collection_fee', 'protect_fee', 'transfer_fee'],
            'OrderNotes' => function ($q) {
                return $q->select([
                    'created', 'note', 'order_id',
                ])->order(['id' => 'DESC'])->limit(1);
            },
            'Users' => ['fields' => ['fullname', 'phone']],
            'SenderAddresses' => [
                'fields' => ['id', 'address', 'ward_id', 'district_id', 'city_id'],
                'Cities' => ['fields' => ['id', 'name']],
                'Districts' => ['fields' => ['id', 'name']],
                'Wards' => ['fields' => ['id', 'name']]
            ], 'Districts' => ['fields' => ['id', 'name']], 'Cities' => ['fields' => ['id', 'name']], 'ServicePacks' => ['fields' => ['id', 'name']]
        ];
        $options['contain'] = ['Orders' => $orderAttr];
        $options['conditions'] = ['PickupTripDetails.order_status' => $orderStatus, 'pickup_trip_id' => $id];
        return $options;
    }

    //update status is complete if all order is updated status.
    public function updateStatus($tripId, $table, $excelComponent = null, $mailComponent = null)
     {
         $this->tableId = $table->getForeignKey();
         //$options['conditions']['is_update_status'] = 0;
         $options['conditions']['order_status IN'] = [$this->orderConstant['pickup_in_progress'], $this->orderConstant['pickup_completed'], $this->orderConstant['return_in_progress']];
         $options['conditions'][$this->tableId] = $tripId;
         $isUpdateAll = $this->find('all', $options);
 
         //updated all
         if (count($isUpdateAll->toArray()) === 0) {
             $trip = $table->get($tripId);
             $trip->status = COMPLETE_STATUS;
             if ($table->save($trip)) {
                 if (!empty($excelComponent) && !empty($mailComponent)) {
                     $user = $table->users->get($trip->user_id);
                     $fullName = $user->fullname;
                     $toMail = $this->getEmailToSend($user->email);
                     $conditions['id'] = $tripId;
                     $conditions['order_status'] = ['return_completed'];
                     $tripDetails = $table->getById($conditions)->return_complete_orders;
                     $attachFile = $excelComponent->getFilenameInPaymentByExcel($tripDetails);
                     $flag = $mailComponent->sendMailPaymentDetail($tripDetails, $toMail, $fullName, $excelComponent, $attachFile, __('return_subject'));
                     if (!$flag) {
                         $this->errorException();
                     }
                     unlink($attachFile);
                 }
             }
         }
 
     }
    public function confirmUpdate($ids, $currentOrderStatus, $newOrderStatus, $pickupTripId, $table, $excelComponent = null, $mailComponent = null)
    {
        $success = false;
        try {
            $this->getConnection()->begin();
            foreach ($ids as $id) {
                $pickupTripDetail = $this->get($id);
                $order = $this->Orders->get($pickupTripDetail->order_id);
                $order->order_status = $newOrderStatus;
                //send api wms
                if (in_array($currentOrderStatus, [1, 2]) && $newOrderStatus == 3) {
                    $this->Customers = $this->getMainTable('Customers');
                    $this->Wms = $this->getMainTable('Wms');
                    $isMemberOfWms = $this->Customers->isMemberOfWms($order->user_id);
                    if ($isMemberOfWms && !empty($order->shopId)) {
                        if(!$this->Wms->confirmStored($order->shopId)){
                            $this->errorException();
                        }
                    }
                }
                $pickupTripDetail->order_status = $newOrderStatus;
                $pickupTripDetail->is_update_status = 1;
                if (!$this->save($pickupTripDetail)) {
                    $this->errorException();
                }
                if (!$this->Orders->save($order)) {
                    $this->errorException();
                }
            }
            $success = true;
            $this->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getConnection()->rollback();
        }
        $this->updateStatus($pickupTripId, $table, $excelComponent, $mailComponent);
        return $success;
    }

    //using in case update fail at pickup trip.

    public function confirmDelete($ids, $newOrderStatus, $pickupTripId, $table, $excelComponent = null, $mailComponent = null)
    {
        try {
            $this->getConnection()->begin();
            foreach ($ids as $id) {
                $pickupTripDetail = $this->get($id);
                $order = $this->Orders->get($pickupTripDetail->order_id);
                $order->order_status = $newOrderStatus;
                if($this->Orders->save($order)) {
                    $this->delete($pickupTripDetail);
                }
            }
            $this->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getConnection()->rollback();
        }
        $this->updateStatus($pickupTripId, $table, $excelComponent, $mailComponent);
    }
}
