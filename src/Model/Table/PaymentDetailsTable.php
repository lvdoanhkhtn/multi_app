<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PaymentDetails Model
 *
 * @property \App\Model\Table\PaymentsTable|\Cake\ORM\Association\BelongsTo $Payments
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \App\Model\Entity\PaymentDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\PaymentDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PaymentDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PaymentDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PaymentDetailsTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payment_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Payments', [
            'foreignKey' => 'payment_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);

		$this->belongsTo('OrderTotal', [
			'className' => 'Orders',
			'foreignKey' => 'order_id',
			'joinType' => 'INNER'
		]);

		$this->belongsTo('OrderCost', [
			'className' => 'Orders',
			'foreignKey' => 'order_id',
			'joinType' => 'INNER'
		]);

        $this->belongsTo('OrderCollect', [
            'className' => 'Orders',
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
            'conditions' => ['charg_status' => 0, 'collect_status' => 0]
        ]);

        $this->belongsTo('OrderCompletePayment', [
            'className' => 'Orders',
            'foreignKey' => 'order_id',
            'conditions' => ['OrderCompletePayment.order_status' => $this->orderConstant['delivery_completed']]
        ]);

        $this->belongsTo('OrderReturnPayment', [
            'className' => 'Orders',
            'foreignKey' => 'order_id',
            'conditions' => ['OrderReturnPayment.order_status' => $this->orderConstant['return_completed']]
        ]);

        $this->belongsTo('OrderCollectedCompletePayment', [
            'className' => 'Orders',
            'foreignKey' => 'order_id',
            'conditions' => ['OrderCollectedCompletePayment.order_status' => $this->orderConstant['delivery_completed'], 'OrderCollectedCompletePayment.pay_status' => COMPLETE_STATUS]
        ]);

        $this->belongsTo('OrderPayment', [
            'className' => 'Orders',
            'foreignKey' => 'order_id',
            'conditions' => ['owe_status' => COMPLETE_STATUS, 'charg_status' => COMPLETE_STATUS]
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['payment_id'], 'Payments'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }

    public function searchOrder($code){
        $this->keyWord = $code;
        $options['contain']['Orders'] = function($q){
            return $q->where([['OR' => [
                "orderId LIKE '%".$this->keyWord."%'",
                "shopId LIKE '%".$this->keyWord."%'"
            ]]])->orderDesc('created');
        };

        $paymentDetails = $this->find('all',$options)->enableHydration(false)->toArray();
        $payment_ids = [];
        if(count($paymentDetails) > 0){
            $payment_ids = array_column($paymentDetails, 'payment_id');
        }
        return $payment_ids;
    }

    public function getPaymentDetail($paymentId){
        $options['fields'] = ['id','modified'];
        $options['conditions']['payment_id'] = $paymentId;
        $options['contain'] = [
            'Orders' => function($q){
                return $q->select(['id', 'orderId', 'total', 'postage', 'sub_fee', 'discount', 'collection_fee', 'protect_fee','transfer_fee']);
            }
        ];
        $options['order'] = ['Orders.id' => 'DESC'];
        return $this->find('all', $options);
    }


    public function getPaymentDetailForWeb($paymentId){
        $options['fields'] = ['id','modified', 'is_update_status'];
        $options['conditions']['payment_id'] = $paymentId;
        $options['contain'] = [
            'Orders' => ['fields' => [
                'pay_status','money_collection', 'address','id', 'orderId', 'address_id', 'shopId','ward_id', 'district_id', 'city_id', 'created', 'finished','order_status',
                'service_pack_id', 'total', 'postage', 'sub_fee', 'discount', 'collection_fee', 'protect_fee','transfer_fee', 'receiver_name', 'arr_receiver_phone'
            ],'SenderAddresses' => ['fields' => ['address', 'ward_id', 'district_id', 'city_id'],
            'Cities' => ['fields' => ['id', 'name']],
            'Districts' => ['fields' => ['id', 'name']],
            'Wards' => ['fields' => ['id', 'name']]]
            ,'Districts' => ['fields' => ['id','name']], 'Cities' => ['fields' => ['id', 'name']], 'ServicePacks' => ['fields' => ['name']]]
        ];
        $options['order'] = ['Orders.id' => 'DESC'];
        return $this->find('all', $options);
    }

    public function confirmPaid($ids, $mailComponent, $excelComponent){
        $this->getConnection()->begin();
        $flag = true;
        try{
            $paymentId = "";
            foreach ($ids as $id){
                $paymentDetail = $this->get($id);
                $paymentDetail->is_update_status = 1;
                $paymentId = $paymentDetail->payment_id;

                $order = $this->Orders->get($paymentDetail->order_id);
                $order->owe_status = COMPLETE_STATUS;
                $order->pay_status = COMPLETE_STATUS;
                if(!$this->save($paymentDetail)){
                    $this->errorException();
                }

                if(!$this->Orders->save($order)){
                    $this->errorException();
                }
//
            }
            $this->getConnection()->commit();
        }catch (\Exception $e){
            $flag = false;
            $this->getConnection()->rollback();
        }
        $this->Payments->checkAndUpdateStatus($paymentId, $mailComponent, $excelComponent);
        return $flag;
    }

    public function confirmRemove($ids){
        $this->getConnection()->begin();
        $flag = true;
        try{
            foreach ($ids as $id){
                $paymentDetail = $this->get($id);
                $order = $this->Orders->get($paymentDetail->order_id);
                $order->awaiting_payment = NO_COMPLETE_STATUS;
                if(!$this->delete($paymentDetail)){
                    $this->errorException();
                }

                if(!$this->Orders->save($order)){
                    $this->errorException();
                }

            }
            $this->getConnection()->commit();
        }catch (Exception $e){
            $flag = false;
            $this->getConnection()->rollback();
        }
        return $flag;
    }
    
    public function getIds($paymentId){
        $options['fields'] = ['id'];
        $options['conditions'] = [
            'payment_id' => $paymentId,
            'is_update_status' => NO_COMPLETE_STATUS
        ];
        $paymentDetails = $this->find('all', $options)->disableHydration(true);
        $ids = [];
        if(count($paymentDetails->toArray()) > 0){
            $ids = array_column($paymentDetails->toArray(), 'id');
        }
        return $ids;
    }

    public function getOrderIds($paymentId){
        $options['fields'] = ['id' => 'order_id'];
        $options['conditions'] = [
            'payment_id' => $paymentId,
            'is_update_status' => NO_COMPLETE_STATUS
        ];
        return $this->find('all', $options)->disableHydration(true)->toArray();
    }

    public function getPaymentCashBook($data, $excelComponent, $mailComponent){
        $options['fields'] = ['id'];
        $options['contain'] = [
            'Orders' => [
                'fields' => [
                    'id',
                    'orderId',
                    'total', 
                    'postage',
                    'sub_fee',
                    'discount',
                    'collection_fee',
                    'protect_fee',
                    'transfer_fee',
                    'order_status'
                ],
            ],
            'Payments' => [
                'fields' => ['id', 'customer_id'],
                'Customers' => [
                    'fields' => ['id', 'user_id'],
                    'Users' => ['fields' => ['fullname', 'email']]
                ],
            ],
        ];
        $options['conditions'] = ['PaymentDetails.is_update_status' => 1];
        $options['order'] = ['PaymentDetails.id' => 'DESC'];
        $options = $this->commonSearchDate($options, $data);
        $orders =  $this->find('all', $options)->toArray();
        if(count($orders) > 0){
            $attachFile = $excelComponent->getFilenameCashBook($orders, true);
            $flag = $mailComponent->sendMailCashBook($attachFile, PAYMENT_TITLE);
            if (!$flag) {
                $this->errorException();
            }
            unlink($attachFile);
        }
    }

}
