<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeliveryTrips Model
 *
 * @property \App\Model\Table\EmployeesTable&\Cake\ORM\Association\BelongsTo $Employees
 * @property \App\Model\Table\DeliveryTripDetailsTable&\Cake\ORM\Association\HasMany $DeliveryTripDetails
 *
 * @method \App\Model\Entity\DeliveryTrip get($primaryKey, $options = [])
 * @method \App\Model\Entity\DeliveryTrip newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DeliveryTrip[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryTrip|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeliveryTrip saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeliveryTrip patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryTrip[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryTrip findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DeliveryTripsTable extends TripsTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('delivery_trips');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Employees', [
            'foreignKey' => 'employee_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('DeliveryTripDetails', [
            'foreignKey' => 'delivery_trip_id'
        ]);


        $this->hasMany('TripDetails', [
            'className' => 'DeliveryTripDetails',
            'foreignKey' => 'delivery_trip_id'
        ]);

        $this->hasMany('CompleteOrders', [
            'className' => 'DeliveryTripDetails',
            'foreignKey' => 'delivery_trip_id',
            'conditions' => ['CompleteOrders.is_update_status' => COMPLETE_STATUS]
        ]);

        $this->hasMany('DeliveryOrders', [
            'className' => 'DeliveryTripDetails',
            'foreignKey' => 'delivery_trip_id',
            'conditions' => ['DeliveryOrders.order_status' => $this->orderConstant['delivery_in_progress']]
        ]);

        $this->hasMany('DeliveryCompleteOrders', [
            'className' => 'DeliveryTripDetails',
            'foreignKey' => 'delivery_trip_id',
            'conditions' => ['DeliveryCompleteOrders.order_status' => $this->orderConstant['delivery_completed']],
            'joinType' => 'INNER'
        ]);

        $this->hasMany('FirstDeliveryCompleteOrders', [
            'className' => 'DeliveryTripDetails',
            'foreignKey' => 'delivery_trip_id',
            'conditions' => ['FirstDeliveryCompleteOrders.order_status' => $this->orderConstant['delivery_completed']],
            'limit' => 1,
            'joinType' => 'INNER',
        ]);



        $this->hasMany('FailedOrders', [
            'className' => 'DeliveryTripDetails',
            'foreignKey' => 'delivery_trip_id',
            'conditions' => ['FailedOrders.order_status IN' => [$this->orderConstant['stored'], $this->orderConstant['pending']]]
        ]);

        $this->hasMany('ReturnOrders', [
            'className' => 'DeliveryTripDetails',
            'foreignKey' => 'delivery_trip_id',
            'conditions' => ['ReturnOrders.order_status' => $this->orderConstant['return']]
        ]);

        $this->hasMany('CheckedOrders', [
            'className' => 'DeliveryTripDetails',
            'foreignKey' => 'delivery_trip_id',
            'conditions' => ['CheckedOrders.order_status IN' => [$this->orderConstant['stored'], $this->orderConstant['pending'], $this->orderConstant['return']], 'is_check' => 1]
        ]);

        $this->hasMany('NoCheckOrders', [
            'className' => 'DeliveryTripDetails',
            'foreignKey' => 'delivery_trip_id',
            'conditions' => ['NoCheckOrders.order_status IN' => [$this->orderConstant['stored'], $this->orderConstant['pending'], $this->orderConstant['return']], 'is_check' => 0]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['employee_id'], 'Employees'));

        return $rules;
    }

    public function search($data){

        $options['conditions'] = [];
        $options['contain'] = [
            'Employees' => [
                'fields' => ['employee_id'],
                'Users' => ['fields' => ['fullname', 'phone']],
            ],
            'TripDetails' => function($q){
                return $q->select(
                    [
                        'delivery_trip_id',
                        'total_orders' => $q->func()->count('*'),
                    ]
                )->group(['delivery_trip_id']);
            },
            'CompleteOrders' => function($q){
                return $q->select(
                    [
                        'delivery_trip_id',
                        'total_complete_orders' => $q->func()->count('*'),
                    ]
                )->group(['delivery_trip_id']);
            },


        ];
        if(!empty($data['id'])){
            $options['conditions']['DeliveryTrips.id'] = $data['id'];
        }
        if(!empty($data['orderId'])){
            //find id with orderId
            $deliveryIds = $this->DeliveryTripDetails->searchOrderCodeInTrip($data['orderId']);
            if(count($deliveryIds) > 0){
                $options['conditions']['DeliveryTrips.id IN'] = $deliveryIds;
            }else{
                $options['conditions']['DeliveryTrips.id'] = null;
            }
        }

        if(!empty($data['employee'])){
            $keyWord = $data['employee'];
            $dataSearchLike = ['OR' => [
                "Employees.employee_id LIKE '%".$keyWord."%'",
                "Users.fullname LIKE '%".$keyWord."%'",
                "Users.phone LIKE '%".$keyWord."%'"
            ]];

            array_push($options['conditions'], $dataSearchLike);
        }
        if(isset($data['status']) && strlen($data['status']) > 0){
            $options['conditions']['DeliveryTrips.status'] = $data['status'];
        }

        $options['order'] = ['DeliveryTrips.id' => 'DESC'];

        $options = $this->commonSearchDate($options, $data, COMPLETE_STATUS);

        if($this->getRoleId() === SHIPPER){
            $options['conditions']['DeliveryTrips.employee_id'] = $this->Employees->getIdByUserId($this->getUserId());
        }

        return $this->find('all', $options);

    }

    public function dataContain(){
        return [
            'DeliveryCompleteOrders' => [
                'fields' => ['delivery_trip_id','total_orders' => 'count(delivery_trip_id)'],
                'OrderCollect'
                    => function ($q){
                        return $q->select([
                            'total_cod' => $q->func()->sum('total'),
                            'total_sub_fee' => $q->func()->sum('sub_fee')
                        ]);
                    },
            ]
        ];
    }

    public function getCollectMoney($data){
        $options['conditions'] = [];
        $options['contain'] = [
            'Employees' => [
                'fields' => ['employee_id'],
                'Users' => ['fields' => ['fullname', 'phone']],
            ],
            'DeliveryCompleteOrders' => [
                'OrderCollect' => ['fields' => ['id', 'total', 'sub_fee']]
            ]
        ];
        $options['order'] = ['DeliveryTrips.id' => 'DESC'];
        if(!empty($data['employee_id'])){
            $options['conditions']['DeliveryTrips.employee_id'] = $data['employee_id'];
        }
        $deliveryTripIds = $this->DeliveryTripDetails->getDeliveryTripIdsHaveCollectOrders();
        if (count($deliveryTripIds) > 0) {
            $options['conditions']['DeliveryTrips.id IN'] = $deliveryTripIds;
        }else{
            $options['conditions']['DeliveryTrips.id'] = 0;
        }
        $options = $this->commonSearchDate($options, $data);

        return $this->find('all', $options);
    }
    
    public function getDeliveryTripInfo($id){
        $options['contain'] = $this->dataContain();
        return $this->get($id, $options);
    }
}
