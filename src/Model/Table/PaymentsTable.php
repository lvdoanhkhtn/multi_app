<?php
namespace App\Model\Table;

use Cake\Core\Exception\Exception;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Payments Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CustomersTable|\Cake\ORM\Association\BelongsTo $Customers
 * @property \App\Model\Table\PaymentDetailsTable|\Cake\ORM\Association\HasMany $PaymentDetails
 *
 * @method \App\Model\Entity\Payment get($primaryKey, $options = [])
 * @method \App\Model\Entity\Payment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Payment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Payment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Payment|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Payment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Payment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Payment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PaymentsTable extends CommonTable
{

    public $components = ['Mail'];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('PaymentDetails', [
            'foreignKey' => 'payment_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('PaymentDetails', [
            'foreignKey' => 'payment_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('NoUpdatedPaymentDetails', [
            'className' => 'PaymentDetails',
            'foreignKey' => 'payment_id',
            'conditions' => ['is_update_status' => NO_COMPLETE_STATUS],
        ]);

        $this->hasMany('OrderPaymentDetails', [
            'className' => 'PaymentDetails',
            'foreignKey' => 'payment_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        $validator
            ->requirePresence('is_update_status', 'create')
            ->notEmpty('is_update_status');

        $validator
            ->scalar('note')
            ->maxLength('note', 1000)
            ->allowEmpty('note');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));
        return $rules;
    }

    public function search($data)
    {
        ini_set('memory_limit', '256M');
        $options['order'] = array('status' => 'ASC', 'Payments.id' => 'DESC');
        if (empty($data['isAdmin'])) {
            $options['conditions']['Payments.status'] = 1;
        }
        if (!empty($data['customer_id'])) {
            $options['conditions']['customer_id'] = $data['customer_id'];
        }
        if (!empty($data['user_id'])) {
            $options['conditions']['Payments.user_id'] = $data['user_id'];
        }
        if (isset($data['status']) && strlen($data['status']) > 0) {
            $options['conditions']['Payments.status'] = $data['status'];
        }

        $commonFields = ['fields' => ['id', 'total', 'sub_fee', 'postage', 'discount', 'collection_fee', 'protect_fee', 'transfer_fee']];
        $collectedCommonFields = ['fields' => ['id', 'total', 'sub_fee', 'money_collection']];
        $options['contain'] = [
            'Customers' => ['Users' => ['fields' => ['id', 'fullname']]],
            'Users' => ['fields' => ['id', 'fullname']],
            'PaymentDetails' => [
                'fields' => ['payment_id'],
                'OrderCompletePayment' => $commonFields,
                'OrderReturnPayment' => $commonFields,
                'OrderCollectedCompletePayment' => $collectedCommonFields,
            ],
        ];

//        $options['conditions']['Payments.id'] = 2006;

        if (!empty($data['code'])) {
            $payment_ids = $this->PaymentDetails->searchOrder($data['code']);
            if (count($payment_ids) > 0) {
                $options['conditions']['Payments.id IN'] = $payment_ids;
            } else {
                $options['conditions']['Payments.id'] = -1;

            }
        }

        $options = $this->commonSearchDate($options, $data, COMPLETE_STATUS);

        return $this->find('all', $options);
    }

    public function addPayments($userIds)
    {
        // $time_start = microtime(true);
        $success = false;
        $this->getConnection()->begin();
        try {
            try {
                foreach ($userIds as $userId) {
                    $orderDatas = $this->PaymentDetails->Orders->getOrderPaymentWithUser($userId);
                    $paymentEntity = $this->newEntity();
                    $paymentEntity->customer_id = $this->Customers->getIdByUserId($userId);
                    $paymentEntity->user_id = $this->getUserId();
                    if ($this->save($paymentEntity)) {
                        $orderIds = $this->addElementToAllArray($orderDatas, 'payment_id', $paymentEntity->id);
                        $orders = $this->addElementToAllArray($orderDatas, 'awaiting_payment', COMPLETE_STATUS);
                        $paymentDetailEntities = $this->PaymentDetails->newEntities($orderIds);
                        $orderEntities = $this->PaymentDetails->Orders->newEntities($orders);
                        if (!$this->PaymentDetails->saveMany($paymentDetailEntities)) {
                            $this->errorException();
                        }
                        if (!$this->PaymentDetails->Orders->saveMany($orderEntities)) {
                            $this->errorException();
                        }
                    } else {
                        $this->errorException();
                    }
                }
                $success = true;
                $this->getConnection()->commit();

            } catch (Exception $e) {
                $this->getConnection()->rollback();
            }
        } catch (\PDOException $pdo) {
            $this->getConnection()->rollback();
        }
        return $success;
        // echo round(microtime(true) - TIME_START, 3);die();
    }

    public function getInfo($id)
    {
        $options['contain'] = [
            'Customers' => [
                'Users' => ['fields' => ['fullname', 'phone', 'email']],
                'MasterAddress' => ['Wards', 'Districts', 'Cities'],
                'BankAccounts' => ['Banks'],
            ],
        ];
        return $this->get($id, $options);
    }

    public function getPaymentInfo($id)
    {
        $commonFields = ['fields' => ['id', 'total', 'sub_fee', 'postage', 'discount', 'collection_fee', 'protect_fee', 'transfer_fee']];
        $options['contain'] = [
            'Customers' => [
                'Users' => ['fields' => ['fullname', 'phone', 'email']],
                'MasterAddress' => ['Wards', 'Districts', 'Cities'],
                'BankAccounts' => ['Banks'],
            ],
            'PaymentDetails' => [
                'fields' => ['payment_id'],
                'OrderCompletePayment' => $commonFields,
                'OrderReturnPayment' => $commonFields,
            ],
        ];
        return $this->get($id, $options);
    }

    public function checkAndUpdateStatus($id, $mailComponent, $excelComponent)
    {
        $flag = false;
        try {
            $options['contain'] = ['NoUpdatedPaymentDetails', 'Customers' => ['Users' => ['fields' => ['fullname', 'email']]]];
            $options['conditions']['Payments.id'] = $id;
            $payment = $this->find('all', $options)->first();
            if (empty($payment['no_updated_payment_details'])) {
                $payment->status = COMPLETE_STATUS;
                if ($this->save($payment)) {
                    //send mail to user;
                    $user = $payment->customer->user;
                    $fullName = $user->fullname;
                    $toMail = $this->getEmailToSend($user->email);
                    $paymentDetails = $this->PaymentDetails->getPaymentDetailForWeb($id);
                    $attachFile = $excelComponent->getFilenameInPaymentByExcel($paymentDetails);
                    $flag = $mailComponent->sendMailPaymentDetail($paymentDetails, $toMail, $fullName, $excelComponent, $attachFile);
                    if (!$flag) {
                        $this->errorException();
                    }
                    unlink($attachFile);
                }
            }
        } catch (RecordNotFoundException $e) {
        }
        return $flag;
    }

    public function deletePayment($id)
    {
        $this->getConnection()->begin();
        $success = false;
        try {
            try {
                $options['contain'] = ['PaymentDetails'];
                $payments = $this->get($id, $options);
                $tbPaymentDetails = $this->PaymentDetails;
                $paymentDetailIds = $tbPaymentDetails->getIds($id);
                $orderDatas = $tbPaymentDetails->getOrderIds($id);
                $orders = $this->addElementToAllArray($orderDatas, 'awaiting_payment', NO_COMPLETE_STATUS);
                $orderEntities = $tbPaymentDetails->Orders->newEntities($orders);
                if (!$tbPaymentDetails->deleteAll(['PaymentDetails.id IN' => $paymentDetailIds])) {
                    $this->errorException();
                }
                if (!$this->Delete($payments)) {
                    $this->errorException();
                }
                if ($tbPaymentDetails->Orders->saveMany($orderEntities)) {
                    $success = true;
                    $this->getConnection()->commit();
                }
            } catch (Exception $e) {
                $this->getConnection()->rollback();
            }
        } catch (\PDOException $pdo) {
            $this->getConnection()->rollback();
        }
        return $success;
    }
}
