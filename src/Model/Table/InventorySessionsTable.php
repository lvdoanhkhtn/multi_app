<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * InventorySessions Model
 *
 * @property \App\Model\Table\CreatersTable&\Cake\ORM\Association\BelongsTo $Creaters
 * @property \App\Model\Table\ModifiersTable&\Cake\ORM\Association\BelongsTo $Modifiers
 *
 * @method \App\Model\Entity\InventorySession get($primaryKey, $options = [])
 * @method \App\Model\Entity\InventorySession newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\InventorySession[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InventorySession|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InventorySession saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InventorySession patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InventorySession[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\InventorySession findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InventorySessionsTable extends CommonTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('inventory_sessions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');


        $this->hasMany('InventorySessionDetails', [
            'foreignKey' => 'inventory_session_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('CheckedInventorySessionDetails', [
            'className' => 'InventorySessionDetails',
            'foreignKey' => 'inventory_session_id',
            'joinType' => 'INNER',
            'conditions' => ['status' => COMPLETE_STATUS]
        ]);

        $this->hasMany('NoCheckedInventorySessionDetails', [
            'className' => 'InventorySessionDetails',
            'foreignKey' => 'inventory_session_id',
            'joinType' => 'INNER',
            'conditions' => ['status' => NO_COMPLETE_STATUS]
        ]);

        $this->belongsTo('Creater', [
            'className' => 'Users',
            'foreignKey' => 'creater_id',
        ]);

        $this->belongsTo('Modifier', [
            'className' => 'Users',
            'foreignKey' => 'modifier_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('type')
            ->allowEmptyString('type');

        $validator
            ->integer('status')
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {

        return $rules;
    }

    public function add($data){
        $success = true;
        $this->getConnection()->begin();
        try{
            //create inventor session.
            $inventory = $this->newEntity($data);
            $inventory->creater_id = $this->getUserId();
            if(!$this->save($inventory)){
                $this->errorException();
            }

            switch ($data['type_id']){
                CASE STORE_TYPE:
                    $orderIds =  $this->InventorySessionDetails->Orders->getOrderIdsWithStatus($this->orderConstant['stored']);
                    break;
                CASE RETURN_TYPE:
                    $orderIds =  $this->InventorySessionDetails->Orders->getOrderIdsWithStatus($this->orderConstant['return']);
                    break;
                CASE PENDING_TYPE:
                    $orderIds =  $this->InventorySessionDetails->Orders->getOrderIdsWithStatus($this->orderConstant['pending']);
                    break;

            }
            $inventorySessionDetails = $this->InventorySessionDetails->addElementToAllArray($orderIds, 'inventory_session_id', $inventory->id);
            $inventorySessionDetails = $this->InventorySessionDetails->newEntities($inventorySessionDetails);
            if(!$this->InventorySessionDetails->saveMany($inventorySessionDetails)){
                $this->getConnection()->rollback();
            }
            $this->getConnection()->commit();

        }catch(Exception $e){
            $success = false;
            $this->getConnection()->rollback();
        }
        return $success;
    }
    
    public function search($data){
        $commonField = ['fields' => ['id', 'inventory_session_id']];
        $options['contain'] = [
            'InventorySessionDetails' => $commonField,
            'CheckedInventorySessionDetails' => $commonField,
            'Creater' => ['fields' => ['fullname']],
            'Modifier' => ['fields' => ['fullname']]
        ];
        if(!empty($data['type'])){
            $options['conditions']['type_id'] = $data['type'];
        }
        if(!empty($data['id'])){
            $options['conditions']['InventorySessions.id'] = $data['id'];
        }
        if(!empty($data['status'])){
            $options['conditions']['status'] = $data['status'];
        }
        if(!empty($data['creater_id'])){
            $options['conditions']['creater_id'] = $data['creater_id'];
        }
        if(!empty($data['modifier_id'])){
            $options['conditions']['modifier_id'] = $data['modifier_id'];
        }
        $options = $this->commonSearchDate($options, $data);
        $options['order'] = ['InventorySessions.id' => 'DESC'];
        $inventorySessions = $this->find('all', $options);
        return $inventorySessions;
    }

    public function getById($id){
        $orderAttr = ['fields' => ['charg_status', 'collect_status', 'id', 'orderId', 'shopId','address_id', 'ward_id', 'district_id', 'city_id', 'created',
            'order_status', 'service_pack_id', 'address', 'receiver_name', 'arr_receiver_phone', 'note', 'content', 'total', 'weight', 'history', 'first_stored_time', 'stored_time'],
            'OrderNotes' => function($q){
                return $q->select([
                    'created', 'note', 'order_id'
                ])->order(['id' => 'DESC']);
            },
            'Users' => ['fields' => ['fullname', 'phone']],
            'SenderAddresses' => ['fields' => ['id','address', 'ward_id', 'district_id', 'city_id'],
                'Cities' => ['fields' => ['id', 'name']],
                'Districts' => ['fields' => ['id', 'name']],
                'Wards' => ['fields' => ['id', 'name']]]
            ,'Districts' => ['fields' => ['id','name']], 'Cities' => ['fields' => ['id', 'name']], 'ServicePacks' => ['fields' => ['id','name']]];

            $options['contain'] = [
                'InventorySessionDetails' => function($q) use ($orderAttr){
                    return $q->contain(['Orders' => $orderAttr])->orderDesc('InventorySessionDetails.modified');
                }


            ];
        return $this->get($id, $options);
    }
    
    public function checkAndUpdateComplete($id){
        $inventorySession = $this->checkComplete($id);
        if(empty($inventorySession['no_checked_inventory_session_details'])){
            $inventorySession->status = COMPLETE_STATUS;
            $inventorySession->modifier_id = $this->getUserId();
            $this->save($inventorySession);
        }
    }


    public function checkComplete($id){
        $commonField = ['fields' => ['id', 'inventory_session_id']];
        $options['contain'] = [
            'NoCheckedInventorySessionDetails' => $commonField
        ];
        $options['conditions']['id'] = $id;
        $inventorySession = $this->find('all', $options)->first();
        return $inventorySession;
    }

    
}
