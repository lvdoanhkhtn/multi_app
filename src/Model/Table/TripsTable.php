<?php
/**
 * Created by PhpStorm.
 * User: doanh.lv
 * Date: 6/23/19
 * Time: 9:28 AM
 */

namespace App\Model\Table;

use Cake\Core\Exception\Exception;
use Cake\ORM\Table;

class TripsTable extends CommonTable
{

    public function getById($data, $type = DELIVERY_TRIP)
    {

        $options['conditions'][$this->getRegistryAlias() . '.id'] = $data['id'];

        $orderAttr = ['fields' => ['id', 'orderId', 'shopId', 'address_id', 'ward_id', 'district_id', 'city_id', 'created', 'order_status', 'service_pack_id', 'address', 'receiver_name', 'arr_receiver_phone', 'note', 'content', 'total', 'weight', 'history', 'finished', 'postage', 'sub_fee', 'discount', 'collection_fee', 'protect_fee', 'transfer_fee'],

            'OrderNotes' => function ($q) {
                return $q->select([
                    'created', 'note', 'order_id',
                ])->order(['id' => 'DESC'])->limit(1);
            },
            'Users' => ['fields' => ['fullname', 'phone']],
            'SenderAddresses' => ['fields' => ['id', 'address', 'ward_id', 'district_id', 'city_id'],
                'Cities' => ['fields' => ['id', 'name']],
                'Districts' => ['fields' => ['id', 'name']],
                'Wards' => ['fields' => ['id', 'name']]]
            , 'Districts' => ['fields' => ['id', 'name']], 'Cities' => ['fields' => ['id', 'name']], 'ServicePacks' => ['fields' => ['id', 'name']]];
        switch ($type) {
            case PICKUP_TRIP:
                $pickupTripField = ['fields' => ['pickup_trip_id','count' => 'COUNT(pickup_trip_id)']];
                $options['contain'] = [
                    'CompleteOrders' => $pickupTripField,
                    'PickupOrders' => $pickupTripField,
                    'PickupCompletedOrders' => $pickupTripField,
                    'PickupStoredOrders' => $pickupTripField,
                    'CancelOrders' => $pickupTripField,
                ];
                break;
            case DELIVERY_TRIP:
                $options['contain'] = [
                    'DeliveryOrders' => ['sort' => ['DeliveryOrders.id' => 'DESC'], 'Orders' => $orderAttr],
                    'CompleteOrders' => ['Orders' => $orderAttr],
                    'DeliveryCompleteOrders' => ['Orders' => $orderAttr],
                    'FailedOrders' => ['Orders' => $orderAttr],
                    'ReturnOrders' => ['Orders' => $orderAttr],
                    'CheckedOrders' => ['Orders' => ['fields' => ['id', 'orderId']]],
                    'NoCheckOrders' => ['Orders' => ['fields' => ['id', 'orderId']]],
                ];
                break;
            case RETURN_TRIP:
                $options['contain'] = [
                    'CompleteOrders' => ['Orders' => $orderAttr],
                    'ReturnOrders' => ['Orders' => $orderAttr],
                    'ReturnCompleteOrders' => ['Orders' => $orderAttr],
                ];
                break;
        }
        return  $this->find('all', $options)->enableBufferedResults(false)->first();
    }

    public function errorException()
    {
        throw new Exception('Error');
    }

    public function getIdByAddressId($addressId)
    {
        $options['conditions']['address_id'] = $addressId;
        $options['conditions']['status'] = 0;
        $id = "";
        $address = $this->find('all', $options)->first();
        if (!empty($address->id)) {
            $id = $address->id;
        }
        return $id;

    }

    public function getInfo($id)
    {
        $field = str_replace("s", "", $this->getTable()) . '_id';
        $options['contain'] = [
            'Employees' => ['fields' => ['employee_id'], 'Users' => ['fields' => ['fullname']]],
            'TripDetails' => function ($q) use ($field) {
                return $q->select(
                    [
                        $field,
                        'total_orders' => $q->func()->count('*'),
                    ]
                )->group([$field]);
            },
        ];
        return $this->get($id, $options);
    }

    public function performance($data, $contains)
    {
        $options['contain'] = $contains;
        $options['conditions'] = [];
        $options = $this->commonSearchDate($options, $data, 0);
        $options['order'] = [$this->getRegistryAlias() . '.id' => 'DESC'];
        return $this->find('all', $options)->enableBufferedResults(false);
    }

}
