<?php
namespace App\Model\Table;

use Cake\Core\Exception\Exception;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Roles Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\HasMany $Users
 *
 * @method \App\Model\Entity\Role get($primaryKey, $options = [])
 * @method \App\Model\Entity\Role newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Role[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Role|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Role|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Role patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Role[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Role findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RolesTable extends CommonTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('roles');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Users', [
            'foreignKey' => 'role_id'
        ]);

        $this->hasMany('Permissions', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');
        
        return $validator;
    }


    public function getList($lang){
        $options['fields'] = [
            'id',
            'name' => ($lang == "vi" || $lang == "vn") ? 'name' : $lang.'_'.'name'
        ];
        $options['conditions'] = ['active' => 1];
        return $this->find('list', $options)->toArray();
    }

    public function updateBasePermissionWithRole($id, $data){
        $options['contain'] = ['Permissions'];
        $role = $this->get($id, $options);
        $this->getConnection()->begin();
        $flag = true;
        try{
            $permissionData = $this->Permissions->find('all', ['conditions' => ['role_id' => $id]])->first();
            if(empty($permissionData->action_id)){
                $permission['action_id'] = $this->Permissions->Actions->getIdsWithBasicPermission();
                $permission['role_id']  = $id;
                $permission = $this->Permissions->newEntity($permission);
                if(!empty($role->permissions[0]->id)){
                    $permission->id = $role->permissions[0]->id;
                }
                if(!$this->Permissions->save($permission)){
                    $this->errorException();
                }
            }
            $role = $this->patchEntity($role, $data);
            if($this->save($role)){
                $this->getConnection()->commit();
            }else{
                $this->getConnection()->rollback();
            }
        }catch (Exception $e){
            $flag = false;
            $this->getConnection()->rollback();
        }
        return $flag;

    }
}
