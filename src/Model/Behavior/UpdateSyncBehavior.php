<?php
/**
 * Created by PhpStorm.
 * User: leminhtoan
 * Date: 3/27/17
 * Time: 14:36
 */

namespace App\Model\Behavior;


use App\Libs\ConfigUtil;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\Http\Session;

class UpdateSyncBehavior extends Behavior
{
    /**
     * Before update event
     *
     * @param Event $event
     * @param EntityInterface $entity
     * @param \ArrayObject $options the options passed to the save method
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $now = new \DateTime();
//        $user = ConfigUtil::getLoginUser();
        $this->Session = new Session();
        $userId = $this->Session->read('Auth.User.id');
        if($entity->isNew()){
            // Insert
            $entity->set('created_at', $now);
            $entity->set('created_by', $userId);
            $entity->set('updated_at', $now);
            $entity->set('updated_by', $userId);
        }else{
            // Update
            $entity->set('updated_at', $now);
            $entity->set('updated_by', $userId);
        }
    }

}