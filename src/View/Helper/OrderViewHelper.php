<?php
/**
 * Created by PhpStorm.
 * User: leminhtoan
 * Date: 3/13/17
 * Time: 18:47
 */

namespace App\View\Helper;


use App\Libs\ConfigUtil;
use App\Libs\ValueUtil;
use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class OrderViewHelper extends Helper
{
    public $helpers = ['Html', 'Url', 'Form', 'Paginator'];

    private function validEmptyOrder($order){
        $flag = true;
        if (empty($order['content']))
            $flag = false;
        if (isset($order['weight']) && strlen($order['weight']) === 0)
            $flag = false;
        if (isset($order['money_collection']) && strlen($order['money_collection']) === 0)
            $flag = false;
        if (empty($order['receiver_name']))
            $flag = false;
        if (empty($order['arr_receiver_phone']))
            $flag = false;
        if (empty($order['address']))
            $flag = false;
        if (empty($order['city_id']))
            $flag = false;
        if (empty($order['district_id']))
            $flag = false;
        if (isset($order['payment']) && strlen($order['payment']) === 0)
            $flag = false;
        return $flag;
    }

    public function checkNumber($number){
        $number_regex = '/^[0-9]+$/';
        return preg_match($number_regex, $number);
    }

    public function checkNumberic($number){
        $number_regex = '/^[0-9.,]+$/';
        return preg_match($number_regex, $number);
    }

    public function formatNumber($number){
        $number = str_replace(",","",strip_tags($number));
        return str_replace(".","",$number);
    }

    private function validNumber($order){
        $flag = true;
        if(!$this->checkNumber($order['city_id']))
            $flag = false;
        if(!$this->checkNumber($order['district_id']))
            $flag = false;
        if(!$this->checkNumber($order['payment']))
            $flag = false;
        if(!$this->checkNumber($order['service_pack_id']))
            $flag = false;
        if(!$this->checkNumberic($order['money_collection']))
            $flag = false;
        if(!$this->checkNumberic($order['weight']))
            $flag = false;
        //echo $order['money_collection'];die;
        return $flag;
    }


    public function checkShopOrderCode($shopId, $userId){
        $this->Orders = TableRegistry::get('Orders');
        return !$this->Orders->checkOrderCode($shopId, $userId);
    }

    public function checkDistrict($districtId, $cityId){
        $this->Districts = TableRegistry::get('Districts');
        $options['conditions'] = [
            'id' => $districtId,
            'city_id' => $cityId
        ];
        $districtCount = $this->Districts->find('all', $options)->count();
        return !empty($districtCount) ? true : false;

    }


    public function validOrder($order){
        $flag = true;
        if(!$this->validEmptyOrder($order))
            $flag = false;
        if(!$this->validNumber($order))
            $flag = false;
    //        if(!$this->isNumberPhone($order['arr_receiver_phone']))
    //            $flag = false;
        if(!$this->checkDistrict($order['district_id'], $order['city_id'])){
            $flag = false;
        }
        if(!$this->checkShopOrderCode($order['shopId'], $order['user_id']))
            $flag = false;
        return $flag;
    }
    

}