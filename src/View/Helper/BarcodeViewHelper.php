<?php
/**
 * Created by PhpStorm.
 * User: leminhtoan
 * Date: 3/13/17
 * Time: 18:47
 */

namespace App\View\Helper;
use Cake\View\Helper;
use Picqer\Barcode\BarcodeGeneratorSVG;


class BarcodeViewHelper extends Helper
{
    public function generateBarcode($orderId){
        $generator = new BarcodeGeneratorSVG();
        echo $generator->getBarcode($orderId, $generator::TYPE_CODE_128, 2, 50);
    }

    public function customGenerateBarcode($order_code){
        $barcodeobj = new TCPDFBarcode($order_code, 'C128');
        $content = $barcodeobj->getBarcodeSVGcode(2, 30, array(0,0,0));
        return $content;
    }

    public function generateBarcodeV2($orderId){
        $generator = new BarcodeGeneratorSVG();
        echo $generator->getBarcode($orderId, $generator::TYPE_CODE_128, 1, 50);
    }

}