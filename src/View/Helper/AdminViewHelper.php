<?php

/**
 * Created by PhpStorm.
 * User: leminhtoan
 * Date: 3/13/17
 * Time: 18:47
 */

namespace App\View\Helper;


use App\Libs\ConfigUtil;
use App\Libs\ValueUtil;
use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class AdminViewHelper extends Helper
{
    public $helpers = ['Html', 'Url', 'Form', 'Paginator'];

    public function initialize(array $config)
    {
        $this->orderConstant = ValueUtil::get('order.order_constant');
        parent::initialize($config); // TODO: Change the autogenerated stub
    }

    /**
     * Create a group menu in side bar
     *
     * @param $menu
     * @return string
     */
    public function createMenu($menu)
    {
        $html = "<div class='panel panel-default'>";
        foreach ($menu as $group) {
            $html .= $this->createMenuGroup($group);
        }
        $html .= "</div>";
        return $html;
    }

    /**
     * Create group menu in side bar
     *
     * @param $group
     * @return string
     */
    public function createMenuGroup($group)
    {
        $html = "<div class='panel-heading-2'>" .
            "<h4 class='panel-title'>" .
            $group['groupName'] .
            "</h4>" .
            "</div>" .
            "<div class='list-menu'>";
        foreach ($group['pages'] as $page) {
            // Create url link
            $url = '';
            if (isset($page['controller'])) {
                $url = $this->Url->build(['controller' => $page['controller'], 'action' => $page['action'], '']);
            } elseif (isset($page['url'])) {
                $url = $this->Url->build($page['url'], true);
            }
            $target = '';
            if (isset($page['target']) && $page['target'] == "_blank") {
                $target = "target = '_blank'";
            }
            // Create HTML
            $html .= '<div class="list-group">' .
                "<a href='{$url}' class='list-group-item' $target>" .
                "<span class='bb-list-icon {$page['icon']}'></span>" .
                "{$page['title']}<br>" .
                "<span>{$page['extra']}</span>" .
                '</a>' .
                '</div>';
        }
        $html .= "</div>";
        return $html;
    }

    /**
     * Create paginate menu
     * @param $option
     * @return string
     */
    public function paginateMenuAndResult($option = array())
    {
        $show_count = isset($option['showCount']) ? $option['showCount'] : true;
        $resultName = isset($option['resultName']) ? $option['resultName'] : __('search_results');
        if ($show_count) {
            $pageParams = $this->Paginator->params();
            $html = '<div class="row"><div class="col-xs-6 col-sm-6 col-md-6">' .
                sprintf('<label class="control-label">%s :</label>', $resultName) . number_format($pageParams['count']) .  ' Items ' .
                number_format($pageParams['start']) . '-' . number_format($pageParams['end']) . '' .
                '</div></div>' .
                '<div class="row form-group"><div class="col-xs-6 col-sm-6 col-md-6">' .
                $this->paginateMenu($option) .
                '</div></div>';
        } else {
            $html = '<div class="col-xs-12">' .
                $this->paginateMenu($option) .
                '</div>';
        }
        return $html;
    }

    /**
     * Create paginate menu
     * @param $option
     * @return string
     */
    public function paginateMenu($option = array())
    {
        $paginateMenu = ConfigUtil::get('paginate_menu');
        $selectValues = array_combine($paginateMenu, $paginateMenu);
        $formOption = isset($option['form']) ? $option['form'] : '';
        $html = '<div class="paginate-menu pull-left" ' . $formOption . '>' .
            '<div class="input-group">
                        <span class="input-group-text block" id="sizing-addon2">' . sprintf('<label class="control-label">%s</label>', __('displayed_results') . ':') . '</span>' .
            $this->Form->select('limit', $selectValues, [
                'class' => 'form-control br0 paginate_menu btn-badge',
                'value' => $this->Paginator->param('perPage'),
                'data-form' => $formOption
            ]) .
            '</div>' .
            '</div>';
        return $html;
    }

    /**
     * Change value to text
     * @param $key, $value
     * @return $result
     */
    public function valueToText($key = '', $value = '')
    {
        if (empty($key) || (empty($value) && strlen($value) == 0)) {
            return null;
        }
        $result = ValueUtil::valueToText($key, $value);
        return $result;
    }

    public function formatDate($data)
    {
        return $data->format(ConfigUtil::get('date'));
    }

    public function formatDateTime($data)
    {

        return !empty($data) ? $data->format(ConfigUtil::get('date_time')) : '';
    }

    public function convertTextToDatetime($str)
    {
        $year = substr($str, 0, 4);
        $month = substr($str, 4, 2);
        $day = substr($str, 6, 2);
        $completeDay =  $year . '/' . $month . '/' . $day;
        if (!strtotime($completeDay)) {
            $completeDay = "";
        }
        return $completeDay;
    }

    public function loadDistrictFromView($cityId)
    {
        $this->Districts = TableRegistry::get('Districts');
        return $this->Districts->getDistrictByCityId($cityId);
    }

    public function loadWardFromView($districtId)
    {
        $this->Wards = TableRegistry::get('Wards');
        return $this->Wards->getWardByCityId($districtId);
    }

    public function getCurrencyCam($total)
    {
        return sprintf("<b>%s (%s USD) </b>", number_format($total * ConfigUtil::get('CAM_CURRENCY')), $total);
    }

    public function getCurrencyCamForPayment($total)
    {
        return sprintf("%s (%s USD)", number_format($total * ConfigUtil::get('CAM_CURRENCY')), $total);
    }

    public function getCustomCurrencyCam($total)
    {
        return sprintf("%s", number_format($total * ConfigUtil::get('CAM_CURRENCY')), $total);
    }

    public function getCustomCurrencyUsd($total)
    {
        return sprintf("(%s USD)", $total);
    }

    public function showActive($active)
    {
        $result = '';
        if (!empty($active)) {
            $result = '<div class="badge badge-success">Actived</div>';
        } else {
            $result = '<div class="badge badge-danger">Locked</div>';
        }
        return $result;
    }

    public function showComplete($active)
    {
        $result = '';
        if (!empty($active)) {
            $result = '<div class="badge badge-success">Completed</div>';
        } else {
            $result = '<div class="badge badge-warning">In Progress</div>';
        }
        return $result;
    }

    public function loadCityAndDistrict($cities, $label, $select = null)
    {
        $html = '<div class="form-group input select">' .
            '<label class="control-label" for="user-id">' . __($label) . '</label>' .
            '<select class="select2 form-control" name="' . $label . '">';
        $html .= '<optgroup label=""><option value="">' . __('select') . '</option></optgroup>';
        foreach ($cities as $city) {
            $html .= '<optgroup label="' . $city->name . '">';
            foreach ($city->districts as $district) {
                $html .= sprintf("<option %s value='%s'>%s</option>", ($select == $district->id) ? 'selected' : '', $district->id, $district->name);
            }
            $html .= '</optgroup>';
        }
        $html .= '</select></div>';
        return $html;
    }

    public function getBreadCum($data)
    {
        $link =
            $this->Html->link(__($data['parentLabel']), ['controller' => $data['parentController'], 'action' => 'index', $data['parentId']], ['class' => 'text-red']) . ' > '
            . $this->Html->link(__($data['label']), ['action' => 'index', $data['id']], ['class' => 'text-red']) . ' > '
            . '<span class="text-red">' . $data['name'] . '</span>';

        if (!empty($data['isChange'])) {
            $link .= '<span class="text-red"> > ' . __($data['action']) . '</span>';
        }
        return $link;
    }

    public function showDefault($default)
    {
        $result = '';
        if (!empty($default)) {
            $result = '<div class="badge badge-success">Default</div>';
        }
        return $result;
    }

    public function showStatusesOnOrder($orderStatuses)
    {
        $statusId = array_flip(ValueUtil::get('order.order_statuses_eng'));
        $codeColorOrderStatus = ValueUtil::get('order.code_color_order_statuses');
        foreach ($orderStatuses as $k => $v) {
            $codeColor = $codeColorOrderStatus[$k];
            echo sprintf('<div class="col-md-2"><button id="' . $statusId[$k] . '" type="button" style="background-color: ' . $codeColor . ';border-color: ' . $codeColor . ' " class="btnStatus btn btn-primary btn-badge text-left">%s <span class="badge" style="color: ' . $codeColor . '">%s</span></button></div>', $k, $v);
        }
    }

    public function arraySum($array, $index)
    {
        return array_sum(array_column($array, $index));
    }

    public function getCollect($data)
    {
        $data = json_decode(json_encode($data), true);
        $orderCollect = array_column($data, 'order_collect');
        $response['total_order'] = count($data);
        $response['total_cod'] = $this->arraySum($orderCollect, 'total');
        $response['total_sub_fee'] = $this->arraySum($orderCollect, 'sub_fee');
        return $response;
    }


    public function getLastUpdate($orderNote)
    {
        return sprintf("%s - %s", $this->formatDatetime($orderNote['created']), $orderNote['note']);
    }

    public function getServicePack($lang)
    {
        return TableRegistry::getTableLocator()->get('ServicePacks')->getList($lang, false);
    }

    public function getDay($date)
    {
        return $date->format('d');
    }

    public function getMonth($date)
    {
        return $date->format('m');
    }

    public function getYear($date)
    {
        return $date->format('Y');
    }

    public function getShopNameById($id)
    {
        return TableRegistry::getTableLocator()->get('Users')->get($id)->fullname;
    }
}
