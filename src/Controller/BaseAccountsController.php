<?php
/**
 * Created by PhpStorm.
 * Users: doanh.lv
 * Date: 8/1/19
 * Time: 9:27 AM
 */

namespace App\Controller;

use App\Controller\BaseController;
use Cake\Event\Event;
use App\Libs\ConfigUtil;
class BaseAccountsController extends BaseController
{
    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->loadModel('Users');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter ($event) ;
    }
    

    public function changePassword()
    {
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $userData = [
                'email' => $this->Auth->user('email'),
                'password' => $data['password']
            ];
            if (!empty($userData['email']) && !empty($userData['password'])) {
                $this->request->data = $userData;
                $user = $this->Auth->identify();
                $result = $this->Users->changePassword($data, $user);
                if(!empty($result['data']['id'])){
                    $this->Flash->success(ConfigUtil::getMessage('ICL003'));
                    $this->redirect(['action' => 'info']);
                }else{
                    $this->Flash->error($result['data']);
                }
            }
        }
        $this->render('/Accounts/change_password');
    }

}