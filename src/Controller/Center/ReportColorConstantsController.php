<?php

namespace App\Controller\Center;
use App\Controller\AppCenterController;
use App\Controller\AppController;
use App\Libs\ConfigUtil;

/**
 * GroupCustomer Controller
 *
 * @property \App\Model\Table\GroupCustomersTable $GroupCustomer
 *
 * @method \App\Model\Entity\GroupCustomer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReportColorConstantsController extends SingeFormController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $request = $this->getRequest()->getQueryParams();
        $this->baseIndex($request);
    }

}