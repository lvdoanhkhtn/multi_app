<?php

namespace App\Controller\Center;


use App\Controller\AppCenterController;
use App\Libs\ConfigUtil;

class PickupTripDetailsController extends TripDetailsController
{
    public function initialize(){
        $this->parentTable = $this->PickupTripDetails->PickupTrips;
        $this->loadModel('Wms');
        $this->loadModel('Customers');
        parent::initialize(); // TODO: Change the PickupTripDetails stub
    }

    public function detail($id, $isView = false){
        $this->baseDetail($id, $this->parentTable);
    }

    public function index($id){
        $this->detail($id);
    }

    public function view($id){
        $this->detail($id);
        if (!empty($_GET['key_word'])){
            $this->baseDetail($id, $this->parentTable, $_GET['key_word']);
        }
        $newOrders = $this->PickupTripDetails->getOrdersByAddressId($id, $this->parentTable, $this->orderConstant['new']);
        $this->set(compact('newOrders'));
    }


    public function confirmComplete()
    {
        $ids = $_POST['ids'];
        $tripId = $_POST['tripId'];
        echo $this->PickupTripDetails->confirmUpdate($ids, $this->orderConstant['pickup_in_progress'], $this->orderConstant['pickup_completed'], $tripId, $this->parentTable);
        exit();
    }

    public function collectFee()
    {
        $ids = $_POST['ids'];
        echo $this->PickupTripDetails->collectFee($ids);
        exit();
    }

    public function confirmFail()
    {
        $ids = $_POST['ids'];
        $note = $_POST['note'];
        $tripId = $_POST['tripId'];
        if (!empty($note)) {
            foreach ($ids as $id) {
                $orderId = $this->PickupTripDetails->get($id)->order_id;
                $this->Orders->OrderNotes->saveNote($orderId, $note);
            }
        }
        $this->PickupTripDetails->confirmDelete($ids, $this->orderConstant['new'], $this->orderConstant['new'], $tripId, $this->parentTable);
        echo 1;
        exit();
    }

    public function confirmCancel()
    {
        $ids = $_POST['ids'];
        $tripId = $_POST['tripId'];
        echo $this->PickupTripDetails->confirmUpdate($ids, $this->orderConstant['pickup_in_progress'], $this->orderConstant['cancel'], $tripId, $this->parentTable);
        exit();
    }


    public function checkAndStored($orderCode, $pickupTripId)
    {
        $orderConstant = $this->orderConstant;
        $orderConditions = [
            'OR' => [
                "Orders.orderId LIKE '" . $orderCode . "'",
                "Orders.id LIKE '" . $orderCode . "'",
                "Orders.shopId LIKE '" . $orderCode . "'"
            ],
            'Orders.order_status' => $orderConstant['pickup_completed']
        ];
        $roleId = $this->Auth->user('role_id');
        if (in_array($roleId, [COORDINATOR, LEAD_COORDINATOR])) {
            unset($orderConditions['Orders.order_status']);
            $orderConditions['Orders.order_status IN'] = [$orderConstant['pickup_in_progress'], $orderConstant['pickup_completed']];
        }
        //check is order_status is stored.
        $order = $this->table->Orders->getOrderWithConditions($orderConditions);
        if (empty($order->id)) {
            return null;
        }

        //check trip detail.
        $tripDetailConditions['order_id'] = $order->id;
        $tripDetailConditions['pickup_trip_id'] = $pickupTripId;
        $tripDetailConditions['order_status'] = $order->order_status;
        $tripDetail = $this->table->getOrderWithConditions($tripDetailConditions);

        if (empty($tripDetail->id)) {
            return null;
        }
        if(!$this->PickupTripDetails->confirmUpdate([$tripDetail->id], $order->order_status, $this->orderConstant['stored'], $pickupTripId, $this->parentTable)){
            return null;
        }
        return $order;
    }

    public function checkOrderAndInsert()
    {
        $orderCode = trim($_GET['orderCode']);
        $pickupTripId = trim($_GET['pickupTripId']);
        echo $this->checkAndStored($orderCode, $pickupTripId);
        exit();
    }

    //add address id after add
    public function refresh($id)
    {
        $this->baseRefresh($id, $this->parentTable, $this->orderConstant['new'], $this->orderConstant['pickup_in_progress']);
    }

    public function collectDetail($pickupTripId)
    {
        $orders = $this->PickupTripDetails->collectDetail($pickupTripId);
        $pickupTrip = $this->PickupTripDetails->PickupTrips->getPickupTripInfo($pickupTripId);
        $this->set(compact('orders', 'pickupTrip', 'pickupTripId'));
    }

}
