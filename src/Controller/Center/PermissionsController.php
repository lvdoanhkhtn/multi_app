<?php

namespace App\Controller\Center;

use App\Libs\ConfigUtil;
/**
 * GroupCustomer Controller
 *
 * @property \App\Model\Table\GroupCustomersTable $GroupCustomer
 *
 * @method \App\Model\Entity\GroupCustomer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PermissionsController extends SingeFormController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub]
        $roles = $this->Permissions->Roles->getList($this->request->getSession()->read('Config.language'));
        $actions = $this->Permissions->Actions->Methods->getListActions();
        $this->set(compact('roles', 'actions'));
        $this->Auth->allow('error403');

    }

    public function index()
    {
        $request = $this->getRequest()->getQueryParams();
        $this->session->write('search_datas', $request);
        $request['contain'] = ['Roles' => ['fields' => ['name']]];
        $datas = $this->Permissions->commonSearch($request);
        $datas = json_decode(json_encode($datas), true);
        for($i=0; $i<count($datas); $i++){
            $datas[$i]['list_action'] = $this->Permissions->Actions->getAllActions($datas[$i]['action_id']);
        }
        $list = $this->table->getCustomList($request, $this->getParam($request));
        $this->set(compact('datas', 'list'));
    }

    public function add($parentId = null)
    {
        if ($this->request->is('post')) {
            $data = $this->getRequest()->getData();
            $data['action_id'] = implode(",", $data['action_id']);
            $permissionEntity = $this->Permissions->newEntity($data);
            if ($this->Permissions->save($permissionEntity)) {
                $this->Flash->success(ConfigUtil::getMessage('ICL001'));
                return $this->redirect(['action' => 'index', $parentId]);
            }
            $this->Flash->error(ConfigUtil::getMessage('ICL002'));
        }
    }

    public function edit($id, $parentId = null){
        $permission = $this->Permissions->get($id);
        $permission['action_id'] = explode(",", $permission['action_id']);
        $this->set('data', $permission);
        if ($this->request->is('post')) {
            $data = $this->getRequest()->getData();
            $data['action_id'] = implode(",", $data['action_id']);
            $permissionEntity = $this->Permissions->patchEntity($permission, $data);
            if ($this->Permissions->save($permissionEntity)) {
                $this->Flash->success(ConfigUtil::getMessage('ICL003'));
                return $this->redirect(['action' => 'index', $parentId]);
            }
            $this->Flash->error(ConfigUtil::getMessage('ICL004'));
        }

    }

    public function error403(){
        if(empty($this->Auth->user('id'))){
            return $this->redirect($this->Auth->logout());
        }
        $this->render('/Layout/Error/error403');
    }


}
