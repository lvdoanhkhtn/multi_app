<?php

namespace App\Controller\Center;


use App\Libs\ConfigUtil;
use Cake\Event\Event;

class DeliveryTripsController extends TripsController
{
    public function beforeFilter(Event $event)
    {
        return parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }
    public function add($parentId = null){
        parent::add($parentId); // TODO: Change the autogenerated stub
    }

    public function index(){
        parent::index();
    }
    public function export(){
        $this->Excel->exportDeliveryTripsByExcel($this->baseExport());
    }
    public function changeShipper(){
        parent::changeShipper(); // TODO: Change the autogenerated stub
    }

    public function delete($id){
        $this->request->allowMethod(['post', 'delete']);
        $deliveryTrip = $this->DeliveryTrips->get($id);
        if($deliveryTrip->status === NO_COMPLETE_STATUS){
            if ($this->DeliveryTrips->delete($deliveryTrip)) {
                $this->Flash->success(ConfigUtil::get('ICL005'));
            } else {
                $this->Flash->error(ConfigUtil::get('ICL006'));
            }
        }
        return $this->redirect(['action' => 'index']);
    }
    
}
