<?php

namespace App\Controller\Center;

use App\Controller\AppCenterController;
use App\Libs\ConfigUtil;

/**
 * Countries Controller
 *
 * @property \App\Model\Table\CountriesTable $Countries
 *
 * @method \App\Model\Entity\Country[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CountriesController extends SingeFormController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $request = $this->getRequest()->getQueryParams();
        $this->baseIndex($request);
    }
}
