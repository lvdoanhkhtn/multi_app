<?php

namespace App\Controller\Center;



use App\Controller\AppCenterController;
use App\Libs\ConfigUtil;

class InventorySessionsController extends AppCenterController
{

    private function index($orderStatus, $type, $backUrl){
        $data = $this->getRequest()->getQueryParams();
        $data = $this->search($data);
        $data['type'] = $type;
        $inventorSessions = $this->InventorySessions->search($data);
        $datas = $this->paginate($inventorSessions);
        $storedOrderCount = count($this->InventorySessions->InventorySessionDetails->Orders->getOrderIdsWithStatus($orderStatus));
        $this->set(compact('datas', 'storedOrderCount', 'backUrl', 'type'));
        $this->render('index');
    }

    public function stored(){
        $this->index($this->orderConstant['stored'], STORE_TYPE, STORE_URL);
    }

    public function returned(){
        $this->index($this->orderConstant['return'], RETURN_TYPE, RETURN_URL);
    }

    public function pending(){
        $this->index($this->orderConstant['pending'], PENDING_TYPE, PENDING_URL);
    }

    public function add($backUrl, $type){
        if($this->getRequest()->is('post')){
            $data = $this->getRequest()->getData();
            if($this->InventorySessions->add($data)){
                $this->Flash->success(ConfigUtil::getMessage('ICL001'));
                $this->redirect(['action' => $backUrl]);
            }else{
                $this->Flash->error(ConfigUtil::getMessage('ICL002'));
            }
        }
        $this->set(compact('type'));
    }

    public function delete($id, $backUrl){
        $this->request->allowMethod(['post', 'delete']);
        $options['contain'] = ['InventorySessionDetails'];
        $inventorySession = $this->InventorySessions->get($id, $options);
        $inventorySessions = $this->InventorySessions->InventorySessionDetails->getIds($id);
        if(empty($inventorySession->status)){
            if($this->InventorySessions->InventorySessionDetails->confirmRemove($inventorySessions)){
                if ($this->InventorySessions->delete($inventorySession)) {
                    $this->Flash->success(ConfigUtil::getMessage('ICL005'));
                } else {
                    $this->Flash->error(ConfigUtil::getMessage('ICL006'));
                }
            }
        }
        return $this->redirect(['action' => $backUrl]);
    }
    
}
