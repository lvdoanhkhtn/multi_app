<?php

namespace App\Controller\Center;



use App\Controller\AppCenterController;

class InventorySessionDetailsController extends AppCenterController
{

    public function index($id, $backUrl){
        $this->InventorySessionDetails->InventorySessions->checkAndUpdateComplete($id);
        $inventorySessions = $this->InventorySessionDetails->InventorySessions->getById($id);
        $this->set(compact('inventorySessions', 'id', 'backUrl'));
    }

    public function inventory(){
        $orderId = !empty($_GET['orderId']) ? $_GET['orderId']: "";
        $inventorySessionId = !empty($_GET['inventorySessionId']) ? $_GET['inventorySessionId']: "";
        echo ($this->InventorySessionDetails->inventory($orderId, $inventorySessionId)) ? 1 : 0;
        exit();
    }

    public function export($id = null){
        $inventorySessions = $this->InventorySessionDetails->InventorySessions->getById($id);
        $this->Excel->exportInventorySessionDetails($inventorySessions);
    }
}
