<?php

namespace App\Controller\Center;
use App\Controller\AppCenterController;


/**
 * GroupCustomer Controller
 *
 * @property \App\Model\Table\GroupCustomersTable $GroupCustomer
 *
 * @method \App\Model\Entity\GroupCustomer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NotifiesController extends AppCenterController
{

    public function read(){
        echo $this->Notifies->read();
        exit();
    }

   
    
}
