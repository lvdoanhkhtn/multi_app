<?php

namespace App\Controller\Center;


/**
 * GroupCustomer Controller
 *
 * @property \App\Model\Table\GroupCustomersTable $GroupCustomer
 *
 * @method \App\Model\Entity\GroupCustomer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MethodsController extends SingeFormController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $request = $this->getRequest()->getQueryParams();
        $this->baseIndex($request);
    }

    public function updateController(){
        $this->loadModel('Methods');
        $this->Controllers->insertOrUpdate();
    }
    
}
