<?php

namespace App\Controller\Center;
use App\Libs\ConfigUtil;

/**
 * GroupCustomer Controller
 *
 * @property \App\Model\Table\GroupCustomersTable $GroupCustomer
 *
 * @method \App\Model\Entity\GroupCustomer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RolesController extends SingeFormController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $request = $this->getRequest()->getQueryParams();
        $this->baseIndex($request);
    }

    public function updateController(){
        $this->loadModel('Methods');
        $this->Methods->insertOrUpdate();
        $this->redirect(['action' => 'index']);
    }

    public function edit($id, $parentId = null){
        if($this->request->is(['patch', 'post', 'put'])){
            $data = $this->getRequest()->getData();
            $data['active'] = !empty($data['active']) ? 1 : 0;
            if(!empty($data['is_view'])){
                $data['is_view'] = !empty($data['is_view']) ? 1 : 0;
            }
            if($this->Roles->updateBasePermissionWithRole($id, $data)){
                $this->Flash->success(ConfigUtil::getMessage('ICL003'));
                return $this->redirect(['action' => 'index', $parentId]);
            }
            $this->Flash->error(ConfigUtil::getMessage('ICL004'));
        }else{
            $this->set(['data' => $this->Roles->get($id)]);
        }
    }

}
