<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Exception\Exception;
use Cake\Mailer\Email;

/**
 * Created by PhpStorm.
 * User: doanh.lv
 * Date: 8/23/19
 * Time: 4:43 PM
 */

class MailComponent extends CommonComponent
{
    public function sendMailPaymentDetail($paymentDetails = null, $toMail = null, $fullName = null, $excelComponent, $attachFile, $subject = null)
    {
        $flag = true;
        try {
            $email = new Email('payment');
            $subject = sprintf("%s (%s) %s %s", ($subject) ? $subject : __('subject'), $fullName, __('date'), date("d/m/Y"));
            $email
                ->setEmailFormat('html')
//                ->setTemplate('payment_details')
                ->setCc(CC_MAIL)
                ->setTo($toMail)
                ->setSubject($subject)
                ->setAttachments($attachFile)
//                ->setViewVars(['paymentDetails' => $paymentDetails, 'excel' => $excelComponent, 'lang' => $this->getController()->getRequest()->getSession()->read('Config.language')])
                ->send();
        } catch (Exception $e) {
            $flag = false;
        }
        return $flag;
    }

    public function sendMailCashBook($attachFile, $subject)
    {
        $flag = true;
        try {
            $email = new Email('payment');
            $subject = sprintf("%s (%s)", $subject, date("d/m/Y"));
            $email
                ->setEmailFormat('html')
                ->setCc(CC_MAIL)
                ->setTo(MAIL_RECEIVER_CASHBOOK)
                ->setSubject($subject)
                ->setAttachments($attachFile)
                ->send();
        } catch (Exception $e) {
            $flag = false;
        }
        return $flag;
    }
}
