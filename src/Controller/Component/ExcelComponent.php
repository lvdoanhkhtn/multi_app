<?php

namespace App\Controller\Component;

use App\Libs\ValueUtil;
use Cake\Controller\Component;

/**
 * Created by PhpStorm.
 * User: doanh.lv
 * Date: 8/23/19
 * Time: 4:43 PM
 */

use PhpOffice\PhpSpreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Cake\ORM\TableRegistry;
use App\Libs\ConfigUtil;

class ExcelComponent extends CommonComponent
{
    public $components = ['Paginator'];

    public $helpers = ['AdminView'];


    public function initialize(array $config)
    {
        //        $this->AdminView = $this->
        $tbServicePacks = TableRegistry::get('ServicePacks');
        $this->servicePacks = $tbServicePacks->getList(ConfigUtil::get('default_languague'), false);
    }

    public function getTotalColumn($xls_data)
    {
        $column = 0;
        foreach ($xls_data as $item) {
            if (!empty($item)) {
                $column++;
            } else {
                break;
            }
        }
        return $column;
    }

    public function getTotalRow($spreadsheet)
    {
        $count = 0;
        $worksheet = $spreadsheet->getActiveSheet();
        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false); // This loops through all cells,
            $cells = [];
            foreach ($cellIterator as $cell) {
                $cells[] = $cell->getValue();
            }
            if (count(array_filter($cells, "strlen")) > 0) {
                $count++;
            }
        }
        return $count;
    }

    public function importOrderByExcel($file)
    {

        //load model.
        $this->Orders = TableRegistry::getTableLocator()->get('Orders');
        $this->Districts = TableRegistry::getTableLocator()->get('Districts');
        $orders = [];
        try {
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file);
            $reader->setReadDataOnly(true);
            $reader->setLoadSheetsOnly(0);
            $spreadsheet = $reader->load($file);
            $xls_data = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);


            $firstRow = 3;
            $firstColumn = 2;
            $totalAvaibleColumn = $this->getTotalColumn($xls_data[$firstColumn]);
            $totalAvaibleRow = $this->getTotalRow($spreadsheet);
            if ($totalAvaibleColumn > 0) {
                for ($i = $firstRow; $i <= $totalAvaibleRow; $i++) {
                    $j = 1;
                    $flag = true;
                    foreach ($xls_data[$i] as $valueField) {
                        $data['service_pack_id'] = ConfigUtil::get('STANDARD');
                        switch ($j) {
                            case 2:
                                $data['content'] = $this->clean($valueField);
                                break;
                            case 3:
                                $data['weight'] = $this->clean($valueField);
                                break;
                            case 4:
                                $data['money_collection'] = $this->clean($valueField);
                                break;
                            case 5:
                                $data['receiver_name'] = $this->clean($valueField);
                                break;
                            case 6:
                                $data['arr_receiver_phone'] = $this->clean($valueField);
                                break;
                            case 7:
                                $data['address'] = $this->clean($valueField);
                                break;
                            case 8:
                                $arr = explode(".", $valueField);
                                $data['city_id'] = !empty($arr[0]) ? $this->clean($arr[0]) : "";
                                $data['city_name'] = !empty($arr[1]) ? $this->clean($arr[1]) : "";
                                break;
                            case 9:
                                $arr = explode(".", $valueField);
                                $data['district_id'] = !empty($arr[0]) ? $this->clean($arr[0]) : "";
                                if (empty($data['district_id'])) {
                                    $data['district_id'] = $this->Districts->getDefaultDistrictByCityId($data['city_id']);
                                    $data['district_name'] = $this->Districts->getDefaultNameDistrictByCityId($data['city_id']);
                                } else {
                                    $data['district_name'] = !empty($arr[1]) ? $this->clean($arr[1]) : "";
                                }
                                break;
                            case 10:
                                $arr = explode(".", $this->clean($valueField));
                                if (!empty($arr[0])) {
                                    $data['payment'] = 1;
                                } else {
                                    $data['payment'] = 0;
                                }
                                break;
                            case 11:
                                $data['note'] = $this->clean($valueField);
                                break;
                            case 12:
                                $data['shopId'] = $this->clean($valueField);
                                break;
                        }
                        $j++;
                    }
                    if ($flag) {
                        array_push($orders, $data);
                    }
                }
            }
        } catch (PhpSpreadsheet\Reader\Exception $e) {
        }
        return $orders;
    }

    public function setNameExcel($data, $sheet)
    {
        foreach ($data as $key => $value) {
            $sheet->setCellValue($key, $value);
        }
        return $sheet;
    }

    public function setHeaders($headerData, $endColumn)
    {
        $i = 0;
        foreach (range('A', $endColumn) as $columnID) {
            $index = $columnID . '1';
            if (isset($headerData[$i])) {
                $headers[$index] = $headerData[$i];
            }
            $i++;
        }
        return $headers;
    }

    public function setValue($values, $endColumn, $step, &$sheet)
    {
        $i = 0;
        foreach (range('A', $endColumn) as $columnID) {
            $index = $columnID . $step;
            if (isset($values[$i])) {
                $sheet->setCellValue($index, $values[$i]);
            }
            $i++;
        }
        return $sheet;
    }

    public function saveFileExcel($spreadsheet)
    {
        $writer = new Xls($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Report_' . date('dmY') . '.xls"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file
    }

    public function exportOrderByExcel($response)
    {
        $this->Orders = TableRegistry::getTableLocator()->get('Orders');
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // Set document properties
        // set the names of header cells
        $headerData = ['', __('code'), __('private_code'), __('creater'), __('date_created'), __('date_finished'), __('stored_time'), __('shop_name'), __('sender_address'), __('receiver_name'), __('receiver_phone'), __('receiver'), __('city'), __('name'), __('service_pack'), __('total'), __('total_fee'), __('total_amount'), __('shipping_fee'), __('cod_fee'), __('insurance_fee'), __('sub_fee'), __('return_fee'),   __('order_status'), __('note'), __('Histories')];
        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);

        // Add some data
        $x = 2;
        $maxPageExcel = ConfigUtil::get('max_page_excel');
        $maxRecordExcel = ConfigUtil::get('max_record_excel');
        $pages = (ceil($response->count() / $maxRecordExcel));
        $pages =  ($pages >= $maxPageExcel) ? $maxPageExcel : $pages;
        for ($i = 1; $i <= $pages; $i++) {
            $settings['limit'] = ConfigUtil::get('max_record_excel');
            $settings['page'] = $i;
            $orders = $this->Paginator->paginate($response, $settings);
            foreach ($orders as $order) {
                $historyText = "";
                if (count($order->order_notes) > 0) {
                  $historiesArr = array_column($order->order_notes, 'note');
                  $historyText = implode($historiesArr, ",");
                }
                $values = [
                    $x - 1,
                    $order->orderId,
                    $order->shopId,
                    !empty($order->user_creater->fullname) ? $order->user_creater->fullname : "",
                    $order->getCreated(),
                    !empty($order->finished) ? $order->getCompleted() : "",
                    $order->getSimpleStoredTime(),
                    $order->user->fullname,
                    $order->getSenderAddress(),
                    h($order->receiver_name),
                    h($order->arr_receiver_phone),
                    $order->getReceiverAddress(),
                    $order->city->name,
                    $order->district->name,
                    !empty($this->servicePacks[$order->getServicePack()]) ? $this->servicePacks[$order->getServicePack()] : "",
                    $order->total,
                    $order->getTotalFee(),
                    $order->getTotalAmount(),
                    $order->postage,
                    $order->collection_fee,
                    $order->protect_fee,
                    $order->sub_fee,
                    ($order->order_status == '14') ? $order->transfer_fee : 0,
                    $order->getStatus(),
                    h($order->note),
                    h($historyText),
                ];
                $this->setValue($values, 'Z', $x, $sheet);
                $x++;
            }
        }

        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }


    public function exportOrderInPaymentByExcel($orders, $isAdmin = false)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();

        $this->exportOrderInPayment($spreadsheet, $orders, $isAdmin);
        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
    }

    private function exportOrderInPayment($spreadsheet, $orders, $isAdmin)
    {
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells
        $headerData = [
            '',
            __('order_code'),
            __('shop_code'),
            __('date_created'),
            __('date_completed'),
            __('receiver_name'),
            __('receiver_phone'),
            __('address'),
            __('city'),
            __('district'),
            __('service_pack'),
            __('order_status'),
            __('total_cod'),
            __('total_fee'),
            __('amount')
        ];
        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($orders as $item) {
            $response = $this->getPaymentDetail($item->order);
            $amount = $response['cod'] - $response['fee'];
            $order = $item->order;
            $values = [
                $x - 1,
                $order->orderId,
                $order->shopId,
                $order->getCreated(),
                $order->getCompleted(),
                h($order->receiver_name),
                h($order->arr_receiver_phone),
                h($order->address),
                h($order->city->name),
                h($order->district->name),
                $this->servicePacks[$item->order->getServicePack()],
                $item->order->getStatus(),
                $response['cod'],
                $response['fee'],
                $amount
            ];

            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }
    }


    public function getFilenameInPaymentByExcel($orders, $isAdmin = false)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $this->exportOrderInPayment($spreadsheet, $orders, $isAdmin);
        //Create file excel.xlsx
        $name = WWW_ROOT . 'files/excel' . DS . 'Report_' . date('dmY') . '.xls';
        $writer = new Xls($spreadsheet);
        $writer->save($name);
        return $name;
    }

    public function exportEmployeeByExcel($employees)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells
        $headerData = [
            '',
            __('status'),
            __('user_name'),
            __('date_created'),
            __('full_name'),
            __('email'),
            __('phone'),
            __('address'),
            __('roles'),
            __('ware_houses')
        ];
        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($employees as $employee) {
            $values = [
                $x - 1,
                ValueUtil::get('order.status')[$employee->user->active],
                h($employee->employee_id),
                $employee->getCreated(),
                h($employee->user->fullname),
                h($employee->user->email),
                h($employee->user->phone),
                h($employee->getAddress()),
                h($employee->name),
                h($employee->ware_house->name)
            ];
            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }
        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }

    public function exportByExcel($datas)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells
        $headerData = [
            '',
            __('id'),
            __('name'),
            __('status')
        ];
        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($datas as $data) {
            $values = [
                $x - 1,
                $data->id,
                h($data->name),
                ValueUtil::get('order.status')[$data->active]
            ];
            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }
        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }

    public function exportAreaByExcel($datas)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells
        $headerData = [
            '',
            __('name'),
            __('description'),
            __('from_province'),
            __('to_province'),
            __('from_district'),
            __('to_district')
        ];
        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($datas as $data) {
            $values = [
                $x - 1,
                h($data->name),
                h($data->description),
                !empty($data->sender_city->name) ? h($data->sender_city->name) : "",
                h($data->city->name),
                !empty($data->sender_district->name) ?  h($data->sender_district->name) : "",
                !empty($data->district->name) ? h($data->district->name) : ""
            ];
            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }
        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }


    public function exportCustomerByExcel($customers)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells
        $headerData = [
            '',
            __('status'),
            __('user_name'),
            __('date_created'),
            __('full_name'),
            __('email'),
            __('phone'),
            __('address'),
            __('group_customers')
        ];

        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($customers as $customer) {
            $values = [
                $x - 1,
                ValueUtil::get('order.status')[$customer->user->active],
                h($customer->user->username),
                $customer->getCreated(),
                h($customer->user->fullname),
                h($customer->user->email),
                h($customer->user->phone),
                h($customer->getAddress()),
                !empty($customer->group_customer->name) ? h($customer->group_customer->name) : ''
            ];

            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }
        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }

    public function exportDeliveryTripDetailsByExcel($datas)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells

        $headerData = [
            '',
            __('order_code'),
            __('sender'),
            __('receiver'),
            __('address'),
            __('description'),
            __('note'),
            __('total_cod')
        ];

        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($datas->delivery_orders as $data) {
            $order = $data->order;
            $values = [
                $x - 1,
                sprintf("%s/ %s", $order->orderId, $order->shopId),
                sprintf("%s/ %s", $order->user->fullname, $order->user->phone),
                sprintf("%s/ %s", $order->receiver_name, $order->arr_receiver_phone),
                sprintf("%s, %s, %s", $order->address, $order->district->name, $order->city->name),
                h($order->content),
                h($order->note),
                $order->total
            ];

            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }

        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }

    public function exportDeliveryTripsByExcel($datas)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells

        $headerData = [
            '',
            __('trip_code'),
            __('date_created'),
            __('date_completed'),
            __('employee_code'),
            __('employee_name'),
            __('employee_phone'),
            __('orders_completed'),
            __('status')
        ];


        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($datas as $data) {
            $values = [
                $x - 1,
                h($data->id),
                $data->getCreated(),
                !empty($data->status === COMPLETE_STATUS) ? $data->getModified() : '',
                h($data->employee->employee_id),
                h($data->employee->user->fullname),
                h($data->employee->user->phone),
                $data->getTotalComplete(),
                $data->getStatus()
            ];
            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }

        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }


    public function exportPickupTripsByExcel($datas, $isPickup = false)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells

        $addressLabel = __('return_address');
        if ($isPickup) {
            $addressLabel = __('pickup_address');
        }
        $headerData = [
            '',
            __('customer'),
            $addressLabel,
            __('date_created'),
            __('date_completed'),
            __('shipper'),
            __('orders_completed'),
            __('status')
        ];

        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($datas as $data) {
            $values = [
                $x - 1,
                h($data->customer->fullname),
                h($data->address->getFullAddress()),
                $data->getCreated(),
                !empty($data->status === COMPLETE_STATUS) ? $data->getModified() : '',
                h($data->employee->user->fullname),
                $data->getTotalComplete(),
                $data->getStatus()
            ];

            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }

        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }

    public function exportCollectMoneySessionByExcel($sessions)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells

        $headerData = [
            '',
            __('trip_code'),
            __('date_created'),
            __('date_updated'),
            __('employee_code'),
            __('employee_name'),
            __('total_orders'),
            __('total_cod'),
            __('sub_fee'),
            __('total_collect')
        ];


        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($sessions as $session) {
            if (empty($session->delivery_complete_orders[0])) {
                continue;
            }
            $order = $this->getCollect($session->delivery_complete_orders);
            $totalCollect = $order['total_cod'] - $order['total_sub_fee'];

            $values = [
                $x - 1,
                $session->id,
                $session->getCreated(),
                $session->getModified(),
                $session->employee->employee_id,
                $session->employee->user->fullname,
                $order['total_order'],
                $order['total_cod'],
                $order['total_sub_fee'],
                $totalCollect
            ];

            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }

        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }


    public function exportAddPaymentsByExcel($users)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells

        $headerData = [
            '',
            __('customer_name'),
            __('address'),
            __('total_orders'),
            __('total_cod'),
            __('total_fee'),
            __('total_amount')
        ];


        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($users as $user) {
            if (empty($user->customers[0]['master_address'][0]) || (empty($user->order_complete_payment[0]) && empty($user->order_return_payment[0]))) {
                continue;
            }
            $address = $user->customers[0]['master_address'][0];
            $order = $this->getPayment($user);
            $values = [
                $x - 1,
                $user->fullname,
                !empty($address) ? sprintf("%s, %s, %s, %s, %s, %s", h($address->agency), h($address->str_phone), h($address->address), !empty($address->ward->name) ? h($address->ward->name) : '', h($address->district->name), h($address->city->name)) : '',
                number_format($order['totalOrder']),
                $this->getCurrencyCam($order['totalCod']),
                $this->getCurrencyCam($order['totalFee']),
                $this->getCurrencyCam($order['totalAmount']),
            ];

            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }

        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }

    public function exportReportSessionByExcel($datas)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells

        $headerData = [
            '',
            __('money_collector'),
            __('shipper'),
            __('order_code'),
            __('type'),
            __('trip_code'),
            __('total_cod'),
            __('total_fee'),
            __('sub_fee'),
            __('total_collect'),
            __('collect_date')
        ];


        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        $sumTotal = 0;
        $sumSubFee = 0;
        $sumAmount = 0;
        foreach ($datas as $data) {
            if (empty($data->order->complete_delivery_trip_details[0]) && empty($data->order->complete_pickup_trip_details[0])) {
                continue;
            }
            $order = $data->order;
            $totalFee = 0;
            if (!empty($data->type)) {
                $tripDetail = $order->complete_pickup_trip_details[0];
                $employeeName = $tripDetail->pickup_trip->employee->user->fullname;
                $tripDetailId = $tripDetail->pickup_trip_id;
                $order->total = 0;
                $order->sub_fee = 0;
                $totalFee = $order->postage + $order->collection_fee + $order->protect_fee;
                $totalCollect = $totalFee;
            } else {
                $tripDetail = $order->complete_delivery_trip_details[0];
                $employeeName = $tripDetail->delivery_trip->employee->user->fullname;
                $tripDetailId = $tripDetail->delivery_trip_id;
                $totalCollect = $order->total - $order->sub_fee;
            }
            $sumTotal += $order->total;
            $sumSubFee += $order->sub_fee;
            $sumAmount += $totalCollect;
            $values = [
                $x - 1,
                $data->user->fullname,
                $employeeName,
                $order->orderId,
                !empty($data->type) ? 'Đơn thu phí trước' : '',
                $tripDetailId,
                $order->total,
                $order->sub_fee,
                $totalFee,
                $totalCollect,
                $data->getCreated()
            ];
            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }

        $values = [
            '',
            '',
            '',
            '',
            '',
            '',
            $this->getCurrencyCam($sumTotal),
            $this->getCurrencyCam($sumSubFee),
            $this->getCurrencyCam($sumAmount),
            '',
        ];
        $this->setValue($values, 'Z', $x, $sheet);


        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }

    public function exportReportAccordingToShipperByExcel($datas)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells

        $headerData = [
            '',
            __('money_collector'),
            __('shipper'),
            __('delivery_trip'),
            __('total_collect'),
            __('collect_date')
        ];


        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        // Add some data
        $x = 2;
        $sumCollect = 0;
        foreach ($datas as $data) {
            $result = $data[0];
            $createds = [];
            foreach ($data as $element) {
                $createds[$element['date_created']][] = $element;
            }
            $emloyeeName = $result['employee']['user']['fullname'];
            $userName = $result['user']['fullname'];
            foreach ($createds as $item) {
                $totalCollect = 0;
                foreach ($item as $created) {
                    $order = $created['order'];
                    if (!empty($created['type'])) {
                        $totalCollect += ($order['postage'] + $order['collection_fee'] + $order['protect_fee']);
                    } else {
                        $totalCollect += $order['total'] - $order['sub_fee'];
                    }
                }
                $sumCollect += $totalCollect;
                $values = [
                    $x - 1,
                    $userName,
                    $emloyeeName,
                    $result['order']['complete_delivery_trip_details'][0]['delivery_trip_id'],
                    $totalCollect,
                    $item[0]['date_created'],
                ];
                $this->setValue($values, 'Z', $x, $sheet);
                $x++;
            }
        }

        $values = [
            '',
            '',
            '',
            '',
            $sumCollect,
            '',
        ];
        $this->setValue($values, 'Z', $x, $sheet);


        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }

    public function exportReportTrip($datas, $parentTable)
    {
        ini_set('max_execution_time', 120); //120 seconds = 2 minutes
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells

        $headerData = [
            '',
            __('code'),
            __('created'),
            __('finished'),
            __('stored_time'),
            __('sender'),
            __('shipper'),
            __('service_pack'),
            __('city'),
            __('district'),
            __('order_status'),
            __('charg_status'),
            __('collect_status'),
            __('pay_status'),
            __('owe_status'),
            __('total'),
            __('postage'),
            __('collection_fee'),
            __('sub_fee'),
            __('protect_fee'),
            __('transfer_fee'),
            __('weight'),
        ];


        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($datas as $data) {
            $order = $data->order;
            $values = [
                $x - 1,
                $order->orderId,
                $order->getCreated(),
                !empty($order->finished) ? $order->getCompleted() : "",
                $order->getSimpleStoredTime(),
                $order->user->fullname,
                $data->{$parentTable}->employee->user->fullname,
                !empty($order->service_pack->name) ? $order->service_pack->name : "",
                !empty($order->city->name) ? $order->city->name : "",
                !empty($order->district->name) ? $order->district->name : "",
                $order->getStatus(),
                $order->getChargStatus(),
                $order->getCollectStatus(),
                $order->getPayStatus(),
                $order->getOweStatus(),
                $order->total,
                $order->postage,
                $order->collection_fee,
                $order->sub_fee,
                $order->protect_fee,
                $order->transfer_fee,
                $order->weight,

            ];
            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }


        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }

    public function exportPerformanceTrip($datas, $tableTotal, $tableDetail)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells

        $headerData = [
            '',
            __('id'),
            __('shipper'),
            __('pickup'),
            __('total')
        ];


        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($datas as $data) {
            $pickup = !empty($data->{$tableDetail}[0]->total) ? $data->{$tableDetail}[0]->total : 0;
            $total = !empty($data->{$tableTotal}[0]->total) ? $data->{$tableTotal}[0]->total : 0;
            $values = [
                $x - 1,
                $data->id,
                $data->employee->user->fullname,
                $pickup,
                $total
            ];
            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }
        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }

    public function exportInventorySessionDetails($inventorySessions)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set the names of header cells

        $headerData = [
            '',
            __('order_code'),
            __('shop_code'),
            __('sender'),
            __('receiver'),
            __('product'),
            __('status')
        ];


        $headers = $this->setHeaders($headerData, 'Z');

        $sheet = $this->setNameExcel($headers, $sheet);
        //            $sheet->setCellValue('I1', __('note'));
        // Add some data
        $x = 2;
        foreach ($inventorySessions['inventory_session_details'] as $inventorySession) {
            $order = $inventorySession->order;
            $values = [
                $x - 1,
                $order->orderId,
                $order->shopId,
                sprintf("%s, %s", $order->user->fullname, $order->user->phone),
                sprintf("%s, %s", $order->receiver_name, $order->arr_receiver_phone),
                sprintf("%s, %s", $order->content, $order->weight),
                ValueUtil::get('order.delivery_status_eng')[$inventorySession->status]
            ];
            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }
        //Create file excel.xlsx
        $this->saveFileExcel($spreadsheet);
        //End Function index
    }

    public function getFilenameCashBook($orders, $isPayment = false)
    {
        // Create new Spreadsheet object
        $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        if ($isPayment) {
            $this->exportCashBookInPayment($spreadsheet, $orders);
        } else {
            $this->exportCashBookInCollect($spreadsheet, $orders);
        }
        //Create file excel.xlsx
        $name = WWW_ROOT . 'files/excel' . DS . 'Report_' . ($isPayment ? 'Payment' : 'Collect') . date('dmY') . '.xls';
        $writer = new Xls($spreadsheet);
        $writer->save($name);
        return $name;
    }


    public function exportCashBookInPayment($spreadsheet, $orders)
    {
        // set the names of header cells
        $sheet = $spreadsheet->getActiveSheet();
        $headerData = [
            '',
            __('order_code'),
            __('shop'),
            __('payment_code'),
            __('total_cod_usd'),
            __('total_fee_usd'),
            __('total_paid_usd')
        ];
        $headers = $this->setHeaders($headerData, 'Z');
        $sheet = $this->setNameExcel($headers, $sheet);
        // Add some data
        $totalCod = 0;
        $totalFee = 0;
        $totalAmount = 0;
        $x = 2;
        foreach ($orders as $item) {
            $response = $this->getPaymentDetail($item->order);
            $totalCod += $response['cod'];
            $totalFee += $response['fee'];
            $amount = $response['cod'] - $response['fee'];
            $totalAmount += $amount;
            $values = [
                $x - 1,
                $item->order->orderId,
                $item->payment->customer->user->fullname,
                $item->id,
                $response['cod'],
                $response['fee'],
                $amount
            ];

            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }

        $values = [
            '',
            '',
            '',
            '',
            $totalCod,
            $totalFee,
            $totalAmount
        ];
        $this->setValue($values, 'Z', $x, $sheet);
    }

    public function exportCashBookInCollect($spreadsheet, $orders)
    {
        // set the names of header cells
        $sheet = $spreadsheet->getActiveSheet();
        $headerData = [
            '',
            __('order_code'),
            __('shipper'),
            __('trip_code'),
            __('total_cod_usd'),
            __('total_sub_fee_usd'),
            __('total_collect_usd')
        ];
        $headers = $this->setHeaders($headerData, 'Z');
        $sheet = $this->setNameExcel($headers, $sheet);
        // Add some data
        $totalCod = 0;
        $totalFee = 0;
        $totalAmount = 0;
        $x = 2;
        foreach ($orders as $item) {
            $order = $item->order;
            $totalCod += $order['total'];
            $totalFee += $order['sub_fee'];
            $amount = $order['total'] + $order['sub_fee'];
            $totalAmount += $amount;
            $trip = $order->complete_delivery_trip_details[0];
            $values = [
                $x - 1,
                $order->orderId,
                $trip->delivery_trip->employee->user->fullname,
                $trip->delivery_trip_id,
                $order['total'],
                $order['sub_fee'],
                $amount
            ];

            $this->setValue($values, 'Z', $x, $sheet);
            $x++;
        }

        $values = [
            '',
            '',
            '',
            '',
            $totalCod,
            $totalFee,
            $totalAmount
        ];
        $this->setValue($values, 'Z', $x, $sheet);
    }
}
