<?php

namespace App\Controller\Component;
use App\Libs\ValueUtil;
use Cake\Controller\Component;

use App\Libs\ConfigUtil;
use Cake\Controller\ComponentRegistry;



/**
 * Created by PhpStorm.
 * User: doanh.lv
 * Date: 8/23/19
 * Time: 4:43 PM
 */

class CommonComponent extends Component
{
    public function __construct(ComponentRegistry $registry, array $config)
    {
        $this->orderConstant = ValueUtil::get('order.order_constant');
        parent::__construct($registry, $config);
    }


    public function clean($str = null)
    {
        return str_replace(array('<', '>', '\\', '"', "'", '?'), ' ', $str);
    }

    public function convertStringToMoney($money)
    {
        $money = str_replace(",", "", $money);
        $money = str_replace(".", "", $money);
        return $money;
    }

    public function search($data){
        if(!empty($data['create_date'])){
            $fromCreateDate = explode("-", $data['create_date']);
            $data['from_create_date'] = $fromCreateDate[0];
            $data['to_create_date'] = $fromCreateDate[1];
        }
        if(!empty($data['complete_date'])){
            $fromCompleteDate = explode("-", $data['complete_date']);
            $data['from_complete_date'] = $fromCompleteDate[0];
            $data['to_complete_date'] = $fromCompleteDate[1];
        }
        if(!empty($data['order_status'])){
            if(is_array($data['order_status'])){
                $data['order_status'] = implode(",",$data['order_status']);
            }
        }
        return $data;
    }

    public function getCollect($data){
        $data = json_decode(json_encode($data), true);
        $orderCollect = array_column($data, 'order_collect');
        $response['total_order'] = count($data);
        $response['total_cod'] = $this->arraySum($orderCollect, 'total');
        $response['total_sub_fee'] = $this->arraySum($orderCollect, 'sub_fee');
        return $response;
    }

    public function getCurrencyCam($total){
        return sprintf("%s (%s USD)", number_format($total * ConfigUtil::get('CAM_CURRENCY')), $total);
    }

    public function getCustomCurrencyCam($total){
        return sprintf("%s", number_format($total * ConfigUtil::get('CAM_CURRENCY')));
    }

    public function arraySum($array,$index){
        return array_sum(array_column($array, $index));
    }

    public function getPayment($user){
        $completeOrderTotal = 0;
        $returnOrderTotal = 0;
        $completeOrderTotalCod = 0;
        $completeOrderTotalFee = 0;
        $returnOrderTotalFee = 0;
        if (!empty($user->order_complete_payment[0])) {
            $completeOrder = $user->order_complete_payment[0];
            $completeOrderTotal = $completeOrder->total_order;
            $completeOrderTotalCod = $completeOrder->total_cod;
            $completeOrderTotalFee = $completeOrder->total_fee;
        }

        if(!empty($user->order_return_payment[0])){
            $returnOrder = $user->order_return_payment[0];
            $returnOrderTotal = $returnOrder->total_order;
            $returnOrderTotalFee = $returnOrder->total_fee;
        }
        $response['totalOrder'] = $completeOrderTotal + $returnOrderTotal;
        $response['totalCod'] = $completeOrderTotalCod;
        $response['totalFee'] = $completeOrderTotalFee + $returnOrderTotalFee;
        $response['totalAmount'] = $response['totalCod'] -  $response['totalFee'];
        return $response;
    }

    public function getPaymentDetail($order){
        $response['cod'] = 0;
        $response['fee'] = $order->transfer_fee;
        if($order->order_status == $this->orderConstant['delivery_completed']){
            $response['cod'] = $order->total;
            $response['fee'] = $order->getTotalFee();
        }
        $response['amount'] = $response['cod'] - $response['fee'];
        return $response;
    }


    public function getSumPayment($paymentDetails){
        $data = json_decode(json_encode($paymentDetails), true);
        $completeOrders = array_column($data, 'order_complete_payment');
        $returnOrders = array_column($data, 'order_return_payment');
        $response['total_order'] = count($data);
        $response['total_cod'] = $this->arraySum($completeOrders, 'total');
        $response['total_fee'] = $this->arraySum($completeOrders, 'postage') + $this->arraySum($completeOrders, 'sub_fee') - $this->arraySum($completeOrders, 'discount')
            + $this->arraySum($completeOrders, 'collection_fee') + $this->arraySum($completeOrders, 'protect_fee') + $this->arraySum($returnOrders, 'transfer_fee');
        $response['total_amount'] = $response['total_cod'] - $response['total_fee'];
        return $response;
    }

    public function getCollected($data)
    {
        $data = json_decode(json_encode($data), true);
        $orderCollected = array_column($data, 'order_collected');
        $response['total_order'] = count($data);
        $response['total_cod'] = 0;
        $response['total_fee'] = $this->arraySum($orderCollected, 'postage') + $this->arraySum($orderCollected, 'collection_fee') + $this->arraySum($orderCollected, 'protect_fee');;
        $response['total_sub_fee'] = $this->arraySum($orderCollected, 'sub_fee');
        return $response;
    }
}