<?php

/**
 * Created by PhpStorm.
 * Users: doanh.lv
 * Date: 6/23/19
 * Time: 5:24 PM
 */

namespace App\Controller\Api;

use Cake\Event\Event;

class PaymentDetailsController extends ApiController
{
    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->loadComponent('RequestHandler');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow();
        $user = $this->Auth->user();
        if ($user['parent_id']) {
            $customer = $this->PaymentDetails->Payments->Customers->getCustomer($user['parent_id']);
            $this->customer_id = $customer['id'];
        } else {
            $this->customer_id =  $user['customers']['id'];
        }
    }

    public function search()
    {
        $data = $this->request->getQuery();
        $paymentDetails = $this->PaymentDetails->getPaymentDetail($data['payment_id']);
        $response = $this->paging($paymentDetails, $data);
        $response['data'] = $this->convertPaymentDetailData($response['data']);
        $this->responseData($response);
    }

    public function convertPaymentDetailData($data)
    {
        $convertPayment = json_decode(json_encode($data), true);
        for ($i = 0; $i < count($convertPayment); $i++) {
            $order = $convertPayment[$i]['order'];
            $convertPayment[$i]['orderId'] = $order['id'];
            $convertPayment[$i]['orderCode'] = $order['orderId'];
            $convertPayment[$i]['total'] =  $order['total'];
            $convertPayment[$i]['cost'] = $order['postage'] + $order['sub_fee'] - $order['discount'] + $order['collection_fee'] + $order['protect_fee'];
            unset($convertPayment[$i]['order']);
        }
        return $convertPayment;
    }
}
