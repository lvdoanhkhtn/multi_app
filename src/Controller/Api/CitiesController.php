<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use App\Controller\AppController;



class CitiesController extends AppController
{

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->loadModel('Cities');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter ($event) ;
        $this->Auth->allow();
    }

    public function index(){
        $this->getCities();
    }

    public function view($countryId){
        $this->getCities($countryId);

    }

    public function getCities($countryId = null){
        $options['fields'] = ['id', 'name', 'code'];
        if(!empty($countryId)){
            $options['conditions']['country_id'] = $countryId;
        }
        $cities = $this->Cities->find('all', $options)->toArray();
        if(count($cities) > 0){
            $this->success($cities);
        }else{
            $this->notFound();
        }
    }

    public function pickUpCities(){
        $options['conditions']= ['Cities.is_get' => 1];
        $cities = $this->Cities->getCitiesForMobile($options['conditions']);
        $this->success($cities);
    }

    public function deliveryCities(){
        $options['conditions']= ['Cities.is_show' => 1];
        $cities = $this->Cities->getCitiesForMobile($options['conditions']);
        $this->success($cities);
    }
}
