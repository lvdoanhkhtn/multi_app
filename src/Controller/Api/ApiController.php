<?php

namespace App\Controller\Api;


use Cake\Event\Event;
use App\Controller\AppController;



class ApiController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow();
        $method = $this->request->getMethod();
        $params['token'] = $this->request->getHeader('Authorization')[0];
        if (!empty($this->request->getHeader('Authorization')[0]) || in_array($method, ['POST', 'PUT', 'PATCH', 'DELETE'])) {
            $token = $this->request->getHeader('Authorization')[0];
            $data = json_decode($this->request->getBody()->getContents(), true);
            if (empty($token)) {
                $this->unAuthorized();
            }
            if (!empty($data['Owner_Code'])) {
                $hash = md5($data['Owner_Code'] . $data['Warehouse_Code']);
                if (trim($token) === trim($hash)) {
                    $params['email'] = $data['Owner_Code'];
                    unset($params['token']);
                }
            }
            if (!$this->verify($params)) {
                $this->unAuthorized();
            } else {
                $request = $this->getRequest();
                $log['data'] = $data;
                $log['controller'] = $request->getParam('controller');
                $log['action'] = $request->getParam('action');
                $log['method'] = $method;
                $user = $this->Auth->user();
                $log['user_id'] = $user['id'];
                $log['user_name'] = $user['fullname'];
                //is wms.
                if(!empty($data['Owner_Code'])){
                    //check if group customer is wms.
                    $this->loadModel('Customers');
                    $this->Customers->updateIsMemberOfWms($user['id']);
                }
                $this->log(json_encode($data), LOG_DEBUG);
                $this->log($log, LOG_DEBUG);
            }
        } else {
            $this->notFound();
        }
    }


    public function verify($data)
    {
        $this->loadModel('Users');
        $user = $this->Users->loadAddresses($data);
        $user['shop_permission_name'] = $this->Users->ShopPermissions->getShopPermissionNameByUserId($user['id']);
        if (!empty($user['id'])) {
            $this->Auth->setUser($user);
            return true;
        }
        return false;
    }
}
