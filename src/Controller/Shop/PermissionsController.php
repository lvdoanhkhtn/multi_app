<?php

namespace App\Controller\Shop;

use App\Controller\AppShopController;
use App\Libs\ConfigUtil;
/**
 * GroupCustomer Controller
 *
 * @property \App\Model\Table\GroupCustomersTable $GroupCustomer
 *
 * @method \App\Model\Entity\GroupCustomer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PermissionsController extends AppShopController
{
    public function error403(){
        $this->render('/Layout/Error/error403');
    }


}
