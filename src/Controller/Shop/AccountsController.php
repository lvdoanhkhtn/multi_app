<?php

/**
 * Created by PhpStorm.
 * Users: doanh.lv
 * Date: 8/1/19
 * Time: 9:27 AM
 */

namespace App\Controller\Shop;

use App\Controller\BaseAccountsController;
use App\Libs\ConfigUtil;
use Exception;

class AccountsController extends BaseAccountsController
{

    public function info()
    {
        $data['id'] = $this->Auth->user('id');
        $account = $this->Users->information($data);
        $this->set(compact('account'));
        if ($this->getRequest()->is('post')) {
            $request = $this->cleanRequestData($this->request->getData());
            $request['id'] = $this->Auth->user('id');
            $request['active'] = $account['active'];
            if (!$this->validCityAndDistrict($request)) {
                $this->Flash->error(ConfigUtil::getMessage("ICL002"));
            } else {
                $result = $this->Users->insertOrUpdate($request, true);
                if (!empty($result['data']['id'])) {
                    $this->Flash->success(ConfigUtil::getMessage("ICL001"));
                    $this->redirect(['action' => 'info']);
                } else {
                    $this->Flash->error(ConfigUtil::getMessage("ICL002"));
                }
            }
        }
        $this->render('/Accounts/info');
    }

    public function adminChangePassword($userId)
    {
        try {
            $user = $this->Users->get($userId, ['conditions' => ['parent_id' => $this->Auth->user('id')]]);
        } catch (Exception $e) {
            $this->redirect(['controller' => 'shop_employees', 'action' => 'index']);
        }

        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $user->password = $data['password'];
            if ($this->Users->save($user)) {
                $this->Flash->success(ConfigUtil::getMessage('ICL003'));
                $this->redirect(['controller' => 'shop_employees', 'action' => 'index']);
            } else {
                $this->Flash->error(ConfigUtil::getMessage('ICL004'));
            }
        }
        $this->render('/Accounts/admin_change_password');
    }
}
