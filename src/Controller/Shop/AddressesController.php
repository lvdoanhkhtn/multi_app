<?php
/**
 * Created by PhpStorm.
 * Users: doanh.lv
 * Date: 8/1/19
 * Time: 9:27 AM
 */

namespace App\Controller\Shop;

use App\Controller\BaseAddressesController;
use App\Libs\ConfigUtil;
use Cake\Event\Event;

class AddressesController extends BaseAddressesController
{
    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->loadModel('Customers');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter ($event) ;
        $this->customer_id = $this->Auth->user('customers.id');

    }

    public function index(){
        $this->baseIndex();

    }

    public function add(){
        $this->baseAdd();
    }
//
//
    public function edit($id){
        $this->baseEdit($id);
    }
    
}