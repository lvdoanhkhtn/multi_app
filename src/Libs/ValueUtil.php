<?php
/**
 * Created by PhpStorm.
 * Users: leminhtoan
 * Date: 11/13/14
 * Time: 19:39
 */

namespace App\Libs;
use App\Model\PLSQL\Common\EncryptFunction;

class ValueUtil
{

    /**
     * Get value list from yml config file
     * @param $keys
     * @param array $options
     * @return array|null
     */
    public static function get($keys, $options = array())
    {
        return ConfigUtil::getValueList($keys, $options);
    }

    /**
     * Get Text from value (in Yml config file)
     * @param $keys
     * @param $value
     * @param null $default
     * @return null
     */
    public static function valueToText($keys, $value, $default = NULL)
    {
        $valueList = self::get($keys);
        if (!isset($valueList[$value])) return $default;
        return $valueList[$value];
    }

    /**
     * Get value from const (in Yml config file)
     * @param $keys
     * @return int|null|string
     */
    public static function constToValue($keys)
    {
        return ConfigUtil::getValue($keys);
    }

    /**
     * Get text from const (in Yml config file)
     * @param $keys
     * @return int|null|string
     */
    public static function constToText($keys)
    {
        return ConfigUtil::getValue($keys, TRUE);
    }

    /**
     * Get value from test i
     * @param $searchText
     * @param $keys
     * @return int|null|string
     */
    public static function textToValue($searchText, $keys)
    {
        $valueList = ValueUtil::get($keys);

        foreach ($valueList as $key => $text) {
            if ($searchText == $text) {
                return $key;
            }
        }

        return NULL;
    }

    /**
     * @param $array
     * @param $key
     * @return array
     */
    public static function getArrayValue($array, $key)
    {
        $results = array();
        foreach ($array as $data) {
            $results[] = $data[$key];
        }

        return $results;
    }
    
    /**
     * Convert date to format Ymd
     * @param string $date
     * @return null|string
     */
    public static function dateToText($date)
    {
        if (empty($date)) {
            return NULL;
        }
        
        if (!strtotime($date)) {
            return $date;
        }
        
        $result = new \DateTime($date);
        return $result->format('Ymd');
    }
    
    /**
     * Create random string
     * @param int $length
     * @return null|string
     */
    public static function RandomString($length) {
        $original_string = array_merge(range(0,9), range('a','z'), range('A', 'Z'));
        $original_string = implode("", $original_string);
        return substr(str_shuffle($original_string), 0, $length);
    }
    public static function getDateString($value,$format) {

        $value = str_replace(' ','',$value);

        $value = new \DateTime($value);

        return $value->format($format);
    }

    /**
     * @param $strDate
     * @param $strFormat base on format of \DateTime class like: Ymd etc
     * @return null|\DateTime
     */
    public static function strToDate($strDate, $strFormat) {
        $dateObj = null;
        if (!empty($strDate) && !empty($strFormat)) {
            $dateObj = \DateTime::createFromFormat($strFormat, $strDate);
        }
        return $dateObj;
    }

    /**
     * Usefull to convert string of date from one format to other format
     *
     * @param $strDate
     * @param $srcFormat base on format of \DateTime class like: Ymd etc
     * @param $desFormat base on format of \DateTime class like: Ymd etc
     * @return string
     */
    public static function formatStrDate($strDate, $srcFormat, $desFormat) {
        $desStr = "";
        $dateObj = null;
        if (!empty($strDate) && !empty($srcFormat) && !empty($desFormat)) {
            $dateObj = ValueUtil::strToDate($strDate, $srcFormat);
            if ($dateObj != null) {
                $desStr = $dateObj->format($desFormat);
            }
        }
        return $desStr;
    }
    
    /**
     * Convert all array keys to lower string
     * @param array $data
     * @return array
     */
    public static function lowerKeyArray($data) {
        $result = [];
        if (!empty($data) && is_array($data)) {
            $result = array_change_key_case($data, CASE_LOWER);
        }
        
        return $result;
    }
    
    /**
     * Convert all array keys to upper string
     * @param array $data
     * @return array
     */
    public static function upperKeyArray($data) {
        $result = [];
        if (!empty($data) && is_array($data)) {
            $result = array_change_key_case($data, CASE_UPPER);
        }
        
        return $result;
    }
    
    /**
     * Convert full width kana to half width kana or half width kana to full width kana
     * @param string $str
     * @param boolean $halfWidth
     * @return array
     */
    public static function convertKana($str, $halfWidth = true) {
        $result = "";
        $formatType = $halfWidth ? 'k' : 'K';
        if (!empty($str)) {
            $result = mb_convert_kana($str, $formatType, 'UTF-8');
        }
        
        return $result;
    }
    
    /**
     * Encrypt a string
     * @param string $str
     * @param int $length
     * @return array
     */
    public static function encryptString($str, $length = 16) {
        $result = "";
        $keyword = ConfigUtil::get('nmp_db_decryption_key');
        if (!empty($str)) {
            $result = EncryptFunction::phpEncrypt($str, $length, $keyword);
        }
        
        return $result;
    }
    
    /**
     * Decrypt a string
     * @param string $str
     * @return array
     */
    public static function decryptString($str) {
        $result = "";
        $keyword = ConfigUtil::get('nmp_db_decryption_key');
        if (!empty($str)) {
            $result = EncryptFunction::phpDecrypt($str, $keyword);
        }
        
        return $result;
    }

    public static function checkTelFormat($str) {
        $flag = false;
        $rex = "/^\\d{2}[0-9\\-]+[0-9]{1}$/u";
        $match = preg_match($rex, $str);
        if ($match) {
            $str = str_replace("-", "", $str);
            if (mb_strlen($str) >= 10 && mb_strlen($str) < 12) {
                $flag = true;
            }
        }
        return $flag;
    }

    public static function checkMailFormat($str) {
        $flag = false;
        $rex = "/^[a-zA-Z0-9\\-\\._\\/\\?!%&~#\\$\\'\\*=\\^`\\{|\\}\\+]+@([a-z0-9\\-_]+\\.)+[a-z0-9\\-_]+$/u";
        if (preg_match($rex, $str)) {
            $flag = true;
        }
        return $flag;
    }
} 