<?php

namespace App\Libs;

//require phpexcel lib
use App\Libs\ConfigUtil;
use Cake\Log\Log;
use Cake\Http\Client;

use PhpOffice\PhpSpreadsheet;


class ExcelLib
{

    /**
     * @param $datas
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     */
    public function clean($str = null)
    {
        return str_replace(array('<', '>', '\\', '"', "'", '?'), ' ', $str);
    }

    public function convertStringToMoney($money)
    {
        $money = str_replace(",", "", $money);
        $money = str_replace(".", "", $money);
        return $money;
    }
//    public function getTotalAvaibleColumn($objPHPExcel, $totalColumn){
//        $totalAvaibleColumn = 0;
//        for($i = 1;$i<$totalColumn;$i++) {
//            $val_col = $objPHPExcel->getActiveSheet(0)->getCellByColumnAndRow($i)->getValue();
//            if(!empty($val_col)){
//                $totalAvaibleColumn ++;
//            }else{
//                continue;
//            }
//        }
//        return $totalAvaibleColumn;
//    }

    public function getTotalAvaibleRow($objPHPExcel, $totalRow)
    {
        $totalAvaibleRow = 0;
        for ($j = 3; $j < $totalRow; $j++) {
            //check field city_id
            $val_row = $objPHPExcel->getActiveSheet(0)->getCellByColumnAndRow(4, $j)->getValue();
            if (!empty($val_row)) {
                $totalAvaibleRow++;
            } else {
                continue;
            }
        }
        return $totalAvaibleRow;
    }

//    function readExcel($sender, $file){
//        $objPHPExcel = \PHPExcel_IOFactory::load($file);
//        $objPHPExcel->setActiveSheetIndex (0);
//        $highestColumm = $objPHPExcel->setActiveSheetIndex (0)->getHighestDataColumn ();
//
//        $totalColumn = \PHPExcel_Cell::columnIndexFromString ( $highestColumm );
//        $totalRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
//        $totalAvaibleColumn = $totalColumn;//$this->getTotalAvaibleColumn($objPHPExcel, $totalColumn);
//        $totalAvaibleRow = $this->getTotalAvaibleRow($objPHPExcel, $totalRow);
//
//        //echo $totalColumn;die;
//        //tinh thanh pho
//        $orders = [];
//        if($totalAvaibleRow < 100 && $totalAvaibleColumn > 0) {
//
//            $totalAvaibleRow  = $totalAvaibleRow + 3;
//            for ($i = 3; $i < $totalAvaibleRow; $i++){
//                $flag = true;
//                for ($j = 0; $j <= $totalAvaibleColumn; $j++) {
//                    $valueField = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j, $i)->getValue();
//                    //0: Mã đơn SHOP.
//                    //1: Tên người nhận (*).
//                    //2: SĐT người nhận (*).
//                    //4: Địa chỉ người nhận (*).
//                    //5: Tỉnh/Thành phố (*).
//                    //6: Quận/Huyện (*).
//                    //7: Phường/Xã.
//                    //8: Bên trả phí.
//                    //9: Tiền thu hộ.
//                    //10: Gói cước.
//                    //11: Khối lượng.
//                    //12: Ghi chú.
//                    //13: Tên hàng hóa.
//                    //14: Khai giá hàng hóa (Phí bảo hiểm 0,5%)
//                    switch ($j) {
//                        case 0:
//                            $data['shop_order_code'] = $this->clean($valueField);
//                            break;
//                        case 1:
//                            if (empty($this->clean($valueField)))
//                                $flag = false;
//                            else
//                                $data['receiver_name'] = $this->clean($valueField);
//                            break;
//                        case 2:
//                            if (empty($this->clean($valueField)))
//                                $flag = false;
//                            else
//                                $data['arr_receiver_phone'] = $this->clean($valueField);
//                            break;
//                        case 3:
//                            if (empty($this->clean($valueField)))
//                                $flag = false;
//                            else
//                                $data['address'] = $this->clean($valueField);
//                            break;
//                        case 4:
//                            if (empty($this->clean($valueField)))
//                                $flag = false;
//                            else {
//                                $city_id = explode('.', $this->clean($valueField));
//                                $data['city_id'] = $city_id[0];
//                                $data['city_name'] = $city_id[1];
//                            }
//                            break;
//                        case 5:
//                            if (empty($this->clean($valueField)))
//                                $flag = false;
//                            else {
//                                $district_id = explode('.', $this->clean($valueField));
//                                $data['district_id'] = $district_id[0];
//                                $data['district_name'] = $district_id[1];
//                            }
//                            break;
//                        case 6:
//                            $ward_id = explode('.', $this->clean($valueField));
//                            $data['ward_id'] = isset($ward_id[0]) ? $ward_id[0] : "";
//                            $data['ward_name'] = isset($ward_id[1]) ? $ward_id[1] : "";
//
//                            break;
//                        case 7:
//                            if (empty($this->clean($valueField)))
//                                $data['payment'] = 1;
//                            else {
//                                $payer = explode('.', $this->clean($valueField));
//                                $data['payment'] = $payer[0];
//                            }
//                            break;
//                        case 8:
//                            $data['money_collection'] = $this->convertStringToMoney($this->clean($valueField));
//                            break;
//                        case 9:
//                            if (empty($this->clean($valueField)))
//                                $data['service_pack'] = 1;
//                            else {
//                                $form_delivery = explode('.', $this->clean($valueField));
//                                $data['service_pack'] = $form_delivery[0];
//                            }
//                            break;
//                        case 10:
//                            $data['weight'] = str_replace(",",".",$this->clean($valueField));
//                            break;
//                        case 11:
//                            $data['note'] = $this->clean($valueField);
//                            break;
//                        case 12:
//                            $data['content'] = $this->clean($valueField);
//                            break;
//                        case 13:
//                            $data['product_value'] = $this->convertStringToMoney($this->clean($valueField));
//                            break;
//                    }
//                }
//                if($flag){
//                    //pr($sender);die;
//                    $temp['Order'] = $data;
//                    array_push($orders, $temp);
//                    //$orders[] = array_merge($sender, $temp);
//                }
//            };
//        }
//        return $orders;
//    }

    function getTotalColumn($xls_data){
        $column = 0;
        foreach($xls_data as $item){
            if(!empty($item)){
                $column++;
            }else{
                break;
            }
        }
        return $column;
    }

    function getTotalRow($xls_data, $index){
        $row = 0;
        while($index < count($xls_data)){
            $item = $xls_data[$index]['B'];
            if(!empty($item)){
                $row++;
                $index++;
            }else{
                break;
            }

        }
        return $row;
    }



    function readExcel($file)
    {
        //include the file that loads the PhpSpreadsheet classes;
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $xls_data = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $firstRow = 3;
        $firstColumn = 2;
        $totalAvaibleColumn = $this->getTotalColumn($xls_data[$firstColumn]);
        $totalRow = $this->getTotalRow($xls_data, $firstRow);
        $totalAvaibleRow = $firstRow+ $totalRow;
        $orders = [];
        if ($totalAvaibleRow <= 100 && $totalAvaibleColumn > 0) {

            for($i=$firstRow; $i<$totalAvaibleRow; $i++) {
                $j = 2;
                $flag = true;
                foreach ($xls_data[$i] as $valueField) {
                    $data['city_id'] = ConfigUtil::get('PHNOMPHENH');
                    $data['service_pack_id'] = ConfigUtil::get('STANDARD');
                    switch ($j)
                    {
                        case 1:
                            $data['content'] = $this->clean($valueField);
                            break;
                        case 2:
                            if(!is_numeric($valueField))
                                $flag = false;
                            else
                                $data['weight'] = $this->clean($valueField);
                            break;
                        case 3:
                            if(!is_numeric($valueField))
                                $flag = false;
                            else
                                $data['money_collection'] = $this->clean($valueField);
                            break;
                        case 4:
                            $data['receiver_name'] = $this->clean($valueField);
                            break;
                        case 5:
                            //var_dump($valueField);
                            if(strlen($valueField)===0)
                                $flag = false;
                            else
                                $data['arr_receiver_phone'] = $this->clean($valueField);
                            break;
                        case 6:
                            //var_dump($valueField);
                            if(strlen($valueField)===0)
                                $flag = false;
                            else
                                $data['address'] = $this->clean($valueField);
                            break;
                        case 7:
                            $arr = explode(".", $valueField);
                            if(!empty($arr[0]))
                            {
                                if(is_numeric($arr[0]))
                                {
                                    if(!$this->Order->District->exists($arr[0]))
                                        $flag = false;
                                    else
                                        $data['district_id'] = $this->clean($arr[0]);
                                }
                                else
                                    $flag = false;
                            }
                            else
                            {
                                if(!empty($data['city_id'])){
                                    $data['district_id'] = $this->Order->District->getDefaultDistrictByCityId($data['city_id']);
                                }else{
                                    $flag = false;
                                }
                            }
                            break;
                        case 8:
                            $arr = explode(".", $valueField);
                            if(isset($arr[0]))
                            {
                                if(is_numeric($arr[0]))
                                {
                                    settype($arr[0],'int');
                                    if($arr[0]===0 || $arr[0]===1)
                                        $data['payment'] = $this->clean($arr[0]);
                                    else
                                        $flag = false;
                                }
                                else
                                    $flag = false;
                            }
                            else
                                $flag = false;
                            break;
                        case 9:
                            $data['note'] = $this->clean($valueField);
                            break;
                        case 10:
                            $data['shopId'] = $this->clean($valueField);
                            break;
                    }
                    $j++;
                }
                if($flag){
                    array_push($orders, $data);
                }
            }
        }
        return $orders;
    }


    function exportExcel($datas)
    {
        //get data
        $dataForTable = $datas['dataForTable'];
        $fileName = $datas['fileName'];
        //call phpexcel
        $objPHPExcel = new \PHPExcel();
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $objWorksheet->fromArray(
            $dataForTable, NULL, 'A1', TRUE
        );
        //$filename = $datas['id'] . '_' . $fileName . '_' . date('Ymd_His') . '.xlsx';
        $objPHPExcel->getActiveSheet()->setTitle($datas['fileName']);
        //create and write excel file
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename={$fileName}");
        header("Content-Transfer-Encoding: binary ");
        $objWriter->save('php://output');
    }

    /**
     * (Note: must place this function should be at the end of controller
     * and must call exit() right after calls this)
     *
     * @param $header
     * @param $data
     * @param $fileName
     * @return int|mixed
     */
    public function exportCSV($header, $data, $fileName)
    {
        // check empty
        if ((!isset($header)) && (!isset($data))) {
            return -1;      // unknown input
        }
        // check header and data's column size
        if (isset($data[0]) && count($header) != count($data[0])) {
            return -2;      // header size and data's column size not match
        }

        try {

            array_unshift($data, $header);

            $rowCnt = 0;
            $contents = null;

            // loop over the rows, outputting them
            foreach ($data as $rowData) {

                foreach ($rowData as &$row) {
                    $row = '"' . preg_replace('/"/', '""', $row) . '"';
                }

                if ($rowCnt > 0) {
                    $contents .= "\n";
                }

                $contents .= mb_convert_encoding(implode(',', $rowData), 'SJIS-win', 'UTF-8');

                $rowCnt++;

            }

            $filePath = ROOT . ConfigUtil::get('path_csv_job_mailmagazine');

            if (!file_exists($filePath)) {
                mkdir($filePath, 0777, true);
            }

            $fullPatchName = $filePath . $fileName;

            file_put_contents($fullPatchName, $contents);

            // Encode file name
            $fileName = urlencode($fileName);

            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$fileName");
            header("Content-Type: application/octet-stream; ");
            header("Content-Transfer-Encoding: binary");
            header('Content-Type: application/octet-stream');

            readfile($fullPatchName);

            unlink($fullPatchName);

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $e->getCode();
        }

        return 0;
    }

    //split array with over record to send csv.
    private function splitToSendCsvAndSendMail($cNmpMelmagaUserCursor, $headerCode, $newJob, $tblNmpJobManager, $from, $to, $cc, $bcc, $job_id, $iCountAll, $startTime, &$appendFile = null, $stepRow = null)
    {

        $listData = array();
        if (!isset($appendFile))
            $listData[] = $headerCode;


        // success B4
        if (!empty($cNmpMelmagaUserCursor)) {
            foreach ($cNmpMelmagaUserCursor as $user) {

                $listService = array();

                // MAILADDRESS
                $listService[] = $user['email_address'];

                // NAME
                $listService[] = $user['name'];

                // USER_ID
                $listService[] = $user['user_id'];

                // CONTRACT_TYPE
                if ($user['contract_type'] == '1') {
                    $listService[] = '個人';
                } elseif ($user['contract_type'] == '2') {
                    $listService[] = '法人';
                } else {
                    $listService[] = $user['contract_type'];
                }

                // SCREEN_NAME
                $listService[] = $user['screen_name'];

                // NAME_KANA
                $listService[] = $user['name_kana'];

                // BIRTHDAY
                $listService[] = $user['birthday'];

                // PREFECTURE
                $listService[] = $user['prefecture'];

                // CITY
                $listService[] = $user['city'];

                // USER_CONFIRM_TYPE
                if ($user['user_confirm_type'] == '0') {
                    $listService[] = '電話番号';
                } elseif ($user['user_confirm_type'] == '1') {
                    $listService[] = '契約番号';
                } else {
                    $listService[] = $user['user_confirm_type'];
                }

                // PHONE_NM
                $listService[] = $user['phone_nm'];

                // CONTRACT_NM
                $listService[] = $user['contract_nm'];

                // MAIL_MAGAZINE
                if ($user['mail_magazine'] == '0') {
                    $listService[] = '受信しない';
                } elseif ($user['mail_magazine'] == '1') {
                    $listService[] = '受信する';
                } else {
                    $listService[] = $user['mail_magazine'];
                }

                // WITHDRAWAL_REASON
                $listService[] = $user['withdrawal_reason'];

                // WITHDRAWAL_REASON_DETAIL
                $listService[] = $user['withdrawal_reason_detail'];

                // CREATE_DATE
                if (!empty($user['create_date'])) {
                    $listService[] = date('Y/m/d H:i:s', strtotime($user['create_date']));
                } else {
                    $listService[] = $user['create_date'];
                }

                // MODIFIED_DATE
                if (!empty($user['modified_date'])) {
                    $listService[] = date('Y/m/d H:i:s', strtotime($user['modified_date']));
                } else {
                    $listService[] = $user['modified_date'];
                }

                // DEL_FLG
                if ($user['del_flg'] == '0') {
                    $listService[] = '未削除';
                } elseif ($user['del_flg'] == '1') {
                    $listService[] = '削除済み';
                } else {
                    $listService[] = $user['del_flg'];
                }

                // LASTLOGIN_DATE
                if (!empty($user['lastlogin_date'])) {
                    $listService[] = date('Y/m/d H:i:s', strtotime($user['lastlogin_date']));
                } else {
                    $listService[] = $user['lastlogin_date'];
                }

                // LOGIN_COUNT
                $listService[] = $user['login_count'];

                $listData[] = $listService;
            }
        }

        //var_dump($appendFile);
        if (isset($appendFile)) {
            //append file csv.
            //pr($listData);
            $fileNameCsv = $this->exportCsv($listData, false, $appendFile, $stepRow);
        } else {
            $fileNameCsv = $this->exportCsv($listData);
            $appendFile = $fileNameCsv;
        }

    }


    function set_height_excel($data, $objPHPExcel)
    {
        foreach ($data as $key => $value)
            $objPHPExcel->getActiveSheet()->getColumnDimension($key)->setWidth($value);

    }

    function set_name_excel($data, $objPHPExcel)
    {
        foreach ($data as $key => $value)
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($key, $value);
    }


    function save_file_excel($objPHPExcel)
    {
        $objPHPExcel->getActiveSheet()->setTitle('Report');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Report_' . date('dmY') . '.xls"');

        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    function init($end_column = null, $bg_color = null)
    {
        $objPHPExcel = new \PHPExcel();
        //set color row.

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'font' => array(
                'bold' => true
            )
        );

        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:' . $end_column)
            ->getFill()
            ->setFillType(\PHPExcel_Style_Border::BORDER_THIN)
            ->getStartColor()->applyFromArray($styleArray);;

        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);


        //$objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);
        return $objPHPExcel;
    }


    function export_excel_order($orders, $order_status)
    {
        //set height.
        $objPHPExcel = $this->init('Z1');
        $data_height = array('A' => 30, 'B' => 30, 'C' => 30, 'D' => 30, 'E' => 30, 'F' => 30, 'G' => 30, 'H' => 30, 'I' => 30, 'J' => 30, 'K' => 30, 'L' => 30, 'M' => 30, 'N' => 30, 'O' => 30, 'P' => 30, 'Q' => 30, 'R' => 30, 'S' => 30, 'T' => 30, 'U' => 30, 'V' => 30, 'W' => 30, 'X' => 30);
        $this->set_height_excel($data_height, $objPHPExcel);
        $objPHPExcel->getActiveSheet()->getStyle('L')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('N')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('O')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('P')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('Q')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('R')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('S')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('T')->getNumberFormat()->setFormatCode(('#,##0'));

        $this->set_name_excel($orders['header'], $objPHPExcel);

        unset($orders['header']);
        //echo "<pre>";print_r($data);exit();
        //pr($orders);die;

        $j = 1;
        foreach ($orders as $item) {
            foreach ($item as $order) {

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . ($j + 1), $order['order_code'])
                    ->setCellValue('B' . ($j + 1), $order['shop_order_code'])
                    ->setCellValue('C' . ($j + 1), $order['create_date'])
                    ->setCellValue('D' . ($j + 1), $order['sender_fullname'])
                    ->setCellValue('E' . ($j + 1), $order['sender_phone'])
                    ->setCellValue('F' . ($j + 1), $order['sender_address'])
                    ->setCellValue('G' . ($j + 1), $order['receiver_fullname'])
                    ->setCellValue('H' . ($j + 1), $order['receiver_phone'])
                    ->setCellValue('I' . ($j + 1), $order['receiver_address'])
                    ->setCellValue('J' . ($j + 1), $order['order_type'])
                    ->setCellValue('K' . ($j + 1), $order['form_deliver'])
                    ->setCellValue('L' . ($j + 1), floatval($order['money_cod']))
                    ->setCellValue('M' . ($j + 1), $order['payer_text'])
                    ->setCellValue('N' . ($j + 1), floatval($order['total_take_user']))
                    ->setCellValue('O' . ($j + 1), floatval($order['shipping_fee']))
                    ->setCellValue('P' . ($j + 1), floatval($order['sub_fee']))
                    ->setCellValue('Q' . ($j + 1), floatval($order['protect_fee']))
                    ->setCellValue('R' . ($j + 1), floatval($order['medial_fee']))
                    ->setCellValue('S' . ($j + 1), floatval($order['money_cash_advance']))
                    ->setCellValue('T' . ($j + 1), floatval($order['total_shop_receive']))
                    ->setCellValue('U' . ($j + 1), $order['product_type'])
                    ->setCellValue('V' . ($j + 1), h($order['note']))
                    ->setCellValue('W' . ($j + 1), h($order['note_change_service']))
                    ->setCellValue('X' . ($j + 1), h($order['status']))
                    ->setCellValue('Y' . ($j + 1), isset($order['status_payment']) ? h($order['status_payment']) : "")
                    ->setCellValue('Z' . ($j + 1), isset($order['payment_code']) ? h($order['payment_code']) : "")
                    ->setCellValue('AA' . ($j + 1), isset($order['time_update_payment']) ? h($order['time_update_payment']) : "");
                $j++;
            }
        }
        $this->save_file_excel($objPHPExcel);
    }


    function export_excel_payment_orders($orders, $order_status)
    {
        //set height.
        $objPHPExcel = $this->init('O1');
        $data_height = array('A' => 30, 'B' => 30, 'C' => 30, 'D' => 30, 'E' => 30, 'F' => 30, 'G' => 30, 'H' => 30, 'I' => 30, 'J' => 30, 'K' => 30, 'L' => 30, 'M' => 30, 'N' => 30, 'O' => 30);
        $this->set_height_excel($data_height, $objPHPExcel);
        $objPHPExcel->getActiveSheet()->getStyle('I')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('J')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('K')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('L')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('M')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('N')->getNumberFormat()->setFormatCode(('#,##0'));
        $objPHPExcel->getActiveSheet()->getStyle('O')->getNumberFormat()->setFormatCode(('#,##0'));

        $this->set_name_excel($orders['header'], $objPHPExcel);

        unset($orders['header']);
        //echo "<pre>";print_r($data);exit();
        //pr($orders);die;

        $j = 1;
        foreach ($orders as $order) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . ($j + 1), $j)
                ->setCellValue('B' . ($j + 1), h($order['order_code']))
                ->setCellValue('C' . ($j + 1), h($order['shop_order_code']))
                ->setCellValue('D' . ($j + 1), h($order['create_date']))
                ->setCellValue('E' . ($j + 1), h($order['product_info']))
                ->setCellValue('F' . ($j + 1), h($order['receiver_fullname']))
                ->setCellValue('G' . ($j + 1), h($order['receiver_phone']))
                ->setCellValue('H' . ($j + 1), h($order['receiver_address']))
                ->setCellValue('I' . ($j + 1), h(floatval($order['total_take_user'])))
                ->setCellValue('J' . ($j + 1), h(floatval($order['shipping_fee'])))
                ->setCellValue('K' . ($j + 1), h(floatval($order['sub_fee'])))
                ->setCellValue('L' . ($j + 1), h(floatval($order['protect_fee'])))
                ->setCellValue('M' . ($j + 1), h(floatval($order['medial_fee'])))
                ->setCellValue('N' . ($j + 1), h(floatval($order['money_cash_advance'])))
                ->setCellValue('O' . ($j + 1), h(floatval($order['total_shop_receive'])));
            $j++;

        }
        $this->save_file_excel($objPHPExcel);
    }

}