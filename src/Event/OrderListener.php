<?php
/**
 * Created by PhpStorm.
 * User: doanh.lv
 * Date: 11/6/19
 * Time: 3:01 PM
 */

namespace App\Event;

use Cake\Log\Log;
use Cake\Event\EventListenerInterface;

class OrderListener implements EventListenerInterface {

    public function implementedEvents() {
        return array(
            'Model.Orders.afterUpdate' => 'afterUpdate'
        );
    }

    public function afterUpdate($event, $entity, $options) {
        Log::write(
            'info',
            'A new comment was published with id: ' . $event->data['id']);
    }
}