<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Psy\Shell as PsyShell;
use Cake\Http\Client;

/**
 * Simple console wrapper around Psy\Shell.
 */
class PaymentShell extends AccountantShell
{
    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main(){
        $this->loadModel('PaymentDetails');
        $searchDatas = [
            'from_create_date' => $this->currentDate,
            'to_create_date' => $this->currentDate,
        ];
        $this->PaymentDetails->getPaymentCashBook($searchDatas, $this->Excel, $this->Mail);        
    }
    
}
