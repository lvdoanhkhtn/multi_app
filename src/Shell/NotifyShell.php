<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Psy\Shell as PsyShell;
use Cake\Http\Client;


/**
 * Simple console wrapper around Psy\Shell.
 */
class NotifyShell extends Shell
{

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main()
    {
       
        $this->loadModel('Orders');
        $message =  $this->Orders->searchOrderToNotify();
        if(!empty($message)){
            $this->loadModel('Notifies');
            $entity = $this->Notifies->newEntity(['message' => $message]);
            if($this->Notifies->save($entity)){
                $this->sendNotify($message);
            }
        } 
    }
    public function sendNotify($message){
        $accessToken = 'key=AAAAj_GgsoQ:APA91bFQhha5PircJciuI3_XMmkwk01RN3nwuQWNzp9ZPpH-4BQs4oboiK9kDQPnKfJMK2ZLLAtS6gSjJin1-NuXE6fMp5hSGEcBGqccKRVv_YlGCXZezbejx3LQU_PoMm_OVuyotwpo';
        $http = new Client([
            'headers' => ['Authorization' => $accessToken, 'Content-Type' => 'application/json']
       ]);
        $data  = [
            'to' => '/topics/alerts',
            'notification' => [
                'title' => 'Notify for orders',
                'body' => $message
            ]
        ];
        $response = $http->post('https://fcm.googleapis.com/fcm/send', json_encode($data));
    }
}
