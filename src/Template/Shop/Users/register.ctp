<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>CambodiaShip</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.4 -->
  <?= $this->Html->css(['admin.min', 'bootstrap/dist/css/bootstrap.min', 'shop']); ?>
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/33767a916f.js"></script>
  <![endif]-->
</head>
<body style="background-image:url('/img/background.jpg');background-position: center;background-repeat: repeat; /* Do not repeat the image */
   background-size: cover;" /><div class="register-box">
  <div class="login-logo">
    <?= $this->Html->image('logo_bill.jpg', ['width' => '200px']); ?>
  </div>
  
  <p class="login-box-msg text-red">Make Cambodia ECommerce Better</p>

  <div class="register-box-body">
    <?= $this->Form->create(null, ['class' => 'validateForm customValidateForm']); ?>
      <div class="row">
        <?= $this->Flash->render() ?>
      </div>
      <?= $this->Element('User/form') ?>

    <div class="col-xs-12 box-header with-border">
        <div class="col-md-12 text-center">
          <button type="submit" class="btn btn-danger"><?= __('REGISTER') ?></button>
        </div>
      </div>

    <?= $this->Form->end(); ?>
    <?= $this->Html->link(__('Already have an account? Click here to sign in or sign up with'), ['controller' => 'users', 'action' => 'login'], ['class' => 'text-center text-red']) ?>
  </div><!-- /.form-box -->
</div><!-- /.register-box -->
</body>
<?= $this->Html->script(['jquery.min', 'jquery/jquery.validate','additional-methods','additional-setting', 'common']); ?>
</html>
