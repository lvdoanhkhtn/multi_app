<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>CambodiaShip</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?= $this->Html->css(['admin.min', './bootstrap/dist/css/bootstrap.min', 'common']); ?>
    <style>
        #hidden1 {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -50px;
            margin-left: -50px;
        }

        div.fadeMe {
            opacity: 0.5;
            background: #000;
            width: 100%;
            height: 100%;
            z-index: 10;
            top: 0;
            left: 0;
            position: fixed;
        }
    </style>
</head>

<body style="background-image:url('/img/background.jpg');background-position: center;background-repeat: no-repeat;
   background-size: cover;">
    <div id="fadeMe1" class="row row-center">
        <button id="hidden1" class="btn btn-lg btn-warning hidden">
            <span class="glyphicon glyphicon-refresh glyphicon-spin"></span>Loading...
        </button>
    </div>
    <div class="login-box">
        <div class="login-logo">
            <?= $this->Html->image('logo_bill.jpg', ['width' => '200px']); ?>
        </div><!-- /.login-logo -->
        <p class="login-box-msg text-red">Make Cambodia ECommerce Better</p>
        <?= $this->Form->create('search_orders', ['id' => 'search_orders']); ?>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= __('search_orders') ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">

                <div class="row form-group">
                    <div class="col-xs-12">
                        <?= $this->Form->control('key_word', ['class' => 'form-control', 'placeholder' => 'CBS123456', 'div' => false, 'label' => false]);  ?>
                    </div>
                </div>

                <div class="row form-group text-center">
                    <button id="btn-search-orders" type="submit" class="btn btn-danger"><?= __('search') ?></button>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        <?php $this->Form->end(); ?>
        <?= $this->Element('Modal/order_result_modal'); ?>
        <?= $this->Html->script(['jquery.min', 'Order/search-order']); ?>
    </div><!-- /.login-box -->
</body>