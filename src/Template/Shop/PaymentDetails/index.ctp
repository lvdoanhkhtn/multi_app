<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php 
                    $link = $this->Html->link(__('payment_history'), ['controller' => 'payments', 'action' => 'index'], ['class' => 'text-red']).'<span class="text-red"> > Payment Detail # </span>';
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">

                    <div class="col-xs-12 form-group text-right">
                        <div class="col-lg-10"></div>
                        <div class="col-lg-1 col-xs-6 form-group">
                            <?= $this->Html->link('<span class="glyphicon glyphicon glyphicon-download"></span>'.__('export'), ['action' => 'export', $id], ['escape' => false,'class' => 'btn btn-danger']); ?>
                        </div>
                        <div class="col-lg-1 col-xs-6 form-group">
                            <?= $this->Html->link('<span class="glyphicon glyphicon-print"></span>'.__('print'), ['controller' => 'payment_details', 'action' => 'printPayment', $id], ['escape' => false,'class' => 'btn btn-warning']); ?>
                        </div>

                    </div>

                    <div class="form-group col-md-12">

                        <?php if($this->Paginator->param('pageCount') > 1){ ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>
                        <?= $this->Element('PaymentDetail/table', ['paymentDetails' => $paymentDetails, 'isIndex' => true]); ?>
                        <?= $this->Element('paging') ?>

                    </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

