<html>
    <head>
        <meta charset="UTF-8">
        <title><?= __('payment') ?></title>
		<?= $this->Html->css(['admin.min', 'bootstrap/dist/css/bootstrap.min']); ?>

		<style type="text/css">
          		html {
 				    font-family: sans-serif;
 				}
         </style>
    </head>
    <body>
        <section>
            <div class="container">
				<?= $this->Element('PaymentDetail/table', ['paymentDetails' => $paymentDetails]); ?>
			</div>
        </section>
    </body>
</html>