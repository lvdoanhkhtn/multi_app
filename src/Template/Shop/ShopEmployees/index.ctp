<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = '<h3 class="box-title text-red">' . __('shop_employee_management') . '</h3>';
                echo $this->Element('breadcum', ['link' => $link]);
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'get']); ?>

                        <div class="row form-group">
                            <div class="col-md-4">
                                <?php echo $this->Form->control('key_word', array('value' => !empty($_GET['key_word']) ? $_GET['key_word'] : "", 'type' => 'text', 'class' => 'form-control', 'placeholder' => __('employee_key_word'), 'label' => __('key_word'))) ?>
                            </div>
                            <div class="col-md-4">
                                <?= $this->Element('Calendar/created_calendar'); ?>
                            </div>
                            <div class="col-md-4">
                                <?php echo $this->Form->control('active', array('default' => (isset($_GET['active']) && strlen($_GET['active']) > 0) ? $_GET['active'] : '', 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $configs['status'], 'label' => __('status'))) ?>
                            </div>
                        </div>



                        <?= $this->Element('CommonForm/search_buttons'); ?>

                        <div class="row form-group">

                        </div>



                        <?= $this->Form->end(); ?>
                        <?php if ($this->Paginator->param('pageCount') > 1) { ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">
                                <thead class="thead-dark">
                                    <tr>
                                        <th class="text-top"><?= __('status') ?></th>
                                        <th class="text-top"><?= __('user_name') ?></th>
                                        <th class="text-top"><?= __('date_created') ?></th>
                                        <th class="text-top"><?= __('full_name') ?></th>
                                        <th class="text-top"><?= __('email') ?></th>
                                        <th class="text-top"><?= __('phone') ?></th>
                                        <th class="text-top"><?= __('address') ?></th>
                                        <th class="text-top"><?= __('access') ?></th>
                                        <th class="text-top mw100"><?= __('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($employees as $employee) {
                                            $user = $employee->user;
                                            $shopPermissionName = !empty($user->shop_permissions[0]->name) ? $user->shop_permissions[0]->name : ""; 
                                         ?>
                                        <tr>
                                            <td><?= $this->AdminView->showActive($user->active); ?></td>
                                            <td><?= h($user->username); ?></td>
                                            <td><?= $employee->getCreated(); ?></td>
                                            <td><?= h($user->fullname); ?></td>
                                            <td><?= h($user->email); ?></td>
                                            <td><?= h($user->phone); ?></td>
                                            <td><?= h($employee->getAddress()); ?></td>
                                            <td><?= h($shopPermissionName); ?></td>
                                            <td>
                                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $employee->id, $employee->user_id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']); ?>
                                                <?= $this->Html->link('<i class="fa fa-exchange-alt"></i>', ['controller' => 'accounts', 'action' => 'adminChangePassword', $employee->user_id, 1], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Change password', 'class' => 'btn btn-outline-info']); ?>

                                            </td>
                                        </tr>

                                    <?php } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->Html->script(['Payment/payment']); ?>