<section class="content">
  <div class="row">
    <div class="col-xs-12 col-md-12">
      <div class="box box-solid">

        <?php
            $link = __('bank_account_info');
            echo $this->Element('breadcum', ['link' => $link]) ;
        ?>

        <div class="box-body">
          <?= $this->Form->create($bankAccount, ['type' => 'post','class' => 'validateForm customValidateForm']); ?>

          <?= $this->Element('BankAccount/form'); ?>


          <div class="col-xs-12 box-header">
            <div class="col-md-12 text-center">
              <button type="submit" class="btn btn-danger"><?= __('UPDATE') ?></button>
            </div>
          </div>

          <?= $this->Form->end(); ?>

          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </div>
</section>
