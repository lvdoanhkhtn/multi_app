<?= $this->Html->css('order/order'); ?>
<script>
	function displayLineChart() {
		var data = [
			<?php $i = 0;
			foreach ($countOrderStatuses as $key => $value) {
				$color = $codeColorOrderStatuses[$key];
				$count = $value;
				$name = $key;
			?> {
					value: <?= $count; ?>,
					color: "<?= $color; ?>",
					highlight: "<?= $color; ?>",
					label: "<?= $name; ?>"
				}
				<?php echo ($i < (count($countOrderStatuses) - 1)) ? "," : ""; ?>

			<?php $i++;
			} ?>


		];

		var ctx = document.getElementById("myPieChart").getContext("2d");
		var options = {};
		var myPieChart = new Chart(ctx).Doughnut(data, options);


		var cityDatas = [
			<?php $i = 0;
			foreach ($reportByCities as $reportByCity) {
				$color = $reportByCity['color'];
				$count = $reportByCity['total'];
				$name = $reportByCity['city']['name'];
			?> {
					value: <?= $count; ?>,
					color: "<?= $color; ?>",
					highlight: "<?= $color; ?>",
					label: "<?= $name; ?>"
				}
				<?php echo ($i < (count($reportByCities) - 1)) ? "," : ""; ?>

			<?php $i++;
			} ?>


		];

		var ctx = document.getElementById("myPieCityChart").getContext("2d");
		var options = {};
		var myPieChart = new Chart(ctx).Doughnut(cityDatas, options);



	}
</script>

<body onload="displayLineChart();">
	<section class="content">
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="box box-solid">

					<?php
					$link = $this->Html->link(__('orders_management'), ['controller' => 'orders', 'action' => 'index'], ['class' => 'text-red']) . '<span class="text-red"> > ' . __('orders_home') . ' </span>';
					echo $this->Element('breadcum', ['link' => $link]);
					?>
					<div class="box-body">
						<div class="form-group col-md-12 table-responsive">

							<div class="box-body table-responsive">
								<?php echo $this->Form->create('FilterOrder', array('type' => 'get', 'id' => 'FilterOrder', 'role' => 'Form')) ?>

								<div class="form-group row">
									<?php if ($user['role_id'] !== CUSTOMER) { ?>
										<div class="col-md-3">
											<?php echo $this->Form->control('user_id', array('default' => !empty($_GET['user_id']) ? $_GET['user_id'] : '', 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $customers, 'label' => __('customer'))) ?>
										</div>
									<?php } ?>
									<div class="col-md-3">
										<?php echo $this->Form->input('date_range', array('value' => isset($_GET['date_range']) ? $_GET['date_range'] : 30, 'id' => 'date-range', 'options' => $rangeOfDate, 'type' => 'select', 'class' => 'form-control', 'label' => __('date_range'))) ?>
									</div>
									<div id="reportDate" class="col-md-3" style="display:<?= (!empty($_GET['date_range']) &&  $_GET['date_range'] == 100) ? 'block' : 'none' ?>">
										<?= $this->Element('Calendar/stored_calendar') ?>
									</div>
								</div>

								<div class="row form-group ">
									<div class="col-xs-12 text-right">
										<?= $this->Form->button('<i class="fa fa-search"></i>' . __('search'), array('class' => 'btn btn-info', 'type' => 'submit')); ?>
									</div>
								</div>
								<?php echo $this->Form->end(); ?>
								<div class="form-group no-padding text-right">

									<div class="col-lg-3 col-sm-6">
										<canvas id="myPieChart" style="width: 300px; height: 300px"></canvas>
										<canvas id="myPieCityChart" style="width: 300px; height: 300px"></canvas>
									</div>
									<div class="col-lg-3 col-sm-6 text-left">
										<ul class="sum-order">
											<li><?= __('total_orders') ?></li>
											<li class="currency"><?= number_format($ordersSummary['total_order']) ?></li>
											<li><?= __('total_cod') ?></li>
											<li class="currency"><?= number_format($ordersSummary['sum_total']) ?></li>
											<li><?= __('total_fee') ?></li>
											<li class="currency"><?= number_format($ordersSummary['sum_cost']) ?></li>
											<li><?= __('return_rate') ?></li>
											<li class="currency"><?php 
													$returnOrders = $countOrderStatuses['Return'] + $countOrderStatuses['Return in progress'] + $countOrderStatuses['Return completed'] + $countOrderStatuses['Pending'] + $countOrderStatuses['Order charged'];
													$returnRate = !empty($returnOrders) ? round((($returnOrders/$ordersSummary['total_order'])*100), 2) : 0;
													echo $returnRate.'%';
												?>
											</li>
											<li><?= __('total_return_fee') ?></li>
											<li class="currency"><?= number_format($ordersSummary['sum_return_cost']) ?></li>
										</ul>
									</div>
									<div class="col-lg-3 col-sm-12 text-left form-group">
										<label>List statuses</label>
										<?php
										foreach ($countOrderStatuses  as $key => $value) {
											$color = $codeColorOrderStatuses[$key];
											$count = $value;
											$name = $key;
										?>
											<ul class="sum-status">
												<li>
													<span class="color" style="background-color: <?= $color ?>"></span>
													<span class="name"><?= $name; ?></span>
													<span class="count"><?= $count; ?></span>
												</li>


											</ul>
										<?php } ?>
									</div>
									<div class="col-lg-3 col-sm-12 text-left form-group">
										<label>List cities</label>
										<?php foreach ($reportByCities as $reportByCity) {
											$color = $reportByCity['color'];
											$count = $reportByCity['total'];
											$name = $reportByCity['city']['name'];
										?>
											<ul class="sum-status">
												<li>
													<span class="color" style="background-color: <?= $color ?>"></span>
													<span class="name"><?= $name; ?></span>
													<span class="count"><?= $count; ?></span>
												</li>
											</ul>
										<?php } ?>
									</div>
								</div><!-- /.box-body -->
							</div>
						</div>
					</div>
				</div>
	</section>
</body>
<?= $this->Html->script('Chart.min'); ?>