<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box ">

                <?php
                $link = $this->Html->link(__('orders_management'), ['controller' => 'orders', 'action' => 'index'], ['class' => 'text-red']) . '<span class="text-red"> > View </span>';
                echo $this->Element('breadcum', ['link' => $link]);
                ?>

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="col-md-6 no-padding-left">
                                <div class="box  box-primary flat">
                                    <div class="box-header">
                                        <div class="col-xs-7">
                                            <h4 class="text-bold"><?= __('status') ?></h4>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 no-padding-left text-left">
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('order_code') ?> :</span>
                                                    <span><?= h($order['orderId']); ?></span>
                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('order_status') ?> :</span>
                                                    <span><?= $order['order_status_name'] ?></span>
                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('date_created') ?> :</span>
                                                    <span><?= !empty($order['created']) ? $this->AdminView->formatDateTime($order['created']) : "" ?></span>
                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('date_completed') ?> :</span>
                                                    <span><?= !empty($order['finished']) ? $this->AdminView->formatDateTime($order['finished']) : "" ?></span>
                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('creater') ?> :</span>
                                                    <span><?= $order['creater'] ?></span>
                                                </div>
                                                <?php if (!empty($order['is_pick_up'])) { ?>
                                                    <div class="col-xs-12 form-group">
                                                        <span><?= __('is_pick_up') ?></span>
                                                    </div>
                                                <?php } ?>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="box  box-primary flat">
                                    <div class="box-header">
                                        <div class="col-xs-7">
                                            <h4 class="text-bold"><?= __('pickup') ?></h4>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 no-padding-left text-left">
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('sender') ?> :</span>
                                                    <span><?= h($order['sender_pickup']); ?></span>
                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('phone') ?> :</span>
                                                    <span><?= h($order['phone_pickup']); ?></span>


                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('address') ?> :</span>
                                                    <span><?= h($order['address_pickup']); ?></span>

                                                </div>
                                                <div class="col-xs-12">
                                                    <span><?= __('province') ?> :</span>
                                                    <span><?= h($order['province_pickup']); ?></span>

                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>


                                <div class="box  box-primary flat">
                                    <div class="box-header">
                                        <div class="col-xs-7">
                                            <h4 class="text-bold"><?= __('delivery') ?></h4>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 no-padding-left text-left">
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('receiver') ?> :</span>
                                                    <span><?= h($order['receiver_name']); ?></span>
                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('phone') ?> :</span>
                                                    <span><?= h($order['arr_receiver_phone']); ?></span>
                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('address') ?> :</span>
                                                    <span><?= h($order['address_delivery']); ?></span>

                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('province') ?> :</span>
                                                    <span><?= h($order['province_delivery']); ?></span>

                                                </div>

                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('delivery_service') ?> :</span>
                                                    <span><?= h($order['delivery_service']); ?></span>

                                                </div>

                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('payer') ?> :</span>
                                                    <span><?= h($order['payer']); ?></span>

                                                </div>

                                                <div class="col-xs-12 form-group">
                                                    <span><?= __('cash_on_delivery') ?> :</span>
                                                    <span><?= h($order['total']); ?></span>

                                                </div>

                                                <div class="col-xs-12">
                                                    <span><?= __('note') ?> :</span>
                                                    <span><?= h($order['note']); ?></span>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="box  box-primary flat">
                                    <div class="box-header">
                                        <div class="col-xs-7">
                                            <h4 class="text-bold"><?= __('product') ?></h4>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 no-padding-left text-left">
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __("private_code") ?> :</span>
                                                    <span><?= h($order['shopId']); ?></span>

                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __("product_name") ?> :</span>
                                                    <span><?= h($order['content']); ?></span>

                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __("product_price") ?> :</span>
                                                    <span><?= h($order['product_value']); ?></span>

                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span><?= __("product_weight") ?> :</span>
                                                    <span><?= h($order['weight']); ?></span>

                                                </div>

                                                <div class="col-xs-12">
                                                    <span><?= __("product_size") ?> :</span>
                                                    <span><?= sprintf("%s (cm) X %s (cm) X %s (cm)", $order['length'], $order['width'], $order['height']); ?></span>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="box  box-primary flat">
                                    <div class="box-header">
                                        <div class="col-xs-7">
                                            <h4 class="text-bold"><?= __('amount') ?></h4>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 no-padding-left text-left">

                                                <?php if($order['order_status'] != '6'){ ?>


                                                <div class="col-xs-12 form-group">
                                                    <span class="text-bold"><?= __("total") ?> :</span>
                                                    <span><?= $this->AdminView->getCurrencyCam($order['total']); ?></span>

                                                </div>

                                                <div class="col-xs-12 form-group">
                                                    <span class="text-bold"><?= __("total_fee") ?> :</span>
                                                    <span><?= $order['total_fee']; ?></span>

                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span class="margin-left-lg"><?= __("shipping_fee") ?> :</span>
                                                    <span><?= $order['postage']; ?></span>

                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <span class="margin-left-lg"><?= __("cod_fee") ?> :</span>
                                                    <span><?= $order['collection_fee']; ?></span>
                                                </div>

                                                <div class="col-xs-12 form-group">
                                                    <span class="margin-left-lg"><?= __("insurance_fee") ?> :</span>
                                                    <span><?= $order['protect_fee']; ?></span>

                                                </div>

                                                <div class="col-xs-12 form-group">
                                                    <span class="margin-left-lg"><?= __("sub_fee") ?> :</span>
                                                    <span><?= $order['sub_fee']; ?></span>

                                                </div>

                                                <?php } ?>

                                                <?php if($order['order_status'] == '6'){ ?>
                                                    
                                                <div class="col-xs-12">
                                                    <span class="text-bold"><?= __("return_fee") ?> :</span>
                                                    <span><?= $order['transfer_fee']; ?></span>
                                                </div>

                                                <?php } ?>

                                                <?php if($order['order_status'] != '6'){ ?>


                                                <div class="col-xs-12 form-group">
                                                    <span class="text-bold"><?= __("amount") ?> :</span>
                                                    <span><?= $this->AdminView->getCurrencyCam($order['total_amount']); ?></span>
                                                </div>

                                                <?php } ?>


                                            </div>

                                        </div>
                                    </div>

                                </div>


                            </div>
                            <div class="col-md-6 no-padding-left">
                                <div class="box  box-default flat">
                                    <div class="box-header">
                                        <div class="col-xs-7">
                                            <h4 class="text-bold"><?= __('history') ?></h4>
                                        </div>
                                    </div>
                                    <?php if (count($histories) > 0) { ?>
                                        <div class="box-body">
                                            <ul class="timeline">
                                                <?php foreach ($histories as $history) { ?>
                                                    <li class="time-label">
                                                        <span class="bg-red"><?= trim($history['modified']); ?></span>
                                                        <span class="bg-red"><?= trim($history['modifier']); ?></span>
                                                    </li>
                                                    <li>
                                                        <?php foreach ($history['text'] as $item) { ?>
                                                            <div class="timeline-body"><?= h(trim($item)); ?></div>
                                                        <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>

                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $("body").children().each(function() {
        $(this).html($(this).html().replace(/\u2028/g, " ")); // remove the space in front of the first semicolon!
    });
</script>