<?= $this->Html->css(['jquery-ui']); ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">
                <?php
                    $link = $this->Html->link(__('orders_management'), ['controller' => 'orders', 'action' => 'index'], ['class' => 'text-red']).'<span class="text-red"> >'.__('add').'</span>';
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <?= $this->Form->create(null, ['id' => 'order_add', 'class' => 'validateForm Order'])?>
                    <div class="form-group col-md-12 table-responsive parent-form">

                        <?= $this->Element('Order/box_order'); ?>

                        <div class="col-xs-12 col-md-12 col-sm-12 no-padding">
                            <div class="btn-group pull-left">
                                <?= $this->Form->button(__('add_1_order'), [
                                    'class' => 'btn btn-info',
                                    'type'  => 'button',
                                    'id'  => 'add_more',
                                ]) ?>
                                <?= $this->Form->button(__('remove_1_order'), [
                                    'class' => 'btn btn-danger',
                                    'type'  => 'button',
                                    'style' => 'display: none; margin-left:5px',
                                    'id'  => 'remove_more',
                                ]) ?>
                            </div>

                            <div class="btn-group pull-right form-group">
                                <?= $this->Form->button(__('create_orders'), ['escape' => false,'class' => 'btn btn-success btnSubmit']); ?>
                            </div>
                        </div>



                    </div>

                    <?= $this->Form->end(); ?>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<?= $this->Html->script('jquery-ui.min', ['block' => 'script']); ?>

<script>
    $(document).ready(function(){
    //add more one form add_orther
        var count = 2;
        $('#add_more').click(function (e) {
            e.preventDefault();
            if (count <= 5) {
                var form = '<div class="panel panel-default add-form">' +
                    '<div class="panel-heading">' +
                    '<div class="row">' +
                    '<div class="col-xs-2"><?= __('order') ?><span id="count_order_'+count+'">'+count+'</span></div>' +
                    '<div class="col-xs-2"><?= __('total_amount').':' ?><span id="total_amount_'+count+'"></span></div>' +
                    '<div class="col-xs-2"><?= __('total_fee').':' ?><span id="total_fee_'+count+'"></span></div>' +
                    '<div class="col-xs-2"><?= __('shipping_fee').':' ?><span id="shipping_fee_'+count+'"></span></div>' +
                    '<div class="col-xs-2"><?= __("cod_fee").':'?><span id="cod_fee_'+count+'"></span></div>' +
                    '<div class="col-xs-2"><?= __('insurance_fee').':' ?>' +
                    '<span id="insurance_fee_'+count+'"></span>' +
                    // '<span class="glyphicon glyphicon-remove remove"></span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="panel-body">' +
                    '<div class="col-xs-12 col-md-12 col-sm-12 no-padding-left no-padding-right">' +
                    '<div class="col-md-2 no-padding margin-top-sm">' +
                    '<label class="fs-12"><?= __('Delivery To:') ?></label>\n' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<input type="text" name="orders['+count+'][receiver_name]" class="form-control required max-length" data-max-length="255" data-name="<?=__("receiver_name").'(*)'?>" placeholder="<?=__("receiver_name").'(*)'?>" id="receiver_name_'+count+'" aria-required="true">' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group input text">' +
                    '<input type="text" name="orders['+count+'][arr_receiver_phone]" class="ui-widget arr_receiver_phone form-control required max-length" data-max-length="25" data-name="<?=__("receiver_phone").'(*)'?>" placeholder="<?=__("receiver_phone").'(*)'?>" id="arr_receiver_phone_'+count+'" aria-required="true">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group input text"><input type="text" name="orders['+count+'][address]" class="form-control required max-length" data-max-length="255" data-name="<?=__("receiver_address").'(*)'?>"" placeholder="<?=__("receiver_address").'(*)'?>" id="address_'+count+'" aria-required="true"></div>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group input select">' +
                    '<select name="orders['+count+'][city_id]" id="city_id_'+count+'" class="form-control required" data-name="<?=__('select_city').'(*)'?>" aria-required="true">' +
                    '<option value=""><?= __('select_city') ?>(*) </option>'+
                        <?php foreach($deliveryCities as $key => $value) { ?>
                            '<option value="<?php echo $key?>"><?php echo $value; ?></option>'+
                        <?php } ?>
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group input select">' +
                    '<select name="orders['+count+'][district_id]" id="district_id_'+count+'" class="form-control">' +
                    '<option value=""><?= __('select_district')?></option>' +
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-xs-12 col-md-12 col-sm-12 no-padding-left no-padding-right">' +
                    '<div class="col-md-2 no-padding margin-top-sm">' +
                    '<label class="fs-12"><?= __('service_pack') ?></label>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group input select">' +
                    '<select name="orders['+count+'][service_pack_id]" id="service_pack_id_'+count+'" class="form-control required" data-name="<?=__('service_pack')?>" aria-required="true">' +
                        <?php foreach($configs['service_pack'] as $key => $value) { ?>
                            '<option value="<?php echo $key?>"><?php echo $value; ?></option>'+
                        <?php } ?>
                    '</select>' +
                    '</div> ' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group input select">' +
                    '<select name="orders['+count+'][payment]" id="payment_'+count+'" class="form-control required" data-name="<?=__('payment')?>" aria-required="true">' +
                        <?php foreach($configs['payment'] as $key => $value) { ?>
                            '<option value="<?php echo $key?>"><?php echo $value; ?></option>'+
                        <?php } ?>
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group input text">' +
                    '<input type="text" data-name="<?=__('money_collection')?>" name="orders['+count+'][money_collection]" id="money_collection_'+count+'" class="required positiveNumber form-control" placeholder="<?=__("money_collection").': $1.00(*)'?>" >' +
                    '</div> ' +
                    '</div>' +
                    '<div class="col-md-4">' +
                    '<div class="form-group input text">' +
                    '<input type="text" name="orders['+count+'][note]" class="form-control max-length" data-max-length="1000" data-name="<?=__("delivery_note")?>" placeholder="<?=__("delivery_note")?>" id="note'+count+'">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-xs-12 col-md-12 col-sm-12 no-padding-left no-padding-right">' +
                    '<div class="col-md-2 no-padding margin-top-sm">' +
                    '<label class="fs-12"><?= __('Shipment Detail:') ?></label>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group input text">' +
                    '<input type="text" name="orders['+count+'][shopId]" id="shop_id_'+count+'" class="form-control max-length exist" data-max-length="20" data-name="<?=__('private_code')?>" placeholder="<?=__('private_code')?>">' +
                    '</div>      ' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group input text">' +
                    '<input type="text" name="orders['+count+'][content]" class="form-control max-length" data-max-length="1000" data-name="<?=__('content')?>" placeholder="<?=__('content')?>" id="content'+count+'">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group input text">' +
                    '<input data-name=<?=__('product_value')?> type="text" name="orders['+count+'][product_value]" id="product_value_'+count+'" class="positiveNumber form-control" data-name="<?=__('product_value')?>"  placeholder="<?=__("product_value").": $1.00"?>">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-1 no-padding-right">' +
                    '<div data-name=<?=__('weight')?> class="form-group input text"><input type="text" name="orders['+count+'][weight]" id="weight_'+count+'" class="positiveNumber form-control" data-name="<?=__('weight')?>"  placeholder=<?=__("weight")."(kg)"?>>' +
                    '</div>      ' +
                    '</div>' +
                    '<div class="col-md-1">' +
                    '<div data-name=<?=__('length')?>  class="form-group input text"><input type="text" id="length_'+count+'" name="orders['+count+'][length]" class="integer form-control" data-name="<?=__('length')?>"  placeholder=<?=__("size")."(cm)"?> id="length'+count+'"></div>' +
                    '</div>' +
                    '<div class="col-md-1">' +
                    '<div class="form-group input text">' +
                    '<input data-name=<?=__('width')?>  type="text" name="orders['+count+'][width]" id="width_'+count+'"   class="integer form-control" placeholder=<?=__("size")."(cm)"?> data-name="<?=__('width')?>"  id="width'+count+'"></div>' +
                    '</div>' +
                    '<div class="col-md-1">' +
                    '<div data-name=<?=__('height')?> class="form-group input text"><input type="text" id="height_'+count+'" name="orders['+count+'][height]" class="integer form-control" data-name="<?=__('height')?>"  placeholder=<?=__("size")."(cm)"?> id="height'+count+'"></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' ;
                $('.add-form-background').append(form);
                $('#remove_more').show();
                var count_form = $('.add-form').length;
                if(count_form >= 5) {
                    $('#add_more').hide();
                }
                count++;
                loadAddressByPhone();
            }
        });

        //click remove one form
        $('#remove_more').click(function () {
            count--;
            var form_add_last = $('#remove_more').closest('.parent-form').find('.add-form-background').children().last();
            form_add_last.remove();
            var count_form = $('.add-form').length;
            if(count_form < 5) {
                $('#add_more').show();
                if(count_form === 1) {
                    $('#remove_more').hide();
                }
            }
        });

        loadAddressByPhone();

    });

    function loadAddressByPhone() {
        $(".arr_receiver_phone").autocomplete({
            source: [
                <?php
                if(count($phones)>0)
                {
                    $i=0;
                    foreach($phones as $phone)
                    {
                        if($i < count($phones))
                            echo "'".trim($phone)."',";
                    };
                    $i++;
                }
                ?>
            ],
            select: function (e, ui) {
                getAddress(e.target.id, $.trim(ui.item.value));
            }
        });

    }
</script>

