<?= $this->Html->css('drap-file'); ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = $this->Html->link(__('orders_management'), ['controller' => 'orders', 'action' => 'index'], ['class' => 'text-red']).'<span class="text-red"> > Import </span>';
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null,
                            [
                                'type' => 'POST',
                                'enctype' => 'multipart/form-data',
                                'id' => 'importOrder'
                            ]
                        );?>
                        <div class="row form-group">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="col-md-2 no-padding margin-top-sm">
                                    <label><?= __('delivery_from'); ?></label>
                                </div>

                                <div class="col-md-10 no-padding">
                                    <?php echo $this->Form->control('address_id',array('type' => 'select', 'class' => 'form-control', 'options' => $addresses, 'label' => false))?>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 row form-group">
                            <b><?= __('excel_alert') ?></b>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-12 form-drag">
                            <div class="upload-container">
                                <div class="upload_input">
                                    <input type="file" name="import_file" id="file" class="upload_file required" data-name="<?= __('file'); ?>"/>
                                    <label for="file"><strong class="upload_dragdrop"><?= __("select_file_from_your_computer") ?></strong></label>
                                </div>
                            </div>
                            <div id="file_name" style="text-align: center"></div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div id="error-mess"></div>
                        </div>

                        <div class="col-xs-12 col-md-12 col-sm-12 no-padding no-padding">
                            <div class="btn-group pull-left">
                                <?= $this->Html->link('<span class="glyphicon glyphicon-download"></span>'.__('download_template'), ['controller' => 'commons', 'action' => 'downloadSpecificFile', \App\Libs\ConfigUtil::get('UrlFileExcel')], ['escape' => false,'class' => 'btn btn-success']); ?>
                            </div>
                          

                            <div class="btn-group pull-right">
                                <?= $this->Form->button('<span class="glyphicon glyphicon-upload"></span>'.__('UPLOAD'),['id' => 'btn-submit','escape' => false,'class' => 'btn btn-success']); ?>
                            </div>

                        </div>

                        <?= $this->Form->end(); ?>

                    </div>
                    
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script('drap-file', ['block' => 'scriptBottom']); ?>

