<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = $this->Html->link(__('orders_management'), ['controller' => 'orders', 'action' => 'index'], ['class' => 'text-red']).'<span class="text-red"> >'.__('edit').'</span>';
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <?= $this->Form->create($order, ['id' => 'order_edit', 'class' => 'validateForm Order'])?>
                    <div class="form-group col-md-12 table-responsive">

                         <?= $this->Element('Order/box_order'); ?>


                        <div class="col-xs-12 col-md-12 col-sm-12 no-padding-left no-padding-right">
                            <div class="pull-right form-group">
                                <?= $this->Form->button(__('update'), [], ['escape' => false,'class' => 'btn btn-success']); ?>
                                <?= $this->Html->link(__('cancel'), ['action' => 'index'], ['class' => 'btn btn-danger']); ?>
                            </div>
                        </div>
                        
                    </div>
                    
                    <?= $this->Form->end(); ?>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>


