<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = __('confirm_upload');
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">

                        <table id="master-table" class="table table-bordered table-condensed" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th><?= __('content')?><span class="errorTxt">(*)</span></th>
                                <th><?= __('weight')?><span class="errorTxt">(*)</span></th>
                                <th><?= __('money_collection')?><span class="errorTxt">(*)</span></th>
                                <th><?= __('receiver_name') ?><span class="errorTxt">(*)</span></th>
                                <th><?= __('receiver_phone') ?><span class="errorTxt">(*)</span></th>
                                <th><?= __('receiver_address') ?><span class="errorTxt">(*)</span></th>
                                <th><?= __('receiver_city')?><span class="errorTxt">(*)</span></th>
                                <th><?= __('receiver_district')?></th>
                                <th><?= __('payer')?></th>
                                <th><?= __('note')?></th>
                                <th><?= __('order_shop')?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?= $this->Form->create('confirm_add_order_excel'); ?>

                            <?php $i=1; $count_error = 0; foreach ($results as $order){
                                $color = '';
                                $flag = $this->OrderView->validOrder($order);
                                if(!$flag){
                                    $color = 'bg-red';
                                    $count_error++;
                                }
                                ?>
                                <?= $this->Form->control(sprintf("orders[%s]",($i-1)),['type' => 'hidden', 'value' => $flag]); ?>
                                <tr class="<?= $color; ?>" >
                                    <td><?= $i++ ?></td>
                                    <td><?= h($order['content']); ?></td>
                                    <td><?= (isset($order['weight']) && strlen($order['weight']) > 0 && $this->OrderView->checkNumberic($order['weight'])) ? h($order['weight']) : '<del>'.$order['weight'].'</del>'; ?></td>
                                    <td><?= (isset($order['money_collection']) && strlen($order['money_collection']) > 0 && $this->OrderView->checkNumberic($order['money_collection'])) ? h(number_format($this->OrderView->formatNumber($order['money_collection']))) : '<del>'.$order['money_collection'].'</del>'; ?></td>
                                    <td><?= h($order['receiver_name']) ?></td>
                                    <td><?= h($order['arr_receiver_phone']) ?></td>
                                    <td><?= h($order['address']); ?></td>
                                    <td><?= (!empty($order['city_id']) && $this->OrderView->checkNumber($order['city_id'])) ? h($deliveryCities[$order['city_id']]) : '<del>'.$order['city_id'].'</del>'; ?></td>
                                    <td><?= ($this->OrderView->checkDistrict($order['district_id'], $order['city_id'])) ? h($order['district_name']) : '<del>'.$order['district_name'].'</del>'; ?></td>
                                    <td><?= (isset($order['payment']) && strlen($order['payment']) > 0 &&  $this->OrderView->checkNumber($order['payment'])) ? h($configs['payment'][$order['payment']]) : '<del>'.$order['payment'].'</del>'; ?></td>
                                    <td><?= h($order['note']); ?></td>
                                    <td><?= h($order['shopId']) ?></td>
                                </tr>

                            <?php } ?>
                            <tr>
                                <td colspan="14">
                                    <?php
                                    if(count($results) > 0)
                                        echo sprintf('<b>%s</b>', \App\Libs\ConfigUtil::getMessage('ICL013',[$count_error, count($results)]));
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="14">
                                    <?= $this->Html->link(__('cancel'), ['action' => 'deleteConfirmAddOrderExcel'], ['class'=>'btn btn-danger pull-left','confirm' => __('are_you_sure_cancel')]) ?>
                                    <?= $this->Form->button(__('confirm'), ['type' => 'submit','class' => 'btn btn-success pull-right']); ?>
                                </td>
                            </tr>


                            <?= $this->Form->end(); ?>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
