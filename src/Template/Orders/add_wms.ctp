<?= $this->Html->css(['jquery-ui']); ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">
                <?php
                $link = $this->Html->link(__('orders_management'), ['controller' => 'orders', 'action' => 'index'], ['class' => 'text-red']) . '<span class="text-red"> >' . __('add') . '</span>';
                echo $this->Element('breadcum', ['link' => $link]);
                ?>

                <div class="box-body">
                    <?= $this->Form->create(null, ['id' => 'order_add', 'class' => 'validateForm Order']) ?>
                    <div class="form-group col-md-12 table-responsive parent-form">
                        <?= $this->Element('Order/box_order', ['isWms' => true]); ?>
                        <div class="row form-group">
                            <div class="col-md-2">
                                <label class="fs-12"><?= __('Items') ?></label>
                            </div>
                            <div id="items-wrapper" class="col-md-10">
                                &nbsp;
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-2">
                                <label class="fs-12"><?= __('Add item') ?></label>
                            </div>
                            <div class="col-md-10">
                                <?php echo $this->Form->control('items', array('id' => 'items', 'type' => 'select', 'class' => 'select2 form-control', 'options' => $products, 'multiple' => true, 'label' => false)) ?>
                            </div>
                        </div>
                        <div class="btn-group pull-right form-group">
                            <?= $this->Form->button(__('create_order'), ['escape' => false, 'class' => 'btn btn-success btnSubmit']); ?>
                        </div>
                    </div>
                </div>

                <?= $this->Form->end(); ?>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    </div>
</section>
<?= $this->Html->script('Order/add-wms') ?>