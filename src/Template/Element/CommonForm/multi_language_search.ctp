<?php $customControllers =  array_values(\App\Libs\ConfigUtil::get('CUSTOM_CONTROLLERS')); ?>
<?= $this->Form->create(null, ['type' => 'GET']); ?>
<div class="row form-group">
    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
        <?php echo $this->Form->control('id', array('default' => !empty($_GET['id']) ? $_GET['id'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $list, 'label' => __('name')))?>
    </div>
</div>

<?= $this->Element('CommonForm/search_buttons'); ?> 
<?php
    if(!empty($isSync)){
        echo $this->Html->link('<i class="fa fa-file-excel"></i>'.__('sync_controller'), ['action' => 'UpdateController'], ['escape' => false,'class' => 'btn btn-success']);
    }
?>


<div class="row form-group">

</div>
<?= $this->Form->end(); ?>

<div class="row form-group">

</div>
<?php if($this->Paginator->param('pageCount') > 1){ ?>
    <?= $this->AdminView->paginateMenuAndResult() ?>
<?php } ?>
<div class="box table-responsive">
    <table class="table table-striped table-sm">
        <thead class="thead-dark">
        <tr>

            <th class="text-top"><?= __('id') ?></th>
            <th class="text-top"><?= __('name') ?></th>
            <th class="text-top"><?= __('eng_name') ?></th>
            <th class="text-top"><?= __('kh_name') ?></th>
            <th class="text-top"><?= __('status') ?></th>
            <th class="text-top"><?= __('action') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($datas as $data){ ?>
            <tr>
                <td><?= $data->id; ?></td>
                <td><?= h($data->name); ?></td>
                <td><?= h($data->eng_name); ?></td>
                <td><?= h($data->kh_name); ?></td>
                <td><?= $this->AdminView->showActive($data->active); ?></td>
                <td>
                    <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $data->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']); ?>
                </td>
            </tr>

        <?php } ?>

        </tbody>
    </table>
    <?= $this->Element('paging') ?>
</div>
