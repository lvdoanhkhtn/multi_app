<div class="pull-right">

    <?= $this->Form->button('<i class="fa fa-search"></i>'.__('search'), array('class' => 'btn btn-info','type'=>'submit')); ?>
    <?= $this->Html->link('<i class="fa fa-plus-circle"></i>'.__('create'), ['action' => 'add', !empty($id) ? $id : ''],['id' => 'btnPrint','escape' => false,'class' => 'btn btn-warning']); ?>
    <?= $this->Html->link('<i class="fa fa-file-excel"></i>'.__('export'), ['action' => 'export'], ['escape' => false,'class' => 'btn btn-success']); ?>
    
</div>