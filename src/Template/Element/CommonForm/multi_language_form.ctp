<div class="row">
    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12">
        <?= $this->Form->control('name', ['class' => 'required form-control max-length','data-max-length' => 20, 'data-name' => __('name'), 'maxlength' => false, 'placeholder' => __('name'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('name').'(*)']);  ?>
    </div>
    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12">
        <?= $this->Form->control('eng_name', ['class' => 'required form-control max-length','data-max-length' => 20, 'data-name' => __('eng_name'), 'maxlength' => false, 'placeholder' => __('eng_name'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('eng_name').'(*)']);  ?>
    </div>
    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12">
        <?= $this->Form->control('kh_name', ['class' => 'required form-control max-length','data-name' => __('kh_name'), 'data-max-length' => 20, 'placeholder' => __('kh_name').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('kh_name').'(*)']); ?>
    </div>
</div>


<div class="row">
    <div class="col-xs-6 material-switch pull-left">
        <?php if(isset($data)){ ?>
            <?= __('active_lock') ?>
            <input id="someSwitchOptionSuccess" <?= !empty($data['active']) ? 'checked' : '' ?>  name="active" type="checkbox"/>
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        <?php } ?>
    </div>
    <div class="col-xs-6 text-right">
        <button type="submit" class="btn btn-success"><?= (isset($data)) ? __('update') : __('create') ?></button>
        <?= $this->Html->link(__('cancel'), ['action' => 'index'], ['class' => 'btn btn-danger']); ?>
    </div>
</div>