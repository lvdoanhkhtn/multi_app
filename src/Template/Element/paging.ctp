<?php if($this->Paginator->param('pageCount') > 1): ?>
    <div class="row row0">
        <div class="col-sm-12 col-md-12 text-center">
            <nav aria-label="Page navigation" class="pull-right">
                <ul class="pagination mb15 mt0 bb-pagination pr0">
                    <?= $this->Paginator->prev('<<') ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next('>>') ?>
                </ul>
            </nav>
        </div>
    </div>
<?php endif; ?>
