<?= $this->AdminView->paginateMenuAndResult() ?>

<div class="form-group row margin-top-lg">
	<?php $this->AdminView->showStatusesOnOrder($orderStatuses); ?>
</div>
<div class="box table-responsive">
<table class="result-table table table-bordered table-condensed table-hover" cellspacing="0" width="100%">
	<thead>
	<tr>
		<th class="text-top"><?= $this->Form->control('check_get_all', array('type' => 'checkbox', 'class' => 'check_get_all', 'label' => false, 'templates' => ['inputContainer' => '{{content}}'])); ?></th>
		<th class="text-top"><?= __('code') ?></th>
		<th class="text-top"><?= __('date_created') ?></th>
		<th class="text-top"><?= __('sender') ?></th>
		<th class="text-top"><?= __('receiver') ?></th>
		<th class="text-top"><?= __('service_pack') ?></th>
		<th class="text-top"><?= __('amount') ?></th>
		<th class="text-top"><?= __('order_status') ?></th>
		<th class="text-top"><?= __('note') ?></th>
		<th class="text-top"><?= __('action') ?></th>
	</tr>
	</thead>
	<tbody>
		<?php foreach($orders as $order){
			$codeColor = \App\Libs\ValueUtil::get('order.code_color_order_statuses')[$order->getStatus()];
			?>
		<tr>
			<td>
				<?= $this->Form->control('check_get', ['name' => 'check_get][]','label' => false,'type' => 'checkbox', 'class' => 'check_get', 'value' => $order->id, 'templates' => ['inputContainer' => '{{content}}'],]); ?></td>
			</td>
			<td><?= $order->getIdNoText(); ?></td>
			<td><?= $order->getCreated(); ?></td>
			<td><?= $order->getSenderAddress(); ?></td>
			<td><?= $order->getFullReceiverAddress(); ?></td>
			<td><?= $configs['service_pack'][$order->getServicePack()]; ?></td>
			<td><?= $this->AdminView->getCurrencyCam($order->getTotalAmount()); ?></td>
			<td><span style="background-color: <?= $codeColor ?> !important;border-color:<?= $codeColor ?>"  class="label label-success"><?= $order->getStatus(); ?></span></td>
			<td><?= h($order->note); ?></td>
			<td>
				<div class="btn-group-vertical">
					<?= $this->Html->link('<i class="fa fa-print"></i>', ['controller' => 'orders', 'action' => 'printOrder', $order->id], ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('print'),'target' => 'blank','escape' => false,'class' => 'btn btn-outline-info']); ?>

					<?php
						if($order->order_status === 0){
							echo $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'orders', 'action' => 'edit', $order->id], ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('edit'),'escape' => false,'class' => 'btn btn-outline-info']);
						}
					?>
					<?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'orders', 'action' => 'view', $order->id], ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('view'), 'escape' => false,'class' => 'btn btn-outline-info']); ?>
					<?= $this->Html->link('<i class="fa fa-copy"></i>', ['controller' => 'orders', 'action' => 'copy', $order->id], ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('copy'), 'escape' => false,'class' => 'btn btn-outline-info']); ?>
					<?php 
					    if($user['role_id'] === CUSTOMER && empty($order->check_cancel_order) && !empty($order->id) && isset($order->order_status) && in_array($order->order_status, [0, 1])){
							echo $this->Html->link('<i class="fa fa-times-circle"></i>', ['controller'=>'orders','action'=>'cancel',$order->id], ['confirm'=>sprintf('Are you sure you want to cancel order %s?', $order->id), 'escape' => false,'class' => 'btn btn-outline-info']);
						}
					?>


				</div>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?= $this->Element('paging'); ?>
</div>
<?= $this->Html->script('Order/order'); ?>



