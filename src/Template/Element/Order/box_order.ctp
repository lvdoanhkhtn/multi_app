<?= $this->Html->css('iCheck/flat/blue', ['block' => 'css']); ?>

<div class="row form-group">
    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="col-md-2 no-padding margin-top-sm">
            <label><?= __('delivery_from'); ?></label>
        </div>

        <div class="col-md-10 no-padding">
            <?php echo $this->Form->control('address_id', array('id' => 'address_id_1', 'type' => 'select', 'class' => 'form-control', 'options' => $addresses, 'label' => false)) ?>
        </div>
    </div>
</div>

<div class="add-form-background">
    <div class="panel panel-default add-form">
        <div class="panel-heading">

            <div class="row ">
                <div class="col-xs-12 col-md-12 col-sm-12 no-padding-left no-padding-right">

                    <div class="col-md-2 "><?= __('order') ?><span id="count_order_1">1</span></div>
                    <div class="col-md-2"><?= __('total_amount') . ':' ?><span id="total_amount_1"><?= isset($order->total) ? $order->total : '' ?></span></div>
                    <div class="col-md-2"><?= __('total_fee') . ':' ?><span id="total_fee_1"><?= isset($order) ? $order->getTotalFee() : '' ?></span></div>
                    <div class="col-md-2"><?= __('shipping_fee') . ':' ?><span id="shipping_fee_1"><?= isset($order->postage) ? $order->postage : '' ?></span></div>
                    <div class="col-md-2"><?= __("cod_fee") . ':' ?><span id="cod_fee_1"><?= isset($order->collection_fee) ? $order->collection_fee : '' ?></span></div>
                    <div class="col-md-2"><?= __('insurance_fee') . ':' ?><span id="insurance_fee_1"><?= isset($order->protect_fee) ? $order->protect_fee : '' ?></span></div>

                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="no-padding-left no-padding-right">

                <div class="col-md-2 no-padding <?= !empty($order) ? 'margin-top-lg' : 'margin-top-sm' ?> ">
                    <label class="fs-12"><?= __('delivery_to'); ?></label>
                </div>
                <div class="col-md-2">
                    <?php echo $this->Form->control("orders[0][receiver_name]", array('id' => 'receiver_name_1', 'value' => !empty($order->receiver_name) ? $order->receiver_name : "", 'type' => 'text', 'class' => 'form-control required max-length', 'required' => false, 'maxlength' => false, 'data-max-length' => 255, 'data-name' => __('receiver_name'), 'placeholder' => __("receiver_name") . '(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => !empty($order) ? __("receiver_name") : false)) ?>
                </div>

                <div class="col-md-2">
                    <?php echo $this->Form->control('orders[0][arr_receiver_phone]', array('id' => 'arr_receiver_phone_1', 'value' => !empty($order->arr_receiver_phone) ? $order->arr_receiver_phone : "", 'type' => 'text', 'class' => 'ui-widget arr_receiver_phone form-control required max-length', 'required' => false, 'maxlength' => false, 'data-max-length' => 25, 'data-name' => __('receiver_phone'), 'placeholder' => __("receiver_phone") . '(*)', 'label' => !empty($order) ? __("receiver_phone") : false)) ?>
                </div>

                <div class="col-md-2">
                    <?php echo $this->Form->control('orders[0][address]', array('id' => 'address_1', 'value' => !empty($order->address) ? $order->address : "", 'type' => 'text', 'class' => 'form-control required max-length', 'required' => false, 'maxlength' => false, 'data-max-length' => 255, 'data-name' => __('receiver_address'), 'placeholder' => __("receiver_address") . '(*)', 'label' => !empty($order) ? __("receiver_address") : false)) ?>
                </div>

                <div class="col-md-2">
                    <?php echo $this->Form->control('orders[0][city_id]', array('default' => !empty($order->city_id) ? $order->city_id : "", 'id' => 'city_id_1', 'type' => 'select', 'class' => 'form-control required', 'required' => false, 'data-name' => __('select_city'), 'options' => $deliveryCities, 'empty' => __('select_city') . '(*)', 'label' => !empty($order) ? __("city") : false)) ?>
                </div>

                <div class="col-md-2">
                    <?php echo $this->Form->control('orders[0][district_id]', array('default' => !empty($order->district_id) ? $order->district_id : "", 'id' => 'district_id_1', 'type' => 'select', 'class' => 'form-control', 'options' => !empty($order['city_id']) ? $this->AdminView->loadDistrictFromView($order['city_id']) : [], 'empty' => __('select_district'), 'label' => !empty($order) ? __("district") : false)) ?>
                </div>

            </div>

            <div class="no-padding-left no-padding-right">
                <div class="col-md-2 no-padding <?= !empty($order) ? 'margin-top-lg' : 'margin-top-sm' ?> ">
                    <label class="fs-12"><?= __('Delivery Service'); ?></label>
                </div>
                <div class="col-md-2">
                    <?php echo $this->Form->control('orders[0][service_pack_id]', array('default' => !empty($order->service_pack_id) ? $order->service_pack_id : "", 'id' => 'service_pack_id_1', 'type' => 'select', 'class' => 'form-control required', 'required' => false, 'data-name' => __('service_pack'), 'options' => $configs['service_pack'], 'label' => !empty($order) ? __("service_pack") : false)) ?>
                </div>

                <div class="col-md-2">
                    <?php echo $this->Form->control('orders[0][payment]', array('default' => !empty($order->payment) ? $order->payment : "", 'id' => 'payment_1', 'type' => 'select', 'class' => 'form-control required', 'required' => false, 'data-name' => __('payer'), 'options' => $configs['payment'], 'label' => !empty($order) ? __("payer") : false)) ?>
                </div>

                <div class="col-md-2">
                    <?php echo $this->Form->control('orders[0][money_collection]', array('data-name' => __('money_collection'), 'value' => (isset($order->money_collection) && strlen($order->money_collection)) ? $order->money_collection : "", 'id' => 'money_collection_1', 'class' => 'required positiveNumber form-control', 'placeholder' => __('money_collection') . " : $1.00(*)", 'label' => !empty($order) ? __("money_collection") : false)) ?>
                </div>

                <div class="col-md-4">
                    <?php echo $this->Form->control('orders[0][note]', array('value' => !empty($order->note) ? h($order->note) : "", 'class' => 'form-control max-length', 'maxlength' => false, 'data-max-length' => 1000, 'data-name' => __('delivery_note'), 'placeholder' => __("delivery_note"), 'label' => !empty($order) ? __("delivery_note") : false)) ?>
                </div>
            </div>

            <div class="no-padding-left no-padding-right">
                <div class="col-md-2 no-padding <?= !empty($order) ? 'margin-top-lg' : 'margin-top-sm' ?> ">
                    <label class="fs-12"><?= __('shipment_detail'); ?></label>
                </div>
                <div class="col-md-2">
                    <?php
                    if (empty($isWms)) {
                        if (!empty($order)) {
                            echo $this->Form->control('orders[0][shopId]', array('value' => $order->shopId, 'id' => 'shop_id_1', 'class' => 'form-control max-length exist', 'maxlength' => false, 'data-max-length' => 50, 'data-name' => __('private_code'), 'placeholder' => __("private_code"), 'label' => !empty($order) ? __("private_code") : false));
                        } else {
                            echo $this->Form->control('orders[0][shopId]', array('id' => 'shop_id_1', 'class' => 'form-control max-length exist', 'maxlength' => false, 'data-max-length' => 50, 'data-name' => __('private_code'), 'placeholder' => __("private_code"), 'label' => false));
                        }
                        echo $this->Form->control('hidden_shop_id', array('id' => 'hidden_shop_id', 'type' => 'hidden', 'value' => !empty($order->shopId) ? $order->shopId : ''));
                    }
                    ?>

                </div>

                <div class="col-md-2">
                    <?php echo $this->Form->control('orders[0][content]', array('value' => !empty($order->content) ? h($order->content) : "", 'class' => 'form-control max-length', 'maxlength' => false, 'data-max-length' => 1000, 'data-name' => __('descriptions'), 'placeholder' => __("descriptions"), 'label' => !empty($order) ? __("descriptions") : false)) ?>
                </div>

                <div class="col-md-2">
                    <?php echo $this->Form->control('orders[0][product_value]', array('data-name' => __('goods_value'), 'value' => (isset($order->product_value) && strlen($order->product_value)) ? h($order->product_value) : "", 'id' => 'product_value_1', 'class' => 'positiveNumber form-control', 'placeholder' => __("goods_value") . ": $1.00", 'label' => !empty($order) ? __("goods_value") : false)) ?>
                </div>

                <div class="col-md-1">
                    <?php echo $this->Form->control('orders[0][weight]', array('data-name' => __('weight'), 'value' => !empty($order->weight) ? h($order->weight) : "", 'id' => 'weight_1', 'class' => 'form-control positiveNumber', 'placeholder' => __("weight") . '(kg)', 'label' => !empty($order) ? __("weight") : false)) ?>
                </div>

                <div class="col-md-1">
                    <?php echo $this->Form->control('orders[0][length]', array('data-name' => __('length'), 'id' => 'length_1', 'value' => !empty($order->length) ? h($order->length) : "", 'class' => 'integer form-control', 'placeholder' => __("size") . "(cm)", 'label' => !empty($order) ? __("length") : false)) ?>
                </div>

                <div class="col-md-1">
                    <?php echo $this->Form->control('orders[0][width]', array('data-name' => __('width'), 'id' => 'width_1', 'value' => !empty($order->width) ? h($order->width) : "", 'class' => 'integer form-control', 'placeholder' => __("size") . "(cm)", 'label' => !empty($order) ? __("width") : false)) ?>
                </div>

                <div class="col-md-1">
                    <?php echo $this->Form->control('orders[0][height]', array('data-name' => __('height'), 'id' => 'height_1', 'value' => !empty($order->height) ? h($order->height) : "", 'class' => 'integer form-control', 'placeholder' => __("size") . "(cm)", 'label' => !empty($order) ? __("height") : false)) ?>
                </div>
            </div>
            <?php if ($user['role_id'] !== CUSTOMER && !empty($id)) { ?>
                <div class="no-padding-left no-padding-right <?= !empty($order) ? 'margin-top-lg' : 'margin-top-sm' ?> ">
                    <div class="col-md-2 no-padding margin-top-sm">
                        <label class="fs-12"><?= __('is_pick_up'); ?></label>
                    </div>
                    <div class="col-md-2">
                        <?php echo $this->Form->control('orders[0][is_pick_up]', array('type' => 'checkbox', 'default' => !empty($order->is_pick_up) ? true : false, 'label' => false)) ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php echo $this->Form->control('check_shop', array('id' => 'check_shop_id', 'type' => 'hidden', 'value' => $this->Url->build(['controller' => 'Orders', 'action' => 'checkShopId']))); ?>
<?php echo $this->Form->control('user_id', array('id' => 'user_id', 'type' => 'hidden', 'value' => !empty($userId) ? $userId : $user['id'])); ?>
<?= $this->Html->script(['Order/order']); ?>