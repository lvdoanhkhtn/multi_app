<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 1/8/20-->
<!-- * Time: 10:21 AM-->
<!-- */-->
<div class="row">
    <div class="col-xs-6">
        <?php if ($user['role_id'] === CUSTOMER) { ?>
            <div class="pull-left">
                <?= $this->Html->link('<i class="fa fa-file-excel"></i>' . __('import'), ['controller' => 'orders', 'action' => 'import'], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Import Orders', 'class' => 'btn btn-success']); ?>
                <?php
                echo $this->Html->link('<i class="fa fa-plus-circle"></i>' . __('add'), ['controller' => 'orders', 'action' => 'add'], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Create Orders', 'class' => 'btn btn-success']);
                if ($isMemberOfWms) {
                    echo $this->Html->link('<i class="fa fa-plus"></i>' . __('Add Wms'), ['controller' => 'orders', 'action' => 'addWms'], ['style' => 'margin-left: 5px', 'escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Create Wms Orders', 'class' => 'btn btn-success']);
                }
                ?>
            </div>
        <?php } ?>
    </div>
    <div class="col-xs-6">
        <div class="pull-right">
            <?= $this->Form->button('<i class="fa fa-search"></i>' . __('search'), array('class' => 'btn btn-info', 'type' => 'submit')); ?>
            <?= $this->Html->link('<i class="fas fa-eraser"></i>' . __('clear'), ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-success']); ?>
            <div class="btn-group">
                <button type="button" class="btn btn-primary">Print</button>
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <?= $this->Html->link('<i class="fa fa-print"></i>' . __('print'), [], ['id' => 'btnPrint', 'escape' => false, 'class' => 'btnPrint btn btn-warning']); ?>
                    </li>
                    <?php
                    if ($user['role_id'] === CUSTOMER) {
                    ?>
                        <li>
                            <?= $this->Html->link('<i class="fa fa-print"></i>' . __('new_print'), [], ['id' => 'btnNewPrint', 'escape' => false, 'class' => 'btnPrint btn btn-warning']); ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<i class="fa fa-print"></i>' . __('new_print_60_40'), [], ['id' => 'btnPrint60_40', 'escape' => false, 'class' => 'btnPrint btn btn-warning']); ?>
                        </li>
                    <?php }
                    ?>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-print"></i>' . __('new_print_100'), [], ['id' => 'btnPrint100', 'escape' => false, 'class' => 'btnPrint btn btn-warning']); ?>
                    </li>
                    <?php if ($user['role_id'] !== CUSTOMER) { ?>
                        <li>
                            <?= $this->Html->link('<i class="fa fa-print"></i>' . __('new_print_100_60'), [], ['id' => 'btnPrint100_60', 'escape' => false, 'class' => 'btnPrint btn btn-warning']); ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>

            <?= $this->Html->link('<i class="fa fa-file-excel"></i>' . __('export'), ['action' => 'export'], ['escape' => false, 'class' => 'btn btn-success']); ?>
        </div>
    </div>
</div>