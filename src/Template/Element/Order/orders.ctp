<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 11/12/19-->
<!-- * Time: 9:50 AM-->
<!-- */-->
<?= $this->Html->css('iCheck/flat/blue', ['block' => 'css']); ?>
<div class="table-responsive">
    <table class="table table-striped table-sm tbResult">

        <thead class="thead-dark">
            <tr>
                <th class="table-10"><?= $this->Form->control('check_get_all', array('type' => 'checkbox', 'class' => 'check_get_all', 'label' => false, 'templates' => ['inputContainer' => '{{content}}'])); ?></th>
                <th class="text-top"><?= __('order_status') ?></th>
                <th class="text-top"><?= __('code') ?></th>
                <th class="text-top"><?= __('date') ?></th>
                <th class="text-top"><?= __('sender') ?></th>
                <th class="text-top"><?= __('receiver') ?></th>
                <th class="text-top"><?= __('delivery_service') ?></th>
                <?php if (!empty($isCollectDetail)) { ?>
                    <th class="table-10"><?= __('total_cod') ?></th>
                    <th class="table-10"><?= __('total_fee') ?></th>
                    <th class="table-10"><?= __('sub_fee') ?></th>
                    <th class="table-10"><?= __('total_collect') ?></th>
                <?php } else { ?>
                    <th class="text-top"><?= __('amount') ?></th>
                <?php } ?>
                <th class="text-top"><?= __('note') ?></th>
                <th class="text-top"><?= __('description') ?></th>
                <th class="text-top"><?= __('warehouse') ?></th>
                <th class="text-top"><?= __('last_updated') ?></th>
                <th class="text-top"><?= __('action') ?></th>
            </tr>
            </>
        <tbody>
            <?php foreach ($orders as $order) {
                if (!empty($isCollectDetail)) {
                    if (!empty($order->order_collected)) {
                        $order = $order->order_collected;
                        $order->total = 0;
                        $order->sub_fee = 0;
                        $order->total_fee = $order->postage + $order->collection_fee + $order->protect_fee;
                        $totalCollect = $order->total_fee;
                    } else {
                        $order = $order->order_collect;
                        $totalCollect = $order->total - $order->sub_fee;
                        $order->total_fee = ($order->postage + $order->collection_fee + $order->protect_fee);
                    }
                }
                $codeColor = \App\Libs\ValueUtil::get('order.code_color_order_statuses')[$order->getStatus()];
            ?>
                <tr style="background-color: <?= !empty($order->pay_status) && !empty($isPickup) ? "green" : "" ?>">
                    <td>
                        <?= $this->Form->control('check_get', ['name' => 'check_get][]', 'label' => false, 'type' => 'checkbox', 'class' => 'check_get', 'value' => $order->id, 'templates' => ['inputContainer' => '{{content}}'],]); ?></td>
                    </td>
                    <td><span style="background-color: <?= $codeColor ?> !important;border-color:<?= $codeColor ?>" class="label label-success"><?= $order->getStatus(); ?></span></td>
                    <td> <?= $this->Html->link(sprintf("%s <br> %s", $order->orderId, $order->shopId), ['controller' => 'orders', 'action' => 'view', $order->id], ['target' => '_blank', 'escape' => false]) ?></td>
                    <td><?= sprintf("Created: %s <br> Complete: %s", $order->getCreated(), !empty($order->finished) ? $order->getCompleted() : ""); ?></td>
                    <td><?= $order->getFullSenderAddress(); ?></td>
                    <td><?= $order->getFullReceiverAddress(); ?></td>
                    <td><?= $configs['service_pack'][$order->getServicePack()]; ?></td>
                    <?php if (!empty($isCollectDetail)) { ?>
                        <td><?= $this->AdminView->getCurrencyCam($order->total); ?></td>
                        <td><?= $this->AdminView->getCurrencyCam($order->total_fee); ?></td>
                        <td><?= $this->AdminView->getCurrencyCam($order->sub_fee); ?></td>
                        <td><?= $this->AdminView->getCurrencyCam($totalCollect); ?></td>
                    <?php } else { ?>
                        <td><?= $this->AdminView->getCurrencyCam($order->total); ?></td>

                    <?php } ?>
                    <td><?= h($order->note); ?></td>
                    <td><?= h($order->content); ?></td>
                    <td><?= h($order->ware_house->name); ?></td>
                    <td><?php
                        if (!empty($order->order_notes[0])) {
                            echo $this->AdminView->getLastUpdate($order->order_notes[0]);
                        }
                        ?></td>
                    <td>
                        <div class="btn-group-vertical">
                            <!-- //check if order not phnomphenh is can not add sub fee -->
                            <?php if (!empty($isCollectDetail)) {
                                if (!empty($deliveryTripId) ) {
                                    echo $this->Html->link('<i class="fa fa-plus"></i>', ['controller' => 'orders', 'action' => 'changeSubFee', $order->id, $deliveryTripId], ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('add_sub_fee'), 'escape' => false, 'class' => 'btn btn-outline-info']);
                                }
                            ?>
                            <?php } else { ?>
                                <?php
                                echo $this->Html->link('<i class="fa fa-print"></i>', ['controller' => 'orders', 'action' => ($user['role_id'] === CUSTOMER) ? 'printOrder' : 'printOrdersV2', $order->id], ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('print'), 'target' => 'blank', 'escape' => false, 'class' => 'btn btn-outline-info']);
                                ?>
                                <?php
                                if (($order->order_status === 0 && $user['role_id'] === CUSTOMER) || ($user['role_id'] != CUSTOMER && $order->order_status != $orderConstant['delivery_completed'] && $order->pay_status === NO_COMPLETE_STATUS && $order->owe_status === NO_COMPLETE_STATUS)) {
                                    echo $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'orders', 'action' => 'edit', $order->id], ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('edit'), 'escape' => false, 'class' => 'btn btn-outline-info']);
                                }
                                ?>
                                <?php
                                if ($user['role_id'] != CUSTOMER && in_array($order->order_status, [5, 13])) {
                                    echo $this->Form->postLink(
                                        "<i class='fa fa-exchange'></i>", // first
                                        ['action' => 'createExchangeOrder', $order->id,],  // second
                                        ['escape' => false, 'title' => __('create_exchange_order'), 'class' => 'btn btn-outline-danger', 'confirm' => __('create_exchange_confirm')]
                                    ); // third
                                }
                                ?>
                                <?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'orders', 'action' => 'view', $order->id], ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('view'), 'escape' => false, 'class' => 'btn btn-outline-info']); ?>
                                <?= $this->Html->link('<i class="fa fa-book"></i>', ['controller' => 'orders', 'action' => 'printOrdersV2', $order->id], ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('print'), 'escape' => false, 'class' => 'btn btn-outline-info']); ?>
                                <?= $this->Html->link('<i class="fa fa-copy"></i>', ['controller' => 'orders', 'action' => 'copy', $order->id], ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('copy'), 'escape' => false, 'class' => 'btn btn-outline-info']); ?>

                            <?php } ?>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?= $this->Element('paging'); ?>