<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 1/13/20-->
<!-- * Time: 10:34 AM-->
<!-- */-->

<?php if ($this->Paginator->param('pageCount') > 1) { ?>
    <?= $this->AdminView->paginateMenuAndResult() ?>
<?php } ?>
<div class="box table-responsive">
    <table class="tb-trip table table-striped table-sm">
        <thead class="thead-dark">
        <tr>
            <th class="text-top"><?= __('order_code') ?></th>
            <th class="text-top"><?= __('sender') ?></th>
            <th class="text-top"><?= __('receiver') ?></th>
            <th class="text-top"><?= __('address') ?></th>
            <th class="text-top"><?= __('description') ?></th>
            <th class="text-top"><?= __('note') ?></th>
            <th class="text-top"><?= __('total_cod') ?></th>
            <th class="text-top mw100"><?= __('action') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 1;
        foreach ($datas as $data) {
            $order = $data->order;
            ?>
            <tr id="<?= $data->id ?>">
                <td class="table-20">
                    <?= $this->Html->link(sprintf("%s <br> %s", $order->orderId, $order->shopId), ['controller' => 'orders', 'action' => 'view', $order->id], ['target' => '_blank','escape' => false]) ?>
                </td>
                <td class="table-20">
                    <?= sprintf("%s <br> %s", $order->user->fullname, $order->user->phone) ?>
                </td>
                <td class="table-20">
                    <?= sprintf("%s <br> %s", $order->receiver_name, $order->arr_receiver_phone) ?>
                </td>
                <td class="table-40">
                    <?= sprintf("%s, %s, %s", $order->address, $order->district->name, $order->city->name) ?>
                </td>
                <td class="table-20">
                    <?= h($order->content) ?>
                </td>
                <td class="table-20">
                    <?= h($order->note) ?>
                </td>
                <td class="table-20">
                    <?= $this->AdminView->getCurrencyCam($order->total) ?>
                </td>
                <td class="table-10">
                    <div class="btn-group">
                        <?= $this->Html->link('<i class="fa fa-print"></i>', ['controller' => 'orders', 'action' => 'printOrder', $data->order_id], ['target' => 'blank', 'escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Print', 'class' => 'btn btn-outline-info']); ?>
                        <?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'orders', 'action' => 'view', $data->order_id], ['target' => 'blank', 'escape' => false, 'data-toggle' => 'tooltip', 'title' => 'view', 'class' => 'btn btn-outline-info']); ?>
                        <a href="#" title="Delete" data-delete-url=<?= $this->Url->build(['action' => 'delete']) ?> id=<?= $data->id ?> class="delete_order btn btn-outline-danger"><i class="fa fa-trash-alt"></i></a>
                    </div>
                </td>
            </tr>

        <?php } ?>
        </tbody>
    </table>
    <?= $this->Element('paging') ?>
</div>