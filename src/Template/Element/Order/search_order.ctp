<?= $this->Form->create(null, ['type' => 'GET', 'id' => 'frmOrder'])?>
<div class="row form-group">

	<div class="col-md-4">
		<?php echo $this->Form->control('key_word',['value' => !empty($_GET['key_word']) ? $_GET['key_word'] : '','type' => 'text', 'class' => 'form-control', 'placeholder' => __('key_word'), 'label' => __('key_word')])?>
	</div>

	<div class="col-md-4">
		<?= $this->Element('Calendar/created_calendar'); ?>
	</div>


	<div class="col-md-4">
		<?= $this->Element('Calendar/completed_calendar'); ?>
	</div>

</div>
<hr>

<div class="box box-default <?= ($haveData) ? '' : 'collapsed-box' ?> ">
	<div class="box-header with-border">
		<h3 class="box-title"><?= __('advance_filter') ?></h3>
		<div class="box-tools">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa <?= ($haveData) ? 'fa-minus' : 'fa-plus' ?> "></i></button>
		</div>
	</div><!-- /.box-header -->
	<div class="box-body">

			<div class="row form-group">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="col-md-4">
						<?php echo $this->Form->control('order_code',array('value' => !empty($_GET['order_code']) ? $_GET['order_code'] : '' ,'type' => 'text', 'class' => 'form-control', 'placeholder' => __('CBS00001;CBS00002;'), 'label' => __('order_code_private_code') ))?>
					</div>

					<div class="col-md-4">
						<?php echo $this->Form->control('order_type',array('default' => !empty($_GET['order_type']) ? $_GET['order_type'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control', 'options' => $configs['type_of_order'], 'label' => __('type_of_order')))?>
					</div>

					<div class="col-md-4">
						<?php echo $this->Form->control('order_status',array('default' => isset($_GET['order_status']) ? $_GET['order_status'] : "",'style' => 'width: 100%','multiple' => 'multiple','type' => 'select', 'class' => 'form-control select2', 'options' => $configs['order_status'], 'label' => __("order_status")))?>
					</div>

				</div>

				<div class="col-xs-12 col-md-12 col-sm-12">

					<div class="col-md-4">
						<?php echo $this->Form->control('address_id',array('default' => !empty($_GET['address_id']) ? $_GET['address_id'] : '' , 'type' => 'select','empty' => __('select'),  'class' => 'form-control', 'options' => $addresses, 'label' => __('delivery_from')))?>
					</div>

					<div class="col-md-4">
						<?php echo $this->Form->control('province',array('default' => !empty($_GET['province']) ? $_GET['province'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control', 'options' => $deliveryCities, 'label' => __('delivery_to')))?>
					</div>

					<div class="col-md-4">
						<?php echo $this->Form->control('fee',array('default' => !empty($_GET['fee']) ? $_GET['fee'] : '' ,'type' => 'select', 'empty' => __('select'), 'class' => 'form-control', 'options' => $configs['fee'], 'label' => __("type_of_fee")))?>
					</div>

				</div>


			</div>

	</div><!-- /.box-body -->

</div><!-- /.box -->

<?= $this->Element('Order/buttons_order'); ?>
<?= $this->Form->end(); ?>