<div class="form-group col-md-12 table-responsive">

    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-university"></i></span>
        <?= $this->Form->control('bank_id', ['options' => $banks,'class' => 'no-border-radius required form-control select-no-border', 'required' => false, 'maxlength' => false,'data-name' => __('bank'), 'empty' => __('Select bank from the list (*)'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>

    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-building"></i></span>
        <?= $this->Form->control('branch_name', ['class' => 'no-border-radius form-control required max-length','data-max-length' => 50,'required' => false, 'maxlength' => false,'data-name' => __('name_of_branch'), 'data-max-length' => 50,'placeholder' => __('name_of_branch').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-credit-card"></i></span>
        <?= $this->Form->control('account_holder', ['class' => 'no-border-radius form-control required max-length', 'data-name' => __('card_holder'), 'maxlength' => false,'required' => false,'data-max-length' => 50, 'placeholder' => __('card_holder').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-credit-card"></i></span>
        <?= $this->Form->control('account_number', ['data-name' => __('card_number'),'class' => 'no-border-radius form-control required max-length', 'required' => false, 'maxlength' => false, 'placeholder' => __('card_number').'(*)', 'data-max-length' => 50, 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>

</div>