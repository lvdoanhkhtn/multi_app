<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 8/16/19-->
<!-- * Time: 4:01 PM-->
<!-- */-->

<div class="form-group col-md-12">

    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
        <?= $this->Form->control('fullname', ['value' => !empty($account['fullname']) ? $account['fullname'] : "",'class' => 'no-border-radius form-control required max-length', 'data-max-length' => 255, 'data-name' => __('name_of_shop_or_sender'), 'placeholder' => __('name_of_shop_or_sender').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]); ?>
    </div>
    <div class="errorTxt form-group"></div>

    <?php if(empty($isAdmin)){ ?>
    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
        <?= $this->Form->control('email', ['value' => !empty($account['email']) ? $account['email'] : "",'class' => 'no-border-radius required email form-control max-length email-exist','data-name' => __('email'), 'data-max-length' => 50, 'placeholder' => __('email').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]); ?>
        <?php
            if(!empty($account['email'])){
                echo $this->Form->control('hidden_email', ['id' => 'hidden_email', 'type' => 'hidden', 'value' => $account['email']]);
        }
        ?>
    </div>
    <div class="errorTxt form-group"></div>

    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
        <?= $this->Form->control('phone', ['value' => !empty($account['phone']) ? $account['phone'] : "",'class' => 'no-border-radius required number form-control max-length phone-exist','data-name' => __('phone_number'), 'data-max-length' => 11, 'placeholder' => __('phone_number').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]); ?>
        <?php
            if(!empty($account['phone'])){
                echo $this->Form->control('hidden_phone', ['id' => 'hidden_phone', 'type' => 'hidden', 'value' => $account['phone']]);
            }
        ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fa fa-home"></i></span>
        <?= $this->Form->control('address', ['value' => !empty($account['address']) ? $account['address'] : "", 'class' => 'no-border-radius form-control required max-length', 'data-name' => __('address'), 'placeholder' => __('address').('(*)'), 'data-max-length' => 100, 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]); ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-map-marker-alt select-marker"></i></span>
        <?= $this->Form->control('city_id', ['default' => !empty($account['city_id']) ? $account['city_id'] : "",'id' => 'city_id_1', 'options' => $pickUpCities, 'class' => 'no-border-radius form-control required select-no-border', 'data-name' => 'City', 'empty' => __('Select city from the list (*)'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]); ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-map-marker-alt select-marker"></i></span>
        <?= $this->Form->control('district_id', ['default' => !empty($account['district_id']) ? $account['district_id'] : "", 'options' => !empty($account['city_id']) ? $this->AdminView->loadDistrictFromView($account['city_id']) : "",'id' => 'district_id_1', 'class' => 'no-border-radius form-control select-no-border required', 'data-name' => __('district') ,'empty' => __('Select district from the list (*)'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]); ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-map-marker-alt select-marker"></i></span>
        <?= $this->Form->control('ward_id', ['default' => !empty($account['ward_id']) ? $account['ward_id'] : "", 'options' => !empty($account['district_id']) ? $this->AdminView->loadWardFromView($account['district_id']) : "", 'id' => 'ward_id_1', 'class' => 'no-border-radius form-control select-no-border', 'empty' => __('Select ward from the list'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]); ?>
    </div>

    <?php } ?>


    <?php if(empty($account)){ ?>
    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-lock"></i></span>
        <?= $this->Form->control('password', ['class' => 'no-border-radius form-control required min-length','data-min-length' => 6, 'data-name' => __('password'),'placeholder' => __('password').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-lock"></i></span>
        <?= $this->Form->control('repeat_password', ['type' => 'password','class' => 'no-border-radius form-control', 'data-rule-equalTo' => "#password",'data-msg-equalTo' => \App\Libs\ConfigUtil::getMessage('ECL005', [__('repeat_password'), __('password')]), 'data-name' => __('password'), 'placeholder' => __('repeat_password').' (*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>

    <?php } ?>


    <div class="errorTxt form-group"></div>

    <?php echo $this->Form->control('check_phone', array('id' => 'check_phone','type' => 'hidden', 'value' => $this->Url->build(['controller' => 'users', 'action' => 'checkPhone'])));?>
    <?php echo $this->Form->control('check_email', array('id' => 'check_email','type' => 'hidden', 'value' => $this->Url->build(['controller' => 'users', 'action' => 'checkEmail'])));?>

</div>