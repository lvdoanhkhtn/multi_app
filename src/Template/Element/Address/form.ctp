<div class="form-group col-md-12 table-responsive">

    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
        <?= $this->Form->control('agency', ['class' => 'no-border-radius form-control max-length','data-max-length' => 50, 'data-name' => __('agency'), 'maxlength' => false, 'placeholder' => __('agency'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
        <?= $this->Form->control('str_phones', ['class' => 'no-border-radius form-control required max-length', 'data-max-length' => 25, 'required' => false, 'maxlength' => false,'data-name' => __('phone') ,'placeholder' => __('phone').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fa fa-home"></i></span>
        <?= $this->Form->control('address', ['class' => 'no-border-radius form-control required max-length', 'data-max-length' => 100, 'required' => false, 'maxlength' => false, 'data-name' => __('address') ,'placeholder' => __('address').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-map-marker-alt select-marker"></i></span>
        <?= $this->Form->control('city_id', ['id' => 'city_id_1','options' => $pickUpCities,'class' => 'no-border-radius required form-control select-no-border', 'required' => false, 'data-name' => __('city') , 'empty' => __('Select city from the list (*)'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-map-marker-alt select-marker"></i></span>
        <?= $this->Form->control('district_id', ['default' => !empty($address['district_id']) ? $address['district_id'] : "",'id' => 'district_id_1','options' => !empty($address['city_id']) ? $this->AdminView->loadDistrictFromView($address['city_id']) : [],'class' => 'no-border-radius form-control select-no-border required','required' => false, 'data-name' => __('district') , 'empty' => __('Select district from the list (*)'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>


    <div class="input-group form-group">
        <span class="input-group-addon"><i class="fas fa-map-marker-alt select-marker"></i></span>
        <?= $this->Form->control('ward_id', ['default' => !empty($address['ward_id']) ? $address['ward_id'] : "", 'id' => 'ward_id_1','options' => !empty($address['district_id']) ? $this->AdminView->loadWardFromView($address['district_id']) : [],'class' => 'no-border-radius form-control select-no-border', 'empty' => __('Select ward from the list'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
    </div>
    <div class="errorTxt form-group"></div>

</div>