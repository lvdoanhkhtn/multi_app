<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 11/7/19-->
<!-- * Time: 2:12 PM-->
<!-- */-->
<div class="col-xs-12 form-group">
                            <ul class="nav nav-tabs mb15" id="priceTab">
                                <?php $i = 0; foreach ($wareHouses as $key => $value) { $i++ ?>
    <li class="<?= ($i === 1) ? 'active' : '' ?>"><a class="nav-link" data-toggle="tab" href="<?= '#ware_house_'.$key ?>"><?= $value ?></a></li>
<?php } ?>
</ul>

<div class="tab-content">
    <!-- kho hcm-->
    <?php foreach ($wareHouses as $key => $value) { ?>
        <div class="tab-pane active" id="ware_house_<?= $key ?>">
            <div class="row widget-header-div">
                <?php foreach ($orders[$key] as $order) {
                    $userId = $order->user_id;
                    ?>
                    <div class="col-md-6">
                        <div
                            class="box box-solid box-primary">
                            <div class="box-header">
                                <p style="font-size: 14px; height:30px"
                                   class="text-bold box-title"><?= $order->getFullSenderAddress("-"); ?></p>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="row row-center">
                                    <div class="col-xs-12">
                                        <div class="col-xs-6 no-padding-left text-left lock_<?= sprintf("%s - %s", $userId, $order->address_id) ?>">
                                            <?= $this->Form->control('check_get', ['default' => 0,'id' => sprintf("%s,%s",$userId, $order->address_id), 'name' => 'check_get][]','label' => false,'type' => 'checkbox', 'class' => 'check', 'value' => $order->id, 'templates' => ['inputContainer' => '{{content}}'],]); ?>
                                            <?php if(!empty($isPickupTrip)){ ?>
                                            <?= $this->Html->link('<i class="fa fa-print"></i>', ['action' => 'writeFileHtml', 1 ,$userId, $order->address_id], ['escape' => false,'class' => 'print', 'target' => 'blank']) ?>
                                            <?= $this->Html->link('<i class="fa fa-book"></i>', ['action' => 'writeFileHtml', 2 ,$userId, $order->address_id], ['escape' => false,'class' => 'print', 'target' => 'blank']) ?>
                                            <?php } ?>
                                            <span><?= $order->getCreated(); ?></span>
                                        </div>
                                        <div class="col-xs-6 no-padding-left text-left">
                                            <span><?= (!empty($isPickupTrip)) ? __('pickup_order') : __('return_order') ?> : </span><span
                                                class="text-red"><?= number_format($order->count) ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

    <?php } ?>


</div>
</div>
</div>
