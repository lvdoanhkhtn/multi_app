<div class="row">
    <div class="form-group col-xl- col-lg-3 col-md-6 col-xs-12">
        <?= $this->Form->control('username', ['value' => !empty($account['username']) ? $account['username'] : (!empty($_REQUEST['username']) ? $_REQUEST['username'] : ''), 'class' => 'form-control max-length username', 'data-max-length' => 25, 'data-name' => __('user_name'), 'maxlength' => false, 'placeholder' => __('user_name'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('user_name')]);  ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12">
        <?= $this->Form->control('fullname', ['value' => !empty($account['fullname']) ? $account['fullname'] : (!empty($_REQUEST['fullname']) ? $_REQUEST['fullname'] : ''), 'class' => 'required form-control max-length', 'data-max-length' => 255, 'data-name' => __('full_name'), 'maxlength' => false, 'placeholder' => __('full_name'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('full_name') . '(*)']);  ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12">
        <?= $this->Form->control('email', ['value' => !empty($account['email']) ? $account['email'] : (!empty($_REQUEST['email']) ? $_REQUEST['email'] : ''), 'class' => 'required email form-control max-length email-exist', 'data-name' => __('email'), 'data-max-length' => 50, 'placeholder' => __('email') . '(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('email') . '(*)']); ?>
        <?php
        if (!empty($account['email'])) {
            echo $this->Form->control('hidden_email', ['id' => 'hidden_email', 'type' => 'hidden', 'value' => $account['email']]);
        }
        ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12">
        <?= $this->Form->control('gender', ['default' => (isset($account['gender']) && strlen($account['gender'])) ? $account['gender'] : (!empty($_REQUEST['gender']) ? $_REQUEST['gender'] : ''), 'options' => $configs['gender'], 'class' => 'form-control select2', 'data-name' => __('gender'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('gender')]); ?>
    </div>
</div>
<div class="row">
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('passport', ['default' => !empty($account['passport']) ? $account['passport'] : (!empty($_REQUEST['passport']) ? $_REQUEST['passport'] : ''), 'class' => 'form-control passport', 'data-name' => __('passport'), 'placeholder' => __('passport'), 'empty' => __('select'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('passport')]); ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('birthday', ['default' => !empty($account['birthday']) ? $this->AdminView->formatDate($account['birthday']) : (!empty($_REQUEST['birthday']) ? $_REQUEST['birthday'] : ''), 'class' => 'selectSingleDate form-control required', 'placeholder' => __('date_of_birth'), 'data-name' => __('date_of_birth'), 'empty' => __('select'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('date_of_birth') . '(*)']); ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('phone', ['value' => !empty($account['phone']) ? $account['phone'] : (!empty($_REQUEST['phone']) ? $_REQUEST['phone'] : ''), 'class' => 'required number form-control max-length phone-exist', 'data-name' => __('phone_number'), 'data-max-length' => 11, 'placeholder' => __('phone_number') . '(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('phone_number') . '(*)']); ?>
        <?php
        if (!empty($account['phone'])) {
            echo $this->Form->control('hidden_phone', ['id' => 'hidden_phone', 'type' => 'hidden', 'value' => $account['phone']]);
        }
        ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('address', ['value' => !empty($account['address']) ? $account['address'] : (!empty($_REQUEST['address']) ? $_REQUEST['address'] : ''), 'class' => 'form-control required max-length', 'data-name' => __('address'), 'placeholder' => __('address') . ('(*)'),  'placeholder' => __('address'), 'data-max-length' => 100, 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('address') . '(*)']); ?>

    </div>
</div>
<div class="row">

    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('city_id', ['default' => !empty($account['city_id']) ? $account['city_id'] : (!empty($_REQUEST['city_id']) ? $_REQUEST['city_id'] : ''), 'id' => 'city_id_1', 'options' => $pickUpCities, 'class' => 'form-control required select2', 'data-name' => 'City', 'empty' => __('select'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('city') . '(*)']); ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('district_id', ['default' => !empty($account['district_id']) ? $account['district_id'] : (!empty($_REQUEST['district_id']) ? $_REQUEST['district_id'] : ''), 'options' => !empty($account['city_id']) ? $this->AdminView->loadDistrictFromView($account['city_id']) : "", 'id' => 'district_id_1', 'class' => 'form-control select2 required', 'data-name' => __('district'), 'empty' => __('select'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('district') . '(*)']); ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('ward_id', ['default' => !empty($account['ward_id']) ? $account['ward_id'] : (!empty($_REQUEST['ward_id']) ? $_REQUEST['ward_id'] : ''), 'options' => !empty($account['district_id']) ? $this->AdminView->loadWardFromView($account['district_id']) : "", 'id' => 'ward_id_1', 'class' => 'form-control required select2', 'data-name' => __('ward'), 'empty' => __('select'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('ward') . '(*)']); ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?php if (!empty($isShop)) { ?>
            <?php echo $this->Form->control('shop_permission_status_id', ['default' => !empty($account['shop_permission_status_id']) ? $account['shop_permission_status_id'] : (!empty($_REQUEST['shop_permission_status_id']) ? $_REQUEST['shop_permission_status_id'] : ''), 'type' => 'select', 'empty' => __('select'), 'class' => 'required form-control select2', 'options' => $shopPermissionStatuses, 'label' => __('shop_permissions') . '(*)', 'data-name' => __('shop_permissions')]) ?>
        <?php } else { ?>
            <?php echo $this->Form->control('role_id', array('default' => !empty($account['role_id']) ? $account['role_id'] : (!empty($_REQUEST['role_id']) ? $_REQUEST['role_id'] : ''), 'type' => 'select', 'empty' => __('select'), 'class' => 'required form-control select2', 'options' => $configs['roles'], 'label' => __('roles') . '(*)', 'data-name' => __('roles'))) ?>
        <?php } ?>
    </div>
</div> <!-- ./row -->

<?php if (!empty($account['role_id']) && $account['role_id'] === SHIPPER) { ?>
    <div class="row">
        <div class="form-group col-xs-3">
            <?= $this->Form->control('is_addmore_trip', array('type' => 'checkbox', 'default' => !empty($account['is_addmore_trip']) ? true : false, 'label' => __('is_addmore_trip'))) ?>
        </div>
    </div>
<?php } ?>



<?php echo $this->Form->control('check_phone', array('id' => 'check_phone', 'type' => 'hidden', 'value' => $this->Url->build(['controller' => 'users', 'action' => 'checkPhone']))); ?>
<?php echo $this->Form->control('check_email', array('id' => 'check_email', 'type' => 'hidden', 'value' => $this->Url->build(['controller' => 'users', 'action' => 'checkEmail']))); ?>