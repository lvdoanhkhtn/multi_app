<!--/**-->
<!--* Created by PhpStorm.-->
<!--* User: doanh.lv-->
<!--* Date: 10/23/19-->
<!--* Time: 10:35 AM-->
<!--*/-->
<?php
    $target = !empty($target) ? $target : 'confirm-failed';
    $button = !empty($button) ? $button : 'btnReturnFail';
    $class = !empty($class) ? $class : 'btnHanding';
?>
<div class="modal" data-backdrop="static" data-keyboard="false" id="<?= $target ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Delivery Failed?
            </div>
            <div class="modal-body">
                <?php if(!empty($table)){ ?>
                    <div class="form-group">
                        <?= $this->Form->control('reason', ['options' => $configs[$table], 'class' => 'select2']) ?>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <?= $this->Form->control(null, ['label' => __('note'),'id' => 'pickup_note','class' => 'note form-control', 'placeholder' => __('note')]) ?>
                </div>

            </div>
            <div class="modal-footer">
                <button id="<?= $button ?>"  class="<?= $class ?> btn btn-success"><?= __('update') ?></button>
                <?= $this->Html->link(__('cancel'), [], ['data-dismiss' => 'modal', 'class' => 'btn btn-danger']); ?>
            </div>
        </div>
    </div>
</div><!-- ./Popup -->
