<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 8/29/19-->
<!-- * Time: 11:14 AM-->
<!-- */-->

<div class="box-header">
    <div class="row form-group">

        <div class="col-xs-12 box-header with-border">
            <div class="col-md-12">
                <h3 class="box-title padding-top-sm text-red">
                    <?= !empty($link) ? $link : "" ?>
                </h3>
            </div>
            <?php echo $this->Element('header_order'); ?>
        </div>
    </div>
</div><!-- /.box-header -->

