<?php $configs['service_pack'] = $this->AdminView->getServicePack($lang); ?>
<div class="box <?= !empty($isIndex) ? 'table-responsive' : '' ?>">
	<table class="table-bordered table-condensed">
		<thead>
			<tr>
				<?php if(!empty($isAdmin)){ ?>
					<th class="text-top"><?= $this->Form->control('check_get_all', array('type' => 'checkbox', 'class' => 'check_get_all', 'label' => false, 'templates' => ['inputContainer' => '{{content}}'])); ?></th>
				<?php } ?>
				<th class="text-top">#</th>
				<th class="text-top"><?= __('order_code') ?></th>
				<th class="text-top"><?= __('shop_code') ?></th>
				<th class="text-top"><?= __('date_created') ?></th>
				<th class="text-top"><?= __('date_completed') ?></th>
				<th class="text-top"><?= __('receiver') ?></th>
				<th class="text-top"><?= __('address') ?></th>
				<th class="text-top"><?= __('service_pack') ?></th>
				<th class="text-top"><?= __('order_status') ?></th>
				<th class="text-top"><?= __('total_cod') ?></th>
				<th class="text-top"><?= __('total_fee') ?></th>
				<th class="text-top"><?= __('amount') ?></th>
				<?php if(isset($isIndex)) {
						echo '<th></th>';
					}
				?>

			</tr>
		</thead>
		<tbody>
		<?php $totalCod = 0; $totalFee = 0; $totalAmount = 0; $i=1; foreach ($paymentDetails as $paymentDetail){
				$order = $paymentDetail->order;
				$response = $excel->getPaymentDetail($order);
				$totalCod += $response['cod'];
				$totalFee += $response['fee'];
				$totalAmount += $response['amount'];
			?>
			<tr>
				<?php
					if(!empty($isAdmin) && empty($paymentDetail->is_update_status)){
						echo '<td>'.$this->Form->control('check_get', ['name' => 'check_get][]','label' => false,'type' => 'checkbox', 'class' => 'check_get', 'value' => $paymentDetail->id, 'templates' => ['inputContainer' => '{{content}}'],]).'</td>';
					}else if(!empty($isAdmin)){
						echo '<td>'.'&nbsp'.'</td>';
					}
				?>
				<td><?= $i++; ?></td>
				<td><?= $this->Html->link($order->orderId, ['controller' => 'orders', 'action' => 'view', $order->id], ['target' => '_blank','escape' => false]) ?></td>
				<td><?= $order->shopId; ?></td>
				<td><?= $order->getCreated(); ?></td>
				<td><?= !empty($order->finished) ? $order->getCompleted() : ""; ?></td>
				<td><?= $order->getNameAndPhoneReceiver(); ?></td>
				<td><?= $order->getReceiverAddress(); ?></td>
				<td><?= $configs['service_pack'][$order->getServicePack()]; ?></td>
				<td><?= $order->getStatus(); ?></td>
				<td><?= $this->AdminView->getCurrencyCam($response['cod']); ?></td>
				<td><?= $this->AdminView->getCurrencyCam($response['fee']); ?></td>
				<td><?= $this->AdminView->getCurrencyCam($response['amount']); ?></td>
				<?php if(isset($isIndex)){ ?>
				<td>
					<?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'orders', 'action' => 'view', $order->id], ['target' => 'blank','data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('view'), 'escape' => false,'class' => 'btn btn-outline-info']); ?>

				</td>
				<?php } ?>
			</tr>
		<?php } ?>
		<tr>
			<td  colspan="<?= !empty($isAdmin) ? 10 : 9 ?>"></td>
			<td><?= $this->AdminView->getCurrencyCam($totalCod); ?></td>
			<td><?= $this->AdminView->getCurrencyCam($totalFee); ?></td>
			<td><?= $this->AdminView->getCurrencyCam($totalAmount); ?></td>
			<?php if(!empty($isAdmin)){ ?>
				<td></td>
			<?php } ?>
		</tr>
		</tbody>
	</table>
</div>
