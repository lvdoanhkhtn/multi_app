<!---->
<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/23/19-->
<!-- * Time: 10:02 AM-->
<!-- */-->

<!---->
<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/23/19-->
<!-- * Time: 10:02 AM-->
<!-- */-->

<table id="dtBasic<?=$tab?>" class="dtBasicExample table table-sm" cellspacing="0" width="100%">
    <thead>
    <tr>
        <td>&nbsp;</td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($datas as $data) {
        $order = $data->order;
        ?>

        <tr id='row-<?= $order->id ?>' class="col-md-4 form-group">
            <td>
                <?php
                    $fields = [
                        'type' => $type,
                        'order' => $data->order,
                        'data' => $data,
                        'card' => $card
                    ];
                    if(!empty($tab)){
                        $fields['tab'] = $tab;
                    }
                    if(!empty($isPickup)){
                        $fields['isPickup'] = $isPickup;
                    }
                    if(!empty($isDelivery)){
                        $fields['isDelivery'] = $isDelivery;
                    }
                    echo $this->Element('DeliveryTripDetail/common_box', $fields);
                ?>
            </td>
        </tr>

    <?php } ?>
    </tbody>
</table>

<?= $this->Form->control(null, ['type' => 'hidden','id' => 'current_tab', 'value' => $tab]) ?>

<script>
    $(document).ready(function () {
        var maxHeight = 0;
         $('.dtBasicExample').each(function () {
                var elements = $(this).find('.boxOrder');
                $(elements).each(function () {
                    var height = $(this).height();
                    if(height > maxHeight){
                        maxHeight = height;
                    }
                });
         });
        $('.card-body table').attr('style', 'height: '+maxHeight+'px !important');
    });

</script>