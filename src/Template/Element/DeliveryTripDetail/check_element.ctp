<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/24/19-->
<!-- * Time: 5:36 PM-->
<!-- */-->


<div style="height: 150px; max-height: 150px; border: 1px solid #ccc; overflow-x: auto">
    <ul id="<?= $id?>" style="list-style: none">
        <?php foreach ($datas as $data) { $order = $data->order; ?>
            <li id='<?= $order->id ?>'><?= (!empty($order->orderId) ? $order->orderId : $order->id) ?></li>
        <?php } ?>
    </ul>
</div>