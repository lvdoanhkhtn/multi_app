<!--/**-->
<!--* Created by PhpStorm.-->
<!--* User: doanh.lv-->
<!--* Date: 10/23/19-->
<!--* Time: 10:35 AM-->
<!--*/-->
<?= $this->Form->create(null, ['id' => 'frmConfirmFailed','type' => 'post', 'action' => 'confirmFailed']) ?>

<div class="modal" data-backdrop="static" data-keyboard="false" id="confirm-failed">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Delivery Failed?
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?= $this->Form->control('order_status', ['options' => \App\Libs\ValueUtil::get('order.order_failed'), 'class' => 'select2']); ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('reason', ['options' => $configs['delivery_trip_reasons'], 'class' => 'select2']) ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('delivery_note', ['id' => 'delivery_note','class' => 'note form-control', 'placeholder' => 'delivery_note']) ?>
                </div>
                <!-- hidden form-->
                <?= $this->Element('Modal/hidden_fields'); ?>

            </div>
            <?= $this->Element('Modal/buttons'); ?>
        </div>
    </div>
</div><!-- ./Popup -->
<?= $this->Form->end(); ?>
