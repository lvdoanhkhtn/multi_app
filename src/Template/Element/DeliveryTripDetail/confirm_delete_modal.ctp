<!--/**-->
<!--* Created by PhpStorm.-->
<!--* User: doanh.lv-->
<!--* Date: 10/23/19-->
<!--* Time: 10:35 AM-->
<!--*/-->
<?= $this->Form->create(null, ['id' => 'frmConfirmDelete','type' => 'post', 'action' => 'confirmDelete']) ?>
<div class="modal" data-backdrop="static" data-keyboard="false" id="confirm-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?= __('delete_confirm'); ?>
            </div>
            <?= $this->Element('Modal/hidden_fields'); ?>
            <?= $this->Element('Modal/buttons'); ?>
        </div>
    </div>
</div> <!-- /.popup -->
<?= $this->Form->end(); ?>

