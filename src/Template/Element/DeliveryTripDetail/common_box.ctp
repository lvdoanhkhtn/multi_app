<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 12/11/19-->
<!-- * Time: 4:29 PM-->
<!-- */-->
<?php $changeOrder = [$orderConstant['pickup_in_progress'], $orderConstant['return_in_progress'], $orderConstant['delivery_in_progress']]; ?>
<div id="order_<?= $order->id ?>" class="widget-header-item card <?= $card ?>">
    <div class="card-header">
        <?php
        if (in_array($type, $changeOrder)) {
            echo $this->Form->control('cbInProcess', ['name' => '', 'default' => 0, 'id' => $data->id, 'label' => false, 'type' => 'checkbox', 'class' => 'check_get cbInProcess-' . $tab, 'value' => $order->id, 'templates' => ['inputContainer' => '{{content}}'],]);
        }
        ?>
        <b><?= $this->Html->link(sprintf("%s - %s", $order->orderId, $order->shopId), ['controller' => 'orders', 'action' => 'view', $order->id], ['target' => '_blank']); ?>
            <?php if ($type === $orderConstant['return'] && $datas->status === 0) { ?>
                <div class="pull-right">
                    <a data-id="<?= $data->id  ?>" href="#" data-toggle="modal" data-target="#confirm-delete" class="confirm"><i class="fa fa-times-circle"></i></a>
                </div>
            <?php } ?>

    </div>
    <div class="card-body panel boxOrder" id="boxOrder<?= $data->id ?>">
        <table class="table">

            <tr>
                <td class="table-3"><i class="fa fa-phone-alt"></i></td>
                <td><a href="tel:<?= $order->user->phone ?>"><?= $order->user->phone; ?></a></td>
            </tr>
            <tr>
                <td class="table-3"><i class="fa fa-user"></i></td>
                <td class="table-30"><b><?= h($order->receiver_name); ?></b></td>
            </tr>
            <tr>
                <td class="table-3"><i class="fa fa-mobile-alt"></i></td>
                <td class="table-30"><a href="tel:<?= $order->arr_receiver_phone ?>"><?= h($order->arr_receiver_phone); ?></a></td>
            </tr>
            <tr>
                <td class="table-3"><i class="fa fa-address-book"></i></td>
                <td class="table-30"><?= h($order->address); ?></td>
            </tr>
            <tr>
                <td class="table-3"><i class="fa fa-map-marker-alt"></i></td>
                <td class="table-30"><b><?= sprintf("%s - %s", $order->city->name, $order->district->name); ?></b></td>
            </tr>
            <tr>
                <td class="table-3"><i class="fa fa-money-bill-alt"></i></td>
                <td class="table-30"><b><?= $this->AdminView->getCurrencyCam($order->total) ?></b></td>
            </tr>
            <tr>
                <td class="table-3"><i class="fa fa-balance-scale"></i></td>
                <td class="table-30"><b><?= $order->weight ?></b></td>
            </tr>
            <tr>
                <td class="table-3"><i class="fa fa-box-open"></i></td>
                <td class="table-30"><?= h($order->content) ?></td>
            </tr>
            <tr class="no-float">
                <td class="table-3"><i class="fa fa-clipboard"></i></td>
                <td class="table-30"><?= h($order->note) ?></td>
            </tr>
            <tr class="no-float">
                <td class="table-3"><i class="fa fa-history"></i></td>
                <td class="table-30"><?php
                                        if (!empty($order->order_notes[0])) {
                                            echo $this->AdminView->getLastUpdate($order->order_notes[0]);
                                        }
                                        ?>
                </td>
            </tr>
        </table>
    </div>
    <?php if (in_array($type, [$orderConstant['delivery_in_progress']])) { ?>
        <div class="card-footer">
            <span><a data-id="<?= $data->id  ?>" href="#" class="confirm btn btn-success" data-toggle="modal" data-target="#confirm-completed">Completed</a></span>
            <span class="pull-right"><a data-id="<?= $data->id  ?>" href="#" class="confirm btn btn-danger" data-toggle="modal" data-target="#confirm-failed">Failed</a></span>
        </div>
    <?php } ?>
</div>