<!--/**-->
<!--* Created by PhpStorm.-->
<!--* User: doanh.lv-->
<!--* Date: 10/23/19-->
<!--* Time: 10:35 AM-->
<!--*/-->
<?= $this->Form->create(null, ['id' => 'frmConfirmComplete','type' => 'post', 'action' => 'confirmComplete']) ?>
<div class="modal" data-backdrop="static" data-keyboard="false" id="confirm-completed">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Delivery Completed?
            </div>
            <div class="modal-body">
                <div class="material-switch">
                    <input id="someSwitchOptionSuccess" name="exchange" type="checkbox"/>
                    <label for="someSwitchOptionSuccess" class="label-success"></label>
                    Delivered some pieces of order/Exchanged order
                </div>
                <div class="form-group">
                    <?= $this->Form->control('delivery_note', ['class' => 'form-control', 'placeholder' => 'delivery_note']) ?>
                </div>
                <!-- hidden form-->
                <?= $this->Element('Modal/hidden_fields'); ?>

            </div>
            <?= $this->Element('Modal/buttons'); ?>
        </div>
    </div>
</div> <!-- /.popup -->
<?= $this->Form->end(); ?>
