<div class="row">
    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-xs-12">
        <?php echo $this->Form->control('location_id', array('options' => $locations,'type' => 'select', 'empty' => __('select'), 'data-name' => __('location'), 'class' => 'required form-control select2', 'label' => __('location').'(*)'))?>
    </div>
    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-xs-12">
        <?= $this->Form->control('description', ['class' => 'form-control max-length','data-max-length' => 255, 'data-name' => __('description'), 'maxlength' => false, 'placeholder' => __('description'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('description')]);  ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-xs-12">
        <?php echo $this->Form->control('sender_city_id', array('id' => 'city_id_1', 'type' => 'select', 'empty' => __('select'), 'data-name' => __('from_province'), 'class' => 'required form-control select2', 'options' => $pickUpCities, 'label' => __('from_province').'(*)'))?>
    </div>
    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-xs-12">
        <?= $this->Form->control('sender_district_id', ['options' => !empty($data['sender_district_id']) ? $this->AdminView->loadDistrictFromView($data['sender_city_id']) : null, 'empty' => __('select'), 'id' => 'district_id_1','class' => 'form-control','data-name' => __('from_district'), 'maxlength' => false, 'placeholder' => __('from_district'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('from_district')]);  ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-xs-12">
        <?php echo $this->Form->control('city_id', array('id' => 'city_id_2', 'type' => 'select', 'empty' => __('select'),'data-name' => __('to_province'), 'class' => 'required  form-control select2', 'options' => $deliveryCities, 'label' => __('to_province').'(*)'))?>
    </div>
    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-xs-12">
        <?= $this->Form->control('district_id', ['options' => !empty($data['district_id']) ? $this->AdminView->loadDistrictFromView($data['city_id']) : null,'empty' => __('select'), 'id' => 'district_id_2', 'class' => 'form-control','data-name' => __('to_district'), 'maxlength' => false, 'placeholder' => __('to_district
        '), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('to_district')]);  ?>
    </div>
</div>


<div class="row">
    <div class="col-xs-6">
    </div>
    <div class="col-xs-6 text-right">
        <button type="submit" class="btn btn-success"><?= isset($data) ? __('update') : __('create') ?></button>
        <?= $this->Html->link(__('cancel'), ['action' => 'index'], ['class' => 'btn btn-danger']); ?>
    </div>

</div>


