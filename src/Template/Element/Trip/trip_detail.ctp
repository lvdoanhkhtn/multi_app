<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/22/19-->
<!-- * Time: 3:26 PM-->
<!-- */-->

<div class="row form-group">

    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <label for=""><?= __('trip_code') ?></label>
        <input type="text" class="form-control" disabled placeholder="<?= $trip->id ?>">
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <label for=""><?= __('date_created') ?></label>
        <input type="text" class="form-control" disabled
               placeholder="<?= $this->AdminView->formatDateTime($trip->created) ?>">
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <label for=""><?= __('shipper') ?></label>
        <input type="text" class="form-control" disabled
               placeholder="<?= sprintf("%s %s", $trip->employee->employee_id, $trip->employee->user->fullname) ?>">
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <label for=""><?= __('total_orders') ?></label>
        <input type="text" class="form-control" disabled
               placeholder="<?= !empty($trip->trip_details[0]->total_orders) ? number_format($trip->trip_details[0]->total_orders) : 0 ?>">
    </div>
</div>