<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/18/19-->
<!-- * Time: 2:39 PM-->
<!-- */-->
<script>
    $(document).ready(function () {
        $('#frmDeliveryTrips').validate({});
    });
</script>


<?= $this->Form->create(null, ['type' => 'post', 'action' => 'add','id' => 'frmDeliveryTrips','class' => 'validateForm']); ?>

<div id="delivery-trip-create" class="modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?= !empty($text) ? $text : __('change_shipper') ?>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?= $this->Form->control('employee_id', ['data-name' => __('shipper'),'empty' => __('select'),'options' => !empty($text) ? $deliveryShippers : $shippers, 'class' => 'required select2', 'label' => __('employee')]) ?>
                </div>
            </div>
            <?= $this->Element('Modal/buttons') ?>
        </div>
    </div>
</div> <!-- /.popup -->

<?= $this->Form->end(); ?>
