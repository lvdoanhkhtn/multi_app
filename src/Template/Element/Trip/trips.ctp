<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 11/7/19-->
<!-- * Time: 3:30 PM-->
<!-- */-->

<div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'get']); ?>

<div class="row form-group">


    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <?php echo $this->Form->control('orderId',array('value' => !empty($_GET['orderId']) ? $_GET['orderId'] : "",'type' => 'text', 'class' => 'form-control', 'placeholder' => __('CBS123456'), 'label' => __('order_code')))?>
    </div>

    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <?php echo $this->Form->control('user_id', array('default' => !empty($_GET['user_id']) ? $_GET['user_id'] : '', 'class' => 'form-control', 'empty' => __('select'),'label' => __('customer'), 'options' => $customers))?>
    </div>


    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <?php echo $this->Form->control('employee_id', array('default' => !empty($_GET['employee_id']) ? $_GET['employee_id'] : '', 'class' => 'form-control','empty' => __('select'), 'label' => __('shipper'), 'options' => $shippers))?>
    </div>

    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <?php echo $this->Form->control('status', array('default' => (isset($_GET['status']) && strlen($_GET['status']) > 0) ? $_GET['status'] : '' , 'type' => 'select', 'empty' => __('select'), 'options' => $configs['delivery_status'],'class' => 'form-control select2', 'label' => __('status')))?>
    </div>
</div>


<div class="row form-group">

    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <?= $this->AdminView->loadCityAndDistrict($deliveryFrom, 'delivery_from', !empty($_GET['delivery_from']) ? $_GET['delivery_from'] : ''); ?>
    </div>


    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <?= $this->Element('Calendar/created_calendar'); ?>
    </div>


    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <?= $this->Element('Calendar/completed_calendar'); ?>
    </div>
</div>


<div class="pull-right">
    <?= $this->Form->button('<i class="fa fa-search"></i>'.__('search'), array('class' => 'btn btn-info','type'=>'submit')); ?>
    <?php
        $action = 'returns';
        $addressLabel = __('return_address');
        $controller = 'return_trip_details';
        if(!empty($isPickupTrip)){
            $action = 'pickup';
            $addressLabel = __('pickup_address');
            $controller = 'pickup_trip_details';

        }
        if(!empty($isPickupTrip) || !empty($isReturnTrip)) {
            echo $this->Html->link('<i class="fa fa-plus-circle"></i>' . __('create'), ['controller' => 'ware_houses', 'action' => $action], ['class' => 'btn btn-warning', 'escape' => false, 'target' => 'blank']);
        }else{ ?>
            <a href="" class="btn btn-warning" data-toggle="modal" data-target="#delivery-trip-create"><i class="fa fa-plus-circle"></i><?= __('create') ?></a>
        <?php } ?>
    <?= $this->Html->link('<i class="fa fa-file-excel"></i>'.__('export'), ['action' => 'export'], ['escape' => false,'class' => 'btn btn-success']); ?>
</div>

<div class="row form-group">

</div>



<?= $this->Form->end(); ?>
<?php if($this->Paginator->param('pageCount') > 1){ ?>
    <?= $this->AdminView->paginateMenuAndResult() ?>
<?php } ?>

<div class="box table-responsive">
    <table class="table table-striped table-sm">
        <thead class="thead-dark">
        <tr>
            <th class="text-top"><?= __('customer') ?></th>
            <th class="text-top"><?= $addressLabel ?></th>
            <th class="text-top"><?= __('date_created') ?></th>
            <th class="text-top"><?= __('date_completed') ?></th>
            <th class="text-top"><?= __('shipper') ?></th>
            <th class="text-top"><?= __('orders_completed') ?></th>
            <th class="text-top"><?= __('status') ?></th>
            <th class="text-top mw150"><?= __('action') ?></th>

        </tr>
        </thead>
        <tbody>
        <?php foreach ($datas as $data){ ?>
            <tr>
                <td><?= h($data->customer_user->fullname); ?></td>
                <td><?= h($data->address->getFullAddress()); ?></td>
                <td><?= $this->AdminView->formatDateTime($data->created); ?></td>
                <td><?= !empty($data->status === COMPLETE_STATUS) ? $this->AdminView->formatDateTime($data->modified) : ''; ?></td>
                <td><?= h($data->employee->user->fullname); ?></td>
                <td><?= $data->getTotalComplete(); ?>
                </td>
                <td><?= $this->AdminView->showComplete($data->status); ?></td>
                <td>
                    <?= (empty($data->status === COMPLETE_STATUS) && empty($isPickupTrip))  ? $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'return_trip_details','action' => 'index', $data->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']) : ''; ?>
                    <?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => $controller,'action' => 'view', $data->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'View', 'class' => 'btn btn-outline-info']); ?>
                    <a href="" data-id="<?= $data->id ?>" class="btnChangeShipper btn btn-outline-info" data-toggle="modal" data-target="#change-shipper"><i class="fa fa-exchange-alt"></i></a>

                </td>
            </tr>

        <?php } ?>

        </tbody>
    </table>
    <?= $this->Element('paging') ?>
</div>

<?php
echo $this->Element('Trip/add_modal');
echo $this->Element('Modal/change_shipper');
?>


</div><!-- /.box-body -->
</div>
