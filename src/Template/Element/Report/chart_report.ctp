<?php $isPickup = !empty($isPickup)  ? $isPickup :  ($link == __('pickup_report') ? true : false) ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">
                <?= $this->Element('breadcum', ['link' => $link]); ?>
                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'GET']) ?>

                        <div class="row form-group">

                            <div class="col-sm-6 col-12">
                                <?php
                                    if(!empty($isPickup)){
                                        echo $this->Element('Calendar/stored_calendar');
                                    }else{
                                        echo $this->Element('Calendar/completed_calendar');
                                    }
                                ?>
                            </div>


                        </div>

                        <div class="pull-right">
                            <?= $this->Form->button('<i class="fa fa-search"></i>' . __('search'), array('class' => 'btn btn-info', 'type' => 'submit')); ?>
                            <?= $this->Html->link('<i class="fa fa-file-excel"></i>' . __('export'), ['action' => !empty($isPickup) ? 'pickupExport' : 'deliveryExport'], ['escape' => false, 'class' => 'btn btn-success']); ?>
                        </div>

                        <?= $this->Form->end(); ?>

                    </div><!-- /.box-body -->

                </div>
                <div id="chartContainer" style="height: 300px; width: 100%;"></div>

            </div>
        </div>
    </div>
</section>
<?= $this->Html->script('jquery/jquery.canvasjs.min'); ?>
