<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/7/19-->
<!-- * Time: 9:09 AM-->
<!-- */-->
<div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead class="thead-dark">
        <tr>
            <th class="table-10"><?= __('code') ?></th>
            <th class="table-10"><?= __('name') ?></th>
            <th class="table-10"><?= __('phone') ?></th>
            <th class="table-10"><?= __('address') ?></th>
            <th class="table-10"><?= __('district') ?></th>
            <th class="table-10"><?= __('default') ?></th>
            <th class="table-10"><?= __('status') ?></th>
            <th class="table-10 mw100"><?= __('action') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($addresses as $address){ ?>
        <tr>
            <td class="table-10"><?= $address->id; ?></td>
            <td class="table-10"><?= h($address->agency); ?></td>
            <td class="table-10"><?= h($address->str_phones); ?></td>
            <td class="table-10"><?= h($address->getAddressForCustomer()) ?></td>
            <td class="table-10"><?= h($address->getFullAddressForCustomer()) ?></td>
            <td class="table-10"><?= $this->AdminView->showDefault($address->is_master_address) ?></td>
            <td class="table-10"><?= $this->AdminView->showActive($address->active); ?></td>
            <td class="table-10">
                <div class="btn-group">
                    <a href="" class="btn btn-outline-info address-customer" data-id="<?= $address->id ?>" data-toggle="modal" data-target="#address-customer" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="" class="btn btn-outline-danger" data-toggle="modal" data-target="#confirm-delete" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-alt"></i></a>
                </div>
            </td>
        </tr>
    <?php } ?>

    <?= $this->Element('Customer/address_customer_modal'); ?>
    </tbody>
    </table>

    <div class="row">
        <div class="col-xs-12 text-right">
            <a href="" class="btn btn-warning address-customer" data-toggle="modal" data-target="#address-customer"><?= __('add') ?></a>
        </div>
    </div>
</div>

