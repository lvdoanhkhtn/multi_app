<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/8/19-->
<!-- * Time: 11:52 AM-->
<!-- */-->

<div class="row">
    <?= $this->Form->control('tab', ['type' => 'hidden', 'value' => 'payment']); ?>

    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-xs-12">
        <?= $this->Form->control('price_id', ['empty' => __('select'),'multiple' => 'multiple', 'default' => !empty($priceUser) ? $priceUser : 0,'options' => $prices, 'data-name' => __('price_list'),'class' => ' form-control select2','placeholder' => __('price_list').'(*)','templates' => ['inputContainer' => '{{content}}'], 'label' => __('price_list')]);  ?>
    </div>

    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-xs-12">
        <?= $this->Form->control('promotion_code', ['value' => !empty($promotionCode) ? $promotionCode : '','data-name' => __('promotion_code'),'class' => ' form-control username', 'placeholder' => __('promotion_code'), 'data-max-length' => 25, 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('promotion_code')]);  ?>
    </div>

</div>

<?= $this->Element('Modal/buttons') ?>

