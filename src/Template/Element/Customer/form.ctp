<div class="row">
    <div class="form-group col-xl- col-lg-3 col-md-6 col-xs-12">
        <?= $this->Form->control('fullname', ['value' => !empty($account['fullname']) ? $account['fullname'] : "", 'class' => 'no-border-radius form-control required max-length', 'data-max-length' => 255, 'data-name' => __('name_of_shop_or_sender'), 'placeholder' => __('name_of_shop_or_sender') . '(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('full_name') . '(*)']); ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12">
        <?= $this->Form->control('email', ['value' => !empty($account['email']) ? $account['email'] : (!empty($_REQUEST['email']) ? $_REQUEST['email'] : ''), 'class' => 'required email form-control max-length email-exist', 'data-name' => __('email'), 'data-max-length' => 50, 'placeholder' => __('email') . '(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('email') . '(*)']); ?>
        <?php
        if (!empty($account['email'])) {
            echo $this->Form->control('hidden_email', ['id' => 'hidden_email', 'type' => 'hidden', 'value' => $account['email']]);
        }
        ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12">
        <?= $this->Form->control('notification_email', ['value' => !empty($account['notification_email']) ? $account['notification_email'] : (!empty($_REQUEST['notification_email']) ? $_REQUEST['notification_email'] : ''), 'class' => 'email form-control max-length email-exist', 'data-name' => __('email'), 'data-max-length' => 50, 'placeholder' => __('email_notification') . '(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('email_notification') . '(*)']); ?>
        <?php
        if (!empty($account['email'])) {
            echo $this->Form->control('hidden_email', ['id' => 'hidden_email', 'type' => 'hidden', 'value' => $account['email']]);
        }
        ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('phone', ['value' => !empty($account['phone']) ? $account['phone'] : (!empty($_REQUEST['phone']) ? $_REQUEST['phone'] : ''), 'class' => 'required number form-control max-length phone-exist', 'data-name' => __('phone_number'), 'data-max-length' => 11, 'placeholder' => __('phone_number') . '(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('phone_number') . '(*)']); ?>
        <?php
        if (!empty($account['phone'])) {
            echo $this->Form->control('hidden_phone', ['id' => 'hidden_phone', 'type' => 'hidden', 'value' => $account['phone']]);
        }
        ?>
    </div>
</div>


<div class="row">
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('address', ['value' => !empty($account['address']) ? $account['address'] : (!empty($_REQUEST['address']) ? $_REQUEST['address'] : ''), 'class' => 'form-control required max-length', 'data-name' => __('address'), 'placeholder' => __('address') . ('(*)'),  'placeholder' => __('address'), 'data-max-length' => 100, 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('address') . '(*)']); ?>

    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('city_id', ['default' => !empty($account['city_id']) ? $account['city_id'] : (!empty($_REQUEST['city_id']) ? $_REQUEST['city_id'] : ''), 'id' => 'city_id_1', 'options' => $pickUpCities, 'class' => 'form-control required select2', 'data-name' => 'City', 'empty' => __('select'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('city') . '(*)']); ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('district_id', ['default' => !empty($account['district_id']) ? $account['district_id'] : (!empty($_REQUEST['district_id']) ? $_REQUEST['district_id'] : ''), 'options' => !empty($account['city_id']) ? $this->AdminView->loadDistrictFromView($account['city_id']) : "", 'id' => 'district_id_1', 'class' => 'form-control select2 required', 'data-name' => __('district'), 'empty' => __('select'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('district') . '(*)']); ?>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('ward_id', ['default' => !empty($account['ward_id']) ? $account['ward_id'] : (!empty($_REQUEST['ward_id']) ? $_REQUEST['ward_id'] : ''), 'options' => !empty($account['district_id']) ? $this->AdminView->loadWardFromView($account['district_id']) : "", 'id' => 'ward_id_1', 'class' => 'form-control required select2', 'data-name' => __('ward'), 'empty' => __('select'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('ward') . '(*)']); ?>
    </div>
</div> <!-- ./row -->

<?php echo $this->Form->control('check_phone', array('id' => 'check_phone', 'type' => 'hidden', 'value' => $this->Url->build(['controller' => 'users', 'action' => 'checkPhone']))); ?>
<?php echo $this->Form->control('check_email', array('id' => 'check_email', 'type' => 'hidden', 'value' => $this->Url->build(['controller' => 'users', 'action' => 'checkEmail']))); ?>

<?php if ($user['role_id'] !== CUSTOMER) { ?>
    <div class="row">
        <div class="form-group col-xl-3 col-lg-3 col-md-6 col-xs-12 col-12">
            <?= $this->Form->control('group_customer_id', ['value' => !empty($account['group_customer_id']), 'empty' => '----', 'data-name' => __('address'), 'placeholder' => __('address') . ('(*)'),  'placeholder' => __('address'), 'data-max-length' => 100, 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('Group customer') . '(*)']); ?>
        </div>
    </div> <!-- ./row -->
<?php } ?>

<div class="row">
    <div class="col-xs-6 material-switch pull-left">
        <?php if (!empty($account)) { ?>
            Active/Lock
            <input id="someSwitchOptionSuccess" <?= !empty($account['active']) ? 'checked' : '' ?> name="active" type="checkbox" />
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        <?php } ?>
    </div>
    <div class="col-xs-6 text-right">
        <button type="submit" class="btn btn-success"><?= (!empty($account)) ? __('update') : __('create') ?></button>
        <?= $this->Html->link(__('cancel'), ['action' => 'index'], ['class' => 'btn btn-danger']); ?>
    </div>
</div>

