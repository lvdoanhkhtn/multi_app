<script>
    $(document).ready(function () {
        $('#frmAddressCustomer').validate({});
    });

</script>
<div class="modal" id="address-customer" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?= __('address') ?>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('id', ['type' => 'hidden','id' => 'addressCustomerId', 'value' => '']); ?>

                        <?= $this->Form->control('tab', ['type' => 'hidden','id' => 'addressCustomerTab', 'value' => '']); ?>

                        <?= $this->Form->control('customer_id', ['type' => 'hidden','id' => 'customer_id', 'value' => $id]); ?>

                        <?= $this->Form->control('agency', ['id' => 'sender_name','class' => 'no-border-radius form-control max-length','data-max-length' => 50, 'data-name' => __('name'), 'maxlength' => false, 'placeholder' => __('name'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('name')]);  ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('str_phones', ['id' => 'sender_phone','class' => 'no-border-radius form-control required max-length', 'data-max-length' => 25, 'required' => false, 'maxlength' => false,'data-name' => __('phone') ,'placeholder' => __('phone').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('phone')]);  ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('address', ['id' => 'sender_address','class' => 'no-border-radius form-control required max-length', 'data-max-length' => 100, 'required' => false, 'maxlength' => false, 'data-name' => __('address') ,'placeholder' => __('address').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('address')]);  ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('city_id', ['id' => 'sender_city_id','options' => $pickUpCities,'class' => 'city_id no-border-radius required form-control select-no-border select2', 'required' => false, 'data-name' => __('city') , 'empty' => __('select'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('city')]);  ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('district_id', ['id' => 'sender_district_id','class' => 'district_id select2 no-border-radius form-control select-no-border required','required' => false, 'data-name' => __('district') , 'empty' => __('select'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('district')]);  ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('ward_id', ['id' => 'sender_ward_id', 'class' => 'ward_id select2 no-border-radius form-control select-no-border', 'empty' => __('select'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('ward')]);  ?>
                    </div>
                </div>
                <?= $this->Element('Modal/buttons') ?>
            </div>
    </div>
</div> <!-- /.Popup -->

