<div class="row">
    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12">
        <?= $this->Form->control('name', ['value' => !empty($data['name']) ? $data['name'] : '','class' => 'required form-control max-length','data-max-length' => 255, 'data-name' => __('name'), 'required' => false,'maxlength' => false, 'placeholder' => __('name'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('name').'(*)']);  ?>
        <?php
            if(!empty($parentName)){
                $this->Form->control($parentName, ['type' => 'hidden','value' => $id]);
            }
        ?>
    </div>
</div>


<div class="row">
    <div class="col-xs-6 material-switch pull-left">
        <?php if(isset($data)){ ?>
        <?= __('active_lock') ?>
        <input id="someSwitchOptionSuccess" <?= (!empty($data['active']) ||  !empty($data['is_show'])) ? 'checked' : '' ?>  name="<?= !empty($data['is_show']) ? 'is_show' : 'active' ?>" type="checkbox"/>
        <label for="someSwitchOptionSuccess" class="label-success"></label>
        <?php } ?>
    </div>
    <div class="col-xs-6 text-right">
        <button type="submit" class="btn btn-success btnSubmit"><?= (isset($data)) ? __('update') : __('create') ?></button>
        <a class="btn btn-danger" href="javascript:history.back()"><?= __('cancel') ?></a>
    </div>
</div>