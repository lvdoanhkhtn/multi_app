<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 9/27/19-->
<!-- * Time: 11:18 AM-->
<!-- */-->
<?php $customControllers =  array_values(\App\Libs\ConfigUtil::get('CUSTOM_CONTROLLERS'))?>

<?= $this->Form->create(null, ['type' => 'GET']); ?>
<div class="row form-group">
    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
        <?php echo $this->Form->control('id', array('default' => !empty($_GET['id']) ? $_GET['id'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $list, 'label' => __('name')))?>
    </div>
</div>

<div class="pull-right">
    <?= $this->Form->button('<i class="fa fa-search"></i>'.__('search'), array('class' => 'btn btn-info','type'=>'submit')); ?>
    <?= ((!empty($id) || in_array($currentController, $customControllers))) ? $this->Html->link('<i class="fa fa-plus-circle"></i>'.__('create'), ['action' => 'add', !empty($id) ? $id : ''], ['escape' => false,'class' => 'btn btn-warning']) : ""; ?>
    <?= $this->Html->link('<i class="fa fa-file-excel"></i>'.__('export'), ['action' => 'export'], ['escape' => false,'class' => 'btn btn-success']); ?>
</div>

<div class="row form-group">

</div>
<?= $this->Form->end(); ?>

<?php if($this->Paginator->param('pageCount') > 1){ ?>
    <?= $this->AdminView->paginateMenuAndResult() ?>
<?php } ?>
<div class="box table-responsive">
    <table class="table table-striped table-sm tbResult">
        <thead class="thead-dark">
        <tr>
            <th class="text-top"><?= __('id') ?></th>
            <th class="text-top"><?= __('name') ?></th>
            <?php if(!empty($controller) && $controller === WEBHOOKS){ ?>
            <th class="text-top"><?= __('url') ?></th>
            <?php } ?>
            <th class="text-top"><?= __('status') ?></th>
            <th class="text-top text-right"><?= __('action') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($datas as $data){ ?>
            <tr>
                <td><?= $data->id; ?></td>
                <td><?= $data->name ?></td>
                <?php if(!empty($controller) && $controller === WEBHOOKS){ ?>
                    <td><?= $data->url ?></td>
                <?php } ?>

                <td><?= (isset($data->is_show)) ?  $this->AdminView->showActive($data->is_show) : $this->AdminView->showActive(COMPLETE_STATUS); ?></td>
                <td class="pull-right">
                    <?php
                        if($currentController !== WARDS &&  $controller !== strtolower($currentController)) {
                            echo $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => $controller,'action' => 'index', $data->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'View', 'class' => 'btn btn-outline-info']);
                        }
                    ?>
                    <?php
                        if(!empty($id) || in_array($currentController, $customControllers)){
                            echo $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $data->id, !empty($id) ? $id : '' ], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']);
                        }
                    ?>
                </td>
            </tr>

        <?php } ?>

        </tbody>
    </table>
    <?= $this->Element('paging') ?>
