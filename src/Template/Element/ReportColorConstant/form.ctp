<div class="row">
    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-xs-12">
        <?= $this->Form->control('value', ['class' => 'form-control max-length','data-max-length' => 25, 'data-name' => __('value'), 'maxlength' => false, 'placeholder' => __('value'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('value')]);  ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-6">
    </div>
    <div class="col-xs-6 text-right">
        <button type="submit" class="btn btn-success"><?= isset($data) ? __('update') : __('create') ?></button>
        <?= $this->Html->link(__('cancel'), ['action' => 'index'], ['class' => 'btn btn-danger']); ?>
    </div>

</div>