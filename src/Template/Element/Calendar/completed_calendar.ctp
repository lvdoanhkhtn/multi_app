<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/31/19-->
<!-- * Time: 11:02 AM-->
<!-- */-->

<div class="form-group">
    <label class="control-label"><?= __('date_completed'); ?></label>
    <div class="input-group">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
        <input name="complete_date" type="text" class="date form-control no-border-radius pull-right" id="complete_date"/>
    </div><!-- /.input group -->
</div><!-- /.form group -->
<?= $this->Form->control('',['type' => 'hidden','id' => 'temp_complete_date', 'value' => !empty($_GET['complete_date']) ? $_GET['complete_date'] : (!empty($startDate) ? $startDate : "")]) ?>
