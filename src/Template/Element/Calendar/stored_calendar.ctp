<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/31/19-->
<!-- * Time: 11:02 AM-->
<!-- */-->

<div class="form-group">
    <label class="control-label"><?= __('stored_date'); ?></label>
    <div class="input-group">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
        <input name="stored_date" type="text" class="date form-control no-border-radius pull-right" id="stored_date"/>
        <?= $this->Form->control('', ['type' => 'hidden', 'id' => 'temp_stored_date', 'value' => !empty($_GET['stored_date']) ? $_GET['stored_date'] : (!empty($startDate) ? $startDate : "")]) ?>
    </div><!-- /.input group -->
</div><!-- /.form group -->