<div class="row">
    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12">
        <?= $this->Form->control('name', ['class' => 'required form-control max-length','data-max-length' => 50, 'data-name' => __('name'), 'maxlength' => false, 'placeholder' => __('name'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('name').'(*)']);  ?>
    </div>
</div>