<script>
    $(document).ready(function () {
        $('#frmShippingFee').validate({});
    });

</script>

<div class="modal" id="shipping-fee" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?= __('shipping_fee') ?>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('id', ['type' => 'hidden','id' => 'shippingFeeId', 'value' => '']); ?>

                        <?= $this->Form->control('tab', ['type' => 'hidden','id' => 'shippingFeeTab', 'value' => '']); ?>

                        <?= $this->Form->control('price_id', ['type' => 'hidden','id' => 'price_id', 'value' => $id]); ?>

                        <?= $this->Form->control('service_pack_id', ['id' => 'service_pack_id','empty' => __('select'),'label' => __('service_pack'),'data-name' => __('service_pack'),'class' => 'required select2 form-control', 'options' => $configs['service_pack']]); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('location_id', ['id' => 'location_id','empty' => __('select'), 'label' => __('delivery_group'),'data-name' => __('delivery_group'),'class' => 'required form-control', 'options' => $locations]); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('start_weight', ['id' => 'start_weight','class' => 'required form-control decimal', 'label' => __('start_weight').'(*)', 'data-name' => __('start_weight'), 'placeholder' => '3.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('start_amount', ['id' => 'start_amount','class' => 'required form-control decimal', 'label' => __('start_amount').'(*)', 'data-name' => __('start_amount'), 'placeholder' => '1.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('addition_weight', ['id' => 'addition_weight','class' => 'required form-control decimal', 'label' => __('addition_weight').'(*)', 'data-name' => __('addition_weight'), 'placeholder' => '0.50']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('addition_amount', ['id' => 'addition_amount','class' => 'required form-control decimal', 'label' => __('addition_amount').'(*)', 'data-name' => __('addition_amount'), 'placeholder' => '1.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('maximum_weight', ['id' => 'maximum_weight','class' => 'required form-control decimal', 'label' => __('maximum_weight').'(*)', 'data-name' => __('maximum_weight'), 'placeholder' => '50.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('pickup_fee', ['id' => 'pickup_fee','class' => 'required form-control decimal', 'label' => __('pickup_fee').'(*)', 'data-name' => __('pickup_fee'), 'placeholder' => '0.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('return_fee', ['id' => 'return_fee','class' => 'required form-control decimal', 'label' => __('return_fee').'(*)', 'data-name' => __('return_fee'), 'placeholder' => '0.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('minimum_return_fee', ['id' => 'minimum_return_fee','class' => 'required form-control decimal', 'label' => __('minimum_return_fee').'(*)', 'data-name' => __('minimum_return_fee'), 'placeholder' => '0.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('exchange_fee', ['id' => 'exchange_fee','class' => 'required form-control decimal', 'label' => __('exchange_fee').'(*)', 'data-name' => __('exchange_fee'), 'placeholder' => '0.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('minimum_exchange_fee', ['id' => 'minimum_exchange_fee','class' => 'required form-control decimal', 'label' => __('minimum_exchange_fee').'(*)', 'data-name' => __('minimum_exchange_fee'), 'placeholder' => '0.00']); ?>
                    </div>
            </div>

            <?= $this->Element('Modal/buttons') ?>


            </div>
    </div>
</div> <!-- /.Popup -->

