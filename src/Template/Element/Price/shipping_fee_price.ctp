<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/2/19-->
<!-- * Time: 5:17 PM-->
<!-- */-->
<table class="table table-striped table-sm table-responsive">
    <thead class="thead-dark">
    <tr>
        <th class="table-10"><?= __('service') ?></th>
        <th class="table-20"><?= __('group') ?></th>
        <th class="table-10"><?= __('start_weight') ?></th>
        <th class="table-10"><?= __('start_amount') ?></th>
        <th class="table-10"><?= __('addition_weight') ?></th>
        <th class="table-10"><?= __('addition_amount') ?></th>
        <th class="table-10"><?= __('maximum_weight') ?></th>
        <th class="table-10"><?= __('pickup_fee') ?></th>
        <th class="table-10"><?= __('return_fee') ?></th>
        <th class="table-10"><?= __('minimum_return_fee') ?></th>
        <th class="table-10"><?= __('exchange_fee') ?></th>
        <th class="table-10"><?= __('minimum_exchange_fee') ?></th>
        <th class="table-10 mw100"><?= __('action') ?></th>
    </tr>
    </thead>
    <tbody>
        <tr><td><?= $this->Form->postLink(null); ?></td></tr>
        <?php foreach ($shippingFees as $shippingFee){ ?>
        <tr <?= $shippingFee->id === PRICE_DEFAULT ? 'class="bg-red"' : "" ?>>
            <td class="table-10"><?= h($configs['service_pack'][$shippingFee->service_pack->id]); ?></td>
            <td class="table-20"><?= h($shippingFee->location->name); ?></td>
            <td class="table-10"><?= $shippingFee->start_weight ?></td>
            <td class="table-10"><?= $shippingFee->start_amount ?></td>
            <td class="table-10"><?= $shippingFee->addition_weight ?></td>
            <td class="table-10"><?= $shippingFee->addition_amount ?></td>
            <td class="table-10"><?= $shippingFee->maximum_weight ?></td>
            <td class="table-10"><?= $shippingFee->pickup_fee ?></td>
            <td class="table-10"><?= $shippingFee->return_fee ?></td>
            <td class="table-10"><?= $shippingFee->minimum_return_fee ?></td>
            <td class="table-10"><?= $shippingFee->exchange_fee ?></td>
            <td class="table-10"><?= $shippingFee->minimum_exchange_fee ?></td>
            <td class="table-10">
                <?= $this->Element('Price/common_buttons', ['shippingFee' => $shippingFee, 'tab' => 'shipping-fee']); ?>
            </td>
        </tr>
        <?php } ?>

        <?= $this->Element('Price/shipping_fee_price_modal'); ?>
    </tbody>
</table>

<div class="row">
    <div class="col-xs-12 text-right">
        <a href="" class="btn btn-warning shipping-fee" data-toggle="modal" data-target="#shipping-fee"><?= __('add') ?></a>
    </div>
</div>

