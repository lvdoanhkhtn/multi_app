<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/2/19-->
<!-- * Time: 5:17 PM-->
<!-- */-->
<div class="row form-group">
    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('code', ['class' => 'required form-control max-length username','data-max-length' => 25, 'data-name' => __('code'), 'maxlength' => false, 'placeholder' => __('code'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('code').'(*)']);  ?>
    </div>
    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
        <?= $this->Form->control('description', ['class' => 'form-control max-length','data-max-length' => 255, 'data-name' => __('description'), 'maxlength' => false, 'placeholder' => __('description'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('description')]);  ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-6 material-switch pull-left">
        <?php if(isset($data)){ ?>
            <?= __('active_lock') ?>
            <input id="someSwitchOptionSuccess" <?= !empty($data['active']) ? 'checked' : '' ?>  name="active" type="checkbox"/>
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        <?php } ?>
    </div>
    <div class="col-xs-6 text-right">
        <button type="submit" class="btn btn-success"><?= !empty($data['code']) ? __('update') : __('create') ?></button>
        <?= $this->Html->link(__('cancel'), ['action' => 'index'], ['class' => 'btn btn-danger']); ?>
    </div>

</div>