<script>
    $(document).ready(function () {
        $('#frmCodFee').validate();
    });
</script>
<div class="modal" id="cod-fee" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?= __('cod_fee') ?>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <?php
                            echo $this->Form->control('id', ['type' => 'hidden','id' => 'codFeeId', 'value' => '']);
                            echo $this->Form->control('price_id', ['type' => 'hidden','id' => 'price_id', 'value' => $id]);
                            echo $this->Form->control('tab', ['type' => 'hidden','id' => 'codFeeTab', 'value' => '']);
                        ?>
                        <?= $this->Form->control('service_pack_id', ['id' => 'service_pack_id_cod','empty' => __('select'),'label' => __('service_pack'),'data-name' => __('service_pack'),'class' => 'required select2 form-control', 'options' => $configs['service_pack']]); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('location_id', ['id' => 'location_id_cod','empty' => __('select'), 'label' => __('delivery_group'),'data-name' => __('delivery_group'),'class' => 'required form-control', 'options' => $locations]); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('from_cod', ['id' => 'from_cod','class' => 'required form-control decimal', 'label' => __('from_cod').'(*)', 'data-name' => __('from_cod'), 'placeholder' => '0.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('to_cod', ['id' => 'to_cod','class' => 'required form-control decimal', 'label' => __('to_cod').'(*)', 'data-name' => __('to_cod'), 'placeholder' => '100.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('collection_fee', ['id' => 'amount','class' => 'required form-control decimal', 'label' => __('amount').'(*)', 'data-name' => __('amount'), 'placeholder' => '0.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('minimum_collection_fee', ['id' => 'minimum_amount','class' => 'required form-control decimal', 'label' => __('minimum_amount').'(*)', 'data-name' => __('minimum_amount'), 'placeholder' => '0.00']); ?>
                    </div>

            </div>
            <?= $this->Element('Modal/buttons') ?>

            </div>
    </div>
</div> <!-- /.Popup -->
