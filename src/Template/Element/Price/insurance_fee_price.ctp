<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/2/19-->
<!-- * Time: 5:17 PM-->
<!-- */-->
<table class="table table-striped table-sm table-responsive">
    <thead class="thead-dark">
    <tr>
        <th class="table-10"><?= __('service_pack') ?></th>
        <th class="table-20"><?= __('delivery_group') ?></th>
        <th class="table-10"><?= __('from_goods_value') ?></th>
        <th class="table-10"><?= __('to_goods_value') ?></th>
        <th class="table-10"><?= __('amount') ?></th>
        <th class="table-10"><?= __('minimum_amount') ?></th>
        <th class="table-10 mw100"><?= __('action') ?></th>
    </tr>
    </thead>
    <tbody>
        <tr><td><?= $this->Form->postLink(null); ?></td></tr>

    <?php foreach ($shippingFees as $shippingFee){ ?>
            <tr <?= $shippingFee->id === PRICE_DEFAULT ? 'class="bg-red"' : "" ?>>
            <td class="table-10"><?= h($configs['service_pack'][$shippingFee->service_pack->id]); ?></td>
            <td class="table-20"><?= h($shippingFee->location->name); ?></td>
            <td class="table-10"><?= $shippingFee->from_goods_value ?></td>
            <td class="table-10"><?= $shippingFee->to_goods_value ?></td>
            <td class="table-10"><?= $shippingFee->protect_fee ?></td>
            <td class="table-10"><?= $shippingFee->minimum_protect_fee ?></td>
            <td class="table-10">
                <?= $this->Element('Price/common_buttons', ['shippingFee' => $shippingFee, 'tab' => 'insurance-fee']); ?>
            </td>
        </tr>
    <?php } ?>

    <?= $this->Element('Price/insurance_fee_price_modal'); ?>

    </tbody>
</table>
<div class="row">
    <div class="col-xs-12 text-right">
        <a href="" class="btn btn-warning insurance-fee" data-toggle="modal" data-target="#insurance-fee">Add</a>
    </div>
</div>
