<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 12/10/19-->
<!-- * Time: 11:23 AM-->
<!-- */-->

<div class="btn-group">
    <a href="" class="btn btn-outline-info <?= $tab ?>" data-id="<?= $shippingFee->id ?>" data-toggle="modal"
       data-target="#<?= $tab ?>" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i>
    </a>

</div>
<?php
    echo $this->Form->postLink(
    "<i class='fa fa-trash-alt'></i>", // first
    ['action' => 'deleteChildPrice', $shippingFee->id, $shippingFee->price_id, $tab],  // second
    ['escape' => false, 'title' => 'Delete', 'class' => 'btn btn-outline-danger', 'confirm' => __('delete_confirm')] // third
    );
 ?>