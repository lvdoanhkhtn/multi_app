<script>
    $(document).ready(function () {
        $('#frmInsuranceFee').validate();
    });
</script>
<div class="modal" id="insurance-fee" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?= __('insurance_fee') ?>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('id', ['type' => 'hidden','id' => 'insuranceFeeId', 'value' => '']); ?>

                        <?= $this->Form->control('tab', ['type' => 'hidden','id' => 'insuranceFeeTab', 'value' => '']); ?>

                        <?= $this->Form->control('price_id', ['type' => 'hidden','id' => 'price_id', 'value' => $id]); ?>

                        <?= $this->Form->control('service_pack_id', ['id' => 'service_pack_id_insurance','empty' => __('select'),'label' => __('service_pack'),'data-name' => __('service_pack'),'class' => 'required select2 form-control', 'options' => $configs['service_pack']]); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('location_id', ['id' => 'location_id_insurance','empty' => __('select'), 'label' => __('delivery_group'),'data-name' => __('delivery_group'),'class' => 'required form-control', 'options' => $locations]); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('from_goods_value', ['id' => 'from_goods_value','class' => 'required form-control decimal', 'label' => __('from_goods_value').'(*)', 'data-name' => __('from_goods_value'), 'placeholder' => '0.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('to_goods_value', ['id' => 'to_goods_value','class' => 'required form-control decimal', 'label' => __('to_goods_value').'(*)', 'data-name' => __('to_goods_value'), 'placeholder' => '100.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('protect_fee', ['id' => 'protect_fee','class' => 'required form-control decimal', 'label' => __('amount').'(*)', 'data-name' => __('amount'), 'placeholder' => '0.00']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $this->Form->control('minimum_protect_fee', ['id' => 'minimum_protect_fee','class' => 'required form-control decimal', 'label' => __('minimum_amount').'(*)', 'data-name' => __('minimum_amount'), 'placeholder' => '0.00']); ?>
                    </div>

            </div>
            <?= $this->Element('Modal/buttons') ?>

            </div>
    </div>
</div> <!-- /.Popup -->
