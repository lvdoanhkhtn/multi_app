<div class="box-body">
    <div class="form-group col-md-12 table-responsive">
        <!--/**-->
        <!-- * Created by PhpStorm.-->
        <!-- * User: doanh.lv-->
        <!-- * Date: 9/27/19-->
        <!-- * Time: 11:18 AM-->
        <!-- */-->
        <?php $customControllers =  array_values(\App\Libs\ConfigUtil::get('CUSTOM_CONTROLLERS')); ?>

        <?= $this->Form->create(null, ['type' => 'GET']); ?>
        <div class="row form-group">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                <?php echo $this->Form->control('id', array('default' => !empty($_GET['id']) ? $_GET['id'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $list, 'label' => __('name')))?>
            </div>
        </div>

        <div class="pull-right">
            <?= $this->Form->button('<i class="fa fa-search"></i>'.__('search'), array('class' => 'btn btn-info','type'=>'submit')); ?>
        </div>

        <div class="row form-group">

        </div>
        <?= $this->Form->end(); ?>

        <?php if($this->Paginator->param('pageCount') > 1){ ?>
            <?= $this->AdminView->paginateMenuAndResult() ?>
        <?php } ?>
        <div class="box">
            <table class="table table-striped table-sm table-responsive">
                <thead class="thead-dark">
                <tr>
                    <th class="text-top"><?= __('id') ?></th>
                    <th class="text-top"><?= __('name') ?></th>
                    <th class="text-top"><?= __('sort') ?></th>
                    <th class="text-top"><?= __('vietnamese_alias') ?></th>
                    <th class="text-top"><?= __('english_alias') ?></th>
                    <th class="text-top"><?= __('khome_alias') ?></th>
                    <th class="text-top"><?= __('view') ?></th>
                    <th class="text-top"><?= __('basic') ?></th>
                    <th class="text-top"><?= __('active') ?></th>
                    <th class="text-top text-right"><?= __('action') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($datas as $data){ ?>
                    <tr>
                        <td><?= $data->id; ?></td>
                        <td><?= h($data->name); ?></td>
                        <td><?= h($data->sort); ?></td>
                        <td><?= h($data->alias); ?></td>
                        <td><?= h($data->eng_alias); ?></td>
                        <td><?= h($data->kh_alias); ?></td>
                        <td><?= $this->AdminView->showActive($data->is_view); ?></td>
                        <td><?= $this->AdminView->showActive($data->is_basic); ?></td>
                        <td><?= $this->AdminView->showActive($data->active); ?></td>
                        <td class="pull-right">
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $data->id, !empty($data->method_id) ? $data->method_id : null ], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']); ?>
                            <?php
                                if(empty($data->method_id)){
                                    echo $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'actions','action' => 'index', $data->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'View', 'class' => 'btn btn-outline-info']);
                                }
                            ?>

                        </td>
                    </tr>

                <?php } ?>

                </tbody>
            </table>
            <?= $this->Element('paging') ?>

        </div><!-- /.box-body -->

    </div>
</div>
