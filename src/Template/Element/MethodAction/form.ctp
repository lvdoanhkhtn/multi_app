<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 11/20/19-->
<!-- * Time: 2:23 PM-->
<!-- */-->
<div class="box-body">
    <?= $this->Form->create($data, ['class' => 'validateForm']); ?>
    <div class="row">
        <div class="form-group col-md-3 col-xs-12">
            <?= $this->Form->control('alias', ['value' => !empty($data['alias']) ? $data['alias'] : '', 'class' => 'required form-control max-length', 'data-max-length' => 100, 'data-name' => __('alias'), 'required' => false, 'maxlength' => false, 'placeholder' => __('vietnamese_alias'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('vietnamese_alias') . '(*)']); ?>
        </div>
        <div class="form-group col-md-3 col-xs-12">
            <?= $this->Form->control('eng_alias', ['value' => !empty($data['eng_alias']) ? $data['eng_alias'] : '', 'class' => 'required form-control max-length', 'data-max-length' => 100, 'data-name' => __('alias'), 'required' => false, 'maxlength' => false, 'placeholder' => __('english_alias'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('english_alias') . '(*)']); ?>
        </div>
        <div class="form-group col-md-3 col-xs-12">
            <?= $this->Form->control('kh_alias', ['value' => !empty($data['kh_alias']) ? $data['kh_alias'] : '', 'class' => 'required form-control max-length', 'data-max-length' => 100, 'data-name' => __('alias'), 'required' => false, 'maxlength' => false, 'placeholder' => __('khome_alias'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('khome_alias') . '(*)']); ?>
        </div>
        <div class="form-group col-md-3 col-xs-12">
            <?= $this->Form->control('sort', ['type' => 'number','min' => 1,'data-name' => __('sort'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('sort') . '(*)']); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 mt20">
            <?= $this->Form->control('is_view', ['type' => 'checkbox', 'data-name' => __('is_view'), 'placeholder' => __('is_view')]); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 mt20">
            <?= $this->Form->control('is_basic', ['type' => 'checkbox', 'data-name' => __('is_basic'), 'placeholder' => __('is_basic')]); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 mt20">
            <?= $this->Form->control('is_show_admin', ['type' => 'checkbox', 'data-name' => __('is_show_admin'), 'placeholder' => __('is_show_admin')]); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 mt20">
            <?= $this->Form->control('active', ['type' => 'checkbox', 'data-name' => __('active'), 'placeholder' => __('active')]); ?>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-6 material-switch pull-left">
        </div>
        <div class="col-xs-6 text-right">
            <button type="submit" class="btn btn-success"><?= (isset($data)) ? __('update') : __('create') ?></button>
            <a class="btn btn-danger" href="javascript:history.back()"><?= __('cancel') ?></a>
        </div>
    </div>

    <?= $this->Form->end(); ?>

    <!-- /.box-body -->
</div>