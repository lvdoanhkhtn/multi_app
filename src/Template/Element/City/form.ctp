<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 12/10/19-->
<!-- * Time: 9:47 AM-->
<!-- */-->

<div class="row">
    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12">
        <?= $this->Form->control('name', ['value' => !empty($data['name']) ? $data['name'] : '', 'class' => 'required form-control max-length', 'data-max-length' => 255, 'data-name' => __('name'), 'required' => false, 'maxlength' => false, 'placeholder' => __('name'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('name') . '(*)']); ?>
    </div>
</div>


<div class="row">
    <div class="col-xs-6 material-switch pull-left">
        <?php if (isset($data)) { ?>
            Delivery/Not delivery
            <input
                id="someSwitchOptionSuccessActive" <?= (!empty($data['is_show'])) ? 'checked' : '' ?>
                name="is_show" type="checkbox"/>
            <label for="someSwitchOptionSuccessActive" class="label-success"></label>
            </br>
            Pickup/Not pickup
            <input
                id="someSwitchOptionSuccessShow" <?= (!empty($data['is_get'])) ? 'checked' : '' ?>
                name="is_get" type="checkbox"/>
            <label for="someSwitchOptionSuccessShow" class="label-success"></label>
        <?php } ?>
    </div>
    <div class="col-xs-6 text-right">
        <button type="submit"
                class="btn btn-success btnSubmit"><?= (isset($data)) ? __('update') : __('create') ?></button>
        <?= $this->Html->link(__('cancel'), ['action' => 'index'], ['class' => 'btn btn-danger']); ?>
    </div>
</div>