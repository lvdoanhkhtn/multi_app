<?php
    $color = "";
    if(!empty($session->delivery_complete_orders)){
        $order = $excel->getCollect($session->delivery_complete_orders);
        $totalCollect = $order['total_cod'] - $order['total_sub_fee'];
        $order['total_fee'] = 0;
    
    }else{
        $color = "green";
        $order = $excel->getCollected($session->pickup_stored_orders);
        $order['total_sub_fee'] = 0;
        $totalCollect = $order['total_fee'];
    }
?>
<tr style="background-color: <?= $color ?>">
    <td><?= $session->id ?></td>
    <td><?= $session->getCreated() ?></td>
    <td><?= $session->getModified() ?></td>
    <td><?= $session->employee->employee_id ?></td>
    <td><?= $session->employee->user->fullname ?></td>
    <td><?= $order['total_order'] ?></td>
    <td><?= $this->AdminView->getCurrencyCam($order['total_cod']) ?></td>
    <td><?= $this->AdminView->getCurrencyCam($order['total_fee']) ?></td>
    <td><?= $this->AdminView->getCurrencyCam($order['total_sub_fee']) ?></td>
    <td><?= $this->AdminView->getCurrencyCam($totalCollect) ?></td>
    <td>
        <?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => !empty($session->delivery_complete_orders) ? 'delivery_trip_details' : 'pickup_trip_details', 'action' => 'collectDetail', $session->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'View', 'class' => 'btn btn-outline-info']); ?>
    </td>

</tr>