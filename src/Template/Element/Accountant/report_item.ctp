<?php
$order = $data->order;
if(!empty($order->complete_delivery_trip_details[0])){
    $deliveryTripDetail = $order->complete_delivery_trip_details[0];
    $employeeName = $deliveryTripDetail->delivery_trip->employee->user->fullname;
    $tripId = $deliveryTripDetail->delivery_trip_id;
}
else if(!empty($order->complete_pickup_trip_details[0])){
    $pickupTripDetail = $order->complete_pickup_trip_details[0];
    $employeeName = $pickupTripDetail->pickup_trip->employee->user->fullname;
    $tripId = $pickupTripDetail->pickup_trip_id;
}
$totalFee = 0;
if (!empty($data->type)) {
    $order->total = 0;
    $order->sub_fee = 0;
    $totalFee = $order->postage + $order->collection_fee + $order->protect_fee;
    $totalCollect = $totalFee;
} else {
    $totalCollect = $order->total - $order->sub_fee;
}
?>

<tr>
    <td><?= $step++; ?></td>
    <td><?= $data->user->fullname; ?></td>
    <td><?= $employeeName ?></td>
    <td><?= $this->Html->link($order->orderId, ['controller' => 'orders', 'action' => 'view', $order->id], ['target' => 'blank']); ?></td>
    <td><?= !empty($data->type) ? __('collected_order') : ''; ?></td>
    <td><?= $this->Html->link($tripId, ['controller' => !empty($deliveryTripDetail->delivery_trip_id) ? 'delivery_trip_details' : 'pickup_trip_details', 'action' => 'view', $tripId], ['target' => 'blank']); ?></td>
    <td><?= $this->AdminView->getCurrencyCam($order->total); ?></td>
    <td><?= $this->AdminView->getCurrencyCam($totalFee); ?></td>
    <td><?= $this->AdminView->getCurrencyCam($order->sub_fee); ?></td>
    <td><?= $this->AdminView->getCurrencyCam($totalCollect); ?></td>
    <td><?= $data->getCreated(); ?></td>
</tr>