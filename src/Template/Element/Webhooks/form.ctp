<div class="row">
    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12">
        <?= $this->Form->control('url', ['value' => !empty($data['url']) ? $data['url'] : '','class' => 'required form-control max-length','data-max-length' => 255, 'data-name' => __('url'), 'required' => false,'maxlength' => false, 'placeholder' => __('url'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('url').'(*)']);  ?>
    </div>
    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12">
        <?= $this->Form->control('name', ['value' => !empty($data['name']) ? $data['name'] : '','class' => 'required form-control max-length','data-max-length' => 255, 'data-name' => __('name'), 'required' => false,'maxlength' => false, 'placeholder' => __('name'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('name').'(*)']);  ?>
    </div>

</div>


<div class="row">
    <div class="col-xs-6 material-switch pull-left">
    </div>
    <div class="col-xs-6 text-right">
        <button type="submit" class="btn btn-success btnSubmit"><?= (isset($data)) ? __('update') : __('create') ?></button>
        <?= $this->Html->link(__('cancel'), ['action' => 'index'], ['class' => 'btn btn-danger']); ?>
    </div>
</div>