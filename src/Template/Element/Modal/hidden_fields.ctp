<?php

/**
 * Created by PhpStorm.
 * User: doanh.lv
 * Date: 10/23/19
 * Time: 4:23 PM
 */
$randomString = bin2hex(openssl_random_pseudo_bytes(3));
echo $this->Form->control('id', ['id' => 'id'.$randomString,'type' => 'hidden', 'class' => 'deliveryTripDetailId']);
echo $this->Form->control('delivery_id', ['id' => 'delivery_id'.$randomString,'type' => 'hidden', 'value' => $id]);
echo $this->Form->control('tab', ['id' => 'tab'.$randomString,'type' => 'hidden', 'class' => 'tab']);
