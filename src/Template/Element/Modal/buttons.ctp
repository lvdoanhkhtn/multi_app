<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/8/19-->
<!-- * Time: 10:14 AM-->
<!-- */-->

<div class="modal-footer">
    <button type="submit" class="btn btn-success"><?= __('update') ?></button>
    <?= $this->Html->link(__('cancel'), [], ['data-dismiss' => 'modal', 'class' => 'btnConfirm btn btn-danger']); ?>
</div>