<div class="modal" id="order_result_popup" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content" style="height: 100%; overflow:scroll">
            <div class="modal-header">
                <span class="modal-title"> <?= __('order_detail') ?>
                </span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="col-xs-12">
                    <div class="col-xs-12 no-padding-left text-left">
                        <div class="col-xs-12 form-group">
                            <span><?= __('order_code') ?> :</span>
                            <span id="order_code"></span>
                        </div>
                        <div class="col-xs-12 form-group">
                            <span><?= __('receiver') ?> :</span>
                            <span id="receiver_name"></span>
                        </div>

                        <div class="col-xs-12 form-group">
                            <span><?= __('phone') ?> :</span>
                            <span id="receiver_phone"></span>
                        </div>

                        <div class="col-xs-12 form-group">
                            <span><?= __('receiver_address') ?> :</span>
                            <span id="receiver_address"></span>
                        </div>

                        <div class="col-xs-12 form-group">
                            <span><?= __('total_cod') ?> :</span>
                            <span id="total_cod"></span>
                        </div>

                        <div class="col-xs-12 form-group">
                            <span><?= __('note') ?> :</span>
                            <span id="note"></span>
                        </div>

                        <div class="col-xs-12 form-group">
                            <span><?= __('order_status') ?> :</span>
                            <span id="order_status"></span>
                        </div>

                        <div class="col-xs-12 form-group" style="height: 200px;overflow-x: scroll;">
                            <div class="box  box-default flat">
                                <div class="box-header">
                                    <div class="col-xs-7">
                                        <h4 class="text-bold"><?= __('history') ?></h4>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <ul class="timeline">

                                    </ul>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div> <!-- /.Popup -->