<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 11/4/19-->
<!-- * Time: 11:28 AM-->
<!-- */-->
<script>
    $(document).ready(function () {
        $('#frmChangeShipper').validate({});
    });
</script>


<?= $this->Form->create(null, ['type' => 'post', 'action' => 'changeShipper','id' => 'frmChangeShipper','class' => 'validateForm']); ?>

<div id="change-shipper" class="modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Change Shipper
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?= $this->Form->control('shipper_id', ['data-name' => __('shipper'),'empty' => __('select'),'class' => 'required select2', 'options' => $deliveryShippers]) ?>
                    <?= $this->Form->control('id', ['id' => 'trip_detail_id','type' => 'hidden', 'value' => '']); ?>
                </div>
            </div>
            <?= $this->Element('Modal/buttons') ?>
        </div>
    </div>
</div> <!-- /.popup -->
<?php $this->Form->end(); ?>