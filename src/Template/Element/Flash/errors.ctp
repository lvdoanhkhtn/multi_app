<?php
if (isset($params['errors'])) {
    $str_errors = '';
    foreach ($params['errors'] as $k => $v) {
        $str_errors .= $this->Html->tag('div', $v);
    }
    echo $this->Html->tag('div', $str_errors, ['class' => 'error message','onclick' => "this.classList.add('hidden');"]);
}
?>