<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = __('Addresses Management');
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>
                
                <div class="box-body">
                    <div class="form-group col-md-12">
                        <?= $this->AdminView->paginateMenuAndResult() ?>
                        <div class="box table-responsive">
                            <table class="table table-bordered table-condensed table-hover" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-top"><?= __('ID') ?></th>
                                    <th class="text-top"><?= __('NAME') ?></th>
                                    <th class="text-top"><?= __('PHONE') ?></th>
                                    <th class="text-top"><?= __('ADDRESS') ?></th>
                                    <th class="text-top"><?= __('ACTION') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($addresses as $address){ ?>
                                        <tr>
                                            <td><?= $address->id ?></td>
                                            <td><?= $address->agency ?></td>
                                            <td><?= $address->str_phones ?></td>
                                            <td><?= $address->getAddress(); ?></td>
                                            <td>
                                                <?= $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', ['controller' => 'addresses', 'action' => 'edit', $address->id], ['escape' => false,'class' => 'btn btn-sm btn-danger pull-right']); ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>

                        <?= $this->Element('paging') ?>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>


