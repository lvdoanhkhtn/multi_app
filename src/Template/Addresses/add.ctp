<section class="content">
  <div class="row">
    <div class="col-xs-12 col-md-12">
      <div class="box box-solid">

        <?php
            $link = $this->Html->link(__('Addresses Management'), ['controller' => 'addresses', 'action' => 'index'], ['class' => 'text-red']). '<span class="text-red"> > Add </span>';
            echo $this->Element('breadcum', ['link' => $link]) ;
        ?>

        <div class="box-body">
          <?= $this->Form->create(null, ['class' => 'validateForm customValidateForm']); ?>
          
          <?= $this->Element('Address/form'); ?>


          <div class="col-xs-12 box-header with-border">
            <div class="col-md-12 text-center">
              <button type="submit" class="btn btn-danger"><?= __('CREATE') ?></button>
            </div>
          </div>

          <?= $this->Form->end(); ?>

          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </div>
</section>
