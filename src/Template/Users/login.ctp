<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>CambodiaShip</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<?= $this->Html->css(['admin.min', './bootstrap/dist/css/bootstrap.min', 'common']); ?>
</head>
<body style="background-image:url('/img/background.jpg');background-position: center;background-repeat: no-repeat;
   background-size: cover;">
<div class="login-box">
	<div class="login-logo">
		<?= $this->Html->image('logo_bill.jpg', ['width' => '200px']); ?>
	</div><!-- /.login-logo -->
	<p class="login-box-msg text-red">Make Cambodia ECommerce Better</p>
	<div class="login-box-body">
		<?= $this->Form->create(null, ['id' => 'login']); ?>

		<div class="form-group has-feedback">
			<?= $this->Form->control('email', ['class' => 'form-control', 'placeholder' => 'Email', 'div' => false, 'label' => false]);  ?>
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<?= $this->Form->control('password', ['class' => 'form-control', 'placeholder' => 'Password', 'div' => false, 'label' => false]);  ?>
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="row">
			<?= $this->Flash->render() ?>
		</div>

		<div class="form-group text-center">
				<button type="submit" class="btnSubmit btn btn-danger"><?= __('LOGIN') ?></button>
		</div>

		<?php $this->Form->end(); ?>


		<?php
			if(empty($isHidden)){
				echo $this->Html->link(__('CREATE NEW ACCOUNT'),['controller' => 'users', 'action' => 'register'], ['class' => 'text-red text-center']);
			}
		?>



	</div><!-- /.login-box-body -->
</div><!-- /.login-box -->
</body>
