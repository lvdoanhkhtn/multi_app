<section class="content">
  <div class="row">
    <div class="col-xs-12 col-md-12">
      <div class="box box-solid">

        <?php
          $link = __('change_password');
          echo $this->Element('breadcum', ['link' => $link]) ;
        ?>

        <div class="box-body">
          <?= $this->Form->create(null, ['class' => 'validateForm customValidateForm']); ?>

          <div class="input-group form-group">
            <span class="input-group-addon"><i class="fas fa-lock"></i></span>
            <?= $this->Form->control('password', ['class' => 'no-border-radius form-control required','data-name' => __('password'),'placeholder' => __('password').'(*)', 'templates' => ['inputContainer' => '{{content}}'], 'label' => false]);  ?>
          </div>
          <div class="errorTxt form-group"></div>

          <div class="col-xs-12 box-header with-border">
            <div class="col-md-6 text-right">
              <button type="submit" class="btn btn-danger"><?= __('update') ?></button>
            </div>

          </div>

          <?= $this->Form->end(); ?>

          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </div>
</section>
