<section class="content">
  <div class="row">
    <div class="col-xs-12 col-md-12">
      <div class="box box-solid">

        <?php
            $link = __('Account info');
            echo $this->Element('breadcum', ['link' => $link]) ;
        ?>

        <div class="box-body">
          <?= $this->Form->create(null, ['class' => 'validateForm customValidateForm']); ?>

          <?= $this->Element('User/form', ['account' => $account]) ?>


          <div class="col-xs-12 box-header no-padding form-group">
            <div class="col-md-6 pull-left">
              <?= $this->Html->link(__('change_password'), ['controller' => 'accounts', 'action' => 'changePassword'], ['escape' => false,'class' => 'btn btn-danger btn-info']); ?>
            </div>

            <div class="col-md-6 text-right">
              <button type="submit" class="btn btn-danger"><?= __('UPDATE') ?></button>
            </div>

          </div>

          <?= $this->Form->end(); ?>

          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </div>
</section>
