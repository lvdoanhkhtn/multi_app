<?php

use Cake\Core\Configure;

?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CambodiaShip</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->

  <?= $this->Html->css('bootstrap/dist/css/bootstrap.min'); ?>



  <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/2.3.1/css/flag-icon.min.css" rel="stylesheet">
  <!-- jQuery 3 -->
  <?php echo $this->Html->script('jquery.min'); ?>

  <!-- Ionicons -->
  <?php echo $this->Html->css('ionicons.min'); ?>
  <!-- Theme style -->
  <?php echo $this->Html->css('admin.min'); ?>

  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <?php echo $this->Html->css('skin-red.min'); ?>

  <!-- Customize style -->
  <?php echo $this->Html->css(['common', 'center', 'fontawesome-free/css/all.min', 'addons/datatables.min']); ?>

  <!-- daterange picker -->
  <!--    <link href="../../plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />-->

  <?= $this->Html->css('daterangepicker/daterangepicker-bs3'); ?>

  <!-- multi select -->

  <?php echo $this->Html->css('select2/dist/css/select2.min', ['block' => 'css']); ?>

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">





  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>

  <!-- Font Awesome -->
  <script src="https://kit.fontawesome.com/33767a916f.js"></script>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <?php echo $this->fetch('css'); ?>

</head>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo $this->Url->build('/'); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">CambodiaShip</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">CambodiaShip</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <?php echo $this->element('nav-top'); ?>
  </header>

  <?php echo $this->element('aside-main-sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <?php echo $this->Flash->render(); ?>
    <?php echo $this->Flash->render('auth'); ?>
    <?php echo $this->fetch('content'); ?>

      <div id="fadeMe1" class="row row-center">
          <button id="hidden1" class="btn btn-lg btn-warning hidden" >
              <span class="glyphicon glyphicon-refresh glyphicon-spin"></span>Loading...
          </button>
      </div>

  </div>
  <!-- /.content-wrapper -->

  <?php echo $this->element('footer'); ?>

  <!-- Control Sidebar -->
  <?php echo $this->element('aside-control-sidebar'); ?>
    <div class="control-sidebar-bg"></div>
    <!-- /.control-sidebar -->

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->

    <?= $this->Form->control('print_url', ['type' => 'hidden', 'id' => 'print_url', 'value' => $this->Url->build(['controller' => 'orders', 'action' => 'printOrderOnSmallPages'])]); ?>

</div>
<!-- ./wrapper -->
</body>



<!-- Bootstrap 3.3.7 -->
<?php echo $this->Html->script('bootstrap.min'); ?>

<?php echo $this->Html->script('app'); ?>

<!-- Slimscroll -->
<?php echo $this->Html->script('jquery-slimscroll/jquery.slimscroll.min'); ?>

<!-- daterange picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>


<!-- Customize -->
<?= $this->Html->script(['jquery/daterangepicker.js', 'jquery/jquery.cookie', 'jquery/jquery.validate', 'additional-methods', 'additional-setting', 'shop', 'url.min', 'common', 'addons/datatables.min']); ?>
<?php echo $this->Html->script('select2/select2.full.min', ['block' => 'script']); ?>


<!-- iCheck -->
<?php echo $this->Html->script('iCheck/icheck.min'); ?>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>


<?php echo $this->fetch('script'); ?>

<?php echo $this->fetch('scriptBottom'); ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".navbar .menu").slimscroll({
            height: "200px",
            alwaysVisible: false,
            size: "3px"
        }).css("width", "100%");

        var a = $('a[href="<?php echo $this->Url->build() ?>"]');
        if (!a.parent().hasClass('treeview') && !a.parent().parent().hasClass('pagination')) {
            a.parent().addClass('active').parents('.treeview').addClass('active');
        }

        $('.mySelect').selectpicker();

        $('[data-toggle="tooltip"]').tooltip();

        $('.icon-ok').remove();
  });

</script>


</html>