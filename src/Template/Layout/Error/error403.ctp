<div class="row">
	<div class="col-sm-12 col-lg-12">
		<div class="text-center col-sm-12 col-lg-12">
			<h1 class="error-title">Oops, 403!</h1>
			<p class="lead"><?= __('not_permission'); ?></p>
			<!-- search form -->

			<form class="form-inline form-search" role="form">
			<?php echo $this->Html->link(__('return_previous_page'), $this->request->referer(), array('type' => 'submit', 'class' => 'btn btn-info btn-lg')); ?>

			</form>
			<!-- /search form -->
		</div>
	</div>
</div>
