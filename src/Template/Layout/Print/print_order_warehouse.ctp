<!DOCTYPE html>
<html>
	<head>
		<title>In đơn hàng</title>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="/img/anzship_favi.ico" type="image/x-icon" />
		<style>
			body{width:1120px;}
			table {border-spacing: 0px;width:520px; height:355px;float:left;margin-right:15px;margin:10px; font-size:13px;overflow: hidden; table-layout: fixed}
 			/*@media print {*/
      			/*footer {page-break-after: always;}*/
  			/*}*/
		</style>
	</head>
	<body>
        <section>
        	<?php $i=1; foreach ($orders as $order){?>
				   <table border="1">
				   		    <tr style='height:100px;'>
								<td style="width: 50%">
                                    <img src='/img/logo_bill.jpg' width=120 height=60 style="display: block" /><br>
                                    <?= __('sender') ?> : <?= h($order['sender_pickup']); ?><br>
									<?php echo ($order['agency']!=="")? __('agency').": ".h($order['agency']) : ""; ?>
									<br>
									<?= __('phone') ?> : <?= h($order['phone_pickup']); ?><br>
									<?= __('order_shop') ?> : <?= h($order['shopId']); ?>
								</td>
								<td style="width: 50%; text-align: center">
                                    <div style="margin-left: 10px; margin-top: 10px">
                                        <?= $this->BarcodeView->generateBarcode($order['orderId']) ?>
                                    </div>
									<?= __('order_code') ?> : <?= h($order['orderId']); ?>
									<?= !empty($order['custom_address']) ? sprintf("</br>".__('custom_pickup_address').": %s",h($order['custom_address'])) : ""; ?>
								</td>
							</tr>
							<tr style='height:100px'>
								<td valign='top' colspan='3'>
									<div style='overflow:hidden; height : 35px'>
										<?= __('receiver') ?> : <?= h($order['full_address_delivery']); ?>;
									</div>
									<?= __('time_to_send') ?> : <?= $order['created']; ?> - <?= __('service_pack') ?> : <?= $order['delivery_service']; ?><br>
                                    <?= __('method_of_payment') ?>  : <?= $order['payer']; ?><br>
									<?= __('total') ?> : <?= $this->AdminView->getCurrencyCam($order['total']); ?><br>
									<?= __('content') ?> : <?= h($order['content']); ?><br>
									<div style='overflow:hidden; height : 45px'>
										<?= __('note') ?> : <?= h($order['note']); ?>
									</div>
								</td>
							</tr>
							<tr style='height:20px'>
								<td valign='top' style='text-align : center'>
									<?= __('shipper_pickup') ?><br>
								</td>
								<td valign='top' style='text-align : center'><?= __('sender') ?><br><br><br><?= h($order['sender_pickup']); ?></td>
							</tr>
							<tr><td colspan='3'>
								<div style="text-align: center; font-size: 8px">
										Hotline: <?php echo HOT_LINE; ?>
								</div>
								</td>
							</tr>
				   </table>

				   <?php echo ($i%4==0) ? '<p style="page-break-after:always"></p>' : ""?>
				<?php $i++; } ?>
    	</section>
    </body>
</html>
<!-- dialog show list image photobucket -->
