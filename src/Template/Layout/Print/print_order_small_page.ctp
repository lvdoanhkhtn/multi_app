<!DOCTYPE html>
<html>
<head>
	<title>In đơn hàng</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="/img/anzship_favi.ico" type="image/x-icon" />
	<style>
		table {width:227px; height:152px;font-size:8px;overflow: hidden; table-layout: fixed;margin: 0;  padding: 0;}
		@media print {
			footer {page-break-after: always;}
		}
		table {
			text-align: center;
			border-collapse: collapse;
		}

		table, th, td {
			border: 1px dashed black;
		}
	</style>
</head>
<body>
<section>
	<?php $i=1; foreach ($orders as $order){ ?>
		<table>
			<tr>
				<td colspan="6" style="text-align: center">
					<div>
						<?= $this->BarcodeView->generateBarcode($order['orderId']) ?>
					</div>
				</td>

			</tr>

			<tr>
				<td colspan="6" style="text-align: center">
					<?= sprintf("<b>Hotline: %s  - %s</b>", HOT_LINE, BRANDNAME); ?>
				</td>
			</tr>

			<tr style="height: 20px !important;">
				<td valign='top' colspan='3'>
					<div style='overflow:hidden; height : 35px'>
						<p>[SHOP] <?= $order['sender_pickup'] .'<br>'. $order['phone_pickup']; ?>;</p>
					</div>
				</td>
				<td valign='top' colspan='3' style="padding-top: 10px">
					<b><?= $order['orderId']; ?></b>
					<br>
					<b><?= $order['shopId']; ?></b>
				</td>
			</tr>

			<tr>
				<td valign='top' colspan='3'>
					<b><?='Create: '. $this->AdminView->formatDateTime($order['created']); ?></>
				</td>
				<td valign='top' colspan='3'>
					<b><?= __('service_pack') ?> : <?= $order['delivery_service']; ?></b>s
				</td>
			</tr>

			<tr>
				<td valign='top' colspan='2'>
					<b><?= sprintf("[%s]", $this->AdminView->getCurrencyCam($order['total'])); ?></>
				</td>
				<td valign='top' colspan='2'>
					<b><?= $order['receiver_name']; ?></b>
					<br>
					<b><?= $order['arr_receiver_phone']; ?></b>
				</td>
				<td valign='top' colspan='2'>
					<b><?= $order['district_delivery']; ?></b>
					<br>
					<b><?= $order['province_delivery']; ?></b>
				</td>
			</tr>

			<tr>
				<td valign='top' colspan='6'>
					<?= h($order['address_delivery']); ?>
				</td>
			</tr>

			<tr>
				<td valign='top' colspan='6'>
					<?= h($order['note']); ?>
				</td>
			</tr>

		</table>

		<p style="page-break-after:always;"></p>
		<?php $i++; } ?>
</section>
</body>
</html>
<!-- dialog show list image photobucket -->
