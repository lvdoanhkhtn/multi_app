<!DOCTYPE html>
<html>

<head>
    <title>In đơn hàng</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="description" content="">
    <meta name="author" content="">
    <style>
        table {
            width: 378px;
            height: 189px !important;
            font-size: 13px;
            overflow: hidden;
            table-layout: fixed;
            margin: 0 auto;
            padding: 0;
        }

        @media print {
            footer {
                page-break-after: always;
            }
        }

        table {
            text-align: center;
            border-collapse: collapse;
        }

        table,
        th,
        td {
            border: 1px dashed black;
        }

        img.logo {
            width: 140px;
            display: block;
            margin-bottom: 10px;
        }


        .center {
            text-align: center;
        }

        .address {
            height: 30px;
            text-align: left;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 2;
            /* number of lines to show */
            -webkit-box-orient: vertical;
        }

        .note {
            text-align: left;
            overflow: hidden;
            display: -webkit-box;
            -webkit-line-clamp: 3;
            /* number of lines to show */
            -webkit-box-orient: vertical;
        }
    </style>
</head>

<body>
    <section>
        <?php $i = 1;
        foreach ($orders as $order) { ?>
            <table>
                <tr>
                    <td valign='top' colspan='3'>
                        <img class="logo" src="/img/print_logo.jpg" />
                        <b><?= HOT_LINE ?></b>
                    </td>
                    <td valign='top' colspan='3' style="padding: 5px">
                        <?= $this->BarcodeView->generateBarcodeV2($order['orderId']) ?>
                        <?php if (strlen($order['shopId']) <= 16) {
                            echo sprintf("<b>%s / </b>", $order['orderId']);
                        } ?>
                        <?= sprintf("<b>%s</b>", $order['shopId']); ?>
                    </td>
                </tr>

                <tr style="text-align: left">
                    <td valign='top' colspan='6'>
                        <?= sprintf("Sender: %s - %s", $order['sender_pickup'], $order['phone_pickup']); ?>
                    </td>
                </tr>

                <tr>
                    <td valign='top' colspan='6'>
                        <span class="address">
                            <?= sprintf("Receiver: %s - %s - %s", h($order['receiver_name']), h($order['arr_receiver_phone']), h($order['address_delivery'])); ?>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        <b><?= $order['district_delivery']; ?></b>
                    </td>
                    <td colspan='2'>
                        <b><?= $order['province_delivery']; ?></b>
                    </td>
                    <td colspan='2'>
                        <b><?= sprintf("Weight: %s (kg)", !empty($order['weight']) ? $order['weight'] : 0) ?></b>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        <span class="note">
                            <?= h($order['note']); ?></b>
                        </span>
                    </td>
                    <td colspan='2'>
                        <span class="note">
                            <?= h($order['content']); ?>
                        </span>
                    </td>
                    <td colspan='2'>
                        <b><?= sprintf("COD: <br> %s <br> %s (USD)", $this->AdminView->getCustomCurrencyCam($order['total']), $order['total']) ?></b>
                    </td>
                </tr>


            </table>

            <p style="page-break-after:always;"></p>
        <?php $i++;
        } ?>
    </section>
</body>

</html>
<!-- dialog show list image photobucket -->