<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = $this->Html->link(__('role_customer_management'), ['action' => 'index'], ['class' => 'text-red']). '<span class="text-red"> > Edit </span>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <?= $this->Form->create($data, ['class' => 'validateForm']); ?>

                        <?= $this->Element('CommonForm/multi_language_form'); ?>

                    <?= $this->Form->end(); ?>

                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
