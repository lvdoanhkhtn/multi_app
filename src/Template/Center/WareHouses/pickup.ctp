<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/25/19-->
<!-- * Time: 5:17 PM-->
<!-- */-->
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = $this->Html->link(__('pickup_trips_management'), ['controller' => 'pickup_trips','action' => 'index'], ['class' => 'text-red']). '<span class="text-red"> >'.__('pickup').'</span>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="col-xs-12 form-group">
                        <div class="col-xs-4 no-padding-left">
                            <?= $this->Form->input('employee_id', array('id' => 'employee_id','options' => $shippers, 'empty' => __('select'), 'class' => 'form-control', 'label' => false)); ?>
                        </div>
                        <div class="col-xs-8 right"><?= $this->Form->button('<span class="fa fa-save margin-right"></span>' . __('create_pickup_trip'), array('id' => 'btnCreateGetTrip', 'type' => 'button', 'class' => 'btnCreateTrip btn btn-flat btn-success', 'escape' => false)); ?></div>
                    </div>
                    <?= $this->Element('WareHouse/ware_house', ['isPickupTrip' => true]) ?>



                </div>

            </div>
        </div>
    </div>
</section>
<?= $this->Html->script('WareHouse/ware_house'); ?>
