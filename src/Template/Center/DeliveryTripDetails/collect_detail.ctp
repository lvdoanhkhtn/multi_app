<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = $this->Html->link(__('collect_money_session'), ['controller' => 'accountants','action' => 'collectMoneySession'], ['class' => 'text-red']). ' > <h3 class="box-title text-red">'.__('collect_detail').'</h3>';
                    echo $this->Element('breadcum', ['link' => $link]) ;
                    echo $this->Form->control('deliveryId', ['id' => 'deliveryId','type' => 'hidden','value' => $deliveryTripId]);
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <!--/**-->
                        <!-- * Created by PhpStorm.-->
                        <!-- * User: doanh.lv-->
                        <!-- * Date: 10/22/19-->
                        <!-- * Time: 3:26 PM-->
                        <!-- */-->
                        <?php if(!empty($deliveryTrip->delivery_complete_orders[0])){
                                $data = $deliveryTrip->delivery_complete_orders[0];
                                $totalCollect = $data->total_cod - $data->total_sub_fee;
                            ?>
                        <div class="row form-group">

                            <div class="form-group col-xl-4 col-lg-4 col-md-4 col-xs-12">
                                <label for=""><?= __('total_cod') ?></label>
                                <input type="text" class="form-control" disabled placeholder="<?= $this->AdminView->getCustomCurrencyCam($data->total_cod) ?>">
                            </div>
                            <div class="form-group col-xl-4 col-lg-4 col-md-4 col-xs-12">
                                <label for=""><?= __('sub_fee') ?></label>
                                <input type="text" class="form-control" disabled
                                       placeholder="<?= $this->AdminView->getCustomCurrencyCam($data->total_sub_fee) ?>">
                            </div>
                            <div class="form-group col-xl-4 col-lg-4 col-md-4col-xs-12">
                                <label for=""><?= __('total_collect') ?></label>
                                <input type="text" class="form-control" disabled
                                       placeholder="<?= $this->AdminView->getCustomCurrencyCam($totalCollect) ?>">
                            </div>
                        </div>

                        <div class="row form-group">

                            <div class="form-group col-xl-4 col-lg-4 col-md-4 col-xs-12">
                                <label for=""><?= __('total_cod') ?></label>
                                <input type="text" class="form-control" disabled placeholder="<?= $this->AdminView->getCustomCurrencyUsd($data->total_cod) ?>">
                            </div>
                            <div class="form-group col-xl-4 col-lg-4 col-md-4 col-xs-12">
                                <label for=""><?= __('sub_fee') ?></label>
                                <input type="text" class="form-control" disabled
                                       placeholder="<?= $this->AdminView->getCustomCurrencyUsd($data->total_sub_fee) ?>">
                            </div>
                            <div class="form-group col-xl-4 col-lg-4 col-md-4col-xs-12">
                                <label for=""><?= __('total_collect') ?></label>
                                <input type="text" class="form-control" disabled
                                       placeholder="<?= $this->AdminView->getCustomCurrencyUsd($totalCollect) ?>">
                            </div>
                        </div>

                        <?php } ?>

                        <div class="row form-group">
                            <div class="pull-right margin-right-lg">
                                <a href="#" class="btn btn-success btnHanding" id="btnCollect"><i class="fa fa-money-bill-alt"></i>Collect money</a>
                            </div>
                        </div>

                        <div class="box">
                            <?= $this->Element('Order/orders', ['isCollectDetail' => true]) ?>
                        </div>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script(['Order/order']); ?>