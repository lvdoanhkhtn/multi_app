<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php

                $link = $this->Html->link(__('delivery_trip_management'), ['controller' => 'delivery_trips','action' => 'index'], ['class' => 'text-red']). ' > '.$this->Html->link(__('delivery_trip_details_management'), ['controller' => 'delivery_trip_details','action' => 'index', $id], ['class' => 'text-red']).' > '.$this->AdminView->showComplete($datas->status);
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="content">
                    <div class="card">
                        <div class="card-header">
                            <?= $this->Element('Trip/trip_detail'); ?>

                                <?php if($datas->status === NO_COMPLETE_STATUS){  ?>
                                    <div class="form-group col-xl-12">
                                        <?= $this->Form->control('order_id', ['type' => 'text','id' => 'orderId', 'class' => 'form-control', 'label' => 'Input order to check']); ?>
                                        <?= $this->Form->control('trip_id', ['type' => 'hidden', 'id' => 'trip_id', 'class' => 'form-control', 'value' => $id]); ?>
                                    </div>

                                    <div class="form-group row">

                                        <div class="form-group col-xs-6">
                                            <label for=""><?= __('orders_unchecked') ?></label>
                                            <?= $this->Element('DeliveryTripDetail/check_element', ['id'=> 'not-check-list','datas' => $datas->no_check_orders]); ?>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for=""><?= __('orders_checked') ?></label>
                                            <?= $this->Element('DeliveryTripDetail/check_element', ['id'=> 'checked-list','datas' => $datas->checked_orders]); ?>
                                        </div>
                                    </div>
                                    <?= $this->Form->control('delivery_trip_id',['id' => 'delivery_trip_id','type' => 'hidden', 'value' => $id]); ?>
                                    <div class="col-xl-12 pull-right">
                                        <button class="btn btn-success" id="btnCheckAll"><?= __('check_all') ?></button>
                                    </div>
                                <?php } ?>
                                <?php if($datas->status === NO_COMPLETE_STATUS){  ?>
                                    <div class="form-group row">
                                        <div class="col-md-4 form-group">
                                            <?= $this->Form->control('cbInProcess', ['default' => 0, 'label' => false, 'type' => 'checkbox', 'id' => 'cbAllInProcess', 'templates' => ['inputContainer' => '{{content}}'],]); ?>
                                            <span class="margin-left-lg margin-right-sm" disabled="true"><?= __('select_all') ?></span>
                                        </div>

                                        <div class="col-md-8 form-group">
                                            <a id="btnDeliveryComplete" class="btnConfirm btn btn-success"><?= __('complete') ?></a>
                                            <a id="btnDeliveryFail" class="btnConfirm btn btn-danger"><?= __('failed') ?></a>
                                        </div>
                                    </div>

                                <?php } ?>
                            <ul class="nav nav-tabs mb15" id="priceTab">
                                <li class="active"><a class="nav-link" data-toggle="tab" href="#delivery-in-progress"><?= __('delivery_in_progress') ?> <i class="badge badge-warning"><?= !empty($datas->delivery_orders) ? count($datas->delivery_orders) : 0 ?></i></a></li>
                                <li><a class="nav-link" data-toggle="tab" href="#delivery-completed"><?= __('delivery_completed') ?> <i class="badge badge-success"><?= !empty($datas->delivery_complete_orders) ? count($datas->delivery_complete_orders) : 0 ?></i></a></li>
                                <li><a class="nav-link" data-toggle="tab" href="#delivery-failed"><?= __('delivery_failed') ?> <i class="badge badge-danger"><?= !empty($datas->failed_orders) ? count($datas->failed_orders) : 0 ?></i></a></li>
                                <li><a class="nav-link" data-toggle="tab" href="#exchange"><?= __('exchange') ?> <i class="badge badge-dark"><?= !empty($datas->return_orders) ? count($datas->return_orders) : 0 ?></i></a></li>

                            </ul>
                        </div>
                        <?= $this->Form->create(null) ?>
                        <div class="col-xl-12">
                            <?= $this->Form->submit(__('confirm'), ['class' => 'pull-right btn btn-success']) ?>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="col-xs-12 tab-pane active" id="delivery-in-progress">
                                    <div class="row widget-header-div">
                                        <?= $this->Element('DeliveryTripDetail/mobile_box', ['isDelivery' => true,'tab' => 'delivery-in-progress','datas' => $datas->delivery_orders, 'card' => 'card-warning', 'type' => $orderConstant['delivery_in_progress']]); ?>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="delivery-completed">
                                    <div class="row widget-header-div">
                                        <?= $this->Element('DeliveryTripDetail/mobile_box', ['isDelivery' => true,'tab' => 'delivery-completed', 'datas' => $datas->delivery_complete_orders, 'card' => 'card-success', 'type' => $orderConstant['delivery_completed']]); ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="delivery-failed">
                                    <div class="row widget-header-div">
                                        <?= $this->Element('DeliveryTripDetail/mobile_box', ['isDelivery' => true,'tab' => 'delivery-failed', 'datas' => $datas->failed_orders, 'card' => 'card-danger', 'type' => $orderConstant['stored']]); ?>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="exchange">
                                    <div class="row widget-header-div">
                                        <?= $this->Element('DeliveryTripDetail/mobile_box', ['isDelivery' => true,'tab' => 'exchange', 'datas' => $datas->return_orders, 'card' => 'card-dark', 'type' => $orderConstant['return']]); ?>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-xl-12 pull-right">
                            <?= $this->Form->submit(__('confirm'), ['class' => 'pull-right btn btn-success']) ?>
                        </div>
                        <?= $this->Form->end(); ?>
                    </div>
                </div> <!-- /.content -->

            </div>
        </div>
    </div>
</section>
<?= $this->Element('DeliveryTripDetail/confirm_complete_modal'); ?>
<?= $this->Element('DeliveryTripDetail/confirm_failed_modal'); ?>
<?= $this->Element('DeliveryTripDetail/confirm_delete_modal'); ?>
<?= $this->Html->script(['DeliveryTrip/delivery_trip_details']); ?>
<?= $this->Html->script(['PickupTrip/pickup_trip_details']); ?>