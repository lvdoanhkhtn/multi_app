<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php

                $link = $this->Html->link(__('delivery_trip_management'), ['controller' => 'delivery_trips','action' => 'index'], ['class' => 'text-red']). ' > '.$this->Html->link(__('delivery_trip_details_management'), ['controller' => 'delivery_trip_details','action' => 'view', $id], ['class' => 'text-red']).' > '.$this->AdminView->showComplete($datas->status);
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Element('Trip/trip_detail'); ?>
                        <?php if($datas->status === NO_COMPLETE_STATUS){ ?>
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
                                <?php
                                    echo $this->Form->control('order_code', ['id' => 'order_code','class' => 'checkOrder form-control', 'placeholder' => 'Input Order Code/Shop Code']);
                                    echo $this->Form->control('delivery_trip_id', ['type' => 'hidden', 'value' => $id, 'id' => 'delivery_trip_id']);
                                ?>

                            </div>


                            <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12 mt25">
                                <div class="pull-right">

                                    <?= $this->Html->link('<i class="fa fa-print"></i>'.__('print'), ['action' => 'printOrderDelivery', $id], ['target' => 'blank','escape' => false,'class' => 'btn btn-warning']); ?> 
                                    <?= $this->Html->link('<i class="fa fa-file-excel"></i>'.__('export'), ['action' => 'export', $id], ['escape' => false,'class' => 'btn btn-success']); ?> 
                                    <?= $this->Html->link('<i class="fa fa-times-circle"></i>'.__('cancel'), ['action' => 'index'], ['escape' => false,'class' => 'btn btn-danger']); ?> 

                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <?= $this->Element('Order/delivery_return_order', ['datas' => $datas->delivery_orders]); ?>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script(['DeliveryTrip/delivery_trip_details']); ?>