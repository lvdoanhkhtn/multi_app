<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 10/21/19-->
<!-- * Time: 4:07 PM-->
<!-- */-->

<html>
    <head>
        <meta charset="UTF-8">
        <title><?= __('shipping_bill')?></title>
<?php echo $this->Html->css('print'); ?>
</head>
<body>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
                <h1>
                    <!--                         	<img src="http://static.proship.vn/f/default/images/proship-banner.png"> -->
                </h1>
            </div>
        </div>
        <div class="col-md-12 print-invoice-container">
            <div class="panel-heading">
                <h2 class="text-center">
                    <?= __('deliver_bill')?>
                </h2>
            </div>
            <div class="staff-info">
                <div class="f-left">
                    <p><?= __('employee')?>: <b><?= $trip->employee->user->fullname ?></b></p>
                    <!-- 		          <p><img alt="" src="http://static.proship.vn/uploads/user/2015/10/30/thumbs/100x100_Proship.VN_1446180970.1168.jpg"></p> -->
                    <br />
                </div>
                <div class="f-right text-right">
                    <b><?= __('time_create_trip')?> <?= $this->AdminView->formatDateTime($trip->created); ?></b><br />
                </div>
            </div>
            <table class="table">
                <thead>
                <tr>
                    <th></th>
                    <th><?= __('profile')?></th>
                    <th><?= __('service_pack')?></th>
                    <th><?= __('executive_note')?></th>
                    <th><?= __('total_receiver')?></th>
                    <th style="width: 150px;"><?= __('signature_receiver')?></th>
                    <th><?= __('note_delivery_shipper')?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sum = 0;
                $i = 1;
                foreach ($datas->delivery_orders as $data){
                    $order = $data->order;
                    $sum += $order['total'];
                    ?>
                    <tr>
                        <td style="text-align: center;"><?php echo $i; ?></td>
                        <td style="text-align: left;">
                            <label><?= __('sender')?>: </label> <?= $order->getSenderAddressForPrint()?> <br><b>
                                <label><?= __('receiver')?>: </label>
                                <?= $order->getFullReceiverAddress()?>
                                <div>
                                    <p><label><?= __('code')?>:</label> <?= $order['id'] ?></p>
                                    <p><label><?= __('order_code')?>:</label> <?= $order['orderId']?></p>
                                </div>

                        </td>
                        <td style="text-align: left;"><?= $configs['service_pack'][$order->getServicePack()]; ?></td>
                        <td style="text-align: left;">
                            <p style="display: block; max-width: 300px; word-wrap:break-word; overflow-wrap: break-word;"><label><?= __('merchandise_content')?>: </label> <?= $order->content ?></p>
                            <p style="display: block; max-width: 300px; word-wrap:break-word; overflow-wrap: break-word;"><label><?= __('note')?>: </label>  - <?= $order->note ?> </p>
                        </td>
                        <td>
                            <div class=""><?= $this->AdminView->getCurrencyCam($order->total); ?> </div>

                        </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                    <?php $i++; } ?>
                <tr class="total">
                    <td colspan="4" style="text-align: center;"><?= __('total')?></td>
                    <td style="text-align: center; color: red"><?= $this->AdminView->getCurrencyCam($sum); ?></td>
                    <td colspan="2"></td>
                </tr>
                </tbody>
            </table>

            <table class="table signature">
                <!--tr>
                    <td colspan="3" style="text-align: right;">TP.Hồ Chí Minh ngày  tháng  năm </td>
                </tr-->
                <tr>
                    <td colspan="3"><br /></td>
                </tr>
                <tr>
                    <td><?= __('shipper')?></td>
                    <td><?= __('stocker')?></td>
                    <td><?= __('table_maker')?></td>
                </tr>
                <tr class="normal">
                    <td>(<?= __('sign_full_name')?>)</td>
                    <td>(<?= __('sign_full_name')?>)</td>
                    <td>(<?= __('sign_full_name')?>)</td>
                </tr>
            </table>
        </div>            </div>
</section>
</body>
</html>