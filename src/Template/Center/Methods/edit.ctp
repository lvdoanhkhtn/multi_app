<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = $this->Html->link(__('method_management'), ['action' => 'index'], ['class' => 'text-red']). '<span class="text-red"> > Edit </span>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                echo $this->Element('MethodAction/form');
                ?>

                
            </div>
        </div>
    </div>
</section>
