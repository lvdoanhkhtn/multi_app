<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 1/13/20-->
<!-- * Time: 10:15 AM-->
<!-- */-->

<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid"> 
                <?php
                    $link = $this->Html->link(__('return_trip_management'), ['controller' => 'return_trips', 'action' => 'index'], ['class' => 'text-red']) . ' > ' . $this->Html->link(__('return_trip_details_management'), ['controller' => 'return_trip_details', 'action' => 'view', $id], ['class' => 'text-red']) . ' > ' . $this->AdminView->showComplete($datas->status);
                    echo $this->Element('breadcum', ['link' => $link]);
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Element('Trip/trip_detail'); ?>
                        <?php if ($datas->status === NO_COMPLETE_STATUS) { ?>
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
                                    <?php
                                        echo $this->Form->control('order_code', ['id' => 'order_code_return', 'class' => 'checkOrder form-control', 'placeholder' => 'Input Order Code/Shop Code']);
                                        echo $this->Form->control('return_trip_id', ['type' => 'hidden', 'value' => $id, 'id' => 'return_trip_id']);
                                        echo $this->Form->control('address_id', ['id' => 'address_id','type' => 'hidden', 'value' => !empty($trip->address_id) ? $trip->address_id : ""]);
                                    ?>

                                </div>


                                <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12 mt25">
                                   
                                </div>
                            </div>
                        <?php } ?>

                        <?= $this->Element('Order/delivery_return_order', ['datas' => $datas->return_orders]); ?>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script(['DeliveryTrip/delivery_trip_details']); ?>