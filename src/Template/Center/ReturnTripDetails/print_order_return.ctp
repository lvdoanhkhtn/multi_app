<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 11/8/19-->
<!-- * Time: 3:04 PM-->
<!-- */-->

<html>
<head>
    <meta charset="UTF-8">
    <title><?= __('payback_bill') ?></title>
    <?php echo $this->Html->css('print'); ?>
    <style type="text/css">
        html {
            font-family: sans-serif;
        }

        th {
            text-align: center;
        }

        td {
            text-align: left;
        }

    </style>
</head>
<body>
<section>
    <div class="container">

        <div class="col-xs-12" style="margin-top: 10px">
            <p class="text-center" style="font-weight: bold; font-size: 17px;"><?= __('report_order_return') ?></p>
        </div>
        <div class="col-xs-12" style="margin-top: 10px">
            <p><b><?= __('return_order_at') ?>:</b>  <?= $datas->return_orders[0]->order->getFullSenderAddress("<b>") ?></p>
            <br>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th></th>
                <th><?= __('receiver') ?></th>
                <th><?= __('phone') ?></th>
                <th><?= __('address_receiver') ?></th>
                <th><?= __('note') ?></th>
            </tr>
            <?php $i = 1;
            foreach ($datas->return_orders as $data) {
                $order = $data->order;
                ?>
                <tr>
                    <td><?= $i; ?></td>
                    <td><?= h($order->receiver_name); ?></td>
                    <td><?= h($order->arr_receiver_phone); ?></td>
                    <td><?= h($order->getReceiverAddress()) ?></td>
                    <td><?= h($order->note); ?></td>
                </tr>
                <?php $i++;
            } ?>
            </thead>
            <tbody>


            <tr style="border: 0px" class="si">
                <td colspan="3">
                    <?= __('report_order_return') ?>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    (<?= __('sign_full_name') ?>)
                </td>
                <td colspan="2">
                    <?= __('sender_order') ?>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    (<?= __('sign_full_name') ?>)
                </td>

            </tr>
            </tbody>
        </table>


    </div>

    </div>
    <p style="page-break-after:always;"></p>
</section>
</body>
</html>