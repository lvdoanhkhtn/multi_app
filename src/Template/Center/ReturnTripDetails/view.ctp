<?= $this->Html->css('PickupTrip/pickup_trips'); ?>
<?php
    $currentTabInProgress = "";
    $currentTabCompleted = "";
    $countInProgress =  !empty($datas->return_orders) ? count($datas->return_orders) : 0;
    $countCompleted = !empty($datas->return_complete_orders) ? count($datas->return_complete_orders) : 0;
    if(empty($_GET['tab'])){
        $currentTabInProgress = "active in";
    }else{
        switch($_GET['tab']){
            case 'return-in-progress':
                $currentTabInProgress = "active in";
                if(!empty($_GET['key_word'])){
                    $countInProgress = count($ids);
                }
                break;

            case 'return-completed':
                $currentTabCompleted = "active in";
                if(!empty($_GET['key_word'])){
                    $countCompleted = count($ids);
                }
                break;


        }
    }
//    var_dump($countInProgress);die;
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php

                    $link = $this->Html->link(__('return_trip_management'), ['controller' => 'return_trips', 'action' => 'index'], ['class' => 'text-red']) . ' > ' . $this->Html->link(__('return_trip_details_management'), ['controller' => 'return_trip_details', 'action' => 'index', $id], ['class' => 'text-red']) . ' > ' . $this->AdminView->showComplete($datas->status);
                    echo $this->Element('breadcum', ['link' => $link]);
                    echo $this->Form->control('trip_id', ['type' => 'hidden', 'id' => 'trip_id', 'class' => 'form-control', 'value' => $id]);
                ?>

                <div class="content">
                    <div class="card">
                        <div class="card-header">
                            <?= $this->Element('Trip/trip_detail'); ?>
                            
                            <div class="form-group col-xs-12">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <ul class="nav nav-tabs mb15" id="priceTab">
                                                <li class="<?= $currentTabInProgress ?>">
                                                    <a class="nav-link" data-toggle="tab" href="#return-in-progress"><?= __('return_in_progress') ?>
                                                        <i class="badge badge-warning"><?= $countInProgress ?></i>
                                                    </a>
                                                </li>
                                                <li class="<?= $currentTabCompleted ?>">
                                                    <a class="nav-link" data-toggle="tab" href="#return-completed"><?= __('return_complete') ?>
                                                        <i class="badge badge-success"><?= $countCompleted ?></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-1 margin-top-sm">
                                            <?php if(count($newOrders) > 0 && empty($datas->status)){ ?>
                                                <?= $this->Form->create(null, ['action' => 'refresh/'.$id, 'type' => 'post']); ?>
                                                    <button type="submit" class="btn btn-info"><i class="fa fa-sync"></i> Refresh</button>
                                                <?= $this->Form->end(); ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>

                            <div class="form-group col-xs-12 col-md-12">
                                <div class="col-md-4 col-xs-12">
                                    <?= $this->Form->control('cbInProcess', ['default' => 0, 'label' => false, 'type' => 'checkbox', 'id' => 'cbAllInProcess', 'templates' => ['inputContainer' => '{{content}}'],]); ?>
                                    <span class="margin-left-lg margin-right-sm" disabled="true">Select All</span>
                                </div>

                                <div class="col-md-8 col-xs-12">
                                    <a id="btnReturnComplete" class="btnConfirm btn btn-success">Completed</a>
                                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#confirm-failed">Failed</a>
                                    <?= $this->Html->link('<i class="fa fa-print"></i>'.__('print'), ['action' => 'printOrderReturn',$id], ['class' => 'btn btn-warning', 'escape' => false, 'target' => 'blank']); ?>
                                </div>
                            </div>

                        </div>


                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane <?= $currentTabInProgress ?>" id="return-in-progress">
                                <div class="row widget-header-div">
                                    <?= $this->Element('DeliveryTripDetail/mobile_box', ['tab' => 'return-in-progress', 'isPickup' => true, 'datas' => $datas->return_orders, 'card' => 'card-warning', 'type' => $orderConstant['pickup_in_progress']]); ?>
                                </div>
                            </div>

                            <div class="tab-pane fade <?= $currentTabCompleted ?>" id="return-completed">
                                <div class="row widget-header-div">
                                    <?= $this->Element('DeliveryTripDetail/mobile_box', ['tab' => 'return-completed', 'isPickup' => true, 'datas' => $datas->return_complete_orders, 'card' => 'card-success', 'type' => $orderConstant['stored']]); ?>

                                </div>
                            </div>
                            

                        </div>
                    </div>

                </div>
            </div> <!-- /.content -->

        </div>
    </div>
    </div>
</section>
<?= $this->Html->script(['PickupTrip/pickup_trip_details']); ?>
<?= $this->Element('PickupTripDetail/pickup_confirm_failed_modal', ['table' => 'return_trip_reasons','class' => 'btnConfirm']); ?>

<?= $this->Form->control(null, ['type' => 'hidden','id' => 'base_url', 'value' => $this->Url->build(['action' => 'detail'])]) ?>


