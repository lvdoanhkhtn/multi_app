<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = $this->Html->link(__('collect_money_session'), ['controller' => 'accountants', 'action' => 'collectMoneySession'], ['class' => 'text-red']) . ' > <h3 class="box-title text-red">' . __('collect_detail') . '</h3>';
                echo $this->Element('breadcum', ['link' => $link]);
                echo $this->Form->control('pickupTripId', ['id' => 'pickupTripId', 'type' => 'hidden', 'value' => $pickupTripId]);
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?php
                        $data = $pickupTrip->pickup_stored_orders[0];
                        $totalCollect = $data->total_postage + $data->total_collection_fee + $data->total_protect_fee;
                        ?>
                        <div class="row form-group">
                            <div class="form-group">
                                <label for=""><?= __('total_collect') ?></label>
                                <input type="text" class="form-control" disabled placeholder="<?= $this->AdminView->getCustomCurrencyCam($totalCollect) ?>">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="form-group">
                                <label for=""><?= __('total_collect') ?></label>
                                <input type="text" class="form-control" disabled placeholder="<?= $this->AdminView->getCustomCurrencyUsd($totalCollect) ?>">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="pull-right margin-right-lg">
                                <a href="#" class="btn btn-success btnHanding" id="btnCollect"><i class="fa fa-money-bill-alt"></i>Collect money</a>
                            </div>
                        </div>

                        <div class="box">
                            <?= $this->Element('Order/orders', ['isCollectDetail' => true, 'isPickup' => true]) ?>
                        </div>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script(['Order/order']); ?>