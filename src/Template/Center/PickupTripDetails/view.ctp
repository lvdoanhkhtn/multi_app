<?php
    $currentTabInProgress = "";
    $currentTabCompleted = "";
    $currentTabCanceled= "";
    $countInProgress =  !empty($datas->pickup_orders[0]['count']) ? $datas->pickup_orders[0]['count'] : 0;
    $countCompleted = !empty($datas->pickup_completed_orders[0]['count']) ? $datas->pickup_completed_orders[0]['count'] : 0;
    $countStored = !empty($datas->pickup_stored_orders[0]['count']) ? $datas->pickup_stored_orders[0]['count'] : 0;
    $countCanceled = !empty($datas->cancel_orders[0]['count']) ? $datas->cancel_orders[0]['count'] : 0;
    if(empty($_GET['tab'])){
        $currentTabInProgress = "active in";
    }else{
        switch($_GET['tab']){
            case 'pickup-in-progress':
                $currentTabInProgress = "active in";
                if(!empty($_GET['key_word'])){
                    $countInProgress = count($ids);
                }
                break;

            case 'pickup-completed':
                $currentTabCompleted = "active in";
                if(!empty($_GET['key_word'])){
                    $countCompleted = count($ids);
                }
                break;

            case 'pickup-stored':
                $currentTabCompleted = "active in";
                if(!empty($_GET['key_word'])){
                    $countStored = count($ids);
                }
                break;

            case 'pickup-canceled':
                $currentTabCanceled = "active in";
                if(!empty($_GET['key_word'])){
                    $countCanceled = count($ids);
                }
                break;

        }
    }
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php

                $link = $this->Html->link(__('pickup_trip_management'), ['controller' => 'pickup_trips', 'action' => 'index'], ['class' => 'text-red']) . ' > <h3 class="box-title text-red">' . __('pickup_trip_details_management') . '</h3>' . ' > ' . $this->AdminView->showComplete($datas->status);
                echo $this->Element('breadcum', ['link' => $link]);
                ?>

                <div class="content">
                    <div class="card">
                        <div class="card-header">
                            <?= $this->Element('Trip/trip_detail'); ?>

                            <?php if ($datas->status === NO_COMPLETE_STATUS) {  ?>

                                <div class="form-group col-xl-12">
                                    <?= $this->Form->control('order_code', ['type' => 'text', 'id' => 'order_code', 'class' => 'checkOrder form-control', 'label' => 'Input order to check']); ?>
                                    <?= $this->Form->control('trip_id', ['type' => 'hidden', 'id' => 'trip_id', 'class' => 'form-control', 'value' => $id]); ?>
                                </div>

                            <?php } ?>


                            <div class="form-group col-xs-12">
                                <div class="row">
                                    <div class="col-md-11 form-group no-padding">
                                        <ul class="nav nav-tabs mb15" id="priceTab">
                                            <li class="<?= $currentTabInProgress ?>">
                                                <a id="pickup_in_progress" class="nav-link" data-toggle="tab" href="#pickup-in-progress"><?= __('pickup_in_progress') ?> <i class="badge badge-warning"> <?= $countInProgress ?></i></a>
                                            </li>
                                            <li class="<?= $currentTabCompleted ?>">
                                                <a id="pickup_completed" class="nav-link" data-toggle="tab" href="#pickup-completed"><?= __('pickup_completed') ?> <i class="badge badge-success"> <?= $countCompleted ?></i></a>
                                            </li>
                                            <li class="<?= $currentTabCompleted ?>">
                                                <a id="stored" class="nav-link" data-toggle="tab" href="#pickup-stored"><?= __('stored') ?> <i class="badge badge-success"> <?= $countStored ?></i></a>
                                            </li>
                                            <li class="<?= $currentTabCanceled ?>">
                                                <a class="nav-link" data-toggle="tab" href="#pickup-canceled"><?= __('pickup_canceled') ?> <i class="badge badge-dark"> <?= $countCanceled ?></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <?php if (count($newOrders) > 0 && empty($datas->status)) { ?>
                                            <?= $this->Form->create(null, ['action' => 'refresh/' . $id, 'type' => 'post']); ?>
                                            <button type="submit" class="btn btn-info"><i class="fa fa-sync"></i> Refresh</button>
                                            <?= $this->Form->end(); ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="no-padding col-md-6 table-responsive">
                                        <table class="tb-trip table table-striped table-sm">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th class="text-top"><?= __('order_code') ?></th>
                                                    <th class="text-top"><?= __('receiver') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <?php if ($datas->status === NO_COMPLETE_STATUS) {  ?>

                                <div class="form-group row">
                                    <div class="col-md-4 form-group">
                                        <?= $this->Form->control('cbInProcess', ['default' => 0, 'label' => false, 'type' => 'checkbox', 'id' => 'cbAllInProcess', 'templates' => ['inputContainer' => '{{content}}'],]); ?>
                                        <span class="margin-left-lg margin-right-sm" disabled="true"><?= __('select_all') ?></span>
                                    </div>

                                    <div class="col-md-8 form-group">
                                        <a id="btnComplete" class="btnConfirm btn btn-success"><?= __('complete') ?></a>
                                        <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#confirm-failed"><?= __('failed') ?></a>
                                        <a id="btnCancel" class="btnConfirm btn btn-dark"><?= __('canceled') ?></a>
                                        <a id="btnPrint100_60" class="btn btn-warning"><i class="fa fa-print"></i> <?= __('print') ?></a>

                                    </div>
                                </div>

                            <?php } ?>


                        </div>


                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane <?= $currentTabInProgress ?>" id="pickup-in-progress">
                                    <div class="row widget-header-div">
                                        <?= $this->Element('DeliveryTripDetail/mobile_box', ['tab' => 'pickup-in-progress', 'isPickup' => true, 'datas' => $pickupOrders, 'card' => 'card-warning', 'type' => $orderConstant['pickup_in_progress']]); ?>
                                        <div class="col-xs-12 right">
                                            <?= $this->Element('paging'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade <?= $currentTabCompleted ?>" id="pickup-completed">
                                    <div class="row widget-header-div">
                                        <?= $this->Element('DeliveryTripDetail/mobile_box', ['tab' => 'pickup-completed', 'isPickup' => false, 'datas' => $pickupCompletedOrders, 'card' => 'card-success', 'type' => $orderConstant['stored']]); ?>
                                    </div>
                                </div>

                                <div class="tab-pane fade <?= $currentTabCompleted ?>" id="pickup-stored">
                                    <!-- <div class="row form-group">
                                        <div class="col-lg-12 pull-left">
                                            <a id="btnCollectFee" class="btnConfirm btn btn-success"><?= __('collect_fee') ?></a>
                                        </div>

                                    </div> -->
                                    <div class="row widget-header-div">
                                        <?= $this->Element('DeliveryTripDetail/mobile_box', ['tab' => 'pickup-stored', 'isPickup' => false, 'datas' => $pickupStoredOrders, 'card' => 'card-success', 'type' => $orderConstant['stored']]); ?>
                                    </div>
                                </div>

                                <div class="tab-pane fade <?= $currentTabCanceled ?>" id="pickup-canceled">
                                    <div class="row widget-header-div">
                                        <?= $this->Element('DeliveryTripDetail/mobile_box', ['tab' => 'pickup-canceled', 'isPickup' => false, 'datas' => $pickupCancelOrders, 'card' => 'card-danger', 'type' => $orderConstant['cancel']]); ?>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div> <!-- /.content -->

            </div>
        </div>
    </div>
</section>
<?= $this->Html->script(['PickupTrip/pickup_trip_details.js?v=20191212']); ?>
<?= $this->Element('PickupTripDetail/pickup_confirm_failed_modal', ['table' => 'pickup_trip_reasons', 'button' => 'btnFail', 'class' => 'btnConfirm']); ?>
<?= $this->Form->control(null, ['type' => 'hidden', 'id' => 'base_url', 'value' => $this->Url->build(['action' => 'detail'])]) ?>
<?= $this->Form->control(null, ['type' => 'hidden', 'id' => 'role_id', 'value' => $user['role_id']]) ?>