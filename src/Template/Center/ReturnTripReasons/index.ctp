<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = '<h3 class="box-title text-red">'.__('return_trip_reason_management').'</h3>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Element('Country/search', ['datas' => $datas, 'controller' => 'return_trip_reasons']); ?>

                    </div><!-- /.box-body -->

                </div>
            </div>
        </div>
    </div>
</section>
