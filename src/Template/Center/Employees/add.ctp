<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = $this->Html->link(__('employee_management'), ['action' => 'index'], ['class' => 'text-red']). '<span class="text-red"> > Add </span>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">

                    <?= $this->Form->create(null, ['class' => 'validateForm']); ?>

                    <?= $this->Element('Employee/form'); ?>

                    <div class="row">
                        <div class="col-xs-6">
                        </div>
                        <div class="col-xs-6 text-right">
                            <button type="submit" class="btn btn-success"><?= __('create') ?></button>
                            <?= $this->Html->link(__('cancel'), ['action' => 'index'], ['class' => 'btn btn-danger']); ?>
                        </div>

                    </div>

                    <?= $this->Form->end(); ?>

                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
