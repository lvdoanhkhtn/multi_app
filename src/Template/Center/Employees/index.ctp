<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = '<h3 class="box-title text-red">'.__('employee_management').'</h3>';
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'get']); ?>

                        <div class="row form-group">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <?php echo $this->Form->control('key_word',array('value' => !empty($_GET['key_word']) ? $_GET['key_word'] : "",'type' => 'text', 'class' => 'form-control', 'placeholder' => __('employee_key_word'), 'label' => __('key_word')))?>
                            </div>
                        </div>

                        <div class="row form-group">

                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                    <?= $this->Element('Calendar/created_calendar'); ?>
                                </div>
                                <?= $this->Form->control('',['type' => 'hidden','id' => 'temp_complete_date', 'value' => !empty($_GET['complete_date']) ? $_GET['complete_date'] : ""]) ?>

                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                    <?php echo $this->Form->control('active', array('default' => (isset($_GET['active']) && strlen($_GET['active']) > 0) ? $_GET['active'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $configs['status'], 'label' => __('status')))?>
                                </div>

                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                    <?php echo $this->Form->control('role_id', array('default' => !empty($_GET['role_id']) ? $_GET['role_id'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $configs['roles'], 'label' => __('roles')))?>
                                </div>

                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                    <?php echo $this->Form->control('ware_house_id', array('default' => !empty($_GET['ware_house_id']) ? $_GET['ware_house_id'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $configs['ware_houses'], 'label' => __('ware_houses')))?>
                                </div>


                        </div>

                        <?= $this->Element('CommonForm/search_buttons'); ?>

                        <div class="row form-group">
                           
                        </div>



                        <?= $this->Form->end(); ?>
                        <?php if($this->Paginator->param('pageCount') > 1){ ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">
                                <thead class="thead-dark">
                                    <tr>

                                        <th class="text-top"><?= __('status') ?></th>
                                        <th class="text-top"><?= __('user_name') ?></th>
                                        <th class="text-top"><?= __('date_created') ?></th>
                                        <th class="text-top"><?= __('full_name') ?></th>
                                        <th class="text-top"><?= __('email') ?></th>
                                        <th class="text-top"><?= __('phone') ?></th>
                                        <th class="text-top"><?= __('address') ?></th>
                                        <th class="text-top"><?= __('roles') ?></th>
                                        <th class="text-top"><?= __('ware_houses') ?></th>
                                        <th class="text-top mw100"><?= __('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($employees as $employee){ ?>
                                    <tr>
                                        <td><?= $this->AdminView->showActive($employee->user->active); ?></td>
                                        <td><?= h($employee->user->username); ?></td>
                                        <td><?= $employee->getCreated(); ?></td>
                                        <td><?= h($employee->user->fullname); ?></td>
                                        <td><?= h($employee->user->email); ?></td>
                                        <td><?= h($employee->user->phone); ?></td>
                                        <td><?= h($employee->getAddress()); ?></td>
                                        <td><?= h($employee->name); ?></td>
                                        <td><?= h($employee->ware_house->name); ?></td>
                                        <td>
                                            <?php
                                                if($employee->user->role_id != SHOP_EMPLOYEE){
                                                    echo $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $employee->id, $employee->user_id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']); 
                                                }  
                                            ?>
                                            <?= $this->Html->link('<i class="fa fa-exchange-alt"></i>', ['controller' => 'accounts','action' => 'adminChangePassword', $employee->user_id, 1], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Change password', 'class' => 'btn btn-outline-info']); ?>

                                        </td>
                                    </tr>

                                   <?php } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->Html->script(['Payment/payment']); ?>