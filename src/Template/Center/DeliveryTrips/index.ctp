<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = '<h3 class="box-title text-red">'.__('delivery_trip_management').'</h3>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'get']); ?>

                        <div class="row form-group">

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <?php echo $this->Form->control('id',array('value' => !empty($_GET['id']) ? $_GET['id'] : "",'type' => 'text', 'class' => 'form-control', 'placeholder' => __('123456'), 'label' => __('trip_code')))?>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <?php echo $this->Form->control('orderId',array('value' => !empty($_GET['orderId']) ? $_GET['orderId'] : "",'type' => 'text', 'class' => 'form-control', 'placeholder' => __('CBS123456'), 'label' => __('order_code')))?>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <?php echo $this->Form->control('employee', array('default' => !empty($_GET['employee']) ? $_GET['employee'] : '', 'class' => 'form-control','placeholder' => __('employee_in_trip_key_word'), 'label' => __('employee_in_trip')))?>
                            </div>
                        </div>


                        <div class="row form-group">

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <?php echo $this->Form->control('status', array('default' => (isset($_GET['status']) && strlen($_GET['status'])) ? $_GET['status'] : '' , 'type' => 'select', 'empty' => __('select'), 'options' => $configs['delivery_status'],'class' => 'form-control select2', 'label' => __('status')))?>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Element('Calendar/created_calendar'); ?>
                            </div>


                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Element('Calendar/completed_calendar'); ?>
                            </div>
                        </div>

                        <div class="pull-right">
                            <?= $this->Form->button('<i class="fa fa-search"></i>'.__('search'), array('class' => 'btn btn-info','type'=>'submit')); ?>
                            <a href="" class="btn btn-warning" data-toggle="modal" data-target="#delivery-trip-create"><i class="fa fa-plus-circle"></i><?= __('create') ?></a>
                            <?= $this->Html->link('<i class="fa fa-file-excel"></i>'.__('export'), ['action' => 'export'], ['escape' => false,'class' => 'btn btn-success']); ?>
                        </div>

                        <div class="row form-group">

                        </div>



                        <?= $this->Form->end(); ?>
                        <?php if($this->Paginator->param('pageCount') > 1){ ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">
                                <thead class="thead-dark">
                                <tr>

                                    <th class="text-top"><?= __('trip_code') ?></th>
                                    <th class="text-top"><?= __('date_created') ?></th>
                                    <th class="text-top"><?= __('date_completed') ?></th>
                                    <th class="text-top"><?= __('employee_code') ?></th>
                                    <th class="text-top"><?= __('employee_name') ?></th>
                                    <th class="text-top"><?= __('employee_phone') ?></th>
                                    <th class="text-top"><?= __('orders_completed') ?></th>
                                    <th class="text-top"><?= __('status') ?></th>
                                    <th class="text-top mw100"><?= __('action') ?></th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($datas as $data){ ?>
                                    <tr>
                                        <td><?= h($data->id); ?></td>
                                        <td><?= $this->AdminView->formatDateTime($data->created); ?></td>
                                        <td><?= !empty($data->status === COMPLETE_STATUS) ? $this->AdminView->formatDateTime($data->modified) : ''; ?></td>
                                        <td><?= h($data->employee->employee_id); ?></td>
                                        <td><?= h($data->employee->user->fullname); ?></td>
                                        <td><?= h($data->employee->user->phone); ?></td>
                                        <td><?= $data->getTotalComplete() ?></td>
                                        <td><?= $this->AdminView->showComplete($data->status); ?></td>
                                        <td>
                                            <?= empty($data->status === COMPLETE_STATUS)  ? $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'delivery_trip_details','action' => 'index', $data->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']) : ''; ?>
                                            <?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'delivery_trip_details','action' => 'view', $data->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'View', 'class' => 'btn btn-outline-info']); ?>
                                            <a href="" data-id="<?= $data->id ?>" class="btnChangeShipper btn btn-outline-info" data-toggle="modal" data-target="#change-shipper"><i class="fa fa-exchange-alt"></i></a>
                                            <?php
                                            if($data->status === NO_COMPLETE_STATUS && $data->countTotalComplete() === 0){
                                                echo $this->Form->postLink(
                                                    "<i class='fa fa-trash-alt'></i>", // first
                                                    ['action' => 'delete', $data->id],  // second
                                                    ['escape' => false, 'title' => 'Delete', 'class' => 'btn btn-outline-danger', 'confirm' => __('delete_confirm')] // third
                                                );
                                            }
                                            ?>
                                        </td>
                                    </tr>

                                <?php } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>
                        <?php
                            echo $this->Element('Trip/add_modal', ['text' => __('create_delivery_trip')]);
                            echo $this->Element('Modal/change_shipper');
                        ?>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script('Trip/trips'); ?>
