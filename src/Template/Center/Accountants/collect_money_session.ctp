<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 11/11/19-->
<!-- * Time: 10:42 AM-->
<!-- */-->
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">
                <?php
                $link = $this->Html->link(__('delivery_trip_management'), ['controller' => 'delivery_trips', 'action' => 'index'], ['class' => 'text-red']) . '<span class="text-red"> >' . __('collect_money_session_management') . '</span>';
                echo $this->Element('breadcum', ['link' => $link]);
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'GET']) ?>

                        <div class="row form-group">


                            <div class="col-md-4 col-sm-6 col-12">
                                <?php echo $this->Form->control('employee_id', array('default' => !empty($_GET['employee_id']) ? $_GET['employee_id'] : '', 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $shippers, 'label' => __('shipper'))) ?>
                            </div>

                            <div class="col-md-4 col-sm-6 col-12">
                                <?= $this->Element('Calendar/created_calendar'); ?>
                            </div>


                            <div class="col-md-4 col-sm-6 col-12">
                                <?= $this->Element('Calendar/completed_calendar'); ?>
                            </div>

                        </div>


                        <div class="row form-group">
                            <div class="pull-right margin-right-lg">
                                <?= $this->Form->button('<i class="fa fa-search"></i>'.__('search'), array('class' => 'btn btn-info','type'=>'submit')); ?>
                                <?= $this->Html->link('<i class="fa fa-file-excel"></i>'.__('export'), ['action' => 'export'], ['escape' => false,'class' => 'btn btn-success']); ?>
                            </div>
                        </div>

                        <?= $this->Form->end(); ?>



                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">

                                <thead class="thead-dark">
                                    <tr>
                                        <th class="table-10"><?= __('trip_code') ?></th>
                                        <th class="table-10"><?= __('date_created') ?></th>
                                        <th class="table-10"><?= __('date_updated') ?></th>
                                        <th class="table-30"><?= __('employee_code') ?></th>
                                        <th class="table-30"><?= __('employee_name') ?></th>
                                        <th class="table-10"><?= __('total_orders') ?></th>
                                        <th class="table-10"><?= __('total_cod') ?></th>
                                        <th class="table-10"><?= __('total_fee') ?></th>
                                        <th class="table-10"><?= __('sub_fee') ?></th>
                                        <th class="table-20"><?= __('total_collect') ?></th>
                                        <th class="table-20"><?= __('action') ?></th>

                                    </tr>
                                <tbody>
                                    <?php
                                    foreach ($pickupTrips as $pickupTrip) {
                                        echo $this->Element('Accountant/collect_money_item', ['session' => $pickupTrip]);
                                    }
                                    foreach ($deliveryTrips as $deliveryTrip) {
                                        echo $this->Element('Accountant/collect_money_item', ['session' => $deliveryTrip]);
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>