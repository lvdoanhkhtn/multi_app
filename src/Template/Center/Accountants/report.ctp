<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 11/11/19-->
<!-- * Time: 10:42 AM-->
<!-- */-->
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">
                <?php
                $link = $this->Html->link(__('delivery_trip_management'), ['controller' => 'delivery_trips', 'action' => 'index'], ['class' => 'text-red']) . '<span class="text-red"> >' . __('collect_money_session_management') . '</span>';
                echo $this->Element('breadcum', ['link' => $link]);
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'GET']) ?>

                        <div class="row form-group">


                            <div class="col-sm-3 col-12">
                                <?php echo $this->Form->control('user_id', array('default' => !empty($_GET['user_id']) ? $_GET['user_id'] : '', 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $accountants, 'label' => __('accountant'))) ?>
                            </div>

                            <div class="col-sm-3 col-12">
                                <?php echo $this->Form->control('employee_id', array('default' => !empty($_GET['employee_id']) ? $_GET['employee_id'] : '', 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $shippers, 'label' => __('shipper'))) ?>
                            </div>

                            <div class="col-sm-3 col-12">
                                <?php echo $this->Form->control('order_code', array('default' => !empty($_GET['order_code']) ? $_GET['order_code'] : '', 'type' => 'text', 'class' => 'form-control', 'label' => __('order_code'))) ?>
                            </div>

                            <div class="col-sm-3 col-12">
                                <?php echo $this->Form->control('delivery_trip_id', array('default' => !empty($_GET['delivery_trip_id']) ? $_GET['delivery_trip_id'] : '', 'type' => 'text', 'class' => 'form-control', 'label' => __('delivery_trip'))) ?>
                            </div>

                            <div class="col-sm-3 col-12">
                                <?= $this->Element('Calendar/created_calendar'); ?>
                            </div>

                        </div>


                        <div class="row form-group">
                            <div class="pull-right margin-right-lg">
                                <?= $this->Form->button('<i class="fa fa-search"></i>' . __('search'), array('class' => 'btn btn-info', 'type' => 'submit')); ?>
                                <?= $this->Html->link('<i class="fa fa-file-excel"></i>' . __('export'), ['action' => 'exportReport'], ['escape' => false, 'class' => 'btn btn-success']); ?>
                            </div>
                        </div>

                        <?= $this->Form->end(); ?>



                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">

                                <thead class="thead-dark">
                                    <tr>
                                        <th class="table-10"></th>
                                        <th class="table-10"><?= __('money_collector') ?></th>
                                        <th class="table-10"><?= __('shipper') ?></th>
                                        <th class="table-10"><?= __('order_code') ?></th>
                                        <th class="table-10"><?= __('type') ?></th>
                                        <th class="table-10"><?= __('trip_code') ?></th>
                                        <th class="table-10"><?= __('total_cod') ?></th>
                                        <th class="table-10"><?= __('total_fee') ?></th>
                                        <th class="table-10"><?= __('sub_fee') ?></th>
                                        <th class="table-10"><?= __('total_collect') ?></th>
                                        <th class="table-10"><?= __('collect_date') ?></th>

                                    </tr>
                                <tbody>
                                    <?php $step = 1;
                                    foreach ($datas as $data) {
                                        $order = $data->order;
                                        if (empty($order->complete_delivery_trip_details[0]) && empty($order->complete_pickup_trip_details[0])) {
                                            continue;
                                        }
                                        echo $this->Element('Accountant/report_item', ['step' => $step, 'data' => $data]);
                                    } ?>
                                </tbody>
                            </table>
                            <?= $this->Element('paging'); ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>