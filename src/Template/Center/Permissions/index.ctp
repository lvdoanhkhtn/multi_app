<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = '<h3 class="box-title text-red">'.__('permission_management').'</h3>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'get']); ?>

                        <div class="pull-right">
                            <?= $this->Html->link('<i class="fa plus-circle"></i>'.__('create'), ['action' => 'add'],['id' => 'btnPrint','escape' => false,'class' => 'btn btn-warning']); ?>
                        </div>

                        <div class="row form-group">

                        </div>



                        <?= $this->Form->end(); ?>
                        <?php if($this->Paginator->param('pageCount') > 1){ ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">
                                <thead class="thead-dark">
                                <tr>

                                    <th class="text-top"><?= __('id') ?></th>
                                    <th class="text-top"><?= __('role_name') ?></th>
                                    <th class="text-top"><?= __('list_action') ?></th>
                                    <th class="text-top"><?= __('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($datas as $data){ ?>
                                    <tr>
                                        <td><?= h($data['id']); ?></td>
                                        <td><?= h($data['role']['name']); ?></td>
                                        <td><?= h($data['list_action']); ?></td>
                                        <td>
                                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $data['id']], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']); ?>
                                        </td>
                                    </tr>

                                <?php } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
