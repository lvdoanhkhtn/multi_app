<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link =
                    $this->Html->link(__('permission_management'), ['controller' => 'methods','action' => 'index'], ['class' => 'text-red']).' > '
                    .'<span class="text-red">'.__('edit').'</span>';

                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <?= $this->Form->create(null, ['class' => 'validateForm']); ?>
                    <div class="row">
                        <div class="form-group col-xl-3 col-lg-3 col-md-3 col-xs-12">
                            <?= $this->Form->control('role_id', ['value' => !empty($data['role_id']) ? $data['role_id'] : '','class' => 'required form-control select2', 'options' => $roles, 'data-name' => __('role'), 'required' => false, 'placeholder' => __('name'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('role').'(*)']);  ?>
                        </div>
                        <div class="form-group col-xl-9 col-lg-9 col-md-9 col-xs-12">
                            <?= $this->Form->control('action_id', ['value' => !empty($data['action_id']) ? $data['action_id'] : '','multiple' => 'multiple','class' => 'required form-control select2 multi','data-name' => __('action'), 'required' => false,'placeholder' => __('action'), 'templates' => ['inputContainer' => '{{content}}'], 'label' => __('action').'(*)']);  ?>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-6 material-switch pull-left">
                        </div>
                        <div class="col-xs-6 text-right">
                            <button type="submit" class="btn btn-success"><?= (isset($data)) ? __('update') : __('create') ?></button>
                            <?= $this->Html->link(__('cancel'), ['action' => 'index'], ['class' => 'btn btn-danger']); ?>
                        </div>
                    </div>
                    <?= $this->Form->end(); ?>

                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
