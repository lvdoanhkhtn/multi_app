<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = '<h3 class="box-title text-red">'.__('return_trip_management').'</h3>';
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>
                <?= $this->Element('Trip/trips', ['isReturnTrip' => true]) ?>

            </div>
        </div>
    </div>
</section>
<?= $this->Html->script('Trip/trips'); ?>
