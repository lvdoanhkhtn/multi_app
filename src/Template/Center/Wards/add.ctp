<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                 $link = $this->AdminView->getBreadCum([
                        'parentController' => 'districts',
                        'parentId' => $parentId,
                        'id' => $id,
                        'name' => $name,
                        'parentLabel' => 'district_management',
                        'label' => 'ward_management',
                        'isChange' => true,
                        'action' => 'add'
                    ]);
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <?= $this->Form->create(null, ['class' => 'validateForm']); ?>
                        <?= $this->Element('Country/form', ['id' => $id]); ?>
                    <?= $this->Form->end(); ?>

                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
