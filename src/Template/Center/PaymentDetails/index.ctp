<?= $this->Html->css('iCheck/flat/blue', ['block' => 'css']); ?>
<?php
    $bankAccount = !empty($payment->customer->bank_accounts[0]) ? $payment->customer->bank_accounts[0] : [];
    $sumPayment = $excel->getSumPayment($payment->payment_details);
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = $this->Html->link(__('payment_history'), ['controller' => 'payments', 'action' => 'index'], ['class' => 'text-red']).'<span class="text-red"> > Payment Detail</span>'.' > '.$this->AdminView->showComplete($payment->status);
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">

                    <div class="row form-group">
                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 col-12">
                            <label for=""><?= __('customer_name') ?></label>
                            <label type="text" class="form-control" ><?= $payment->customer->user->fullname ?></label>
                        </div>
                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 col-12">
                            <label for=""><?= __('phone') ?></label>
                            <label type="text" class="form-control" ><?= $payment->customer->user->phone ?></label>
                        </div>
                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 col-12">
                            <label for=""><?= __('email') ?></label>
                            <label type="text" class="form-control" ><?= $payment->customer->user->email ?></label>

                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 col-12">
                            <label for=""><?= __('bank_name') ?></label>
                            <label type="text" class="form-control" ><?= !empty($bankAccount->bank->name) ?  $bankAccount->bank->name : ''?></label>

                        </div>
                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 col-12">
                            <label for=""><?= __('bank_account') ?></label>
                            <label type="text" class="form-control" ><?= !empty($bankAccount->account_number) ?  $bankAccount->account_number : ''?></label>


                        </div>
                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 col-12">
                            <label for=""><?= __('card_holder') ?></label>
                            <label type="text" class="form-control" ><?= !empty($bankAccount->account_holder) ?  $bankAccount->account_holder : ''?></label>

                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 col-12">
                            <label for=""><?= __('total_cod') ?></label>
                            <label type="text" class="form-control" ><?= $this->AdminView->getCurrencyCamForPayment($sumPayment['total_cod']) ?></label>

                        </div>
                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 col-12">
                            <label for=""><?= __('total_fee') ?></label>
                            <label type="text" class="form-control" ><?= $this->AdminView->getCurrencyCamForPayment($sumPayment['total_fee']) ?></label>

                        </div>
                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-xs-12 col-12">
                            <label for=""><?= __('total_amount') ?></label>
                            <label type="text" class="form-control" ><?= $this->AdminView->getCurrencyCamForPayment($sumPayment['total_amount']) ?></label>

                        </div>
                    </div>

                    <div class="col-xs-12 form-group text-right">
                        <div class="col-xs-2 text-left no-padding-left">
                            <?php if(empty($payment->status)){ ?>
                                <?= $this->Form->create('paid', ['action' => 'paid/'.$id]); ?>
                                    <button id="btnPaid" class="btn btn-info"><i class="fa fa-credit-card"></i><?= __('paid') ?></button>
                                <?= $this->Form->end(); ?>
                            <?php } ?>
                        </div>
                        <div class="col-xs-10 no-padding-right">
                            <?= $this->Html->link('<i class="fa fa-print"></i>'.__('print'), ['controller' => 'payment_details', 'action' => 'printPayment', $id], ['escape' => false,'class' => 'btn btn-warning']); ?>
                            <?= $this->Html->link('<span class="fa fa-file-excel"></span>'.__('export'), ['action' => 'export', $id], ['escape' => false,'class' => 'btn btn-success']); ?>
                            <button id="btnRemove" class="btnHanding btn btn-danger"><i class="fa fa-trash-alt"></i><?= __('remove') ?></button>
                        </div>
                    </div>

                    <div class="form-group col-md-12 table-responsive">

                        <?php if($this->Paginator->param('pageCount') > 1){ ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>
                        <?= $this->Element('PaymentDetail/table', ['paymentDetails' => $paymentDetails, 'isIndex' => true]); ?>
                        <?= $this->Element('paging') ?>

                    </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

<?= $this->Html->script(['Order/order']); ?>