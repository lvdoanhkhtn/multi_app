<html>
<head>
    <meta charset="UTF-8">
    <title><?= __('payment') ?></title>
    <?php echo $this->Html->css('print'); ?>
</head>
<body>
<section>
    <div class="container">
        <div class="col-xs-12" style="margin-top: 10px">
            <p class="text-center" style="font-weight: bold; font-size: 17px;">
                <?= __('payment_charge_costs_deposits') ?>
                <br>
                <?= __('day') ?> :
                <?= (!empty($payment->status)) ? $payment->getModified() :  date("d/m/Y H:i:s"); ?>
            </p>
            <br>
        </div>
        <div class="col-xs-12">
            <label><?= __('customer_info') ?> : </label>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-2"><?= __('phone_num') ?> :</div>
            <div class="col-xs-10"><?= $payment->customer->user->phone; ?></div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-2"><?= __('customer') ?> </div>
            <div class="col-xs-10"><?= $payment->customer->user->fullname; ?></div>
        </div>
        <div class="col-xs-12 form-group">
            <div class="col-xs-2"><?= __('addr') ?></div>
            <div class="col-xs-10">
                <div class="col-xs-7">
                    <?= $payment->customer->master_address[0]->getAddress(); ?>
                </div>
            </div>
        </div>

        <?php if (!empty($payment->customer->bank_accounts[0])) {
            $bankAccount = $payment->customer->bank_accounts[0];
            ?>
            <div class="col-xs-12 form-group">
                <label><?= __('tranfer_info') ?> : </label>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-2"><?= __('account_number') ?> :</div>
                <div class="col-xs-10"><?= $bankAccount->account_number; ?></div>
            </div>

            <div class="col-xs-12">
                <div class="col-xs-2"><?= __('account_holder') ?> :</div>
                <div class="col-xs-10"><?= $bankAccount->account_holder; ?></div>
            </div>

            <div class="col-xs-12" style="margin-bottom: 20px">
                <div class="col-xs-2"><?= __('bank') ?> :</div>
                <div class="col-xs-10"><?= sprintf("%s - %s", $bankAccount->bank->name, $bankAccount->branch_name); ?></div>
            </div>

        <?php } ?>

        <?= $this->Element('PaymentDetail/table', ['isAdmin' => false]) ?>

    </div>
</section>
</body>
</html>