<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = '<h3 class="box-title text-red">'.__('price_management').'</h3>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'get']); ?>

                        <div class="row form-group">

                            <div class="col-xl-6 col-lg-6 col-md- col-sm-6 col-12">
                                <?php echo $this->Form->control('key_word',array('value' => !empty($_GET['key_word']) ? $_GET['key_word'] : "",'type' => 'text', 'class' => 'form-control', 'placeholder' => __('price_key_word'), 'label' => __('price_key_word')))?>

                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <?php echo $this->Form->control('active', array('default' => !empty($_GET['active']) ? $_GET['active'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $configs['status'], 'label' => __('status')))?>
                            </div>


                        </div>

                        <div class="pull-right">
                            <?= $this->Form->button('<i class="fa fa-search"></i>'.__('search'), array('class' => 'btn btn-info','type'=>'submit')); ?>
                            <?= $this->Html->link('<i class="fa fa-plus-circle"></i>'.__('create'), ['action' => 'view'],['id' => 'btnPrint','escape' => false,'class' => 'btn btn-warning']); ?>
                        </div>

                        <div class="row form-group">

                        </div>



                        <?= $this->Form->end(); ?>
                        <?php if($this->Paginator->param('pageCount') > 1){ ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">
                                <thead class="thead-dark">
                                <tr>

                                    <th class="text-top"><?= __('status') ?></th>
                                    <th class="text-top"><?= __('code') ?></th>
                                    <th class="text-top"><?= __('description') ?></th>
                                    <th class="text-top"><?= __('date_created') ?></th>
                                    <th class="text-top"><?= __('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($datas as $data){ ?>
                                    <tr>
                                        <td><?= $this->AdminView->showActive($data->active); ?></td>
                                        <td><?= h($data->code); ?></td>
                                        <td><?= h($data->description); ?></td>
                                        <td><?= !empty($data->created) ? $this->AdminView->formatDateTime($data->created) : ''; ?></td>
                                        <td>
                                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'view', $data->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']); ?>
                                            <?= $this->Form->postLink(
                                                "<i class='fa fa-copy'></i>", // first
                                                ['action' => 'copy', $data->id],  // second
                                                ['escape' => false, 'title' => 'Copy', 'class' => 'btn btn-outline-info', 'confirm' => __('copy_confirm')] // third
                                            );
                                            ?>
                                        </td>
                                    </tr>

                                <?php } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
