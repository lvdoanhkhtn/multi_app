<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = $this->Html->link(__('price_management'), ['action' => 'index'], ['class' => 'text-red']). '<span class="text-red"> >'.__('view').'</span>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-header">
                </div>
                <div class="box-body">

                    <ul class="nav nav-tabs mb15" id="priceTab">
                        <li class="active"><a class="nav-link" data-toggle="tab" href="#information"><?= __('information') ?></a></li>
                        <?php if(isset($data)){ ?>
                            <li><a class="nav-link" data-toggle="tab" href="#shipping_fee"><?= __('shipping_fee') ?></a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#cod_fee"><?= __('cod_fee') ?></a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#insurance_fee"><?= __('insurance_fee') ?></a></li>
                        <?php } ?>
                    </ul>

                    <div class="tab-content">
                        <div id="information" class="tab-pane fade in active">
                            <?php
                            echo $this->Form->create(!empty($price) ? $price : null, ['class' => 'validateForm']);
                            echo $this->Element('Price/information_price');
                            echo $this->Form->end();
                            ?>
                        </div>
                        <div id="shipping_fee" class="tab-pane fade">
                            <?php
                                echo $this->Form->create($data, ['type' => 'post', 'action' => 'insertOrUpdate/'.$id, 'id' => 'frmShippingFee', 'class' => 'validateForm']);
                                echo $this->Element('Price/shipping_fee_price') ;
                                echo $this->Form->end();
                            ?>
                        </div>
                        <div id="cod_fee" class="tab-pane fade">
                            <?php
                                echo $this->Form->create(null, ['type' => 'post', 'action' => 'insertOrUpdate/'.$id,'id' => 'frmCodFee','class' => 'validateForm']);
                                echo $this->Element('Price/cod_fee_price');
                                echo $this->Form->end();
                            ?>
                        </div>
                        <div id="insurance_fee" class="tab-pane fade">
                            <?php
                                echo $this->Form->create(null, ['type' => 'post', 'action' => 'insertOrUpdate/'.$id,'id' => 'frmInsuranceFee','class' => 'validateForm']);
                                echo $this->Element('Price/insurance_fee_price');
                                echo $this->Form->end();
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Form->control('base_url', ['type' => 'hidden','id' => 'base_url', 'value' => $this->Url->build(['action' => 'detail'])]) ?>
<?= $this->Html->script('Price/price'); ?>
