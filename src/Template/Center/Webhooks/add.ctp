<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = $this->Html->link(__('webhook_management'), ['action' => 'index'], ['class' => 'text-red']). '<span class="text-red"> > Add </span>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <?= $this->Form->create(null, ['class' => 'validateForm']); ?>
                        <?= $this->Element('Webhooks/form'); ?>
                    <?= $this->Form->end(); ?>

                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
