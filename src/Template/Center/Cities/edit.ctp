<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = $this->AdminView->getBreadCum([
                        'parentController' => 'countries',
                        'parentId' => $parentId,
                        'id' => $id,
                        'name' => $name,
                        'parentLabel' => 'country_management',
                        'label' => 'city_management',
                        'isChange' => true,
                        'action' => 'edit'
                    ]);
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>


                <div class="box-body">
                    <?= $this->Form->create($data, ['class' => 'validateForm']); ?>
                        <?= $this->Element('City/form'); ?>
                    <?= $this->Form->end(); ?>

                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
