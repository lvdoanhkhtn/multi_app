<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = $this->AdminView->getBreadCum([
                        'parentController' => 'countries',
                        'parentId' => $parentId,
                        'id' => $id,
                        'name' => $name,
                        'parentLabel' => 'country_management',
                        'label' => 'city_management'
                    ]);
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Element('Country/search', ['id' => $id, 'datas' => $datas, 'controller' => 'districts']); ?>
                    </div><!-- /.box-body -->

                </div>
            </div>
        </div>
    </div>
</section>
