<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link =
                    $this->Html->link(__('method_management'), ['controller' => 'actions','action' => 'index',$parentId], ['class' => 'text-red']).' > '
                    .'<span class="text-red">'.__('edit').'</span>';

                echo $this->Element('breadcum', ['link' => $link]) ;
                echo $this->Element('MethodAction/form');

                ?>
                
            </div>
        </div>
    </div>
</section>
