<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php

                $link =
                    $this->Html->link(__('method_management'), ['controller' => 'methods','action' => 'index','?' => ['id' => $parentId]], ['class' => 'text-red']).' > '
                    .'<span class="text-red">'.$name.'</span>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                echo $this->Element('MethodAction/index') ;
                ?>
        </div>
    </div>
</section>
