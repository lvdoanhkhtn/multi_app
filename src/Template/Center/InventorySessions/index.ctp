<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = $this->Html->link(__('inventor_session_management'), ['controller' => 'inventory_sessions','action' => $backUrl], ['class' => 'text-red']);
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'get']); ?>

                        <div class="row form-group">

                            <div class="col-sm-6 col-xs-12">
                                <?php echo $this->Form->control('id',array('value' => !empty($_GET['id']) ? $_GET['id'] : "",'type' => 'text', 'class' => 'form-control', 'label' => __('')))?>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <?php echo $this->Form->control('status',array('empty' => __('select'),'type' => 'select','options' => $configs['delivery_status'],'value' => !empty($_GET['status']) ? $_GET['status'] : "",'class' => 'form-control', 'label' => __('status')))?>
                            </div>

                        </div>

                        <div class="row form-group">

                            <div class="col-sm-6 col-xs-12">
                                <?php echo $this->Form->control('creater_id',array('empty' => __('select'),'options' => $coordinators,'value' => !empty($_GET['creater_id']) ? $_GET['creater_id'] : "",'type' => 'select', 'class' => 'form-control',  'label' => __('creater')))?>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <?php echo $this->Form->control('modifier_id',array('empty' => __('select'),'options' => $coordinators,'value' => !empty($_GET['modifier_id']) ? $_GET['modifier_id'] : "",'type' => 'select', 'class' => 'form-control', 'label' => __('modifier')))?>
                            </div>

                        </div>


                        <div class="row form-group">

                            <div class="col-sm-6 col-xs-12">
                                <?= $this->Element('Calendar/created_calendar'); ?>
                            </div>


                            <div class="col-sm-6 col-xs-12">
                                <?= $this->Element('Calendar/completed_calendar'); ?>
                            </div>
                        </div>

                        <div class="pull-right">
                            <?= $this->Form->button('<i class="fa fa-search"></i>'.__('search'), array('class' => 'btn btn-info','type'=>'submit')); ?>
                            <?= $this->Html->link('<i class="fa fa-plus-circle"></i>' . __('create'), [ 'action' => 'add/'.$backUrl.'/'.$type], ['class' => 'btn btn-warning', 'escape' => false, 'target' => 'blank']); ?>
                        </div>

                        <div class="row form-group">

                        </div>



                        <?= $this->Form->end(); ?>
                        <?php if($this->Paginator->param('pageCount') > 1){ ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">
                                <thead class="thead-dark">
                                <tr>
                                    <th class="text-top"><?= __('code') ?></th>
                                    <th class="text-top"><?= __('date_created') ?></th>
                                    <th class="text-top"><?= __('total_order_in_warehouse') ?></th>
                                    <th class="text-top"><?= __('total_order_in_session') ?></th>
                                    <th class="text-top"><?= __('total_order_in_real') ?></th>
                                    <th class="text-top"><?= __('creater') ?></th>
                                    <th class="text-top"><?= __('modifier') ?></th>
                                    <th class="text-top"><?= __('date_completed') ?></th>
                                    <th class="text-top"><?= __('status') ?></th>
                                    <th class="text-top"><?= __('actions') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($datas as $data){ ?>
                                        <tr>
                                            <td><?= $data->id; ?></td>
                                            <td><?= $data->getCreated(); ?></td>
                                            <td><?= $storedOrderCount; ?></td>
                                            <td><?= count($data['inventory_session_details']); ?></td>
                                            <td><?= count($data['checked_inventory_session_details']); ?></td>
                                            <td><?= !empty($data['creater']['fullname']) ? $data['creater']['fullname'] : ""; ?></td>
                                            <td><?= !empty($data['modifier']['fullname']) ? $data['modifier']['fullname'] : ""; ?></td>
                                            <td><?= !empty($data->status) ? $data->getModified() : ""; ?></td>
                                            <td><?= $this->AdminView->showComplete($data->status) ?></td>
                                            <td>
                                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'inventory_session_details','action' => 'index', $data->id, $backUrl], ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => __('edit'),'escape' => false,'class' => 'btn btn-outline-info']); ?>
                                                <?php
                                                    if(empty($data->status)){
                                                        echo $this->Form->postLink(
                                                            "<i class='fa fa-trash-alt'></i>", // first
                                                            ['action' => 'delete', $data->id, $backUrl],  // second
                                                            ['escape' => false, 'title' => 'Delete', 'class' => 'btn btn-outline-danger', 'confirm' => __('delete_confirm')]);// third
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script('Trip/trips'); ?>
