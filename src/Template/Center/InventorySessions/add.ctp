<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = $this->Html->link(__('inventor_session_management'), ['action' => 'index'], ['class' => 'text-red']). '<span class="text-red"> >'.__('add').'</span>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <?= $this->Form->create(null, ['class' => 'validateForm']); ?>
                    <div class="row">
                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-xs-12">
                            <?php echo $this->Form->control('type_id', ['default' => $type, 'options' => $configs['inventor_session_types'],'type' => 'select', 'empty' => __('select'), 'data-name' => __('type_of_inventor_session'), 'class' => 'required form-control select2', 'label' => __('type_of_inventor_session').'(*)'])?>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-6">
                        </div>
                        <div class="col-xs-6 text-right">
                            <button type="submit" class="btn btn-success"><?= isset($data) ? __('update') : __('create') ?></button>
                            <?= $this->Html->link(__('cancel'), ['action' => 'index'], ['class' => 'btn btn-danger']); ?>
                        </div>

                    </div>



                    <?= $this->Form->end(); ?>

                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
