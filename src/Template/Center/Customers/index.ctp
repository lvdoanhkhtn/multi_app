<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = '<h3 class="box-title text-red">' . __('customer_management') . '</h3>';
                echo $this->Element('breadcum', ['link' => $link]);
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'get']); ?>

                        <div class="row form-group">

                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?php echo $this->Form->control('key_word', array('value' => !empty($_GET['key_word']) ? $_GET['key_word'] : "", 'type' => 'text', 'class' => 'form-control', 'placeholder' => __('employee_key_word'), 'label' => __('key_word'))) ?>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?= $this->Element('Calendar/created_calendar'); ?>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?php echo $this->Form->control('active', array('default' => (isset($_GET['active']) && strlen($_GET['active']) > 0) ? $_GET['active'] : '', 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $configs['status'], 'label' => __('status'))) ?>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?php echo $this->Form->control('group_customer_id', array('default' => !empty($_GET['group_customer_id']) ? $_GET['group_customer_id'] : '', 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $groupCustomers, 'label' => __('group_customer'))) ?>
                            </div>


                        </div>

                        <div class="pull-right">
                            <?= $this->Form->button('<i class="fa fa-search"></i>' . __('search'), array('class' => 'btn btn-info', 'type' => 'submit')); ?>
                            <?= $this->Html->link('<i class="fa fa-plus-circle"></i>' . __('create'), ['action' => 'view'], ['id' => 'btnPrint', 'escape' => false, 'class' => 'btn btn-warning']); ?>
                            <?= $this->Html->link('<i class="fa fa-file-excel"></i>' . __('export'), ['action' => 'export'], ['escape' => false, 'class' => 'btn btn-success']); ?>
                        </div>

                        <div class="row form-group">

                        </div>



                        <?= $this->Form->end(); ?>
                        <?php if ($this->Paginator->param('pageCount') > 1) { ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">
                                <thead class="thead-dark">
                                    <tr>

                                        <th class="text-top"><?= __('status') ?></th>
                                        <th class="text-top"><?= __('user_name') ?></th>
                                        <th class="text-top"><?= __('date_created') ?></th>
                                        <th class="text-top"><?= __('full_name') ?></th>
                                        <th class="text-top"><?= __('email') ?></th>
                                        <th class="text-top"><?= __('phone') ?></th>
                                        <th class="text-top"><?= __('address') ?></th>
                                        <th class="text-top"><?= __('group_customers') ?></th>
                                        <th class="text-top mw150"><?= __('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($customers as $customer) {
                                        $groupCustomerName = !empty($customer->group_customer->name) ? h($customer->group_customer->name) : "";
                                    ?>
                                        <tr>
                                            <td><?= $this->AdminView->showActive($customer->user->active); ?></td>
                                            <td><?= h($customer->user->username); ?></td>
                                            <td><?= $customer->getCreated(); ?></td>
                                            <td><?= h($customer->user->fullname); ?></td>
                                            <td><?= h($customer->user->email); ?></td>
                                            <td><?= h($customer->user->phone); ?></td>
                                            <td><?= h($customer->getAddress()); ?></td>
                                            <td><?= $groupCustomerName  ?></td>
                                            <td>

                                                <div class="btn-group">
                                                    <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'view', $customer->id, $customer->user_id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']); ?>
                                                    <?= $this->Html->link('<i class="fa fa-file-excel"></i>', ['controller' => 'orders', 'action' => 'import', $customer->user_id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Import Orders', 'class' => 'btn btn-outline-info']); ?>
                                                    <?php
                                                    echo $this->Html->link('<i class="fa fa-plus-circle"></i>', ['controller' => 'orders', 'action' => 'add', $customer->user_id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Create Orders', 'class' => 'btn btn-outline-info']);
                                                    if ($groupCustomerName === \App\Libs\ValueUtil::get('wms.is_wms')) {
                                                        echo $this->Html->link('<i class="fa fa-plus"></i>', ['controller' => 'orders', 'action' => 'addWms', $customer->user_id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Create Wms Orders', 'class' => 'btn btn-outline-info']);
                                                    }
                                                    ?>
                                                    <?= $this->Html->link('<i class="fa fa-exchange-alt"></i>', ['controller' => 'accounts', 'action' => 'adminChangePassword', $customer->user_id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Change password', 'class' => 'btn btn-outline-info']); ?>

                                                </div>
                                            </td>
                                        </tr>

                                    <?php } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->Html->script(['Payment/payment']); ?>