<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = $this->Html->link(__('customer_management'), ['action' => 'index'], ['class' => 'text-red']). '<span class="text-red"> >'.__('view').'</span>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-header">
                </div>
                <div class="box-body">

                    <ul class="nav nav-tabs mb15" id="priceTab">
                        <li class="active"><a class="nav-link" data-toggle="tab" href="#information"><?= __('information') ?></a></li>
                        <?php if(isset($account)){ ?>
                            <li><a class="nav-link" data-toggle="tab" href="#addresses"><?= __('addresses') ?></a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#payment"><?= __('payments') ?></a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#staticstic"><?= __('staticstic') ?></a></li>
                        <?php } ?>
                    </ul>

                    <div class="tab-content">
                        <div id="information" class="tab-pane fade in active">
                            <?php
                            echo $this->Form->create(null, ['class' => 'validateForm']);
                            echo $this->Element('Customer/form', ['data' => []]);
                            echo $this->Form->end();
                            ?>
                        </div>
                        <div id="addresses" class="tab-pane fade">
                            <?php
                                echo $this->Form->create(null, ['type' => 'post', 'action' => 'insertOrUpdate/'.$id.'/'.$userId, 'id' => 'frmAddressCustomer', 'class' => 'validateForm']);
                                echo $this->Element('Customer/address_customer') ;
                                echo $this->Form->end();
                            ?>
                        </div>
                        <div id="payment" class="tab-pane fade">
                            <?php
                                echo $this->Form->create($bankAccount, ['type' => 'post', 'action' => 'updatePayment/'.$id.'/'.$userId,'id' => 'frmPaymentCustomer','class' => 'validateForm']);
                                echo $this->Element('Customer/payment_customer');
                                echo $this->Form->end();
                            ?>
                        </div>
                        <div id="staticstic" class="tab-pane fade">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->Form->control('base_url', ['type' => 'hidden','id' => 'base_url', 'value' => $this->Url->build(['action' => 'detail'])]) ?>
<?= $this->Html->script('Customer/customer'); ?>
