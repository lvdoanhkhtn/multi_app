<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: doanh.lv-->
<!-- * Date: 11/8/19-->
<!-- * Time: 3:58 PM-->
<!-- */-->
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">
                <?php
                    $link = $this->Html->link(__('orders_management'), ['controller' => 'orders', 'action' => 'index'], ['class' => 'text-red']).'<span class="text-red"> >'.__('handing').'</span>';
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'GET']) ?>

                        <div class="row form-group">

                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?php echo $this->Form->control('key_word', ['value' => !empty($_GET['key_word']) ? $_GET['key_word'] : '', 'type' => 'text', 'class' => 'form-control', 'placeholder' => __('employee_key_word'), 'label' => __('key_word')]) ?>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?php echo $this->Form->control('user_id', array('default' => !empty($_GET['user_id']) ? $_GET['user_id'] : '', 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $customers, 'label' => __('customer'))) ?>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?= $this->Element('Calendar/created_calendar'); ?>
                            </div>


                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?= $this->Element('Calendar/completed_calendar'); ?>
                            </div>

                        </div>

                        <div class="pull-right">
                            <?= $this->Form->button('<i class="fa fa-search"></i>' . __('search'), array('class' => 'btn btn-info', 'type' => 'submit')); ?>

                            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#confirm-re-delivery"><i class="fa fa-backward"></i><?= __('redelivery') ?></a>
                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#confirm-return"><i class="fa fa-forward"></i> <?= __('return') ?></a></a>

                        </div>

                        <?= $this->Form->end(); ?>

                        <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?= $this->Element('Order/orders') ?>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script(['PickupTrip/pickup_trip_details', 'Order/order']); ?>

<?= $this->Element('PickupTripDetail/pickup_confirm_failed_modal', ['target' => 'confirm-re-delivery', 'button' => 'btnReDelivery']); ?>
<?= $this->Element('PickupTripDetail/pickup_confirm_failed_modal', ['target' => 'confirm-return', 'button' => 'btnReturn']); ?>

