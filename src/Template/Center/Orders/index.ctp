<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = __('orders_management');
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'GET', 'id' => 'frmOrder'])?>

                        <div class="row form-group">

                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?php echo $this->Form->control('key_word',['value' => !empty($_GET['key_word']) ? $_GET['key_word'] : '','type' => 'text', 'class' => 'form-control', 'placeholder' => __('employee_key_word'), 'label' => __('key_word')])?>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?php echo $this->Form->control('user_id', array('default' => !empty($_GET['user_id']) ? $_GET['user_id'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $customers, 'label' => __('customer')))?>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?= $this->Element('Calendar/created_calendar'); ?>
                            </div>


                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                <?= $this->Element('Calendar/completed_calendar'); ?>
                            </div>

                        </div>

                        <div class="box box-default <?= ($haveData) ? '' : 'collapsed-box' ?> ">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= __('advance_filter') ?></h3>
                                <div class="box-tools">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa <?= ($haveData) ? 'fa-minus' : 'fa-plus' ?> "></i></button>
                                </div>
                            </div><!-- /.box-header -->
                            <div class="box-body">

                                <div class="row form-group">
                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                            <?= $this->AdminView->loadCityAndDistrict($deliveryFrom, 'delivery_from', !empty($_GET['delivery_from']) ? $_GET['delivery_from'] : ''); ?>
                                        </div>

                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                            <?= $this->AdminView->loadCityAndDistrict($deliveryTo, 'delivery_to', !empty($_GET['delivery_to']) ? $_GET['delivery_to'] : ''); ?>
                                        </div>

                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <?php echo $this->Form->control('ware_house_id',array('default' => !empty($_GET['ware_house_id']) ? $_GET['ware_house_id'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $configs['ware_houses'], 'label' => __('ware_houses')))?>
                                    </div>
                                </div>

                                <div class="row form-group">

                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <?php echo $this->Form->control('service_pack_id',array('empty' => __('select'),  'default' => !empty($_GET['service_pack_id']) ? $_GET['service_pack_id'] : "",'type' => 'select', 'class' => 'form-control select2', 'options' => $configs['service_pack'], 'label' => __("service_pack")))?>
                                    </div>

                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <?php echo $this->Form->control('order_type',array('empty' => __('select'), 'default' => !empty($_GET['order_type']) ? $_GET['order_type'] : "",'type' => 'select', 'class' => 'form-control select2', 'options' => $configs['type_of_order'], 'label' => __("type_of_order")))?>
                                    </div>

                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <?php echo $this->Form->control('order_status',array('default' => !empty($_GET['order_status']) ? $_GET['order_status'] : "",'style' => 'width: 100%','multiple' => 'multiple','type' => 'select', 'class' => 'form-control select2', 'options' => $configs['order_status'], 'label' => __("order_status")))?>
                                    </div>


                                </div>

                            </div><!-- /.box-body -->

                        </div><!-- /.box -->

                        <?= $this->Element('Order/buttons_order'); ?>

                        <?= $this->Form->end(); ?>


                        <?= $this->AdminView->paginateMenuAndResult() ?>

                        <div class="form-group row margin-top-lg">
                            <?php $this->AdminView->showStatusesOnOrder($orderStatuses); ?>
                        </div>
                        <div class="box">
                            <?= $this->Element('Order/orders') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script('Order/order'); ?>

