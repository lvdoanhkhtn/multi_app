<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = $this->Html->link(__('orders_management'), ['controller' => 'orders', 'action' => 'index'], ['class' => 'text-red']).'<span class="text-red"> > Change sub fee </span>';
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <?= $this->Form->create($order, ['id' => 'order_edit', 'class' => 'validateForm Order'])?>
                    <div class="form-group col-md-12 table-responsive">


                        <div class="add-form-background">
                            <div class="panel panel-default add-form">
                                <div class="panel-heading">
                                </div>
                                <div class="panel-body">
                                    <div class="col-xs-12 col-md-12 col-sm-12 no-padding-left no-padding-right">

                                        <div class="col-md-2">
                                            <?php echo $this->Form->control('sub_fee', ['type' => 'text', 'class' => 'form-control', 'placeholder' => __("sub_fee")]); ?>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12 col-md-12 col-sm-12 no-padding-left no-padding-right">
                            <div class="btn-group pull-right form-group">
                                <?= $this->Form->button(__('update'), [], ['escape' => false,'class' => 'btn btn-success']); ?>
                            </div>
                        </div>

                    </div>

                    <?= $this->Form->end(); ?>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>


