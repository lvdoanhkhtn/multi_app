<script>
    window.onload = function () {

        var options = {
            animationEnabled: true,
            title:{
                text: ""
            },
            axisX: {
                valueFormatString: "MMM/YYYY"
            },
            axisY: {
                title: "",
                prefix: "",
                includeZero: false
            },
            data: [{
                yValueFormatString: "#,###",
                xValueFormatString: "MMM/YYYY",
                type: "spline",
                dataPoints: [
                    <?php foreach ($trips as $trip){
                        $year = $this->AdminView->getYear($trip->date);
                        $month = $this->AdminView->getMonth($trip->date) - 1;
                        ?>
                        {
                            x: new Date(<?= $year ?>, <?= $month ?>), y: <?= $trip->order_count ?>
                        },
                    <?php } ?>
                ]
            }]
        };
        $("#chartContainer").CanvasJSChart(options);
    }
</script>
<?= $this->Element('Report/chart_report', ['link' => $link]) ?>
