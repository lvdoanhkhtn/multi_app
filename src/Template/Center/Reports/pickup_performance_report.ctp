<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = __('pickup_performance_report');
                    echo $this->Element('breadcum', ['link' => $link]);
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'GET']) ?>

                        <div class="row form-group">

                            <div class="col-sm-6 col-12">
                                <?= $this->Element('Calendar/created_calendar'); ?>
                            </div>


                        </div>
                        
                        <div class="pull-right">
                            <?= $this->Form->button('<i class="fa fa-search"></i>'.__('search'), array('class' => 'btn btn-info','type'=>'submit')); ?>
                            <?= $this->Html->link('<i class="fa fa-file-excel"></i>' . __('export'), ['action' => 'pickupPerformanceExport'], ['escape' => false, 'class' => 'btn btn-success']); ?>
                        </div>

                        <?= $this->Form->end(); ?>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script('Order/order'); ?>

