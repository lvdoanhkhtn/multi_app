<script>
    window.onload = function () {

        var options = {
            animationEnabled: true,
            title:{
                text: ""
            },
            axisX: {
                valueFormatString: "DD/MM/YYYY"
            },
            axisY: {
                title: "",
                prefix: "",
                includeZero: false
            },
            data: [{
                yValueFormatString: "#,###",
                xValueFormatString: "DD/MM/YYYY",
                type: "spline",
                dataPoints: [
                    <?php foreach ($trips as $trip){
                        $day = $this->AdminView->getDay($trip->date);
                        $month = $this->AdminView->getMonth($trip->date) - 1;
                        $day = $this->AdminView->getDay($trip->date);
                    ?>
                    {
                            x: new Date(<?= $year ?>, <?= $month ?>,<?= $day ?>), y: <?= $trip->order_count ?>
                    },
                    <?php } ?>
                ]
            }]
        };
        $("#chartContainer").CanvasJSChart(options);
    }
</script>
<?= $this->Element('Report/chart_report', ['link' => $link]) ?>