<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = '<h3 class="box-title text-red">'.__('area_management').'</h3>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'get']); ?>

                        <div class="row form-group">

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <?php echo $this->Form->control('key_word',array('value' => !empty($_GET['key_word']) ? $_GET['key_word'] : "",'type' => 'text', 'class' => 'form-control', 'placeholder' => __('employee_key_word'), 'label' => __('key_word')))?>

                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <?php echo $this->Form->control('sender_city_id', array('default' => !empty($_GET['sender_city_id']) ? $_GET['sender_city_id'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $pickUpCities, 'label' => __('from_province')))?>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <?php echo $this->Form->control('city_id', array('default' => !empty($_GET['city_id']) ? $_GET['city_id'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $deliveryCities, 'label' => __('from_province')))?>
                            </div>


                        </div>

                        <div class="pull-right">
                            <?= $this->Form->button('<i class="fa fa-search"></i>'.__('search'), array('class' => 'btn btn-info','type'=>'submit')); ?> 
                            <?= $this->Html->link('<i class="fa fa-plus-circle"></i>'.__('create'), ['action' => 'add'],['id' => 'btnPrint','escape' => false,'class' => 'btn btn-warning']); ?> 
                            <?= $this->Html->link('<i class="fa fa-file-excel"></i>'.__('export'), ['action' => 'export'], ['escape' => false,'class' => 'btn btn-success']); ?> 
                        </div>

                        <div class="row form-group">

                        </div>



                        <?= $this->Form->end(); ?>
                        <?php if($this->Paginator->param('pageCount') > 1){ ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">
                                <thead class="thead-dark">
                                <tr>

                                    <th class="text-top"><?= __('name') ?></th>
                                    <th class="text-top"><?= __('description') ?></th>
                                    <th class="text-top"><?= __('from_province') ?></th>
                                    <th class="text-top"><?= __('to_province') ?></th>
                                    <th class="text-top"><?= __('from_district') ?></th>
                                    <th class="text-top"><?= __('to_district') ?></th>
                                    <th class="text-top"><?= __('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($datas as $data){ ?>
                                    <tr>
                                        <td><?= h($data->location->name); ?></td>
                                        <td><?= h($data->description); ?></td>
                                        <td><?= h($data->sender_city->name); ?></td>
                                        <td><?= h($data->city->name); ?></td>
                                        <td><?= !empty($data->sender_district->name) ? h($data->sender_district->name) : ''; ?></td>
                                        <td><?= !empty($data->district->name) ? h($data->district->name) : ''; ?></td>
                                        <td>
                                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $data->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']); ?>
                                        </td>
                                    </tr>

                                <?php } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
