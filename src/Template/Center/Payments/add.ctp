<?= $this->Html->css('iCheck/flat/blue', ['block' => 'css']); ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = '<h3 class="box-title text-red">'.__('payment_history').'</h3>';
                    $link = $this->Html->link(__('payment_management'), ['controller' => 'payments', 'action' => 'index'], ['class' => 'text-red']).'<span class="text-red"> >'.__('add').'</span>';
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>
                <?= $this->Form->create('payment_add', ['type' => 'post', 'id' => 'payment-add']); ?>
                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'get']); ?>

                        <div class="row form-group">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <?= $this->Form->control('user_ids',array('value' => !empty($_GET['user_ids']) ? $_GET['user_ids'] : "",'options' => $customers, 'class' => 'form-control select2', 'id' => 'cbCustomers','multiple' => 'multiple','label' => __('customer')))?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-xs-12">
                                <div class="pull-right">
                                    <?= $this->Form->button('<i class="fa fa-plus-circle"></i>'.__('create'), array('class' => 'btn btn-warning btnAddPayment','type'=>'submit')); ?>
                                    <?= $this->Html->link('<i class="fa fa-file-excel"></i>'.__('export'), ['action' => 'export'], ['escape' => false,'class' => 'btn btn-success']); ?>
                                </div>
                            </div>
                        </div>

                        <?= $this->Form->end(); ?>
                        <?php if($this->Paginator->param('pageCount') > 1){ ?>
                        <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="result-table table table-bordered table-condensed table-hover" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th class="table-10">
                                            <?= $this->Form->control('check_get_all', array('type' => 'checkbox', 'class' => 'check_get_all', 'label' => false, 'templates' => ['inputContainer' => '{{content}}'])); ?>
                                        </th>
                                        <th class="text-top"><?= __('customer_name') ?></th>
                                        <th class="text-top"><?= __('address') ?></th>
                                        <th class="text-top"><?= __('total_orders') ?></th>
                                        <th class="text-top"><?= __('total_cod') ?></th>
                                        <th class="text-top"><?= __('total_fee') ?></th>
                                        <th class="text-top"><?= __('total_amount') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($users as $user){
                                        if(empty($user->customers[0]['master_address'][0]) || (empty($user->order_complete_payment[0]) && empty($user->order_return_payment[0]))){
                                                continue;
                                            }
                                            $address = $user->customers[0]['master_address'][0];
                                            $order = $Excel->getPayment($user);
                                            
                                        ?>
                                    <tr>
                                        <td>
                                            <?= $this->Form->control('check_get', ['name' => 'check_get[]','label' => false,'data-name' => $user->fullname,'type' => 'checkbox', 'class' => 'check_get', 'id' => 'cb_'.$user->id,'value' => $user->id, 'templates' => ['inputContainer' => '{{content}}'],]); ?>
                                        </td>
                                        </td>
                                        <td><?= $user->fullname ?></td>
                                        <td><?= $address->getAddress(); ?></td>
                                        <td><?= number_format($order['totalOrder']) ?></td>
                                        <td><?= $this->AdminView->getCurrencyCam($order['totalCod']) ?></td>
                                        <td><?= $this->AdminView->getCurrencyCam($order['totalFee']) ?></td>
                                        <td><?= $this->AdminView->getCurrencyCam($order['totalAmount']) ?></td>
                                    </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</section>

<?= $this->Html->script('Order/order'); ?>