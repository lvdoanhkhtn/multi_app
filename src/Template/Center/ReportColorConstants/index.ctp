<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = '<h3 class="box-title text-red">'.__('report_color_constant_management').'</h3>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?php if($this->Paginator->param('pageCount') > 1){ ?>
                        <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">
                                <thead class="thead-dark">
                                    <tr>

                                        <th class="text-top"><?= __('position') ?></th>
                                        <th class="text-top"><?= __('value') ?></th>
                                        <th class="text-top"><?= __('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($datas as $data){ ?>
                                    <tr>
                                        <td><?= h($data->position); ?></td>
                                        <td><span class="color"
                                                style="width: 20px; height: 20px; display: block;background-color: <?= $data->value ?>"></span>
                                        </td>
                                        <td>
                                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $data->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-outline-info']); ?>
                                        </td>
                                    </tr>

                                    <?php } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>