<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = $this->Html->link(__('report_color_constant_management'), ['action' => 'index'], ['class' => 'text-red']). '<span class="text-red"> >'.__('edit').'</span>';
                echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <?= $this->Form->create($data, ['class' => 'validateForm']); ?>
                    <?= $this->Element('ReportColorConstant/form'); ?>
                    <?= $this->Form->end(); ?>

                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>