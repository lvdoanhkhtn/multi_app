<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                $link = '<h3 class="box-title text-red">' . __('temp_orders_management') . '</h3>';
                echo $this->Element('breadcum', ['link' => $link]);
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">

                        <?php if ($this->Paginator->param('pageCount') > 1) { ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">
                                <thead class="thead-dark">
                                    <tr>

                                        <th class="text-top"><?= __('id') ?></th>
                                        <th class="text-top"><?= __('description') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($tempOrders as $tempOrder) {
                                        $processedDatas = json_decode($tempOrder->data, true);
                                    ?>
                                        <tr>
                                            <td><?= $tempOrder->id; ?></td>
                                            <td><?php
                                                foreach ($processedDatas as $key => $value) {
                                                    // echo $processedDatas['user_id'];
                                                    // die;
                                                    switch ($key) {
                                                        case 'user_id':
                                                            $value = $this->AdminView->getShopNameById($processedDatas[$key]);
                                                            $key = __('shop_name');
                                                    }
                                                    echo sprintf("%s: %s; ", $key, $value);
                                                }
                                                ?></td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>