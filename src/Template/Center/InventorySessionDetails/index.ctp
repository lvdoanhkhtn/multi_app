<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = $this->Html->link(__('inventor_session_management'), ['controller' => 'inventory_sessions','action' => $backUrl], ['class' => 'text-red']). ' > '.$this->AdminView->showComplete($inventorySessions->status);
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                            <div class="row form-group">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
                                    <?php if($inventorySessions->status === NO_COMPLETE_STATUS){ ?>
                                        <?php
                                            echo $this->Form->control('order_code', ['id' => 'inventory_order_code','class' => 'checkOrder form-control', 'placeholder' => 'Input Order Code/Shop Code']);
                                            echo $this->Form->control('inventory_session_id', ['type' => 'hidden', 'value' => $id, 'id' => 'inventory_session_id']);
                                        ?>
                                    <?php } ?>
                                </div>

                                <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12 mt25">
                                    <div class="pull-right">
                                        <?= $this->Html->link('<i class="fa fa-file-excel"></i>'.__('export'), ['action' => 'export', $id], ['escape' => false,'class' => 'btn btn-success']); ?> 
                                    </div>
                                </div>
                                
                            </div>

                        <!--/**-->
                        <!-- * Created by PhpStorm.-->
                        <!-- * User: doanh.lv-->
                        <!-- * Date: 1/13/20-->
                        <!-- * Time: 10:34 AM-->
                        <!-- */-->

                        <?php if ($this->Paginator->param('pageCount') > 1) { ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>
                        <div class="box table-responsive">
                            <table class="table table-striped table-sm">
                                <thead class="thead-dark">
                                <tr>

                                    <th class="text-top">#</th>
                                    <th class="text-top"><?= __('order_code') ?></th>
                                    <th class="text-top"><?= __('sender') ?></th>
                                    <th class="text-top"><?= __('receiver') ?></th>
                                    <th class="text-top"><?= __('product') ?></th>
                                    <th class="text-top"><?= __('status') ?></th>
                                    <th class="text-top"><?= __('time') ?></th>
                                    <th class="text-top mw100"><?= __('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1;
                                foreach ($inventorySessions['inventory_session_details'] as $inventorySession) {
                                    $order = $inventorySession->order;
                                    ?>
                                    <tr>
                                        <td class="table-10"><?= $i++; ?></td>
                                        <td class="table-20">
                                            <?= $this->Html->link(sprintf("%s <br> %s", $order->orderId, $order->shopId), ['controller' => 'orders', 'action' => 'view', $order->id], ['target' => '_blank','escape' => false]) ?>
                                        </td>
                                        <td class="table-20">
                                            <?= sprintf("%s <br> %s", $order->user->fullname, $order->user->phone) ?>
                                        </td>
                                        <td class="table-20">
                                            <?= sprintf("%s <br> %s", $order->receiver_name, $order->arr_receiver_phone) ?>
                                            <br>
                                            <?= sprintf("%s, %s, %s", $order->address, $order->district->name, $order->city->name) ?>

                                        </td>
                                        <td class="table-20">
                                            <?= sprintf("%s <br> %s", $order->content, $order->weight) ?>
                                        </td>
                                        <td class="table-20">
                                            <input disabled type="checkbox" <?= !empty($inventorySession->status) ? 'checked' : '' ?>  />
                                        </td>
                                        <td class="table-20">
                                            <?= $order->getStoredTime() ?>
                                        </td>
                                        <td class="table-10">
                                            <div class="btn-group">
                                                <?= $this->Html->link('<i class="fa fa-print"></i>', ['controller' => 'orders', 'action' => 'printOrder', $order->order_id], ['target' => 'blank', 'escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Print', 'class' => 'btn btn-outline-info']); ?>
                                                <?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'orders', 'action' => 'view', $order->order_id], ['target' => 'blank', 'escape' => false, 'data-toggle' => 'tooltip', 'title' => 'view', 'class' => 'btn btn-outline-info']); ?>
                                            </div>
                                        </td>
                                    </tr>

                                <?php } ?>
                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script(['DeliveryTrip/delivery_trip_details']); ?>