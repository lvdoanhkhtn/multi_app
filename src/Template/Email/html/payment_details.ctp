<html>
<?php //if(isset($paymentDetails)){ ?>
    <head>
        <meta charset="UTF-8">
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
                color: black;
            }
            th{
                background-color: aqua;
            }
        </style>
    </head>
    <body>
    <section>
        <div class="container">

            <div class="col-xs-12">
                <p style="text-align: center; font-weight: bold;"><?= __('payment_title') ?></p>
                <br>
            </div>
            <?= $this->Element('PaymentDetail/table', ['paymentDetails' => $paymentDetails, 'isIndex' => true]); ?>
        </div>
    </section>
    </body>

<?php //} ?>
</html>
