<footer class="main-footer">
    <div class="row form-group">

        <div class="col-md-4">
                <strong><i class="fa fa-fw fa-copyright text-red"></i><a class="text-red" href="/"> <?= HOME_PAGE ?></a></strong>
        </div>

        <div class="col-md-4">
                <strong><i class="fas fa-phone-alt text-red"></i><span class="text-red" href="/"> <?= HOT_LINE ?></span></strong>
        </div>


        <div class="col-md-4">
            <strong><i class="far fa-envelope text-red"></i><span class="text-red" href="/"> <?= INFO_MAIL; ?></span></strong>
        </div>

    </div>

</footer>