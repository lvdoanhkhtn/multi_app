<?php use Cake\Core\Configure; ?>
<nav class="navbar navbar-static-top">

  <?php if (isset($layout) && $layout == 'top'): ?>
  <div class="container">

    <div class="navbar-header">
      <a href="<?php echo $this->Url->build('/'); ?>" class="navbar-brand"><?php echo Configure::read('Theme.logo.large') ?></a>
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
        <i class="fa fa-bars"></i>
      </button>
    </div>

    <!-- /.navbar-collapse -->
    <?php else: ?>

      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

    <?php endif; ?>



    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Notifications: style can be found in dropdown.less -->
        <?php if (in_array($this->Session->read('Auth.User.role_id'), [ADMIN, ACCOUNTANT, COORDINATOR])) { ?>

        <li class="dropdown notifications-menu">
          <a id="notify" href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            <span style="position: relative;top: -10px;right: 10px;" id="notifyBadge" class="badge badge-light"><?= !empty($countNotifies) ? $countNotifies : ''; ?></span>
            <span class="label label-warning"></span>
          </a>
          <ul class="dropdown-menu" style="height: 200px; overflow: scroll">
            <div class="list-group">
              <?php foreach($notifies as $notify){ ?>
                <a id="<?= $notify->id ?>" class="list-group-item">
                  <?= $notify->message ?>
                  <span style="display: block"><?= $this->AdminView->formatDateTime($notify->created) ?></span>
                </a>
              <?php } ?>
            </div>
          </ul>
        </li>

        <?php } ?>
        
        <li class="dropdown">

            <select id="lang" class="mySelect mt12">
              <option data-content='<span class="flag-icon flag-icon-gb"></span>' <?= ($lang == "eng") ? 'selected="selected"' : '' ?> value="eng"></option>
              <option data-content='<span class="flag-icon flag-icon-kh"></span>'  <?= ($lang == "kh") ? 'selected="selected"' : '' ?>  value="kh">Cambodia</option>
              <option data-content='<span class="flag-icon flag-icon-vn"></span>'  <?= ($lang == "vn") ? 'selected="selected"' : '' ?> value="vn">Vietnamese</option>
            </select>

        </li>

        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <?= $this->Html->image('avartar.png', array('class' => 'user-image', 'alt' => 'User Image')); ?>
            <span class="hidden-xs"><?= $user['fullname'] ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->

            <li class="user-header">
              <?= $this->Html->image('avartar.png', array('class' => 'img-circle', 'alt' => 'User Image')); ?>
              <p>
                <?= $user['fullname'] ?>
                </br>
                <?= $user['role']['name'] ?>
                <small><?= $this->AdminView->formatDatetime($user['created']); ?></small>
              </p>
            </li>
            <!-- Menu Body -->
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <?php
                  //if($user['role_id'] == 2){
                    echo $this->html->link(__('Profile'), ['controller' => 'accounts', 'action' => 'info'],['class' => 'btn btn-default btn-flat']);
                  //}
                ?>
              </div>
              <div class="pull-right">
                <?= $this->html->link(__('Sign out'), ['controller' => 'users', 'action' => 'logout'],['class' => 'btn btn-default btn-flat']) ?>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>

    <?php if (isset($layout) && $layout == 'top'): ?>
  </div>
<?php endif; ?>
</nav>