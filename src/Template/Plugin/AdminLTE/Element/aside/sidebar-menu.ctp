<ul class="sidebar-menu" data-widget="tree">
  <li class="header"><?= __('main_navigation') ?></li>
  <?php
  $backupAlias = "";
  $backupMethodName = "";
  $backupActionName = "";
  $shopPermissionControllers = \App\Libs\ValueUtil::get('user.shop_permissions');
  foreach ($methods as $method) {
    if (!empty($user['parent_id']) && (in_array($method->name, ['ShopEmployees', 'BankAccounts']) || $shopPermissionControllers[$method->name] != $user['shop_permission_name'])) {
      continue;
    }
  ?>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-book"></i> <span><?= $method->alias ?></span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <?php
        foreach ($method->actions as $action) {
          if ($method->name === 'Orders' && $action->name === 'home' && $user['role_id'] !== CUSTOMER) {
            $backupAlias = $action->alias;
            $backupMethodName = $method->name;
            $backupActionName = $action->name;
            continue;
          }
          if ($method->name === 'Reports' && $action->name === 'pickupReport' && $user['role_id'] !== CUSTOMER) {
            echo '<li>' . $this->Html->link('<i class="fa fa-circle-o"></i>' . $backupAlias . '</a>', ['controller' => $backupMethodName, 'action' => $backupActionName], ['escape' => false]) . '</li>';
          }
        ?>
          <li><?= $this->Html->link('<i class="fa fa-circle-o"></i>' . $action->alias . '</a>', ['controller' => $method->name, 'action' => $action->name], ['escape' => false]) ?></li>
        <?php } ?>
      </ul>
    </li>
  <?php } ?>
</ul>