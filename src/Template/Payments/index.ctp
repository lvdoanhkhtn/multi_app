<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-solid">

                <?php
                    $link = '<h3 class="box-title text-red">'.__('payment_history').'</h3>';
                    echo $this->Element('breadcum', ['link' => $link]) ;
                ?>

                <div class="box-body">
                    <div class="form-group col-md-12 table-responsive">
                        <?= $this->Form->create(null, ['type' => 'get']); ?>

                        <div class="row form-group">

                                <div class="<?= (!empty($isAdmin)) ? 'col-md-3' : 'col-md-6' ?>">
                                    <?php echo $this->Form->control('code',array('value' => !empty($_GET['code']) ? $_GET['code'] : "",'type' => 'text', 'class' => 'form-control', 'placeholder' => __('CBS00001;CBS00002;'), 'label' => __('order_code_private_code')))?>
                                </div>

                                <div class="<?= (!empty($isAdmin)) ? 'col-md-3' : 'col-md-6' ?>">
                                    <?= $this->Element('Calendar/completed_calendar'); ?>
                                </div>

                                <?php if(!empty($isAdmin)){ ?>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                    <?php echo $this->Form->control('status', array('default' => (isset($_GET['status']) && strlen($_GET['status']) > 0) ? $_GET['status'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $configs['delivery_status'], 'label' => __('status')))?>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                    <?php echo $this->Form->control('customer_id', array('default' => !empty($_GET['customer_id']) ? $_GET['customer_id'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $customers, 'label' => __('customer')))?>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                    <?php echo $this->Form->control('user_id', array('default' => !empty($_GET['user_id']) ? $_GET['user_id'] : '' , 'type' => 'select', 'empty' => __('select'), 'class' => 'form-control select2', 'options' => $accountants, 'label' => __('accountant')))?>
                                </div>
                                <?php } ?>
                        </div>

                        <div class="row form-group">
                            <div class="col-xs-12">
                                <?= $this->Form->button('<span class="fa fa-search""></span>'.__('search'), array('class' => 'btn btn-success pull-right','type'=>'submit')); ?>
                                <?= $this->Html->link('<i class="fas fa-eraser"></i>'.__('clear'), ['action' => 'index'], ['escape' => false,'class' => 'btn margin-right-sm btn-success pull-right']); ?>

                            </div>
                        </div>

                        <?= $this->Form->end(); ?>
                        <?php if($this->Paginator->param('pageCount') > 1){ ?>
                            <?= $this->AdminView->paginateMenuAndResult() ?>
                        <?php } ?>

                        <div class="box table-responsive">
                            <table class="result-table table table-bordered table-condensed table-hover" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-top"><?= __('payment_id') ?></th>
                                    <?php if(!empty($data['isAdmin'])){ ?>
                                        <th class="text-top"><?= __('employee_name') ?></th>
                                        <th class="text-top"><?= __('customer_name') ?></th>
                                        <th class="text-top"><?= __('status') ?></th>
                                    <?php } ?>
                                    <th class="text-top"><?= __('date_created') ?></th>
                                    <th class="text-top"><?= __('date_completed') ?></th>
                                    <th class="text-top"><?= __('total_orders') ?></th>
                                    <th class="text-top"><?= __('total_cod') ?></th>
                                    <th class="text-top"><?= __('total_fee') ?></th>
                                    <th class="text-top"><?= __('amount') ?></th>
                                    <th class="text-top"></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($payments)){ foreach ($payments as $payment){
                                        ?>
                                    <tr>
                                        <?php
                                            $order = $Excel->getSumPayment($payment['payment_details']);
                                        ?>
                                        <td><?= $payment->id; ?></td>
                                        <?php if(!empty($data['isAdmin'])){ ?>
                                            <td><?= $payment->user->fullname ?></td>
                                            <td><?= !empty($payment->customer->user->fullname) ? $payment->customer->user->fullname : "" ?></td>
                                            <td><?= $this->AdminView->showComplete($payment->status); ?></td>
                                        <?php } ?>
                                        <td><?= $this->AdminView->formatDate($payment->created); ?></td>
                                        <td><?= !empty($payment->status) ? $this->AdminView->formatDate($payment->modified) : ""; ?></td>
                                        <td><?= number_format($order['total_order']) ?></td>
                                        <td><?= $this->AdminView->getCurrencyCam($order['total_cod']) ?></td>
                                        <td><?= $this->AdminView->getCurrencyCam($order['total_fee'])  ?></td>
                                        <td><?= $this->AdminView->getCurrencyCam($order['total_amount'])  ?></td>
                                        <td class="mw100">
                                            <?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'payment_details','action' => 'index', $payment->id], ['escape' => false, 'data-toggle' => 'tooltip', 'title' => 'View', 'class' => 'btn btn-outline-info']); ?>
                                            <?php
                                                if(empty($payment->status)){
                                                    echo $this->Form->postLink(
                                                    "<i class='fa fa-trash-alt'></i>", // first
                                                    ['action' => 'delete', $payment->id],  // second
                                                    ['escape' => false, 'title' => 'Delete', 'class' => 'btn btn-outline-danger', 'confirm' => __('delete_confirm')]);// third
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php }
                                    } ?>

                                </tbody>
                            </table>
                            <?= $this->Element('paging') ?>
                        </div>

                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->Html->script(['Payment/payment']); ?>