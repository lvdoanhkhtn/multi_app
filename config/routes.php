<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */

$exp_domain = explode(".", env("HTTP_HOST"));
$default_prefix = false; // default prefix is false
if (count($exp_domain) > 2 && $exp_domain[0] != "www") {
    $default_prefix = $exp_domain[0];
}
if (in_array($default_prefix, ["test-cbs-api", "test-cbs", "api"])) {
    Router::scope('/', ['prefix' => 'api'], function ($routes) {
        $routes->resources('Orders');
        $routes->resources('Addresses');
        $routes->resources('Cities');
        $routes->resources('Districts');
        $routes->resources('Wards');
        $routes->resources('Users');
        $routes->resources('WareHouses');
        $routes->resources('ServicePacks');
        $routes->resources('Accounts');
        $routes->resources('Configs');
        $routes->resources('Payments');
        $routes->resources('PaymentDetails');
        $routes->resources('BankAccounts');
        $routes->resources('Banks');
        $routes->resources('Countries');
        $routes->resources('Commons');
        $routes->resources('PickupTrips');
        $routes->fallbacks('InflectedRoute');

        $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
            'httpOnly' => true
        ]));

        $routes->applyMiddleware('csrf');
    });
}
if (in_array($default_prefix, ["test-ecom", "ecom"])) {
    Router::scope('/', ['prefix' => 'shop'], function ($routes) {
        $routes->connect('/', ['controller' => 'Orders', 'action' => 'index']);
        $routes->fallbacks('InflectedRoute');

        $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
            'httpOnly' => true
        ]));
        $routes->applyMiddleware('csrf');
    });
}

if (in_array($default_prefix, ["test-center", "center"])) {
    Router::scope('/', ['prefix' => 'center'], function ($routes) {
        $routes->connect('/', ['controller' => 'Orders', 'action' => 'index']);
        $routes->fallbacks('InflectedRoute');
        $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
            'httpOnly' => true
        ]));

        $routes->applyMiddleware('csrf');
    });
}

if (empty($default_prefix)) {
    Router::scope('/', ['prefix' => 'center'], function ($routes) {
        // Register scoped middleware for in scopes.
        $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
            'httpOnly' => true
        ]));

        /**
         * Apply a middleware to the current route scope.
         * Requires middleware to be registered via `Application::routes()` with `registerMiddleware()`
         */
        $routes->applyMiddleware('csrf');

        /**
         * Here, we are connecting '/' (base path) to a controller called 'Pages',
         * its action called 'display', and we pass a param to select the view file
         * to use (in this case, src/Template/Pages/home.ctp)...
         */
        $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

        /**
         * ...and connect the rest of 'Pages' controller's URLs.
         */
        $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

        /**
         * Connect catchall routes for all controllers.
         *
         * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
         *
         * ```
         * $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);
         * $routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);
         * ```
         *
         * Any route class can be used with this method, such as:
         * - DashedRoute
         * - InflectedRoute
         * - Route
         * - Or your own route class
         *
         * You can remove these routes once you've connected the
         * routes you want in your application.
         */
        $routes->fallbacks(DashedRoute::class);
    });
}
