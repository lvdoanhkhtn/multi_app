<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.8
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

/*
 * Configure paths required to find CakePHP + general filepath constants
 */
require __DIR__ . '/paths.php';

/*
 * Bootstrap CakePHP.
 *
 * Does the various bits of setup that CakePHP needs to do.
 * This includes:
 *
 * - Registering the CakePHP autoloader.
 * - Setting the default application paths.
 */
require CORE_PATH . 'config' . DS . 'bootstrap.php';

use Cake\Cache\Cache;
use Cake\Console\ConsoleErrorHandler;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Core\Plugin;
use Cake\Database\Type;
use Cake\Datasource\ConnectionManager;
use Cake\Error\ErrorHandler;
use Cake\Http\ServerRequest;
use Cake\Log\Log;
use Cake\Mailer\Email;
use Cake\Mailer\TransportFactory;
use Cake\Utility\Inflector;
use Cake\Utility\Security;

/**
 * Uncomment block of code below if you want to use `.env` file during development.
 * You should copy `config/.env.default to `config/.env` and set/modify the
 * variables as required.
 *
 * It is HIGHLY discouraged to use a .env file in production, due to security risks
 * and decreased performance on each request. The purpose of the .env file is to emulate
 * the presence of the environment variables like they would be present in production.
 */
// if (!env('APP_NAME') && file_exists(CONFIG . '.env')) {
//     $dotenv = new \josegonzalez\Dotenv\Loader([CONFIG . '.env']);
//     $dotenv->parse()
//         ->putenv()
//         ->toEnv()
//         ->toServer();
// }

/*
 * Read configuration file and inject configuration into various
 * CakePHP classes.
 *
 * By default there is only one configuration file. It is often a good
 * idea to create multiple configuration files, and separate the configuration
 * that changes from configuration that does not. This makes deployment simpler.
 *
 */
define('STAGING', 'staging');
define('PRODUCTION', 'production');

try {
    Configure::config('default', new PhpConfig());
    Configure::load('app', 'default');
    //load config foreach envoirement.
    $env = getenv('APP_ENV');
    switch ($env) {
        case STAGING:
            Configure::load('Environment/staging', 'default');
            break;
        case PRODUCTION:
            Configure::load('Environment/production', 'default');
            break;
        default:
            Configure::load('Environment/local', 'default');
            break;
    }
    //    Configure::load('database', 'local');
} catch (\Exception $e) {
    exit($e->getMessage() . "\n");
}

/*
 * Load an environment local configuration file.
 * You can use a file like app_local.php to provide local overrides to your
 * shared configuration.
 */
//Configure::load('app_local', 'default');

/*
 * When debug = true the metadata cache should only last
 * for a short time.
 */
if (Configure::read('debug')) {
    \App\Application::addPlugin('DebugKit', ['bootstrap' => true]);

    Configure::write('Cache._cake_model_.duration', '+2 minutes');
    Configure::write('Cache._cake_core_.duration', '+2 minutes');
    // disable router cache during development
    Configure::write('Cache._cake_routes_.duration', '+2 seconds');
}

/*
 * Set the default server timezone. Using UTC makes time calculations / conversions easier.
 * Check http://php.net/manual/en/timezones.php for list of valid timezone strings.
 */
date_default_timezone_set(Configure::read('App.defaultTimezone'));

/*
 * Configure the mbstring extension to use the correct encoding.
 */
mb_internal_encoding(Configure::read('App.encoding'));

/*
 * Set the default locale. This controls how dates, number and currency is
 * formatted and sets the default language to use for translations.
 */
ini_set('intl.default_locale', Configure::read('App.defaultLocale'));

/*
 * Register application error and exception handlers.
 */
$isCli = PHP_SAPI === 'cli';
if ($isCli) {
    (new ConsoleErrorHandler(Configure::read('Error')))->register();
} else {
    (new ErrorHandler(Configure::read('Error')))->register();
}

/*
 * Include the CLI bootstrap overrides.
 */
if ($isCli) {
    require __DIR__ . '/bootstrap_cli.php';
}

/*
 * Set the full base URL.
 * This URL is used as the base of all absolute links.
 *
 * If you define fullBaseUrl in your config file you can remove this.
 */
if (!Configure::read('App.fullBaseUrl')) {
    $s = null;
    if (env('HTTPS')) {
        $s = 's';
    }

    $httpHost = env('HTTP_HOST');
    if (isset($httpHost)) {
        Configure::write('App.fullBaseUrl', 'http' . $s . '://' . $httpHost);
    }
    unset($httpHost, $s);
}

Cache::setConfig(Configure::consume('Cache'));
ConnectionManager::setConfig(Configure::consume('Datasources'));
TransportFactory::setConfig(Configure::consume('EmailTransport'));
Email::setConfig(Configure::consume('Email'));
Log::setConfig(Configure::consume('Log'));
Security::setSalt(Configure::consume('Security.salt'));

/*
 * The default crypto extension in 3.0 is OpenSSL.
 * If you are migrating from 2.x uncomment this code to
 * use a more compatible Mcrypt based implementation
 */
//Security::engine(new \Cake\Utility\Crypto\Mcrypt());

/*
 * Setup detectors for mobile and tablet.
 */
ServerRequest::addDetector('mobile', function ($request) {
    $detector = new \Detection\MobileDetect();

    return $detector->isMobile();
});
ServerRequest::addDetector('tablet', function ($request) {
    $detector = new \Detection\MobileDetect();

    return $detector->isTablet();
});

/*
 * Enable immutable time objects in the ORM.
 *
 * You can enable default locale format parsing by adding calls
 * to `useLocaleParser()`. This enables the automatic conversion of
 * locale specific date formats. For details see
 * @link https://book.cakephp.org/3.0/en/core-libraries/internationalization-and-localization.html#parsing-localized-datetime-data
 */
Type::build('time')
    ->useImmutable();
Type::build('date')
    ->useImmutable();
Type::build('datetime')
    ->useImmutable();
Type::build('timestamp')
    ->useImmutable();

/*
 * Custom Inflector rules, can be set to correctly pluralize or singularize
 * table, model, controller names or whatever other string is passed to the
 * inflection functions.
 */
//Inflector::rules('plural', ['/^(inflect)or$/i' => '\1ables']);
//Inflector::rules('irregular', ['red' => 'redlings']);
//Inflector::rules('uninflected', ['dontinflectme']);
//Inflector::rules('transliteration', ['/å/' => 'aa']);

define('HCM', 1);
define('DN', 48);
define('BD', 47);
define('PHNOMPHENH', 64);
define('DISTRICT_PHNOMPHENH', 701);
define('DISTRICT_PHNOMPHENH_GROUP2', 702);
define('OVER_WIGHT', 3000);
define('OVER_WEIGHT_HCM_PHNOMPENH', 1000);
define('OVER_WIGHT_OTHER', 500);
define('STANDER_WEIGHT', 1000);
define('OVER_WEIGHT_MONEY', 0.25);
define('OVER_WEIGHT_MONEY_OTHER', 1.00);
define('OVER_MONEY_COLLECTION', 100);
define('VALUE_MONEY_COLLECTION', 0.01);
define('VALUE_MONEY_COLLECTION_OTHER', 0.01);
define('OVER_MONEY_PRODUCT_VALUE', 100);
define('VALUE_MONEY_PRODUCT_VALUE', 0.01);
define('HOME_PAGE', 'https://cambodiaship.com');

define('CODE_ERROR', 500);
define('CODE_SUCCESS', 200);
define('HOT_LINE', '(+855) 2363 55555');
define('INFO_MAIL', 'info@cambodiaship.com');

define('COMPARE_DATE', '2019-09-08');
//define('COMPARE_DATE', '2018-11-20');
define('TRANSFER_FEE', 0.5);

define('ADMIN', 1);
define('CUSTOMER', 2);
define('SHIPPER', 3);
define('ACCOUNTANT', 4);
define('COORDINATOR', 7);

define('LOAD_ICHECK_CONTROLER', 'orders');
define('ACTION', 'edit');
define('WARDS', 'wards');
define('DISTRICTS', 'districts');
define('CITIES', 'cities');

define('PRICE_DEFAULT', 1);
define('CONVERT_KG_G', 1000);

define('BRANDNAME', 'CambodiaShip');
define('COMPLETE_STATUS', 1);
define('NO_COMPLETE_STATUS', 0);
define('EXCHANGE_PREFIX', 'TH');
define('CREATED', 1);
define('FINISHED', 2);
define('MODIFIED', 3);
define('STORED_DATE', 4);
define('FINISHED_CONSTANT', 'finished');

define('PICKUP_TRIP', 1);
define('DELIVERY_TRIP', 2);
define('RETURN_TRIP', 3);

define('PERMISSIONS', 'permissions');
define('ERROR', 'error403');

date_default_timezone_set('Asia/Ho_Chi_Minh');

// Configure::load('adminlte', 'default');

define('CC_MAIL', 'ducta@cambodiaship.com');
define('MAIL_TEST', 'lvdoanhkhtn@gmail.com');
define('BASE_URL', 'http://api.master.com');
define('TEST_URL', 'http://center.cbs.com/');
define('TOKEN', '1f023a0e5862d5280b05321375529c51');
define('INDEX', 'index');

define('ORDER_STATUS', 'order_status');
define('WEBHOOKS', 'webhooks');
define('CANCEL', 9);
define('ReturnTripDetails', 'ReturnTripDetails');

define('STORE_TYPE', 1);
define('RETURN_TYPE', 2);
define('PENDING_TYPE', 3);

define('STORE_URL', 'stored');
define('RETURN_URL', 'returned');
define('PENDING_URL', 'pending');

define('STORED', 3);

define('MAIL_RECEIVER_CASHBOOK', 'accountant@cambodiaship.com');
define('PAYMENT_TITLE', 'Báo cáo tổng đơn đã thanh toán trong ngày');
define('COLLECT_TITLE', 'Báo cáo tổng đơn đã thu tiền trong ngày');
define('NEW_TOKEN','MDIlew1ZtzA6HXVAdXigJz1jGIR9FS');

define('SHOP_EMPLOYEE', 15);
define('LEAD_COORDINATOR', 14);


