<?php
/**
 * Created by PhpStorm.
 * User: doanh.lv
 * Date: 8/13/19
 * Time: 2:46 PM
 */
return [
    'Theme' => [
        'title' => 'Cambodiaship',
        'logo' => [
            'mini' => '<b>CBS</b>',
            'large' => '<b>Cambodiaship</b>'
        ],
        'login' => [
            'show_remember' => false,
            'show_register' => false,
            'show_social' => false
        ],
        'folder' => ROOT,
        'skin' => 'red'
    ]
];
