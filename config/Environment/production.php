<?php

	return[
		'debug' => filter_var(env('DEBUG', false), FILTER_VALIDATE_BOOLEAN),
		'Datasources' => [
			'default' => [
				'className' => 'Cake\Database\Connection',
				'driver' => 'Cake\Database\Driver\Mysql',
				'persistent' => false,
				'host' => 'localhost',
				'username' => 'root',
				'password' => 'yMQ1qP1okICeBsQp@',
				'database' => 'new_center',
				'encoding' => 'utf8',
				'timezone' => 'UTC',
				'cacheMetadata' => true,
				'quoteIdentifiers' => false,
				'log' => false,
				//'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
				'url' => env('DATABASE_TEST_URL', null),
			]
		]
	];

