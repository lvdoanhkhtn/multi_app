<?php

return [
	'debug' => filter_var(env('DEBUG', false), FILTER_VALIDATE_BOOLEAN),
	'Datasources' => [
		'default' => 	[
			'className' => 'Cake\Database\Connection',
			'driver' => 'Cake\Database\Driver\Mysql',
			'persistent' => false,
			'host' => 'localhost',
			'username' => 'root',
			'password' => 'yMQ1qP1okICeBsQp@',
			'database' => 'new_center',
			'encoding' => 'utf8',
			'timezone' => 'UTC',
			'cacheMetadata' => true,
			'quoteIdentifiers' => false,
			'log' => false,
			'url' => env('DATABASE_TEST_URL', null),
		],
		'test' => 	[
			'className' => 'Cake\Database\Connection',
			'driver' => 'Cake\Database\Driver\Mysql',
			'persistent' => false,
			'host' => '112.78.1.142',
			'username' => 'root',
			'password' => 'mJKa?!Gp4z',
			'database' => 'new_test_cbs',
			'encoding' => 'utf8',
			'timezone' => 'UTC',
			'cacheMetadata' => true,
			'quoteIdentifiers' => false,
			'log' => false,
			'url' => env('DATABASE_TEST_URL', null),
		]


	]
];
