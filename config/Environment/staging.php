<?php

	return[
		'debug' => filter_var(env('DEBUG', true), FILTER_VALIDATE_BOOLEAN),
		'Datasources' => [
			'default' => [
				'className' => 'Cake\Database\Connection',
				'driver' => 'Cake\Database\Driver\Mysql',
				'persistent' => false,
				'host' => 'localhost',
				'username' => 'root',
				'password' => '5zfZ5tA7tzbs7taA',
				'database' => 'test_transfer',
				'encoding' => 'utf8',
				'timezone' => 'UTC',
				'cacheMetadata' => true,
				'quoteIdentifiers' => false,
				'log' => false,
				'url' => env('DATABASE_TEST_URL', null),
			]
		]
	];

