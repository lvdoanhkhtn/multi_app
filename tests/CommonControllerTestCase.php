<?php
/**
 * User: junade
 * Date: 20/12/2016
 * Time: 16:32
 */

namespace App\Test;

use GuzzleHttp;

trait CommonControllerTestCase
{
    public $http;

    public function __construct()
    {
        $this->http = new GuzzleHttp\Client(['base_uri' => BASE_URL, 'headers' => ['Accept' => 'application/json', 'authorization' => TOKEN]]);
    }

    public function setUp(){
        $this->http = new GuzzleHttp\Client(['base_uri' => BASE_URL, 'headers' => ['Accept' => 'application/json', 'authorization' => TOKEN]]);
    }

    public function tearDown()
    {
        $this->http = null;
    }

    public function testGet($action)
    {
        $response = $this->http->request('GET', $action);
        $data = json_decode($response->getBody(), true);
        return !empty($data) ? $data : [];
    }

    public function testResponse($response = []){
        $data = json_decode($response->getBody(), true);
        return !empty($data) ? $data : [];

    }

    public function testPost($url = "",$data = [], $isJson = true){
        if($isJson){
            $response = $this->http->post( $url, [GuzzleHttp\RequestOptions::JSON => $data]);
        }else{
            $response = $this->http->post( $url,$data);
        }
        return $this->testResponse($response);

    }


    public function testPut($url = "",$data = [], $isJson = true){
        if($isJson){
            $response = $this->http->put( $url, [GuzzleHttp\RequestOptions::JSON => $data]);
        }else{
            $response = $this->http->put( $url,$data);
        }
        return $this->testResponse($response);

    }

}