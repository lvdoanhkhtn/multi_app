<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WebhooksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WebhooksTable Test Case
 */
class WebhooksTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WebhooksTable
     */
    public $Webhooks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Webhooks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Webhooks') ? [] : ['className' => WebhooksTable::class];
        $this->Webhooks = TableRegistry::getTableLocator()->get('Webhooks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Webhooks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
