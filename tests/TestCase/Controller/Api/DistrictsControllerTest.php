<?php
/**
 * User: junade
 * Date: 20/12/2016
 * Time: 16:32
 */
namespace App\Test\TestCase\Controller\Api;
namespace App\Test;


class DistrictsControllerTest
{
    public $trait;
    public function setUp(){
        $this->trait = $this->getMockForTrait(CommonControllerTestCase::class);
    }

    public function testIndex(){
        $response = $this->trait->testGet('districts/1');
        $this->assertEquals(200, $response['code']);
    }
}