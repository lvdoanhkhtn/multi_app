<?php
/**
 * User: junade
 * Date: 20/12/2016
 * Time: 16:32
 */

namespace App\Test\TestCase\Controller\Api;
namespace App\Test;


class WareHousesControllerTest
{
    public $trait;
    public function setUp(){
        $this->trait = $this->getMockForTrait(CommonControllerTestCase::class);
    }
    
    public function testIndex(){
        $response = $this->trait->testGet('ware_houses');
        $this->assertEquals(200, $response['code']);
    }

}