<?php
/**
 * User: junade
 * Date: 20/12/2016
 * Time: 16:32
 */
namespace App\Test\TestCase\Controller\Api;


namespace App\Test;

class PaymentsControllerTest
{

    public $trait;
    public function setUp(){
        $this->trait = $this->getMockForTrait(CommonControllerTestCase::class);
    }

    public function testSearch(){
        $response = $this->trait->testGet('payments/search?limit=10');
        $this->assertEquals(200, $response['code']);
    }
}