<?php
/**
 * User: junade
 * Date: 20/12/2016
 * Time: 16:32
 */
namespace App\Test\TestCase\Controller\Api;


namespace App\Test;

use PHPUnit\Framework\TestCase;

class BankAccountsControllerTest extends TestCase
{
    public $trait;
    public function setUp(){
        $this->trait = $this->getMockForTrait(CommonControllerTestCase::class);
    }

    public function testIndex(){
        $response = $this->trait->testGet('bank_accounts');
        $this->assertEquals(200, $response['code']);
    }

    public function testAdd(){
        $data = [
            'bank_id' => '3',
            'account_number' => '10065578678',
            'account_holder' => 'luong van doanh 1212',
            'branch_name' => 'sài gòn'
        ];
        $response = $this->trait->testPost('bank_accounts', $data);
        $this->assertEquals(200, $response['code']);

    }

    public function testEdit(){
        $data = [
            'bank_id' => '3',
            'account_number' => '10065578678',
            'account_holder' => 'luong van doanh 1212',
            'branch_name' => 'sài gòn'
        ];
        $response = $this->trait->testPut('bank_accounts/11', $data);
        $this->assertEquals(200, $response['code']);

    }
}