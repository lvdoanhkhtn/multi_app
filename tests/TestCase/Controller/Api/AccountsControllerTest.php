<?php
/**
 * User: junade
 * Date: 20/12/2016
 * Time: 16:32
 */
namespace App\Test\TestCase\Controller\Api;


namespace App\Test;

use PHPUnit\Framework\TestCase;

class AccountsControllerTest extends TestCase
{
    public $trait;
    public function setUp()
    {
        $this->trait = $this->getMockForTrait(CommonControllerTestCase::class);
    }

    public function testIndex(){
        $response = $this->trait->testGet('accounts');
        $this->assertEquals(200, $response['code']);
    }

    public function testEdit(){
        $data = [
            'fullname' => 'doanh 123',
            'address' => '212wewe hoa cau',
            "phone" => "0904411811",
            'ward_id' => '9178',
            'district_id' => '544',
            'city_id' => '1'
        ];
        $response = $this->trait->testPut('accounts/5', $data);
        $this->assertEquals(200, $response['code']);
    }

    public function testChangePassword(){
        $data = [
            'password' => '1234567',
            'newPassword' => '1234567',
            "newConfirmPassword" => "1234567"
        ];
        $response = $this->trait->testPost('accounts/changePassword', $data);
        $this->assertEquals(200, $response['code']);
    }
}