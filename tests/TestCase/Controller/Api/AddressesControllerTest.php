<?php
/**
 * User: junade
 * Date: 20/12/2016
 * Time: 16:32
 */
namespace App\Test\TestCase\Controller\Api;


namespace App\Test;

use PHPUnit\Framework\TestCase;

class AddressesControllerTest extends TestCase
{

    public $trait;
    public function setUp(){
        $this->trait = $this->getMockForTrait(CommonControllerTestCase::class);
    }

    public function testIndex(){
        $response = $this->trait->testGet('addresses');
        $this->assertEquals(200, $response['code']);
    }

    public function testAdd(){
        $data = [
            'address' => '21 hoa cau',
            'str_phones' => '0988285437',
            'ward_id' => '9178',
            'district_id' => '544',
            'city_id' => '1'
        ];
        $response = $this->trait->testPost('addresses', $data);
        $this->assertEquals(200, $response['code']);

    }

    public function testEdit(){
        $data = [
            'address' => '21 hoa cau',
            'str_phones' => '0988285437',
            'ward_id' => '9178',
            'district_id' => '544',
            'city_id' => '1'
        ];
        $response = $this->trait->testPut('addresses/4', $data);
        $this->assertEquals(200, $response['code']);

    }
}