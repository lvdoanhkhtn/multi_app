<?php
/**
 * User: junade
 * Date: 20/12/2016
 * Time: 16:32
 */
namespace App\Test\TestCase\Controller\Api;

namespace App\Test;



class UsersControllerTest
{

    public $trait;
    public function setUp(){
        $this->trait = $this->getMockForTrait(CommonControllerTestCase::class);
    }


    public function testLogin()
    {
        $data = [
            'form_params' => [
                'email' => 'lvdoanhkhtn@gmail.com',
                'password' => '1234567'
            ]
        ];
        $response = $this->trait->testPost('users/login', $data, false);
        $this->assertEquals(200, $response['code']);
    }

}