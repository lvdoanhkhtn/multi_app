<?php
/**
 * User: junade
 * Date: 20/12/2016
 * Time: 16:32
 */
namespace App\Test\TestCase\Controller\Api;


namespace App\Test;

use PHPUnit\Framework\TestCase;

class CitiesControllerTest extends TestCase
{
    public $trait;
    public function setUp(){
        $this->trait = $this->getMockForTrait(CommonControllerTestCase::class);
    }
    public function testIndex(){
        $response = $this->trait->testGet('cities');
        $this->assertEquals(200, $response['code']);
    }

    public function testGetPickUp(){
        $response = $this->trait->testGet('cities/pickUpCities');
        $this->assertEquals(200, $response['code']);
    }
}