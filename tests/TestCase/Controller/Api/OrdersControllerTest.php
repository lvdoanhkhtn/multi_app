<?php
/**
 * User: junade
 * Date: 20/12/2016
 * Time: 16:32
 */
namespace App\Test\TestCase\Controller\Api;


namespace App\Test;

class OrdersControllerTest
{

    public $trait;
    public function setUp(){
        $this->trait = $this->getMockForTrait(CommonControllerTestCase::class);
    }

    public function testSearch(){
        $response = $this->trait->testGet('orders/search?limit=10');
        $this->assertEquals(200, $response['code']);
    }

    public function testView(){
        $response = $this->trait->testGet('orders/58811');
        $this->assertEquals(200, $response['code']);
    }

    public function testHistory(){
        $response = $this->trait->testGet('orders/history/58811');
        $this->assertEquals(200, $response['code']);
    }

    public function testCheckShopId(){
        $response = $this->trait->testGet('orders/checkShopId?shopId=test123');
        $this->assertEquals(200, $response['code']);
    }

    public function testAddViewCost(){
        $data = [
            'receiver_name' => 'doanh test 1',
            'arr_receiver_phone' => '016891858 - 0988285547',
            'address' => 'បេនឡានខេត្តកំពុងស្ពឺ',
            'district_id' => '544',
            'city_id' => '1',
            'money_collection' => 0,
            'product_value' => 17,
            'weight' => 1.5,
            'note' => 'money &~/ on delivery and call to taxi 016304445 to take money from taxi 12',
            'content' => 'tx21',
            'is_view_cost' => 1
        ];
        $response = $this->trait->testPost('orders', $data);
        $this->assertEquals(200, $response['code']);

    }

    public function testAdd(){
        $data = [
            'receiver_name' => 'doanh test 1',
            'arr_receiver_phone' => '016891858 - 0988285547',
            'address' => 'បេនឡានខេត្តកំពុងស្ពឺ',
            'district_id' => '544',
            'city_id' => '1',
            'money_collection' => 0,
            'product_value' => 17,
            'weight' => 1.5,
            'note' => 'money &~/ on delivery and call to taxi 016304445 to take money from taxi 12',
            'content' => 'tx21'
        ];
        $response = $this->trait->testPost('orders', $data);
        $this->assertEquals(200, $response['code']);

    }

    public function testEdit(){
        $data = [
            'receiver_name' => 'doanh test 1',
            'arr_receiver_phone' => '016891858 - 0988285547',
            'address' => 'បេនឡានខេត្តកំពុងស្ពឺ',
            'district_id' => '544',
            'city_id' => '1',
            'money_collection' => 0,
            'product_value' => 17,
            'weight' => 1.5,
            'note' => 'money &~/ on delivery and call to taxi 016304445 to take money from taxi 12',
            'content' => 'tx21'
        ];
        $response = $this->trait->testPut('orders/58978', $data);
        $this->assertEquals(200, $response['code']);
    }

    public function testSearchPhone(){
        $response = $this->trait->testGet('orders/searchPhone?keyWord=0');
        $this->assertEquals(200, $response['code']);
    }


    public function testGetAddressByPhone(){
        $response = $this->trait->testGet('orders/getAddressByPhone?phone=09852367411');
        $this->assertEquals(200, $response['code']);
    }

}