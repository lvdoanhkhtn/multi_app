<?php

/**
 * Created by PhpStorm.
 * User: doanh.lv
 * Date: 10/17/19
 * Time: 10:58 AM
 */
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\AdminViewHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

use Facebook\WebDriver\WebDriverBy;


class AdminViewHelpTest
{
    public $helper = null;

    // Here we instantiate our helper
    public function setUp()
    {
        parent::setUp();
        $View = new View();
        $this->helper = new AdminViewHelper($View);
    }

    // Testing the usd() function
    public function testGetCurrencyCam()
    {
//        $this->assertTrue(true);
//        var_dump($this->helper->getCurrencyCam(24));
        $this->assertEquals('<b>98,400 (24 USD) </b>', $this->helper->getCurrencyCam(24));
    }
//
//    public function testShouldContainSearchInput()
//    {
//        // Load the URL (will wait until page is loaded)
//        $this->wd->get('http://www.w3.org/'); // $this->wd holds instance of \RemoteWebDriver
//
//        // Do some assertion
//        $this->assertContains('W3C', $this->wd->getTitle());
//
//        // You can use $this->log(), $this->warn() or $this->debug() with sprintf-like syntax
//        $this->log('Current page "%s" has title "%s"', $this->wd->getCurrentURL(), $this->wd->getTitle());
//
//        // Make sure search input is present
//        $searchInput = $this->wd->findElement(WebDriverBy::cssSelector('#search-form input'));
//        // Or you can use syntax sugar provided by Steward (this is equivalent of previous line)
//        $searchInput = $this->findByCss('#search-form input');
//
//        // Assert title of the search input
//        $this->assertEquals('Search', $searchInput->getAttribute('title'));
//    }


}